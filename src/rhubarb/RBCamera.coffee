#requres RBGroup
class RBCamera extends RBEntity
    constructor: (X, Y, Width, Height, @screenwidth=0, @screenheight=0, name="UnknownCamera") ->
        super(X, Y, Width, Height, name)
        @offset = new RBPoint() #the x/y of where the cam is looking
        @.set_anchor(0,0)
        @.alwaysOnScreen = true
        @followentity = null
        @scale = new RBPoint(1, 1)
        @canvas = document.createElement('canvas')
        @canvas.width = Width
        @canvas.height = Height
        ###
        if @screenwidth is 0
            @screenwidth = Width
        if @screenheight is 0
            @screenheight = Height
        ###
        @ctx = @canvas.getContext('2d')
        @_RB.set_smoothing(@ctx, @_RB._canvasSmoothing)

    resize: (w, h) ->
        @.set_width(w)
        @.set_height(h)

    set_width: (w) ->
        super(w)
        @canvas.width = w
        @_RB.set_smoothing(@ctx, @_RB._canvasSmoothing)
    set_height: (h) ->
        super(h)
        @canvas.height = h
        @_RB.set_smoothing(@ctx, @_RB._canvasSmoothing)

    zoom: (xzoom, yzoom) ->
        xchange = @width*xzoom - @width
        ychange = @height*yzoom - @height
        @ctx.translate(-xchange/2, -ychange/2)
        @ctx.scale(xzoom, yzoom)
        @_RB.set_smoothing(@ctx, @_RB._canvasSmoothing)
                
        

    follow: (entitytofollow, someparams) ->
        @followentity = entitytofollow
    
    update:(dt) ->
        if dt <= 0
            #console.log("cam update dt was 0")
            return
        if @followentity?
            @offset.x = -@followentity.x + @width/(2 * @scale.x) # |0
            @offset.y = -@followentity.y + @height/(2 * @scale.y) #|0
        
        #Store offsets so we can use them
        
        @_RB.screen().set_position( -@offset.x, -@offset.y)
        

    zoomdata: () ->
        #zoom code
        imgData = @ctx.getImageData(0,0,@width, @height).data
        zoom = 2
        #Draw the zoomed-up pixels to a different canvas context
        for x in [0..@width/2]
            for y in [0..@height/2]
                #Find the starting index in the one-dimensional image data
                i = (y*@width + x)*4
                r = imgData[i  ]
                g = imgData[i+1]
                b = imgData[i+2]
                a = imgData[i+3]
                @_RB.cctx.fillStyle = "rgba("+r+","+g+","+b+","+(a/255)+")"
                @_RB.cctx.fillRect(x*zoom,y*zoom,zoom,zoom)


    #get this camera to draw an object
    draw: (object) ->
        #Clear our local canvas
        @ctx.clearRect(0,0,@width, @height)
        #Draw the scene into our canvas
        @_RB.push_cctx()
        @_RB.set_cctx(@ctx)
        @ctx.translate(@offset.x|0, @offset.y|0)
        #@ctx.translate(@offset.x, @offset.y)
        object.render()
        @ctx.translate(-@offset.x|0, -@offset.y|0)
        #@ctx.translate(-@offset.x, -@offset.y)
        #now draw it on the main canvas
        @_RB.restore_cctx()
        @_RB.cctx.save()
        #clip to this camera rect only
        @_RB.cctx.beginPath()
        @_RB.cctx.rect(@left|0, @top|0, @width|0, @height|0)
        @_RB.cctx.closePath()
        @_RB.cctx.clip()
        #draw the local camera canvas onto the main context
        @_RB.cctx.drawImage(@canvas, @x|0, @y|0)
        @_RB.cctx.restore()


