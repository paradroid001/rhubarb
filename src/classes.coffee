class Dot extends RBEntity
    constructor: (X, Y, name="NoName") ->
        super(X, Y, 2, 2, name)
        @physics = new RBPhysics()
        @physics.velocity.x = 2
        @physics.velocity.y = 2
        @physics.floating = false 
        @.add_component(@physics)
        #@rgb = rgb_to_html() #"#FF3300"
        @.set_colour(random_int(0,255), random_int(0,255), random_int(0,255))

    update: (dt) ->
        super(dt)
        w = @_RB.screen()
        physics = @.get_component("physics")
        if @x > w.right or @x < w.left
            if @x > w.right
                @x = w.right - (@x - w.right)
            if @x < w.left
                @x = w.left + (w.left - @x)
            physics.velocity.x = physics.velocity.x * -1
        if @y > w.bottom or @y < w.top
            if @y > w.bottom
                @y = w.bottom - (@y - w.bottom)
            if @y < w.top
                @y = w.top + (w.top - @y)
            physics.velocity.y = physics.velocity.y * -1

    draw: () ->
        #@.pre_render() #xoffset, yoffset)
        @_RB.cctx.fillStyle = @_colour.htmlrgb
        totopleft = @.xy_to_topleft()
        @_RB.cctx.fillRect(totopleft.x|0, totopleft.y|0, @width|0, @height|0)
        super()
        #@.post_render()

class Brick extends RBEntity
    constructor: (X, Y, Width=3, Height=3, name="NoName", r, g, b) ->
        super(X, Y, Width, Height, name)
        
        physics = new RBPhysics()
        physics.velocity.x = 0
        physics.velocity.y = 0
        physics.floating = false
        @.add_component(physics)


        @_colour.set_rgb([r,g,b])
        @orig_rgb = new RBColour([r, g, b])

    update: (dt) ->
        super(dt)
        #reset rgb
        #@_colour.set_rgb(@orig_rgb.rgba)
        w = @_RB.screen()
        p = @.get_component("physics")
        if (@right > w.right) or (@left < w.left)
            if (@right > w.right) and (p.velocity.x > 0)
                p.velocity.x = -p.velocity.x
                this.adjust_right(w.right- (@right-w.right))
            if (@left <= w.left) and (p.velocity.x < 0)
                p.velocity.x = -p.velocity.x
                this.adjust_left(w.left + (w.left - @left))
        
        if (@bottom > w.bottom) or (@top < w.top)
            if (@bottom > w.bottom) and (p.velocity.y > 0)
                p.velocity.y = -p.velocity.y
                this.adjust_bottom(w.bottom - (@bottom-w.bottom))
                #this.adjust_bottom(w.bottom)
            if (@top < w.top) and (p.velocity.y < 0)
                p.velocity.y = -p.velocity.y
                this.adjust_top(w.top + (w.top - @top))
                #this.adjust_top(w.top)

    draw: () ->
        @_RB.cctx.fillStyle = @_colour.htmlrgba
        totopleft = @.xy_to_topleft()
        @_RB.cctx.fillRect(totopleft.x|0, totopleft.y|0, @width|0, @height|0)
        super()


    hit_top: (robject, collider_velocity) ->
        @_colour.set_rgb(RB_RED)
    hit_bottom: (robject, collider_velocity) ->
        @_colour.set_rgb(RB_YELLOW)
    hit_left: (robject, collider_velocity) ->
        @_colour.set_rgb(RB_PURPLE)
    hit_right: (robject, collider_velocity) ->
        @_colour.set_rgb(RB_GREY)
    

class Dude extends RBSprite
    constructor: (X, Y, name="Unnamed") -> 
        super(X, Y, "data/platformer/Warrior.png", 24, 24, name)
        @physics = new RBPhysics()
        @physics.velocity.x = 0
        @physics.velocity.y = 0
        @physics.floating = false
        @physics.staticphysics = false
        @.add_component(@physics)
        @angle = random_float(-3.14, 3.14)
        ss = @_RB.media.get_spritesheet("warrior")
        @.add_animation("Walk", new RBAnimation(ss, [0,1,2], RB_ANIM_LOOP, 200) )
        @.set_animation("Walk")
    update: (dt) ->
        super(dt)
        w = @_RB.screen()
        p = @physics
        if (@right > w.right) or (@left < w.left)
            if @right > w.right
                this.adjust_right(w.right)
            if @left <= w.left
                this.adjust_left(w.left)
            p.velocity.x = p.velocity.x * -1
        
        if (@bottom > w.bottom) or (@top < w.top)
            if @bottom > w.bottom
                this.adjust_bottom(w.bottom)
            if @top < w.top
                this.adjust_top(w.top)
            p.velocity.y = p.velocity.y * -1

class Block extends RBSprite
    constructor: (X, Y, Width, Height) ->
        console.log("initing block: #{X}, #{Y}, #{Width}, #{Height}")
        super(X, Y, "data/platformer/env_tiles.png", Width, Height)
        ss = @_RB.media.get_spritesheet("env")
        console.log(ss)
        console.log("!")
        @.add_animation("default", new RBAnimation(ss,[1], RB_ANIM_NORMAL))
        @.set_animation("default")
        
        @active = true
        @fixed = true
        @static = true

class BlockWithController extends Block
    constructor:(X, Y, W, H) ->
        super(X, Y, W, H)
        @fixed = false
        @static = false

class Avatar extends Brick
    constructor:(X, Y, name) ->
        super(X,Y, 8, 8, "test")
        @nameobj = new RBText(0, -14, "booga")
        @nameobj.x = -RBText.width
        @nameobj.set_colour(30,80,220)
        @.add_child(@nameobj)
        #@showname = true
        #@.show_name(@showname)
        @iswalkleft = false
        @_RB.input.bind('keydown', filter=37, @.walk_left)

    show_name: (val) ->
        if val
            @showname = true
            @nameobj.visible = true
        else
            @showname = false
            @nameobj.visible = false

    walk_left: (key) =>
        if stop
            @iswalkleft = false
        if @iswalkleft? and @iswalkleft
            return
        else
            console.log('avatar, walk left received, keycode = ' + key)
    
    #draw: () ->
    #    super()
    #    @nameobj.draw()



class TestProtocolHandler extends RBProtocolHandler
    constructor: (stageobject) ->
        super(RBProtocol) #construct the super with RBProtocol as the prot
        @stage = stageobject
        @.add_function_mapping('hello', @.hello)
        @.add_function_mapping('call', @.object_call)
        @.add_function_mapping('uobj', @.object_update)
        @.add_function_mapping('nobj', @.object_new)

        @.add_class_mapping('dot', Dot)
        @.add_class_mapping('dude', Dude)
        @.add_class_mapping('avatar', Avatar)


    hello:(arglist) ->
        console.log("TestProtocolHandler says Hello! Args: " + arglist)

    #Call a method on an object
    object_call:(objid, funcname, args) =>
        return
    #Update an object with new values
    object_update:(args) =>
        console.log('obj update')
        objid = args[0]
        
        theobj = @stage.get_member_by_id(objid)
        if theobj != null
            #update x and y
            theobj.x = args[1][0]
            theobj.y = args[1][1]
        else
            console.log("No object with id " + objid)
        return

    #Create a new object, unless it already exists
    object_new:(args) => #this needs to be bound to this object
        
        console.log('obj new:')
        console.log( args)
        args_id = args[0]
        args_class = args[1]
        args_x = args[2][0]
        args_y = args[2][1]
        args_vx = args[2][2]
        args_vy = args[2][3]
        #console.log(@.get_class_mapping(args_class))
        #console.log(@stage)
        #newthing = new @.get_class_mapping(args[1])(args[2][0], args[2][1])
        theclass = @.get_class_mapping(args_class)
        console.log("creating new dot with x=" + args_x + " and y=" + args_y)
        newthing = new theclass(args_x, args_y)
        newthing.velocity.x = 0#args_vx
        newthing.velocity.y = 0#args_vy
        #newthing.width = 8
        #newthing.height = 8
        newthing.id = args_id
        #newthing = new Brick(200, 200, 20, 20, "", 40, 200, 80)

        @stage.add_member(newthing)
        return

class TestRequests
    constructor: (stageobject) ->
        super(RBProtocol) #construct the super with RBProtocol as the prot
        @stage = stageobject
        @.add_function_mapping('hello', @.hello)
        @.add_function_mapping('call', @.object_call)
        @.add_function_mapping('uobj', @.object_update)
        @.add_function_mapping('nobj', @.object_new)

        @.add_class_mapping('dot', Dot)
        @.add_class_mapping('dude', Dude)
        @.add_class_mapping('avatar', Avatar)


    hello:(arglist) ->
        console.log("TestProtocolHandler says Hello! Args: " + arglist)

    #Call a method on an object
    object_call:(objid, funcname, args) =>
        return
    #Update an object with new values
    object_update:(args) =>
        console.log('obj update')
        objid = args[0]
        
        theobj = @stage.get_member_by_id(objid)
        if theobj != null
            #update x and y
            theobj.x = args[1][0]
            theobj.y = args[1][1]
        else
            console.log("No object with id " + objid)
        return

    #Create a new object, unless it already exists
    object_new:(args) => #this needs to be bound to this object
        
        console.log('obj new:')
        console.log( args)
        args_id = args[0]
        args_class = args[1]
        args_x = args[2][0]
        args_y = args[2][1]
        args_vx = args[2][2]
        args_vy = args[2][3]
        #console.log(@.get_class_mapping(args_class))
        #console.log(@stage)
        #newthing = new @.get_class_mapping(args[1])(args[2][0], args[2][1])
        theclass = @.get_class_mapping(args_class)
        console.log("creating new dot with x=" + args_x + " and y=" + args_y)
        newthing = new theclass(args_x, args_y)
        newthing.velocity.x = 0#args_vx
        newthing.velocity.y = 0#args_vy
        #newthing.width = 8
        #newthing.height = 8
        newthing.id = args_id
        #newthing = new Brick(200, 200, 20, 20, "", 40, 200, 80)

        @stage.add_member(newthing)
        return


class PreloaderTestScene extends RBScene
    constructor: () ->
        super("Preloader Test Scene")
        
    
class TestCollisions2Scene extends RBPreLoaderScene
    constructor: () ->
        super("TestCollisions2Scene")
        @group1 = new RBLayer()
        @group2 = new RBLayer()
        @stage.add_child(@group1)
        @stage.add_child(@group2)
        @text = new RBText(RB().screen().width/2, RB().screen().height-20, "Move with arrows", "Courier", 20, null)
        @text.set_colour(200,50,70)
        console.log(@text._colour)
        @stage.add_member(@text)
        @input = RB().input

    on_preloaded: () ->
        console.log("preloaded")
        @myblock = new Block(100, 100, 40, 80)
        @group1.add_member(@myblock)
        @group2.add_member(new Block(200, 200, 50, 50))


    update: (dt) ->
        super(dt) #updates stage
        if @input.key_pressed(@input.keys.up)
            @myblock.move_by(0, -5)
        if @input.key_pressed(@input.keys.down)
            @myblock.move_by(0, 5)
        if @input.key_pressed(@input.keys.left)
            @myblock.move_by(-5, 0)
        if @input.key_pressed(@input.keys.right)
            @myblock.move_by(5, 0)

        @_RB.collide(@group1.members, @group2.members)
        if @myblock?
            #check collisions
            if (@myblock.is_hit(RB_COLLIDE_BOTTOM))
                #console.log("hit bottom")
                @text.set_text("Hit bottom")
            if (@myblock.is_hit(RB_COLLIDE_TOP))
                #console.log("hit top")
                @text.set_text("Hit top")
            if (@myblock.is_hit(RB_COLLIDE_LEFT))
                #console.log("hit left")
                @text.set_text("Hit left")
            if (@myblock.is_hit(RB_COLLIDE_RIGHT))
                #console.log("hit right")
                @text.set_text("Hit right")

            if (@myblock.is_hit(RB_COLLIDE_NONE))
                @text.set_text("Not colliding")

    draw: () ->
        super()
        @stage.render()
