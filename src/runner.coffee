class PLXLayerObject extends RBSprite
    constructor: (X, Y, num) ->
        super(X, Y, RBV_IMG.scenery, 320, 200)
        ss = @_RB.media.get_spritesheet(RBV_SS.scenery)
        @.add_animation("default", new RBAnimation(ss, [num], RB_ANIM_NORMAL) )
        @.set_animation("default")

class RunnerGameScene extends RBScene
    constructor: () ->
        super("Runner Game Scene")
        @input.add(RB_KEY_UP, @.on_key_up)
        @stage.add_child(new RBText(100, 100, "Game"))
        layerx = 320
        layery= 200

        #Create all the background layers
        @bglayer1 = new RBLoopLayer(layerx, layery)
        @bglayer2 = new RBLoopLayer(layerx, layery)
        @bglayer3 = new RBLoopLayer(layerx, layery)
        @bglayer4 = new RBLoopLayer(layerx, layery)
        @bglayer5 = new RBLoopLayer(layerx, layery)
        #Set their relative movement (for use with a camera)
        @bglayer1.set_parallax(2.5)
        @bglayer2.set_parallax(3.5)
        @bglayer3.set_parallax(4.5)
        @bglayer4.set_parallax(5.5)
        @bglayer5.set_parallax(6.5)
       
        #Add them to the scene, and optionally give them velocity
        #This could give the illusion of movement if we weren't using a camera.
        v = 50
        vplus = 50
        h = 100
        num = 4
        for layer in [@bglayer1, @bglayer2, @bglayer3, @bglayer4, @bglayer5 ] #@bglayer3, @bglayer2, @bglayer1]

            
            p = new RBPhysics()
            layer.physics = p
            p.floating = true
            layer.add_component(p)
            p.velocity.x = -v
            p.velocity.y = 0
            v+= vplus
            layer.add_child( new PLXLayerObject(160,100, num) ) 
            num -= 1
            @stage.add_child(layer)

        #Create a UI Layer and a score 
        @uilayer = new RBLayer()
        @uilayer.set_parallax(0, 0)
        @score = new RBText(0, 0, "Score: 700", "Arial", 25)
        @score.set_anchor(0, 0)
        @score.set_colour(255, 255, 255)
        @score.set_stroke_width(1.5)
        @score.set_stroke(0, 0, 0)
        @score.isStroke=  true 
        @uilayer.add_child(@score)

        @envlayer = new RBLayer()
        @envlayer.name = "Environment"
        @enemylayer = new RBLayer()
        @enemylayer.name = "Enemies"
        @pickupslayer = new RBLayer()
        @pickupslayer.name = "Pickups"

        #Create a Player layer and a player
        @playerlayer = new RBLayer()
        @player = new RBRectEntity(100, 100, 10, 10)
        @player.physics = new RBPhysics()
        @player.physics.floating = true
        @player.add_component(@player.physics)
        @player.physics.velocity.x = 50
        #@player.physics.velocity.y = 0
        @playerlayer.add_child(@player)


        #add the other layers to the scene.
        #Remember the draw order you want.
        
        @stage.add_child(@envlayer)
        @stage.add_child(@enemylayer)
        @stage.add_child(@pickupslayer)
        @stage.add_child(@playerlayer)
        @stage.add_child(@uilayer)
        @cam = new RBCamera(0, 0, RB().screen().width, RB().screen().height)
        @cam.follow(@player)
        @.add_child(@cam)
        @editor = new RBEditor(32, 32, [@envlayer, @pickupslayer, @enemylayer], {
        }, @)
        @.add_child(@editor)


    on_key_up: (event) =>
        code = event.keyCode
        keys = @_RB.input.keys
        if code is keys.esc
            @.pop()
    update: (dt) ->
        super(dt) #updates stage, cam, editor
        @score.set_text(@score.x)
        keys = @_RB.input.keys
        if RB().input.key_pressed(keys.up)
            @player.physics.velocity.y -= 5
        if RB().input.key_pressed(keys.down)
            @player.physics.velocity.y += 5

    draw: () ->
        #For a non cam scenario:
        @stage.render()
        #@editor.render()
        #For a cam scenario:
        @cam.draw(@stage)
        #@cam.draw(@editor)
        #@uilayer.render()

class RunnerTitleScene extends RBPreLoaderScene
    constructor: () ->
        super("Runner title scene")
        console.log("Constructing runner title")
        @input.add(RB_KEY_UP, @.on_key_up)
        @loading_text = new RBText(@_RB.screen().width-100, @_RB.screen().height-12, "")
        @stage.add_child(@loading_text)
        @logo = new RBSprite(@_RB.screen().width-32, @_RB.screen().height-32, "rblogo", 32, 32)
        @logo.set_anchor(0, 0)
        @stage.add_child(@logo)
    
    on_preloaded: () ->
        console.log("preloaded!")
        @continue_text = new RBText(@_RB.screen().width/2, 100, "Press Space To Continue")
        @stage.add_child(@continue_text)
    
    on_key_up: (event) =>
        code = event.keyCode
        keys = @_RB.input.keys
        if code is keys.space
            @.pop()

    
    update: (dt) ->
        super(dt)
        @loading_text.set_text("Loading...[#{@.pl.stats.total}]")

class Runner extends RBGame
    constructor: () ->
        console.log("constructing runner")
        super()
        @titlescene = @gamescene = @gameoverscene = null
        @_RB.events.add(RB_EVENT_ON_START, @.show_title)

    show_title: () =>
        if @titlescene is null
            @_RB.events.remove(RB_EVENT_ON_START, @.show_title)
            @titlescene = new RunnerTitleScene()
            @titlescene.events.add(RB_EVENT_ON_SCENE_EXIT, @.start_game)
        @titlescene.push()
    start_game: () =>
        if @gamescene isnt null
            @gamescene.destroy()
        @gamescene = new RunnerGameScene()
        @gamescene.events.add(RB_EVENT_ON_SCENE_EXIT, @.game_over)
        @gamescene.push()

    game_over: () =>
        if @gameoverscene is null
            @gameoverscene = new RunnerGameOverScene()
            @gameoverscene.events.add(RB_EVENT_ON_SCENE_EXIT, @.show_title)
        @gameoverscene.push()

_runnergame = null
window.run_runner = () ->
    RBV(RBV_ENG, {
        fps: 60
        screenwidth: 320
        screenheight: 200
        canvaswidth: 640
        canvasheight: 480
        canvasid: "canvas"
        canvassmoothing: false
    })
    RBV(RBV_IMG, {
        scenery: "data/runner/scenery2.png"
    })
    RBV(RBV_SND, {
    })
    RBV(RBV_MUSIC, {
    })

    RBV(RBV_SS, {
        scenery: ["scenery", RBV(RBV_IMG).scenery, 1, 5] #, 96, 32]
    })

    RBV(RBV_GAME,{
    })

    _runnergame = new Runner()
    RB().start()



