# RBTimer is a completely independent timer.
# You should use this when you need a timer which is not reliant
# on the update of the main game, and RB(), because this is 
# independant of that.
# But.
# It is expensive.
# Every time you want to check it, it has to create date objects
# and subtract them etc.
# 
# If you want to use lots of little timers to keep track of things,
# try a RBIntervalTimer instead.
#
# Also, RBTimer has no idea how long it has been running. It only knows
# how long it has been since someone last checked.
#
class RBTimer
    constructor: () ->
        @_dt = 0
        @_totalelapsed = 0
        @_previousvalue = null
        @_currentvalue = null
        @.reset()

    # Resets the timer.
    # Calling elapsed immediately after this would return 0
    #
    reset: () ->
        @_previousvalue = new Date()
        @_currentvalue = new Date()
    
    # Just to avoid any misunderstanding, 
    # elapsed() returns the number of milliseconds that have elapsed
    # since the last call to elapsed. It is not the total
    # time since the timer was started
    #
    elapsed: () ->
        d = new Date()
        @_currentvalue.setTime(d.getTime() - @_previousvalue.getTime() )
        @_dt = @_currentvalue.getMilliseconds()
        @_previousvalue = d
        #ret
        @_dt

#this class was a nice idea - a cheap class offset against a real RBTimer.
#in practice it doesn't really work. You can't use it to time anything
#that takes less than a frame (or whatever your parent timers resolution is).
#you could only time things that begin in one frame and end in the next, giving the
#parent timer time to be updated in the meantime.
# ahh well.
###
class RBSimpleTimer
    constructor: (@parent_timer = RB()._timer) ->
        @_internaltime = new Date()
        @_parentoffset = 0
        @_dt = 0
    reset: () ->
        @_parentoffset = @parent_timer._previousvalue.getTime()
        @_dt = 0
    elapsed: () ->
        @_internaltime.setTime( @parent_timer._previousvalue.getTime() - @_parentoffset )
        #@_dt = @_elapsed.getMilliseconds()
        @.reset()
        @_dt = @_internaltime.getMilliseconds()
        @_dt
###
    

# RBInterval timer is useful for when you want to create
# a timer with a timeout, which you can test whenever you like
# to find out if your 'interval' has happened.
# The timer starts when you create or reset it.
class RBIntervalTimer extends RBTimer
    # @param [Number] desired_interval The timeout value you'd like to use
    #   for this timer.
    constructor: (desired_interval) ->
        super()
        @_interval = 0
        @_interval_elapsed = 0
        @_has_interval_expired = false
        @.set_interval(desired_interval)

    # Sets the interval for this timer, and resets it.
    # @param [Number] interval The new interval
    set_interval: (interval) ->
        @_interval = interval
        @.reset()

    # Gets the number of milliseconds since this timer
    # was last reset
    get_elapsed: () ->
        @_interval_elapsed

    # Resets the timer so that it has an elapsed time of zero,
    # and clears the expired flag
    reset: () ->
        @_has_interval_expired = false
        @_interval_elapsed = 0
        super() #super reset
        
    # Returns true if the interval has expired, false if not.
    has_interval_expired: () ->
        if (@_has_interval_expired)
            return true
        else
            @_interval_elapsed += @.elapsed()
            if (@_interval_elapsed > @_interval)
                @_has_interval_expired = true
            else
                @_has_interval_expired = false
        @_has_interval_expired
   
