class RBSceneManager
    _instance = null
    @get_instance: () ->
        _instance ?= new RBSceneManagerPrivate()
    
    class RBSceneManagerPrivate
        constructor: () ->
            @_scenes = new RBGroup("SceneStack") #the scene stack
            @_sceneCache = []
            @_currentscene = null
        current_scene: () ->
            return @_currentscene
        
        #returns named scene from cache
        #null if it didn't exist
        #if name is not unique, returns first found scene with that name
        #which may not be in the order pushed
        get_cached_scene: (scenename) ->
            for scene in @_sceneCache
                if scene.name == scenename
                    return scene
            return null
        push_scene: (scene) ->
            if @_currentscene == null
                @_scenes.add_member(scene)
            else
                @_scenes.add_after(scene, @_currentscene)
            @_currentscene = scene
            @_sceneCache[scene.id] = scene

        pop_scene:(scene, removeFromCache=false) ->
            if @_currentscene != scene
                console.warn("You tried to pop a scene that was not the current scene")
                return
            beforescene = @_scenes.get_before(scene)
            @_scenes.remove_member_by_id(scene.id)
            @_currentscene = beforescene
            if removeFromCache
                cacheindex = @_sceneCache.indexOf(scene)
                if cacheindex >= 0
                    @_sceneCache.splice(cacheindex, 1)

        replace_scene:(scene, existingscene) ->
            if !@_scenes.is_member(scene)
                @_scenes.add_before(scene, existingscene)
            @_scenes.remove_member_by_id(existingscene.id)
            @_currentscene = scene
             

class RBScene extends RBEntity
    constructor: (scenename="UnnamedScene") ->
        super(0,0,0,0,scenename)
        @manager = RBSceneManager.get_instance()
        @stage = new RBEntity(0,0,0,0)
        @stage.alwaysOnScreen = true
        @.add_child(@stage)
        @has_input_focus = false
        @events = new RBDelegateCollection(scenename,
            [RB_EVENT_ON_SCENE_ENTER,
            RB_EVENT_ON_SCENE_EXIT,
            RB_EVENT_ON_SCENE_REVEAL,
            RB_EVENT_ON_SCENE_ECLIPSE] )
        @input = new RBDelegateCollection("#{scenename}_input",
            [_RBInputEvents.mousemove,
            _RBInputEvents.mousedown,
            _RBInputEvents.mouseup,
            _RBInputEvents.touchstart,
            _RBInputEvents.touchend,
            _RBInputEvents.touchcancel,
            _RBInputEvents.touchmove,
            _RBInputEvents.keydown,
            _RBInputEvents.keyup,
            _RBInputEvents.keydown] )

    destroy: () ->
        #console.log("destroying Scene #{@name}")
        @stage.destroy()
        @input.destroy()
        @events.destroy()
        super()


    #push this scene onto the scene manager stack.
    #we should become the current scene
    push: () ->
        cs = @manager.current_scene()
        @manager.push_scene(@)
        if cs != null
            cs.on_eclipse()
        @.on_enter()
        return @
    
    #pop this scene off the stack (wherever it is)
    pop: (removeFromCache=false) ->
        @manager.pop_scene(@, removeFromCache)
        @.on_exit()
        cs = @manager.current_scene()
        if cs != null
            cs.on_reveal()
        return @

    #update: (dt) ->
    #
    #    @stage.update(dt)
    #    #override this

    draw: () ->
        #override this

    focus: (value) ->
        if value? and value != @focus
            @has_input_focus = value
            if @focus
                @.on_gain_focus()
            else
                @.on_lose_focus()

        else
            @has_input_focus
    has_focus: () ->
        @.focus()

    #what to do when we get input focus
    on_gain_focus: () ->
        #console.log("Scene #{@name} enabling input")
        @input.enable()
    #what to do when we lose input focus
    on_lose_focus: () ->
        #console.log("Scene #{@name} disabling input")
        @input.disable()
    #what to do on enter
    on_enter: () ->
        #console.log("Scene #{@name} entered.")
        @focus(true)
        @events.call_delegates(RB_EVENT_ON_SCENE_ENTER)
    #what to do on exit
    on_exit: () ->
        #console.log("Scene #{@name} exited.")
        @focus(false)
        @events.call_delegates(RB_EVENT_ON_SCENE_EXIT)
    #called on a scene when it is the current scene, and 
    #another scene is pushed on top
    on_eclipse: () ->
        #console.log("Scene #{@name} eclipsed by #{@manager.current_scene().name}.")
        @events.call_delegates(RB_EVENT_ON_SCENE_ECLIPSE)

    on_reveal: () ->
        #console.log("Scene #{@name} revealed.")
        @events.call_delegates(RB_EVENT_ON_SCENE_REVEAL)

# This type of scene will load all the images, sounds, and spritesheets
# in the global settings object, and call the on_preloaded function
# when done.
class RBPreLoaderScene extends RBScene
    constructor: (name) -> #, @imagelist, @soundlist, @spritesheetlist) ->
        super(name)
        @pl = RB().preloader
        @preloaded = false

    on_enter: () ->
        super()
        #@_RB.input.bind(RB_KEY_UP, @.on_key_up)
        if not @finished
            s = @_RB.env("settings")
            @imagelist = (img for name, img of s.images)
            #console.log(@imagelist)
            @soundlist = (snd for name, snd of s.sounds)
            #console.log(@soundlist)
            @musiclist = (ms for name, ms of s.music)
            #console.log(@musiclist)
            @spritesheetlist = (args for name, args of s.spritesheets)
            #console.log(@spritesheetlist)
            @pl.queue_images(@imagelist)
            @pl.queue_sounds(@soundlist)
            @pl.queue_music(@musiclist)
            @pl.queue_spritesheets(@spritesheetlist)
            @pl.start()

    on_exit: () ->
        super()
        #@_RB.input.unbind(RB_KEY_UP, @.on_key_up)


    update: (dt) ->
        super(dt)
        if not @preloaded
            @pl.update()
            if @pl.done
                @preloaded = true
                @.on_preloaded()

    draw: () ->
        super()
        @stage.render()

    on_preloaded: () ->
        #override me
