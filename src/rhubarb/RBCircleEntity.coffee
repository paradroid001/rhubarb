class RBCircleEntity extends RBEntity
    constructor: (X, Y, radius, name="RBCircleEntity") ->
        super(X, Y, radius*2, radius*2, name)
        @radius = radius
        @shouldFill = false
        @fillColour = new RBColour(0, 0, 0, 255)
    #call set_fill with null or false as first arg to 
    #turn off fill for this rect. Otherwise pass
    #r, g, g, a as 0-255 values
    set_fill:(r=0, g=0, b=0, a=255) -> #expect [r,g,b,a]
        if r == null or r == false
            @shouldFill = false
        else
            @shouldFill = true
            @fillColour.set_rgba([r, g, b, a])
    draw:() ->
        RB().cctx.beginPath()
        RB().cctx.arc(0, 0, @radius, 0, 2 * Math.PI, false)
        RB().cctx.closePath()
        if @shouldFill
            @_RB.cctx.fillStyle = @fillColour.htmlrgba
            @_RB.cctx.fill()
        @_RB.cctx.strokeStyle = @_colour.htmlrgb
        @_RB.cctx.stroke()       
