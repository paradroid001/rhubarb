#!/bin/bash
echo "Creating dist"
version=$(cat VERSION)
echo "Version = $version"
sed -Ei "" "s|RB_VERSION\ \=\ .*$|RB_VERSION = \"${version}\"|g" src/rhubarb/RBConstants.coffee
./build_all.sh
versiondir="dist/${version}"
mkdir -p ${versiondir}

cp -r docs ${versiondir}/
mkdir -p ${versiondir}/js
cp js/rhubarb.js ${versiondir}/js/
cp js/rhubarb.map ${versiondir}/js/
cp js/jquery* ${versiondir}/js
cp -r css ${versiondir}/
cp *.css ${versiondir}/css
