# Rhubarbs Keys object
class RBKeys
    # @property [Number] Escape Key
    esc:  27
    # @property [Number] Enter Key
    enter:  13
    # @property [Number] Space Key
    space:  32
    # @property [Number] Up Arrow Key
    up:  38
    # @property [Number] Down Arrow Key
    down:  40
    # @property [Number] Left Arrow Key
    left:  37
    # @property [Number] Right Arrow Key
    right:  39
    # @property [Number] Question Mark Key
    qmark:  191
    # @property [Number] Tilde Key
    tilde:  192
    # @property [Number] Plus Key (not numpad)
    plus:  187
    # @property [Number] Minus Key (not numpad)
    minus:  189
    # @property [Number] Tab Key
    tab:  9
    # @property [Number] Delete Key
    delete: 46
    # @property [Number] Shift Key
    shift:  16
    # @property [Number] Ctrl Key
    ctrl:  17
    # @property [Number] Alt Key
    alt:  18
    # @property [Number] Number 0 Key (not numpad)
    num_0:  48
    # @property [Number] Number 1 Key (not numpad)
    num_1:  49
    # @property [Number] Number 2 Key (not numpad)
    num_2:  50
    # @property [Number] Number 3 Key (not numpad)
    num_3:  51
    # @property [Number] Number 4 Key (not numpad)
    num_4:  52
    # @property [Number] Number 5 Key (not numpad)
    num_5:  53
    # @property [Number] Number 6 Key (not numpad)
    num_6:  54
    # @property [Number] Number 7 Key (not numpad)
    num_7:  55
    # @property [Number] Number 8 Key (not numpad)
    num_8:  56
    # @property [Number] Number 9 Key (not numpad)
    num_9:  57
    # @property [Number] a Key
    a:  65
    # @property [Number] b Key
    b:  66
    # @property [Number] c Key
    c:  67
    # @property [Number] d Key
    d:  68
    # @property [Number] e Key
    e:  69
    # @property [Number] f Key
    f:  70
    # @property [Number] g Key
    g:  71
    # @property [Number] h Key
    h:  72
    # @property [Number] i Key
    i:  73
    # @property [Number] j Key
    j:  74
    # @property [Number] k Key
    k:  75
    # @property [Number] l Key
    l:  76
    # @property [Number] m Key
    m:  77
    # @property [Number] n Key
    n:  78
    # @property [Number] o Key
    o:  79
    # @property [Number] p Key
    p:  80
    # @property [Number] q Key
    q:  81
    # @property [Number] r Key
    r:  82
    # @property [Number] s Key
    s:  83
    # @property [Number] t Key
    t:  84
    # @property [Number] u Key
    u:  85
    # @property [Number] v Key
    v:  86
    # @property [Number] w Key
    w:  87
    # @property [Number] x Key
    x:  88
    # @property [Number] y Key
    y:  89
    # @property [Number] z Key
    z:  90
    # @property [Number] \ Key
    backslash:  220
    # @property [Number] / Key
    forwardslash:  191
    # @property [Number] . Key
    period:  190
    # @property [Number] - Key
    dash:  189
    # @property [Number] , Key
    comma:  188
    # @property [Number] =  Key
    equals:  187
    # @property [Number] numlock Key
    numlock:  144

_RBKeys = new RBKeys()


_KeyStates =
    up: 1   #the key has been released, but not checked
    down: 2 #the key is pressed down
    clear: 0 #the key has not been touched since last check


_RBInputEvents =
    mousemove: RB_MOUSE_MOVE
    mousedown: RB_MOUSE_DOWN
    mouseup:   RB_MOUSE_UP
    mousewheel: RB_MOUSE_WHEEL
    touchstart: RB_TOUCH_START
    touchend: RB_TOUCH_END
    touchcancel: RB_TOUCH_CANCEL
    touchmove: RB_TOUCH_MOVE
    keydown: RB_KEY_DOWN
    keyup: RB_KEY_UP
    dblclick: RB_MOUSE_DOUBLECLICK

###
class RBInputType
    constructor: ()

class RBPressableInput
    constructor: () ->
        @_state = _KeyStates.clear
    is_clear:() ->
        return @_state == _KeyStates.clear
    is_pressed:() ->
        return @.is_down()
    is_up:() ->
        return @_state == _KeyStates.up
    is_down:() ->
        return @_state == _KeyStates.down
    update_state: (newstate) =>
        @_state = newstate
    true: () =>
        #override with some logic that evaluates to true for some specific context
        #i.e. if the key is held down, you call this 'true'

class RBTimedPressableInput extends RBPressableInput
    constructor: (@threshold)
        super()
        
        @_time_since_last_event = 0.0
        @_timer = new RBTimer()
    true: ()
        if super() and @_time_since_last_event > @threshold
            return true
        else
            return false

class RBKeyHeld extends RBPressableInput
    constructor: (@keycode) ->
        super()

class RBMouseHeld
    constructor: (@mousecode) ->
        super()

class RBTimedKeyHeld extends RBTimedPressableInput
    constructor(@keycode, @mousecode)

class RBTimedMouseHeld

class RBKeyPressed

class RBMousePressed

#example
class UpKeyHeldDown extends RBStatefulInput
    constructor: ()
        super()
        @keycode = _RBKeys.up
        @upevent = _RBInputEvents.keyup
        @downevent = _RBInputEvents.keydown
    true:() =>
        return @.is_down()


class RBInputListenerList
    true: () =>
        #is true when any of its elements are true.

class RBInputListener
    
    #RBinputlisteners can be configured with actions which map to lists of input events
    #They end up binding to RBInput events
    constructor: () ->

    registerInputAction: () ->
    
    unregisterInputAction: () ->

        #registerInputAction takes the name, and a list of lists. Within in the inner lists, they will evaluate to true when any
        #of their contents is true. i.e. they are ored together
        #the entire action will be true when all lists given evaluate to true, i.e. they are anded together.
    #'jump' will be true, when any of the list is true
    RBInputListener.registerInputAction("jump", [[KeyHeld(_RBKeys.up), MouseHeld(_RBKeys.mouse1)]]) #'held' means is true if key is down
    RBInputListener.registerInputAction("walkright", [[KeyHeld(_RBKeys.right)]])
    RBInputListener.registerInputAction("fire", [[KeyPressed(_RBKeys.ctrl), MousePressed(_RBKeys.mouse2)]]) #'pressed' means pushed down, resets when pushed again
    RBInputListener.registerInputAction("chargeweapon", [[TimedKeyHeld(_RBKeys.z, 0.5)]]) #timedkeyheld means true if key is held more than n seconds
###

# A class to represent the mouse
class RBMouse extends RBRect
    constructor: () ->
        super(0, 0, 2, 2)
        @_entlist = new RBGroup()
        @dragentity = null
        @rawx = 0
        @rawy = 0
        @leftpressed = false #up
        @rightpressed = false #up
        @middlepressed = false #up
        @wheeldelta = 0

    #cross platform mouse wheel delta (FF is reversed. This handles it)
    calc_wheel_delta: (e) ->
        @wheeldelta = Math.max(-1, Math.min(1, (e.wheelDelta || -e.detail)))
    reset_wheel_delta: () ->
        @wheeldelta = 0
    
    # Pass it a single RBEntity (Generally RBLayer), or
    # a list of entities. If one of these entities is 
    # destroyed, it will also be removed from drag registration
    # 
    register_drag: (entity) ->
        @_entlist.add_member(entity)
    # You can manually call this to remove an entity from drag 
    # registration, but only if it was directly registered.
    # If it is a child of an RBEntity that was registered
    # e.g. an RBLayer, then the only way to deregister it is
    # to remove it from that RBEntity.
    unregister_drag: (entity) ->
        @_entlist.remove_member(entity)
        #if...
        #console.log("removed a draggable entity with id: #{entity.id}")
        
    # @private
    # Updates the mouse position. Called by RB().input's mouse move
    update_position: (@rawx, @rawy) ->
        @.set_position(
            @rawx / RBV(RBV_ENG).screenscalex + RB().screen().x,
            @rawy / RBV(RBV_ENG).screenscaley + RB().screen().y)


    # @private
    # Checks for draggable entities having collisions with the mouse
    check_drag: () ->
        if @dragentity is null
            #console.log("checking for drag")
            e = @.overlaps_in_rlist(@_entlist.members)
            #console.log(e)
            if e? and e isnt null
                @dragentity = e
                if @dragentity.on_drag_start?
                    @dragentity.on_drag_start(@)
                return true
        return false

    # @private
    # Drops the currently held entity
    check_drop: () ->
        if @dragentity isnt null
            #console.log("checking for drop")
            if @dragentity.on_drag_end?
                @dragentity.on_drag_end(@)
            @dragentity = null
            return true
        return false

    # @updates the position of entities.
    do_drag_update: (dt) ->
        if @dragentity isnt null
            @dragentity.set_position(@.x, @.y)



class RBInput extends RBDelegateCollection
    constructor: (@_RB) ->

        super("RBInputDelegates",
            [_RBInputEvents.mousemove,
            _RBInputEvents.mousedown,
            _RBInputEvents.mouseup,
            _RBInputEvents.mousewheel,
            _RBInputEvents.dblclick,
            _RBInputEvents.touchstart,
            _RBInputEvents.touchend,
            _RBInputEvents.touchcancel,
            _RBInputEvents.touchmove,
            _RBInputEvents.keydown,
            _RBInputEvents.keyup,
            _RBInputEvents.keydown])

        @mouse = new RBMouse()
        @keys = _RBKeys
        @keysvalues = []
        for k, v of @keys
            @keysvalues.push(v)
        @keynav = null
        @keystate = {}
        @_inited = false
        @.init()
    
    init: () ->
        if @_inited
            return
        #Ok. Bind the dom level events
        window.addEventListener('resize', @.on_resize, false)
        binder = document
        #binder = @_RB._canvas #note, at this point, canvas is null
        
                       
        binder.addEventListener('mousemove', @.on_mouse_move, false)
        binder.addEventListener('mousedown', @.on_mouse_down, false)
        binder.addEventListener('mouseup', @.on_mouse_up, false)
        
        #Mousewheel IE8+, Chrome, Safari
        binder.addEventListener('mousewheel', @.on_mouse_wheel, false)
        #binder.addEventListener('dblclick', @.on_mouse_doubleclick, false)
        #binder.addEventListener('click', @.on_mouse_click, false)
        #Mousewheel FF
        binder.addEventListener('DOMMouseScroll', @.on_mouse_wheel, false)
        binder.addEventListener('touchstart', @.on_touch_start, false)
        binder.addEventListener('touchend', @.on_touch_end, false)
        binder.addEventListener('touchcancel', @.on_touch_cancel, false)
        binder.addEventListener('touchmove', @.on_touch_move, false)
        binder.addEventListener('keydown', @.on_key_down, false)
        binder.addEventListener('keyup', @.on_key_up, false)
    
    bind: (evname, callback, consume=false) ->
        @.add_delegate(evname, callback, consume)
    
    unbind: (evname, callback) ->
        @.remove_delegate(evname, callback)
    
    call_handlers: (evname, event) ->
        @.call_delegates(evname, [event])
        @.call_scene_handlers(evname, [event])
    
    #Automatically call the input handlers for the current scene.
    call_scene_handlers: (evname, args) ->
        cs = RBSceneManager.get_instance().current_scene()
        if cs?
            #console.log("Calling delegates in #{cs.name} for event #{evname}")
            cs.input.call_delegates(evname, args)

    on_mouse_click: (event) =>
        console.log("click")
        @.swallow(event)

    on_mouse_doubleclick: (event) =>
        console.log("double click")
        @.swallow(event)

    on_mouse_wheel: (event) =>
        @.mouse.calc_wheel_delta(event)
        @.call_handlers(_RBInputEvents.mousewheel, event)
        @.mouse.reset_wheel_delta()

    on_mouse_down: (event) =>
        #@mouse.clicking = true
        #if @mouse.mouseoverentity
        #    @mouse.dragentity = @mouse.mouseoverentity
        # TODO: This won't work for IE, which uses a different map
        switch event.button
            when 0
                @mouse.leftpressed = true
            when 1
                @mouse.middlepressed = true
            when 2
                @mouse.rightpressed = true
        if not @mouse.check_drag()
            @.call_handlers(_RBInputEvents.mousedown, event)

    on_mouse_move: (event) =>
        @mouse.update_position(
            event.clientX - @_RB.canvas().x,
            event.clientY - @_RB.canvas().y)
        ###
        @mouse.rawx = event.clientX - @_RB.canvas().x
        @mouse.rawy = event.clientY - @_RB.canvas().y

        @mouse.x = @mouse.rawx / RBV(RBV_ENG).screenscalex + @_RB.screen().x
        @mouse.y = @mouse.rawy / RBV(RBV_ENG).screenscaley + @_RB.screen().y

        #check for mouseover
        mrect = new RBRect(@mouse.x, @mouse.y, 10,10)
        @mouse.mouseoverentity = null
        if @mouse.dragentity
            @mouse.dragentity = @mouse.x
            @mouse.dragentity = @mouse.y
        ###
        @mouse.do_drag_update()
        @.call_handlers(_RBInputEvents.mousemove, event)

    on_mouse_up: (event) =>
        switch event.button
            when 0
                @mouse.leftpressed = false
            when 1
                @mouse.middlepressed = false
            when 2
                @mouse.rightpressed = false
        #@mouse.clicking = false
        #@mouse.dragentity = null
        if not @mouse.check_drop()
            @.call_handlers(_RBInputEvents.mouseup, event)

    on_touch_start: (event) =>
        @.swallow(event)
        #@mouse.clicking = true
        console.log("Touch start")
        @.call_handlers(_RBInputEvents.touchstart, event)
    
    on_touch_end: (event) =>
        @.swallow(event)
        #@mouse.clicking = false
        console.log("Touch end")
        @.call_handlers(_RBInputEvents.touchend, event)
    
    on_touch_cancel: (event) =>
        return
    
    on_touch_move: (event) =>
        return
    
    on_key_down: (event) =>
        if event.keyCode in @keysvalues
            @keystate[event.keyCode] = _KeyStates.down
        @.call_handlers(_RBInputEvents.keydown, event)
        if (event.keyCode is @keys.tab)
            @.swallow(event)

    on_key_up: (event) =>
        if event.keyCode in @keysvalues
            @keystate[event.keyCode] = _KeyStates.up
        @.call_handlers(_RBInputEvents.keyup, event)

    on_resize: (event) =>
        #Do nothing
        #@.resize_canvas()

    key_released: (keyname) =>
        if @keystate[keyname]? and @keystate[keyname] is _KeyStates.up
            @keystate[keyname] = _KeyStates.clear 
            return true
        else
            return false
    key_pressed: (keyname) =>
        if @keystate[keyname]? and @keystate[keyname] is _KeyStates.down
            return true
        else
            return false

    swallow: (event) ->
        event.preventDefault()



