#!/bin/bash
#The dir jsdoc is in. Don't end this in a slash
#JSDOC_DIR=../jsdoc_toolkit-2.4.0/jsdoc-toolkit
#java -jar $JSDOC_DIR/jsrun.jar $JSDOC_DIR/app/run.js -a -t=$JSDOC_DIR/templates/jsdoc/ -d=./docs/ js/rhubarb.js

#docco -o docs/ src/rhubarb.coffee

#crojsdoc -o cdocs/ -q src/rhubarb.coffee

#codo -v -n Rhubarb -o cododocs/ src/rhubarb/RB.coffee src/rhubarb/RBBase.coffee src/rhubarb/RBBase.coffee src/rhubarb/RBInput.coffee src/rhubarb/RBAction.coffee src/rhubarb/RBAnimation.coffee src/rhubarb/RBComponent.coffee src/rhubarb/RBCamera.coffee src/rhubarb/RBEntity.coffee src/rhubarb/RBGroup.coffee src/Rhubarb/RBInput.coffee

codo -v -n Rhubarb --title "Rhubarb Documentation" -r README.md -o docs/ src/rhubarb/*.coffee
