#!/bin/bash
echo "Building Rhubarb..."
fileslist=`./buildfiles.sh`
#--join doesn't seem to be working now, so instead of doing the following:
#coffee -b -m -o js --join src/rhubarb.coffee --compile $fileslist
#We will just make a src/rhubarb.coffee and then compile it.
cat $fileslist > src/rhubarb.coffee
coffee -b -m -o js --compile src/*.coffee
