#requires RBBase.coffee

# RBGroup
# -------
# Add RBObjects to groups. Can only be a member of the group once.
# Don't try to add non-RBObjects RBGroups.
# Objects are retrievable by ID, and removeable by object and by ID
# You can loop over the children by using a 'for obj in <group>.members
# You can add groups to groups
# You can't add groups to themselves.
# You can't add the same object to the group twice
# Group members are stored sequentially in a list, so order is preserved
#
# RBGroup is meant to be kind of abstract (not in the OO sense,
# in the 'it is a virtual concept rather than a physical thing' sense).
# It kind of sucks that it inherits from RBObject (so you can put
# RBGroups within RBGroups), and that means it gets X,Y,Width,Height.
# BUT, it makes sense for RBEntities to 'be' groups (so they can
# contain other entities), and therefore the XYWH thing is probably
# not so bad.
class RBGroup extends RBObject
    constructor: (@name = "NoName", X, Y, Width, Height, id=null) ->
        super(X, Y, Width, Height)
        if id? and id != null
            @id = id
            #RB().log(id)
        @members = []
        @_idmap = {} #a map from objectid to list indice
        @count = 0
        @.empty()
    # Returns id of the object if added, otherwise null
    # The position is the index we want to splice into
    # This isn't important externally, but is used by 
    # functions that want to add members before or 
    # after other objects. So the absolute indexes
    # aren't important, but the relative positions are.
    add_member: (obj, position=-1) ->
        #console.log("Addmember called")
        ret = null
        if not (obj is @)
            #console.log("obj not this")
            if obj.id? #does the obj have an id?
                if @_idmap[obj.id]?
                    ret = obj.id #already existed
                else
                    @_idmap[obj.id] = obj
                    if position == -1
                        @members.push(obj)
                    else
                        @members.splice(position, 0, obj)
                    ret = obj.id
                    @count++
            else
                #no id present
                #this is ok - we add an id
                obj.id = @_RB.newID() #@members.length
                @_idmap[obj.id] = obj.id
                if position == -1
                    @members.push(obj)
                else
                    @members.splice(position, 0, obj)
                ret = obj.id
                @count++
        #else
        #    #console.log("obj was this??")
        #    return null
    
        return ret

    add_before: (obj, existingobj) ->
        existingindex = @members.indexOf(existingobj)
        if existingindex >= 0
            @.add_member(obj, existingindex)
    add_after: (obj, existingobj) ->
        existingindex = @members.indexOf(existingobj)
        if existingindex >= 0
            @.add_member(obj, existingindex+1)
    get_before:(obj) ->
        ret = null
        eindex = @members.indexOf(obj)
        if eindex >=1
            ret = @members[eindex-1]
        return ret
    get_after: (obj) ->
        ret = null
        eindex = @members.indexOf(obj)
        if eindex >=0 and eindex < @members.length-1
            ret = @members[eindex+1]
        return ret

    is_member: (obj) ->
        if @members.indexOf(obj) >= 0
            return true
        return false

    get_member_ids:() ->
        return (id for id of @_idmap) #return the 'keys'
    
    get_member_by_id: (id) ->
        ret = null
        if @_idmap[id]?
            ret = @_idmap[id]
        return ret

    remove_member_by_id: (id) ->
        if @_idmap[id]?
            obj = @_idmap[id]
            #TODO: This won't work on ie 7, 8: need to define if so.
            index = @members.indexOf(obj)
            if index >= 0
                #console.log("Group #{@name}: Removing member with id #{id} at index #{index}")
                @members.splice(index, 1)
            #else
            #    console.log("Group #{@name}: Object for id #{id} did not exist in members.")
            delete @_idmap[id]
            
            --@count
            return true
        return false
            
    #Only removes if it finds your object, else returns null
    remove_member: (obj) ->
        @.remove_member_by_id(obj.id)


    #emties a group by resetting its members, idmap, and count
    empty: () ->
        for id in @.get_member_ids()
            @.remove_member_by_id(id)

    #pass the val (could be a value, object, whatever) and the comparison function,
    #which should take 2 args - the first should be your passed value, the second
    #should be an RBObject as iterated in the members list.
    #Your function should return false on non match
    #returns the object on success, null on fail. Returns only the first matching obj
    find: (val, cmpfnc) ->
        for member in @members
            if cmpfnc(val, member)
                return member
        return null
