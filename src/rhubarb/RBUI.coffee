RBUI_LEFT = 1
RBUI_RIGHT = 2
RBUI_CENTER = 3
RBUI_TOP = 4
RBUI_BOTTOM = 5
RBUI_AUTO = 20

class RBUIComponent extends RBEntity #base class
    constructor: (X, Y, Width=RBUI_AUTO, Height=RBUI_AUTO) ->
        super(X, Y, Width, Height)
        @margin = [0,0,0,0] #top, right, bottom, left
        @padding = [0,0,0,0] #top, right, bottom, left
        @top = 0
        @bottom = 0
        @left = 0
        @right = 0
        @valign = RBUI_LEFT
        @halign = RBUI_TOP
        @zdepth = 0
        @_RB.add_ui_object(@)
        @eventDelegates = {'on_click':[]}
        @mouse_is_over = false

    hide: () =>

    on_hover: () =>

    #this function is just for checking if a click was on this component
    _pre_on_click: (args) =>
        m = @_RB.input.mouse
        mrect = new RBRect(m.x, m.y, 2, 2)
        if @.overlaps(mrect)
            @.on_click(args)
    _pre_on_touch: (args) =>
        $("#testlog").append("x: #{args.targetTouches[0].pageX}, y: #{args.targetTouches[0].pageY}")
    _pre_on_mm: (args) =>
        m = @_RB.input.mouse
        mrect = new RBRect(m.x, m.y, 2, 2)
        if @.overlaps(mrect)
            console.log("Mouse over!")
            if not @mouse_is_over
                @.on_mouse_enter(args)
                @mouse_is_over = true
        else if @mouse_is_over
            @.on_mouse_leave(args)
            @mouse_is_over = false
        

    on_click: (args) =>

    on_drag: () =>

    on_blur: () =>

    on_mouse_enter: (args) =>
    on_mouse_leave: (args) =>

    destroy: () =>
        @_RB.remove_ui_object(@)
        super()


#class RBFrame extends RBUIComponent #frame is just a focussable rect, for layout

#class RBWindow extends RBFrame  #window has a frame but also some controls, is draggable

class RBButton extends RBUIComponent
    constructor: (btnText = "Button Text", X, Y, Width=RBUI_AUTO, Height=RBUI_AUTO) ->
        super(X, Y, Width, Height)
        @txt = new RBText(0, 0, btnText, "arial", 10)
        @txt.set_colour(200, 200, 200)
        @.set_colour(0,0,0)
        @.add_child(@txt)
        @_RB.input.bind(RB_MOUSE_UP, @._pre_on_click, consume=true)
        @_RB.input.bind(RB_TOUCH_START, @._pre_on_touch, consume=true)
        @_RB.input.bind(RB_MOUSE_MOVE, @._pre_on_mm, consume=true)
    

    destroy: () ->
        @_RB.input.unbind(RB_MOUSE_UP, @._pre_on_click, consume=true)
        @_RB.input.unbind(RB_TOUCH_START, @._pre_on_touch, consume=true)
        @_RB.input.unbind(RB_MOUSE_MOVE, @._pre_on_mm, consume=true)

    # Set the text for the button
    set_text: (newtext) ->
        @txt.set_text(newtext)

    # Set the font for the button
    # @param [String] fontname The name of the font to use
    # @param [Number] size The size of the font to use
    set_text_font: (fontname, size=10) ->
        @txt.set_font(fontname, size)
    
    # Set the colour of the text
    # r, g, b, a - (0-255). a defaults to 255
    set_text_colour: (r, g, b, a=255) ->
        @txt.set_colour(r, g, b, a)

    on_click: (args) =>
        super(args)
        #console.log("clicked!: args=" + args)
        onclickdelegates = @eventDelegates['on_click']
        for delegate in onclickdelegates
            delegate(args)

    on_mouse_enter: (args) =>
        super(args)
        if @_RB.isDebug()
            @.set_colour(255, 0, 0)
    on_mouse_leave: (args) =>
        super(args)
        if @_RB.isDebug()
            @.set_colour(0,0,0)


    add_on_click: (fn) =>
        @eventDelegates['on_click'].push(fn)

    remove_on_click: (fn) =>
        index = @eventDelegates['on_click'].indexOf(fn)
        if index >= 0
            console.log("removing onclick delegate")
            @eventDelegates['on_click'].splice(index, 1)

    pre_render: () ->
        super()
        p = @.xy_to_topleft()
        @_RB.cctx.strokeStyle = @._colour.htmlrgba
        @_RB.cctx.strokeRect(p.x, p.y, @width, @height)
#class RBTextArea extends RBUIComponent
#
class RBImageButton extends RBButton
    constructor: (buttontext, X, Y, W, H, upimagename, downimagename, hoverimagename) ->
        super(buttontext, X, Y, W, H)
        @upimage = null
        @downimage = null
        @hoverimage = null
        
        #@upimage = new RBSprite(0, 0, "data/button_up.png", 50, 50)
        #@.add_child(@upimage)
        if upimagename?
            @upimage = new RBSprite(0, 0, upimagename, @.width, @.height)
            @.add_child(@upimage)
            #@upimage.hide()
        if downimagename?
            @downimage = new RBSprite(0, 0, downimagename, @.width, @.height)
            @downimage.hide()
            @.add_child(@downimage)
        if hoverimagename?
            @hoverimage = new RBSprite(0, 0, hoverimagename, @.width, @.height)
            @hoverimage.hide()
            @.add_child(@hoverimage)
    on_mouse_enter: (args) ->
        super(args)
        if @hoverimage isnt null
            @hoverimage.show()
        if @upimage isnt null
            @upimage.hide()
    on_mouse_leave: (args) ->
        super(args)
        if @hoverimage isnt null
            @hoverimage.hide()
        if @upimage isnt null
            @upimage.show()
    on_click: (args) ->
        super(args)
        if @downimage isnt null
            @downimage.show()
        if @upimage isnt null
            @upimage.hide()
        if @hoverimage isnt null
            @hoverimage.hide()
