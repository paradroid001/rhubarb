# Text entities.
# RBText has an anchor in it's centre by default.
class RBText extends RBEntity
    constructor:(X, Y, @text="Test", @fontname="arial", @size=10, @maxWidth=null, @isStroke=false, @isFill=true, isstatic = true) ->
        super(X, Y)
        @static = isstatic
        @.set_font(@fontname, @size)
        @.set_colour(0,0,0)
        @.set_text(@text)
        @strokeWidth = 1
        @strokeColour = new RBColour([0,40,0,255])
        @isShadow = false
        @_shadowOffsetX = 0
        @_shadowOffsetY = 0
        @_shadowBlur = 0
        @_shadowColour = new RBColour([0, 0, 0, 255])
    
    # Whether or not the text should fill (on by default)
    # set_colour() determines fill colour
    set_fill: (fill=true) ->
        @isFill = fill
    
    # Whether or not the text should stroke. (off by default)
    # set_stroke_colour() determines stroke colour
    set_stroke: (stroke=true) ->
        @isStroke = stroke

    # Sets the stroke colour for a text object
    set_stroke_colour: (r, g, b, a=255) ->
        @strokeColour.set_rgba([r, g, b, a])

    set_stroke_width: (val) ->
        if val >= 0
            @strokeWidth = val

    # Set shadow params. Offset x, offset y, colour=[R, G, B, A], blur
    set_shadow: (offx, offy, colour=[0, 0, 0, 255], blur=0) ->
        if offx is false
            @isShadow = false
        else if (offx? and offy?)
            @isShadow = true
            @_shadowOffsetX = offx
            @_shadowOffsetY = offy
            @_shadowBlur = blur
            @_shadowColour.set_rgba(colour)
        else
            @isShadow = false

    # Sets the font for a text object
    set_font:(@fontname, @size) ->
        @cssfont = "#{@size}px #{@fontname}"
        @_lines = []
        @.refresh()

    set_size:(@size) ->
        @.set_font(@fontname, @size)
    
    refresh: () ->
        @height = @size
        super()

    # Sets the text for a text object
    set_text:(text) ->
        @text = text
        @_RB.cctx.save()
        @_RB.cctx.font=@cssfont
        @width = @_RB.cctx.measureText(@text).width
        @_RB.cctx.restore()
        @.refresh()
        
    #wrap_text: () ->
    #    words = @text.split(' ')
    #    line = ''
    #    @lines = []
    #    for n in [0..words.length]
    #        testLine = line + words[n] + ' '
    #        metrics = context.measureText(testLine)
    #        testWidth = metrics.width
    #        if (testWidth > maxWidth && n > 0)
    #            context.fillText(line, x, y)
    #            line = words[n] + ' '
    #            #y += lineHeight
    #      
    #        else 
    #            line = testLine
    #      
    #    
    #    context.fillText(line, x, y);


    append_text:(text) ->
        @.set_text(@text + text)

    draw:() ->
        #@.pre_render()
        @_RB.cctx.font=@cssfont
        totopleft = @.xy_to_topleft()
        totopleft.y += @height
        if @isFill
            if @isShadow
                @_RB.cctx.save()
                @_RB.cctx.shadowOffsetX = @_shadowOffsetX
                @_RB.cctx.shadowOffsetY = @_shadowOffsetY
                @_RB.cctx.shadowBlur = @_shadowBlur
                @_RB.cctx.shadowColor = @_shadowColour.htmlrgba
            @_RB.cctx.fillStyle = @_colour.htmlrgb
            #@_RB.cctx.fillText(@text, 0, 0) #TODO: maxwidth
            @_RB.cctx.fillText(@text, totopleft.x|0, totopleft.y|0) #TODO: maxwidth
            if @isShadow
                @_RB.cctx.restore()

        if @isStroke #or @isShadow
            @_RB.cctx.strokeStyle = @strokeColour.htmlrgb
            @_RB.cctx.lineWidth = @strokeWidth
            @_RB.cctx.strokeText(@text, totopleft.x|0, totopleft.y|0) #TODO: maxwidth
        #@.post_render()
