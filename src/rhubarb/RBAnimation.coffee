#Animation frameset.
 # Pass in the width and height of your animation frame, and 
 # the list of indexes in your image that make up the 
 # animation. You can also pass in a frametime,
 # Playtype: string, values can be "Normal", "Loop", "Pingpong" (pingpong TODO)
 # Pingpong: bool, whether self animation should loop alternately backwards
 # 
 # Internally we have:
 # currentFrame: which frame of animation we are at
 # totalFrames: how many frames in the animation list
 # currentImageIndex: the value at the currentFrame
 # isFinished: if we have finished playing the animation 
 #              (false while loop or pingpong is true)
 # sx, sy: The current x and y offsets needed to draw self slice of image
 #

class RBAnimation
    #constructor: (@xframes=1, @yframes=1, @width, @height, framelist, @xoffset=0, @yoffset=0, @framedt=100, playtype = "Normal") ->
    constructor: (@spritesheet, framelist, playtype = RB_ANIM_NORMAL, @framedt=100, @xoffset=0, @yoffset=0) ->
        @_frames = framelist
        @isLoop = @isPingPong = @isNormal = false
        @currentFrame = 0
        @totalFrames = 0
        @currentImageIndex = 0
        @isFinished = false
        @animationTimer = 0
        @sx = 0
        @sy = 0
        @currentFrameRect = new RBRect(0, 0, 0, 0)
        @on_finished = new RBDelegateGroup()
        #looping options
        if playtype is RB_ANIM_LOOP
            @isLoop = true
        #not yet supporting ping pong
        else if playtype is RB_ANIM_NORMAL
            @isNormal = true

        @totalFrames = @_frames.length
        @currentImageIndex = @_frames[@currentFrame]
        if @spritesheet != null
            #console.log("anim (#{framelist}) initial framerect (frame #{@currentImageIndex}): ")
            #console.log(@currentFrameRect)
            #console.log(@spritesheet)
            @currentFrameRect = @spritesheet.frame_at(@currentImageIndex, @currentFrameRect, @xoffset, @yoffset)
        #else
        #    console.log("spritesheet was null in constructor of anim")

    loop: () ->
        @isLoop = true
        @isNormal = false
    normal: () ->
        @isNormal = true
        @isLoop = false

    reset: () ->
        @isFinished = false
        @currentFrame = 0

    next: (dt) ->
        if not @isFinished
            @animationTimer += dt
            if @animationTimer >= @framedt
                if @currentFrame < @totalFrames-1
                    @animationTimer = 0
                    @currentFrame++
                else if @isLoop
                    @animationTimer = 0
                    @currentFrame = 0
                else
                    @isFinished = true
                    @currentFrame = @totalFrames-1 #stay on last frame
                    @on_finished.call_delegates()
                @currentImageIndex = @_frames[@currentFrame]
                if @spritesheet != null
                    @currentFrameRect = @spritesheet.frame_at(@currentImageIndex, @currentFrameRect, @xoffset, @yoffset)
                #else
                #    console.log("Spritesheet was null!")
        @ #return this


