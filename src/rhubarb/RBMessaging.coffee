_SUBSCRIBERS = {}

class RBMessage
    constructor:( @key, @args ) ->
    
RBBroadcast = (key, args...) ->
    #m = new RBMessage(key, args)
    if _SUBSCRIBERS[key]?
        for item in _SUBSCRIBERS[key]
            item(args)

###*
    subscriber should just be a callback function
    that will know what to do with ARGS
###
RBSubscribeMessage = (key, subscriber) ->
    if not _SUBSCRIBERS[key]?
        _SUBSCRIBERS[key] = []
    _SUBSCRIBERS[key].push(subscriber)
