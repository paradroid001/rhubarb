class RBLog
    LOG=1
    DEBUG=2
    WARN=3
    ERROR=4
    constructor: (@_useconsole=true) ->
        @_enabled = true
        @linenum = 0
        @outer = $('<div style="display:block; position: absolute; width:100%; bottom:0px;"></div>')
        @header = $('<div style="border-top:1px solid #6f6f6f; border-bottom: 1px solid #6f6f6f;  background-color: #afafaf; -moz-box-shadow:0 1px 3px rgba(0,0,0,0.5); -webkit-box-shadow: 0 1px 3px rgba(0,0,0,0.5); text-shadow: 0 -1px 1px rgba(0,0,0,0.25);"><div style="color: white; padding-left:7px; font-weight:bold;">LOGGING</div></div>')
        @le = $('<div style="display:block; background-color: #efefef; height:100px; overflow:scroll; padding-left:7px; padding-top:3px;"></div>')
        @header.click( (e) =>
            if @le.is(":hidden")
                @le.show()
            else
                @le.hide() )

        @outer.append(@header)
        @outer.append(@le)

        if !console?
            @_useconsole = false

    attach: () ->
        $("body").append(@outer)

    enable: () ->
        @_enabled = true
    disable: () ->
        @_enabled = false


    log: (message) ->
        @.out(message, LOG)
    debug: (message) ->
        @.out(message, DEBUG)
    warn: (message) ->
        @.out(message, WARN)
    error: (message) ->
        @.out(message, ERROR)

    out: (message, msgtype=LOG) ->
        if @_enabled
            if @_useconsole and console?
                switch msgtype
                    when LOG then console.log(message)
                    when DEBUG then console.debug(message)
                    when WARN then console.warn(message)
                    when ERROR then console.error(message)
            else
                @le.append(@linenum++ + ": " + message+'<br />')

###
See below for reasonably awesome logger that keeps line numbers.
See heere: http://stackoverflow.com/questions/11308239/console-log-wrapper-that-keeps-line-numbers-and-supports-most-methods


                _log = (function (methods, undefined) {

    var Log = Error; // does this do anything?  proper inheritance...?
    Log.prototype.write = function (args, method) {
        /// <summary>
        /// Paulirish-like console.log wrapper.  Includes stack trace via @fredrik SO suggestion (see remarks for sources).
        /// </summary>
        /// <param name="args" type="Array">list of details to log, as provided by `arguments`</param>
        /// <param name="method" type="string">the console method to use:  debug, log, warn, info, error</param>
        /// <remarks>Includes line numbers by calling Error object -- see
        /// * http://paulirish.com/2009/log-a-lightweight-wrapper-for-consolelog/
        /// * http://stackoverflow.com/questions/13815640/a-proper-wrapper-for-console-log-with-correct-line-number
        /// * http://stackoverflow.com/a/3806596/1037948
        /// </remarks>

        // via @fredrik SO trace suggestion; wrapping in special construct so it stands out
        var suffix = {
            "@": (this.lineNumber
                    ? this.fileName + ':' + this.lineNumber + ":1" // add arbitrary column value for chrome linking
                    : extractLineNumberFromStack(this.stack)
            )
        };

        args = args.concat([suffix]);
        // via @paulirish console wrapper
        if (console && console[method]) {
            if (console[method].apply) { console[method].apply(console, args); } else { console[method](args); } // nicer display in some browsers
        }
    };
    var extractLineNumberFromStack = function (stack) {
        /// <summary>
        /// Get the line/filename detail from a Webkit stack trace.  See http://stackoverflow.com/a/3806596/1037948
        /// </summary>
        /// <param name="stack" type="String">the stack string</param>

        // correct line number according to how Log().write implemented
        var line = stack.split('\n')[3];
        // fix for various display text
        line = (line.indexOf(' (') >= 0
            ? line.split(' (')[1].substring(0, line.length - 1)
            : line.split('at ')[1]
            );
        return line;
    };

    // method builder
    var logMethod = function(method) {
        return function (params) {
            /// <summary>
            /// Paulirish-like console.log wrapper
            /// </summary>
            /// <param name="params" type="[...]">list your logging parameters</param>

            // only if explicitly true somewhere
            if (typeof DEBUGMODE === typeof undefined || !DEBUGMODE) return;

            // call handler extension which provides stack trace
            Log().write(Array.prototype.slice.call(arguments, 0), method); // turn into proper array & declare method to use
        };//--  fn  logMethod
    };
    var result = logMethod('log'); // base for backwards compatibility, simplicity
    // add some extra juice
    for(var i in methods) result[methods[i]] = logMethod(methods[i]);

    return result; // expose
})(['error', 'debug', 'info', 'warn']);//--- _log

###
# Constants for RB
#RB_VERSION_MAJOR = 1
#RB_VERSION_MINOR = 0
#RB_VERSION_REV = 7
RB_VERSION = "2.0.1"

#Constants for RBVars
RBV_GAME_STR = "game"
RBV_IMG_STR = "images"
RBV_SND_STR = "sounds"
RBV_MUSIC_STR = "music"
RBV_SS_STR = "spritesheets"
RBV_ENG_STR = "engine"

# The Colour Red
RB_RED =    [255,0,0]
# The Colour Green
RB_GREEN =  [0,255,0]
# The Colour Blue
RB_BLUE =   [0,0,255]
# The Colour Yellow
RB_YELLOW = [0,255,255]
# The Colour Purple
RB_PURPLE = [255,0,255]
# The Colour Grey
RB_GREY =   [128,128,128]
# The Colour White
RB_WHITE =   [255,255,255]

# The colour used for HIT TOP in RBDebug
RB_DEBUG_HIT_TOP = [0, 128, 255]
# The colour used for HIT TOP in RBDebug
RB_DEBUG_HIT_BOTTOM = [255, 128, 0]
# The colour used for HIT  in RBDebug
RB_DEBUG_HIT_RIGHT = [255, 128, 255]
# The colour used for HIT LEFT in RBDebug
RB_DEBUG_HIT_LEFT = [128, 255, 128]
# The colour used for HIT NONE in RBDebug
RB_DEBUG_HIT_NONE = RB_GREY

RB_COLLIDE_NONE = 0
RB_COLLIDE_TOP = 1
RB_COLLIDE_BOTTOM = 2
RB_COLLIDE_LEFT = 4
RB_COLLIDE_RIGHT = 8

# Event name for key down
RB_KEY_DOWN = "keydown"
# Event name for key down
RB_KEY_UP = "keyup"
# Event name for key down
RB_MOUSE_MOVE = "mousemove"
# Event name for key down
RB_MOUSE_DOWN = "mousedown"
# Event name for key down
RB_MOUSE_UP = "mouseup"
# Event name for double click
RB_MOUSE_DOUBLECLICK = "dblclick"
# Event name for mouse wheel
RB_MOUSE_WHEEL = "mousewheel"
# Event name for key down
RB_TOUCH_START = "touchstart"
# Event name for key down
RB_TOUCH_END = "touchend"
# Event name for key down
RB_TOUCH_MOVE = "touchmove"
# Event name for key down
RB_TOUCH_CANCEL = "touchcancel"

#RBScene.enter, .exit, .eclipse, .reveal
RB_EVENT_ON_SCENE_ENTER = "on_enter"
RB_EVENT_ON_SCENE_EXIT = "on_exit"
RB_EVENT_ON_SCENE_ECLIPSE = "on_eclipse"
RB_EVENT_ON_SCENE_REVEAL = "on_reveal"
#RB.start() and RB.stop()
RB_EVENT_ON_START = "on_start"
RB_EVENT_ON_STOP = "on_stop"
RB_EVENT_ON_PAUSE = "on_pause"
RB_EVENT_ON_UNPAUSE= "on_unpause"

RB_RENDER_MODE_NORMAL = "none"
RB_RENDER_MODE_MULTIPLY = "lighter"

RB_ANIM_LOOP = "Loop"
RB_ANIM_NORMAL = "Normal"

RB_DEFAULT_SOUND_CACHE_SIZE = 3 #Every sfx loaded 3 times
####### UTILS####################

# Random Numbers
# --------------

random_float = (min, max) ->
    max - (Math.random() * (max-min))

random_int = (min, max) ->
    max - Math.floor(Math.random()*(max-min+1))

# Chance is a number between 0 and 1
random_chance = (chance) ->
    num = Math.random()
    if num >= (1.0-chance)
        return true
    return false

random_item = (list) ->
    r = random_int(0, list.length-1)
    return list[r]

clamp_num = (num, min, max) ->
    if num < min
        num = min
    if num > max
        num = max
    return num

#util function to work out where our canvas is
#Based on: http://www.quirksmode.org/js/findpos.html
get_cumulative_offset = (obj) ->
    left = top = 0
    if obj.offsetParent
        left += obj.offsetLeft
        top += obj.offsetTop

    retval =
        x: left
        y: top

array_contains = (arr, obj) ->
    for item in arr
        if item is obj
            return true
    false

print_array = (arr) ->
    for item in arr
        console.log(item)

int_to_hex = (i) ->  
    hex = parseInt(i).toString(16)  
    if hex.length < 2 
        return "0" + hex
    else hex


hex_to_hex = (hex) ->
    return parseInt("0x" + hex)


rgb_to_hex =(r, g, b) ->
    return ("0x" + int_to_hex(r)+int_to_hex(g)+int_to_hex(b))
rgb_to_html =(r, g, b) ->
    return ("#" + int_to_hex(r)+int_to_hex(g)+int_to_hex(b))
rgba_to_html = (r, g, b, a) ->
    return "rgba(#{r}, #{g}, #{b}, #{a/255})"

class RBColour
    constructor: (colour = [0,0,0,255]) ->
        @.set_rgba(colour)

    set_alpha: (alpha=255) ->
        @a = alpha
        @.set_rgba() #refresh

    set_rgb:(col) ->
        @.set_rgba([col[0],col[1],col[2],@a])
    #calling without value is like doing a refresh, setting nothing
    set_rgba:(colour) ->
        if colour?
            @r = colour[0]
            @g = colour[1]
            @b = colour[2]
            if colour[3]?
                @a = colour[3]
            else
                @a =255
        @rgba = [@r,@g,@b,@a]
        _r = clamp_num(@r, 0, 255)|0
        _g = clamp_num(@g, 0, 255)|0
        _b = clamp_num(@b, 0, 255)|0
        _a = clamp_num(@a, 0, 255)|0
        @htmlrgb = rgb_to_html(_r,_g,_b)
        @htmlrgba = rgba_to_html(_r, _g, _b, _a)
# RBTimer is a completely independent timer.
# You should use this when you need a timer which is not reliant
# on the update of the main game, and RB(), because this is 
# independant of that.
# But.
# It is expensive.
# Every time you want to check it, it has to create date objects
# and subtract them etc.
# 
# If you want to use lots of little timers to keep track of things,
# try a RBIntervalTimer instead.
#
# Also, RBTimer has no idea how long it has been running. It only knows
# how long it has been since someone last checked.
#
class RBTimer
    constructor: () ->
        @_dt = 0
        @_totalelapsed = 0
        @_previousvalue = null
        @_currentvalue = null
        @.reset()

    # Resets the timer.
    # Calling elapsed immediately after this would return 0
    #
    reset: () ->
        @_previousvalue = new Date()
        @_currentvalue = new Date()
    
    # Just to avoid any misunderstanding, 
    # elapsed() returns the number of milliseconds that have elapsed
    # since the last call to elapsed. It is not the total
    # time since the timer was started
    #
    elapsed: () ->
        d = new Date()
        @_currentvalue.setTime(d.getTime() - @_previousvalue.getTime() )
        @_dt = @_currentvalue.getMilliseconds()
        @_previousvalue = d
        #ret
        @_dt

#this class was a nice idea - a cheap class offset against a real RBTimer.
#in practice it doesn't really work. You can't use it to time anything
#that takes less than a frame (or whatever your parent timers resolution is).
#you could only time things that begin in one frame and end in the next, giving the
#parent timer time to be updated in the meantime.
# ahh well.
###
class RBSimpleTimer
    constructor: (@parent_timer = RB()._timer) ->
        @_internaltime = new Date()
        @_parentoffset = 0
        @_dt = 0
    reset: () ->
        @_parentoffset = @parent_timer._previousvalue.getTime()
        @_dt = 0
    elapsed: () ->
        @_internaltime.setTime( @parent_timer._previousvalue.getTime() - @_parentoffset )
        #@_dt = @_elapsed.getMilliseconds()
        @.reset()
        @_dt = @_internaltime.getMilliseconds()
        @_dt
###
    

# RBInterval timer is useful for when you want to create
# a timer with a timeout, which you can test whenever you like
# to find out if your 'interval' has happened.
# The timer starts when you create or reset it.
class RBIntervalTimer extends RBTimer
    # @param [Number] desired_interval The timeout value you'd like to use
    #   for this timer.
    constructor: (desired_interval) ->
        super()
        @_interval = 0
        @_interval_elapsed = 0
        @_has_interval_expired = false
        @.set_interval(desired_interval)

    # Sets the interval for this timer, and resets it.
    # @param [Number] interval The new interval
    set_interval: (interval) ->
        @_interval = interval
        @.reset()

    # Gets the number of milliseconds since this timer
    # was last reset
    get_elapsed: () ->
        @_interval_elapsed

    # Resets the timer so that it has an elapsed time of zero,
    # and clears the expired flag
    reset: () ->
        @_has_interval_expired = false
        @_interval_elapsed = 0
        super() #super reset
        
    # Returns true if the interval has expired, false if not.
    has_interval_expired: () ->
        if (@_has_interval_expired)
            return true
        else
            @_interval_elapsed += @.elapsed()
            if (@_interval_elapsed > @_interval)
                @_has_interval_expired = true
            else
                @_has_interval_expired = false
        @_has_interval_expired
   
# A class representing a point in 2D space.
# It has an x and a y property.
class RBPoint
    # @property [Number] The x position
    x: 0
    # @property [Number] The y position
    y: 0
    
    # Construct a new point
    # @param [Number] x The x position of the point
    # @param [Number] y The y position of the point
    constructor: (@x=0, @y=0) ->

    # @private
    uispec: () ->
        ret = {}
        ret['X'] = ['x', 'x', 'number']
        ret['Y'] = ['y', 'y', 'number']
        return ret

    # Add another point's x/y to this point
    add: (point) ->
        @x += point.x
        @y += point.y
        return @

    # Subtract another point's x/y from this point
    sub: (point) ->
        @x -= point.x
        @y -= point.y
        return @
    
    # Multiply x and y by a constant
    mult: (n) ->
        @x *= n
        @y *= n
        return @

    # Set this point's x and y to another point's x and y
    set: (point) ->
        @x = point.x
        @y = point.y
        return @

    # Returns the distance to another point
    dist: (point) ->
        Math.sqrt(this.distsq(point))
    
    # Returns the square of the distance to another point
    distsq: (point) ->
        a = @x - point.x
        b = @y - point.y
        (a*a) + (b*b)
    
    # Finds the angle between this point and another, from
    # the 'up' Y axis (treating this as 0 degrees).
    angle_to: (point) ->
        dx = point.x - @.x
        dy = point.y - @.y
        return Math.atan2(dy,dx) * 180 / Math.PI

    # Sets this point to have x and y values
    # at a 'magnitude' distance and angle in degrees.
    cartesian: (angle, magnitude) ->
        angle = angle % 360
        if angle < 0
            angle = 360 + angle
        rads = angle * Math.PI / 180.0
        @x = Math.sin(rads) * magnitude
        @y = -Math.cos(rads) * magnitude


# Represents a rectangle, with x, y, width, and height.
# Also has top, bottom, left, and right.
# Rects have an anchor point which is where their x and y 
# position is considered to 'pin' them.
# 
class RBRect extends RBPoint
    # @property [Number] The object width
    width: 1
    # @property [Number] The object height
    height: 1
    # @property [Number] The top 'y' value of the object
    top: 0
    # @property [Number] The bottom 'y' value of the object
    bottom: 0
    # @property [Number] The left 'x' value of the object
    left: 0
    # @property [Number] The right 'x' value of the object
    right: 0


    constructor: (X=0, Y=0, @width=1, @height=1) ->
        super(X, Y)
        @_XYtoTL = new RBPoint() #used for storage of current offset from anchor to top left.
        @_anchor = new RBPoint()
        @_scale = new RBPoint(1.0, 1.0)
        @.set_scale(1.0, 1.0)
        @.set_anchor()
        @_colour = new RBColour([255,255,255,255])
        @.refresh()

    clone_rect: () ->
        r = new Rect(@x, @y, @width, @height)
        r.set_anchor(@_anchor.x, @_anchor.y)
        r.set_scale(@_scale.x, @_scale.y)
        return r

    # @private
    get_screen_XY: () ->
        p = new RBPoint(@x, @y)
        p.sub(@_RB._screenRect)
        return p
        #return new RBPoint(@x, @y) #TODO obviously wrong

    get_abs_rect: () ->
        return @

    # @private
    uispec: () ->
        ret = super()
        ret['Anchor'] = ['_anchor', 'set_anchor', RBPoint]
        ret['Scale'] = ['_scale', 'set_scale', RBPoint]
        ret['Colour'] = ['_colour', 'set_colour', RBColour]
        ret['Width'] = ['width', null, 'number']
        ret['Height'] = ['height', null, 'number']
        ret['Top'] = ['top', null, 'number']
        ret['Bottom'] = ['bottom', null, 'number']
        ret['Left'] = ['left', null, 'number']
        ret['Right'] = ['right', null, 'number']
        return ret



    # set the anchor point (origin) for this object
    set_anchor: (a_x = 0.5, a_y = 0.5) ->
        @_anchor.x = a_x
        @_anchor.y = a_y
        @.refresh()

    # Set the scale of this object
    set_scale:(scalex, scaley) ->
        @_scale.x = scalex
        @_scale.y = scaley
        #@height = @unscaled_height# * @_scale.y
        #@width = @unscaled_width# * @_scale.x
        #@.refresh()
    get_scale: () ->
        @_scale
        
    # Set the colour of this object - r, g, b, optionally a. (0-255)
    set_colour:(r=0, g=0, b=0, a=255) -> #colour = [0,0,0,255]) ->
        @_colour.set_rgba([r, g, b, a])

    # Set the alpha / opacity of this object (0-255)
    set_alpha: (a=255) ->
        @_colour.set_alpha(a)

    # Set the width of this object
    set_width: (@width) ->
        @.refresh()
    # Set the height of this object
    set_height: (@height) ->
        @.refresh()

    # @private
    refresh: () ->
        @top = @y - (@height * @_anchor.y)
        @left = @x - (@width * @_anchor.x)
        @bottom = @top + @height #@y + @height * (1.0-@_anchor.y)
        @right = @left + @width #@x + @width * (1.0-@_anchor.x)
        @_XYtoTL = @.xy_to_topleft(@_XYtoTL)
        @


    # Move this object so its bottom border aligns with passed value
    adjust_bottom: (newbottom) ->
        @y = newbottom - (@bottom-@y)
        @.refresh()
    # Move this object so its top border aligns with passed value
    adjust_top: (newtop) ->
        @y = newtop + (@y-@top)
        @.refresh()
    # Move this object so its left border aligns with passed value
    adjust_left :(newleft) ->
        @x = newleft + (@x-@left)
        @.refresh()
    # Move this object so its right border aligns with passed value
    adjust_right: (newright) ->
        @x = newright - (@right-@x)
        @.refresh()

    # Set position to newx, newy
    set_position: (newx, newy) ->
        @x = newx
        @y = newy
        @.refresh()
    # Move by dx, dy
    move_by: (dx, dy) ->
        @x +=dx
        @y +=dy
        @.refresh()

    # an important function to work out how far our xy pos is from the top left
    # useful for canvas rendering, since it uses top left
    # Pass in a point to modify it, otherwise a new point is generated.
    xy_to_topleft: (p) ->
        ret = p
        if not ret?
            ret = new RBPoint(0,0)
        ret.x = @left - @x
        ret.y = @top - @y
        return ret
    
    # find the distance to the middle of the object. Useful if you want to
    # rotate an object by its middle, or flip the canvas
    xy_to_middle: () ->
        return new RBPoint((@right-@left)/2 - (@x-@left), (@bottom-@top)/2 - (@y-@top) )

    # Recursively check a list of entities and their children
    # for the first overlapping entity.
    # @param [Array<RBEntity>] rlist
    #   An array of RBEntities.
    # @param [RBRect] crect
    #   Optional rect structure to populate with collision boundaries.
    overlaps_in_rlist: (rlist, crect) ->
        retval = null
        myrect = @ #@.get_abs_rect()
        for obj in rlist
            #if myrect.overlaps(obj.get_abs_rect(), crect)
            if myrect.overlaps(obj, crect)
                retval = obj
            else if obj.members?
                retval = @.overlaps_in_rlist(obj.members, crect)
            if retval != null
                return retval
        return null

    # Returns true when overlaps.
    # Optionally pass in a collision rect,
    # Which is populated if the rects collide
    # @param [RBRect] rect
    #   The rect to check for collisions with
    # @param [RBRect] crect
    #   An optional rect to populate with the boundaries
    #   of the collision.
    overlaps: (rect, crect) ->
        #track the outside boundaries
        leftmost = rightmost = topmost = bottommost = null
        #track the inside boundaries
        nleftmost = nrightmost = ntopmost = nbottommost = null
        
        #rect = null
        #If this is true, 
        #if RB().collisionFlagAbsolute
        #    rect = orect.clone_rect()
        #    p = rect.get_screen_XY()
        #    rect.set_position(p.x, p.y)
        #else
        #    rect = orect

        if @left <= rect.left
            leftmost = @
            nleftmost = rect
        else
            leftmost = rect
            nleftmost = @

        if @right >= rect.right
            rightmost = @
            nrightmost = rect
        else
            rightmost = rect
            nrightmost = @

        if @top <= rect.top
            topmost = @
            ntopmost = rect
        else
            topmost = rect
            ntopmost = @
        
        if @bottom >= rect.bottom
            bottommost = @
            nbottommost = rect
        else
            bottommost = rect
            nbottommost= @

        #calculate a collision rect which is a function of ourself.
        #So if we were entirely within the colliding rect, our
        #collision rect would have l, r, t, b equal to our own.
        if (Math.abs(rightmost.right - leftmost.left) < (leftmost.width + rightmost.width)) and (Math.abs(bottommost.bottom - topmost.top) < (bottommost.height + topmost.height) )
            if crect?
                #console.log(nleftmost)
                #console.log(nrightmost)
                crect.x = nleftmost.left - @.left
                crect.y = ntopmost.top - @.top
                crect.width = nrightmost.right - nleftmost.left
                crect.height = nbottommost.bottom - ntopmost.top
                #console.log("#{crect.x}, #{crect.y}, #{crect.width}, #{crect.height}")
                crect.set_anchor(0, 0)
            return true
        else
            return false

    # Draw the border of this rectangle, in the 'current colour'
    draw:() ->
        @_RB.cctx.strokeRect(@left, @top, @width, @height)
        #@_RB.cctx.strokeRect(0, 0, @width, @height)

#
# RBObject

class RBObject extends RBRect
    # @property The id of the object
    id: -1
    constructor:(X, Y, Width, Height) ->
        super(X, Y, Width, Height)
        @_RB = RB()
        @id = @_RB.newID()

#requires RBBase.coffee

# RBGroup
# -------
# Add RBObjects to groups. Can only be a member of the group once.
# Don't try to add non-RBObjects RBGroups.
# Objects are retrievable by ID, and removeable by object and by ID
# You can loop over the children by using a 'for obj in <group>.members
# You can add groups to groups
# You can't add groups to themselves.
# You can't add the same object to the group twice
# Group members are stored sequentially in a list, so order is preserved
#
# RBGroup is meant to be kind of abstract (not in the OO sense,
# in the 'it is a virtual concept rather than a physical thing' sense).
# It kind of sucks that it inherits from RBObject (so you can put
# RBGroups within RBGroups), and that means it gets X,Y,Width,Height.
# BUT, it makes sense for RBEntities to 'be' groups (so they can
# contain other entities), and therefore the XYWH thing is probably
# not so bad.
class RBGroup extends RBObject
    constructor: (@name = "NoName", X, Y, Width, Height, id=null) ->
        super(X, Y, Width, Height)
        if id? and id != null
            @id = id
            #RB().log(id)
        @members = []
        @_idmap = {} #a map from objectid to list indice
        @count = 0
        @.empty()
    # Returns id of the object if added, otherwise null
    # The position is the index we want to splice into
    # This isn't important externally, but is used by 
    # functions that want to add members before or 
    # after other objects. So the absolute indexes
    # aren't important, but the relative positions are.
    add_member: (obj, position=-1) ->
        #console.log("Addmember called")
        ret = null
        if not (obj is @)
            #console.log("obj not this")
            if obj.id? #does the obj have an id?
                if @_idmap[obj.id]?
                    ret = obj.id #already existed
                else
                    @_idmap[obj.id] = obj
                    if position == -1
                        @members.push(obj)
                    else
                        @members.splice(position, 0, obj)
                    ret = obj.id
                    @count++
            else
                #no id present
                #this is ok - we add an id
                obj.id = @_RB.newID() #@members.length
                @_idmap[obj.id] = obj.id
                if position == -1
                    @members.push(obj)
                else
                    @members.splice(position, 0, obj)
                ret = obj.id
                @count++
        #else
        #    #console.log("obj was this??")
        #    return null
    
        return ret

    add_before: (obj, existingobj) ->
        existingindex = @members.indexOf(existingobj)
        if existingindex >= 0
            @.add_member(obj, existingindex)
    add_after: (obj, existingobj) ->
        existingindex = @members.indexOf(existingobj)
        if existingindex >= 0
            @.add_member(obj, existingindex+1)
    get_before:(obj) ->
        ret = null
        eindex = @members.indexOf(obj)
        if eindex >=1
            ret = @members[eindex-1]
        return ret
    get_after: (obj) ->
        ret = null
        eindex = @members.indexOf(obj)
        if eindex >=0 and eindex < @members.length-1
            ret = @members[eindex+1]
        return ret

    is_member: (obj) ->
        if @members.indexOf(obj) >= 0
            return true
        return false

    get_member_ids:() ->
        return (id for id of @_idmap) #return the 'keys'
    
    get_member_by_id: (id) ->
        ret = null
        if @_idmap[id]?
            ret = @_idmap[id]
        return ret

    remove_member_by_id: (id) ->
        if @_idmap[id]?
            obj = @_idmap[id]
            #TODO: This won't work on ie 7, 8: need to define if so.
            index = @members.indexOf(obj)
            if index >= 0
                #console.log("Group #{@name}: Removing member with id #{id} at index #{index}")
                @members.splice(index, 1)
            #else
            #    console.log("Group #{@name}: Object for id #{id} did not exist in members.")
            delete @_idmap[id]
            
            --@count
            return true
        return false
            
    #Only removes if it finds your object, else returns null
    remove_member: (obj) ->
        @.remove_member_by_id(obj.id)


    #emties a group by resetting its members, idmap, and count
    empty: () ->
        for id in @.get_member_ids()
            @.remove_member_by_id(id)

    #pass the val (could be a value, object, whatever) and the comparison function,
    #which should take 2 args - the first should be your passed value, the second
    #should be an RBObject as iterated in the members list.
    #Your function should return false on non match
    #returns the object on success, null on fail. Returns only the first matching obj
    find: (val, cmpfnc) ->
        for member in @members
            if cmpfnc(val, member)
                return member
        return null
#A class for recording callback functions and 
#properties relating to how they should be 
#called
class RBEventCallback extends RBObject
    constructor: (callback, consume) ->
        super(0, 0, 0, 0)
        @callback = callback
        @consume = consume
    fire: (ev) ->
        @callback(ev)
    will_consume: () ->
        return @consume

class RBDelegateGroup extends RBGroup
    call_delegates: (arglist) ->
        for cb in @members
            cb.fire.apply(cb, arglist)

    find_callback: (value, obj) ->
        if (obj.callback? and obj.callback == value)
            return true
        else
            return false

    add: (callback, consume=false) ->
        if not @.find(callback, @.find_callback)
            @.add_member(new RBEventCallback(callback, consume))
    remove: (callback) ->
        cb = @.find(callback, @.find_callback)
        if cb
            @.remove_member(cb)

class RBDelegateCollection extends RBGroup
    constructor: (name="DelegateCollection", groupnames) ->
        super(name, 0,0,0,0)
        @name = name
        @enabled = true
        @delegates = {}
        for eventname in groupnames
            @.add_event_type(eventname)

    destroy: () ->
        #there is no super destroy
        console.log("destroying RBDelegateCollection #{@name}")
        for evname, group in @delegates
            group.empty()
            delete @delegates[evname]

    enable: () ->
        @enabled = true
    disable: () ->
        @enabled = false

    add_event_type: (eventname) ->
        if not @delegates[eventname]
            @delegates[eventname] = new RBDelegateGroup(eventname)

    
    add_delegate: (eventname, callback, consume = false) ->
        dgroup = @delegates[eventname]
        if dgroup?
            dgroup.add(callback, consume)
    
    add: (eventname, callback, consume) ->
        @.add_delegate(eventname, callback, consume)

    remove_delegate: (eventname, callback) ->
        dgroup = @delegates[eventname]
        if dgroup?
            dgroup.remove(callback)

    remove:(eventname, callback) ->
        @.remove_delegate(eventname, callback)

    call_delegates: (eventname, arglist) ->
        if @enabled
            dgroup = @delegates[eventname]
            if dgroup?
                dgroup.call_delegates(arglist)
# RBActionList is the global list of running actions on objects.
# It is for internal use only
# @private
class RBActionList extends RBGroup
    constructor:(name="RBActionList") ->
        super(name)
        @entity_map = {}
        @paused = false #used by RBEditor to pause all actions

    add_for_entity: (ent, runnable) ->
        if not @entity_map[ent.id]?
            @entity_map[ent.id] = []
        @entity_map[ent.id].push(runnable)
        @.add_member(runnable)
        return runnable

    op_for_entity: (op, ent, actionlist) ->
        ids = null
        l = @entity_map[ent.id]
        if l? and l.length>0
            if actionlist? and actionlist.length>0
                ids = (action.id for action in actionlist)
            for runnable in l
                if (ids is null) or (runnable.action.id in ids)
                    #console.log("Running #{op} for ent #{ent.id}")
                    if op is "stop"
                        runnable.stop()
                    else if op is "start"
                        runnable.start()
                    else if op is "finish"
                        runnable.isFinished = true
                    else if op is "cancel"
                        runnable.isCancelled = true

    start_for_entity:(ent, actionlist) ->
        @.op_for_entity("start", ent, actionlist)
    stop_for_entity:(ent, actionlist) ->
        @.op_for_entity("stop", ent, actionlist)
    finish_for_entity:(ent, actionlist) ->
        @.op_for_entity("finish", ent, actionlist)
    cancel_for_entity:(ent, actionlist) ->
        @.op_for_entity("cancel", ent, actionlist)



    update: (dt) =>
        if @paused #used by RBEditor to pause all actions
            return
        finishedactions = []
        #console.log("total actions: " + @count)
        for action in @members
            if action? #why is the action even staying here?
                action.update(dt)

        for action in @members
            if action.isFinished or action.isCancelled
                #console.log("finishing action" + action)
                finishedactions.push(action)

        #now remove finished actions
        for action in finishedactions
            #console.log("Removed finished action " + action)
            entactionlist = @entity_map[action.target.id]
            if entactionlist? and action in entactionlist
                index = entactionlist.indexOf(action)
                if index >= 0
                    entactionlist.splice(index, 1)
                if entactionlist.length is 0
                    delete @entity_map[action.target.id]
            @.remove_member(action)

# RBRunnable represents the running state of an action
# It is for internal use only
class RBRunnable extends RBObject
    constructor:(@target, @action, @time, @args) ->
        super()
        @.reset()
    # @private
    dt_seconds: () ->
        return @dt / 1000.0

    on_finished:(func) ->
        @onFinished = func
    
    stop: () ->
        @stopped = true
    start: () ->
        @stopped = false

    reset: () ->
        @currenttime = 0
        @endtime = @time * 1000
        @dt = 0
        @isFinished = false
        @isCancelled = false
        @onFinished = null #callback when this finishes.
        @isInited = false
        @onInit = null
        @stopped = false



    # @private
    update: (dt) ->
        if @stopped or @isFinished
            return
        
        @dt = dt
        if @currenttime == 0 and not @isInited
            @isInited = true
            #console.log("should init")
            @action.init.apply(@target, [this, @args])
        if not (@isFinished or @isCancelled)
            @currenttime += dt
            if @currenttime <= @endtime
                @action.action.apply(@target, [this, @args])
            else
                #do the last little step.
                @dt -= (@endtime - @currenttime)
                @currenttime = @endtime
                @action.action.apply(@target, [this, @args])
                @isFinished = true
                @action.finish.apply(@target, [this, @args])
                if @onFinished isnt null
                    #console.log("calling action on finished")
                    @onFinished(@)

# An RBAction defines a transformation to apply to an object 
# over a duration. It can be targetted on an object, and 
# instanced into an RBRunnable, and inserted into the 
# RBActionList object, where it will be cleaned up when finished.
#
# Most RBActions require a time, for which they will execute, and 
# then some other data to define some transformation - for
# example moving position or changing colour. Other RBActions are
# simple one offs, like calling a function, or they provide a way
# of chaining several actions together to run in series or parallel.
#
# RBActions are independent of their targets - that is, you create
# an RBAction without specifying what it will act on. Then you can tell
# several objects to be acted on by the RB@currenttime = 0
# act in the same manner, but independently.
class RBAction extends RBObject
    constructor:(time=1.0, args...) ->
        super()
        @time = time
        @args = args
    # Initialises the action. Usually properties are set on
    # the runnable, which represents the dynamic or 'running'
    # state of the action.
    # @private
    init:(runnable, args) ->
        return

    # @private
    action:(runnable, args) ->
        return

    # @private
    finish: (runnable, args) ->
        return

    run_on: (rbobject) ->
        return @.run_action_on(rbobject)

    # @private
    run_action_on:(rbobject) ->
        #this is where you instance off the runnable
        #implicit return of runnable
        return @_RB.actions.add_for_entity(rbobject, new RBRunnable(rbobject, @, @time, @args))

# RB Repeat
class RBRepeatAction extends RBAction
    constructor: (repeatedaction, times=0) ->
        super(1, repeatedaction, times)
        @repeatedaction = repeatedaction
        @times = times

    init: (runnable, args) ->
        runnable.repeatedaction = args[0]
        #runnable.repeatedrunnable = null
        if runnable.repeatedrunnable?
            runnable.repeatedrunnable.times = args[1]
            if runnable.repeatedrunnable.times <= 0
                runnable.repeatedrunnable.repeatforever = true
            else
                runnable.repeatedrunnable.repeatforever = false

    fire_action: (repeatedrunnable) ->
        #console.log("firing action")
        repeatedrunnable.times -= 1
        if repeatedrunnable.repeatforever or repeatedrunnable.times > 0
            #console.log('repeating runnable')
            tmp = repeatedrunnable.onFinished
            repeatedrunnable.reset()
            repeatedrunnable.on_finished(tmp)
    #        return true
    #    else
    #        return false

    ###
    action: (runnable, args) ->
        #console.log("RBRepeatAction.action()")
        #console.log(runnable)
        runnable.currenttime = -1000
        super(runnable, args)
        runnable.isFinished = false
        #if runnable.repeatedrunnable?
        #    if runnable.repeatedrunnable.isFinished
        #        if runnable.repeatforever or runnable.times > 0
        #            runnable.times -= 1
        #            console.log('repeating runnable')
        #            runnable.repeatedrunnable.reset()
        #            runnable.isFinished = false
        #        else
        #            runnable.isFinished = true
        #    #else
        #    #    console.log("runnable repeatedaction wasn't finished")
        #else
        #    console.log("there was no repeated runnable.")
    ###
    
    #finish: (runnable, args) ->

    run_action_on: (rbobject) ->
        runnable = super(rbobject)
        runnable.repeatedrunnable = @repeatedaction.run_action_on(rbobject)
        #console.log("RBRepearAction runnable and repeatedrunnable")
        #console.log(runnable)
        #console.log(runnable.repeatedrunnable)
        #@@.fire_action(rbobject)
        runnable.repeatedrunnable.on_finished(@.fire_action)
        return runnable

class RBActionSequence extends RBAction
    constructor: (actionlist) ->
        totaltime = 0
        for action in actionlist
            totaltime += action.time
        super(totaltime, actionlist)

    init: (runnable, args) ->
        #console.log("action sequence 2 args...:")
        #console.log(args)
        runnable.actionlist = args[0]
        runnable.currentactionindex = -1
        runnable.next_action = (rbobject) ->
            runnable.currentactionindex += 1
            if runnable.currentactionindex < runnable.actionlist.length
                action = runnable.actionlist[runnable.currentactionindex]
                return new RBRunnable(rbobject, action, action.time, action.args)
            else
                return null
        runnable.start_next_action = () ->
            #console.log("starting next action, current index is: #{runnable.currentactionindex}, actionlist is #{runnable.actionlist}")
            nextrunnable = runnable.next_action(runnable.target)
            if nextrunnable isnt null
                #console.log("got next action")
                nextrunnable.on_finished(runnable.start_next_action)
                RB().actions.add_for_entity(runnable.target, nextrunnable)
        runnable.start_next_action()


    #run_action_on: (rbobject) ->
    #    runnable = super(rbobject)
        

class RBDelay extends RBAction
    constructor: (time) ->
        super(time)
        @time = time

# Move an object by xmove, ymove, (pixels) over time (seconds).
class RBMoveBy extends RBAction
    constructor:(time, xmove, ymove) ->
        super(time, xmove, ymove)
        @time = time
        @xmove = xmove
        @ymove = ymove

    # @private
    init: (runnable, args) ->
        super(runnable, args)
        runnable.dx = args[0]
        runnable.dy = args[1]
        runnable.cumulativex = 0
        runnable.cumulativey = 0
    
    # @private
    action: (runnable, args) ->
        super(runnable, args)
        dtsec = runnable.dt_seconds()
        dx = dtsec*(runnable.dx/runnable.time)
        dy = dtsec*(runnable.dy/runnable.time)
        runnable.cumulativex += dx
        runnable.cumulativey += dy
        this.move_by(dx, dy)

    # @private
    finish: (runnable, args) ->
        super(runnable, args)
        this.move_by(runnable.dx-runnable.cumulativex, runnable.dy-runnable.cumulativey)

# Move an object to X, Y, over time (seconds)
class RBMoveTo extends RBMoveBy
    constructor: (time, xdest, ydest) ->
        super(time, xdest, ydest)
        @time = time
        @xdest = xdest
        @ydest = ydest

    # @private
    init:(runnable, args) ->
        super(runnable, args)
        runnable.dx = args[0] - this.x
        runnable.dy = args[1] - this.y

# Rotate an object around its anchor by some angle over time
class RBRotateBy extends RBAction
    constructor: (time, rotby) ->
        super(time, rotby)
        @time = time
        @rotby = rotby
    # @private
    init: (runnable, args) ->
        super(runnable, args)
        runnable.rotby = args[0]
        runnable.cumulativerot = 0
    # @private
    action: (runnable, args) ->
        super(runnable, args)
        drot = runnable.dt_seconds()*(runnable.rotby/runnable.time)
        runnable.cumulativerot +=drot
        angle = this.get_angle()
        this.set_angle(angle + drot)
    # @private
    finish: (runnable, args) ->
        super(runnable, args)
        angle = this.get_angle()
        this.set_angle( angle +  runnable.rotby - runnable.cumulativerot)

# Rotate an object around its anchor to some angle over time
class RBRotateTo extends RBRotateBy
    constructor: (time, rotdest) ->
        super(time, rotdest)
        @time = time
        @rotdest = rotdest
    # @private
    init: (runnable, args) ->
        super(runnable, args)
        runnable.rotby = args[0] - this.get_angle()
        #console.log("Rotby is #{runnable.rotby} over #{runnable.time}")

# Scale an {RBEntity} by a particular amount (from its existing amount)
class RBScaleBy extends RBAction
    # @param time [Number]
    # @param scalebyx [Number] The x scale
    # @param scalebyy [Number] The y scale (defaults to x scale)
    constructor: (time, scalebyx, scalebyy=scalebyx) ->
        super(time, scalebyx, scalebyy)
        @time = time
        @scalebyx = scalebyx
        @scalebyy = scalebyy

    # @private
    init: (runnable, args) ->
        super(runnable, args)
        runnable.scalebyx = args[0]
        runnable.scalebyy = args[1]
        runnable.cumulativescalex = 0
        runnable.cumulativescaley = 0
    # @private
    action: (runnable, args) ->
        super(runnable, args)
        scx = runnable.dt_seconds()*(runnable.scalebyx/runnable.time)
        scy = runnable.dt_seconds()*(runnable.scalebyy/runnable.time)
        runnable.cumulativescalex += scx
        runnable.cumulativescaley += scy
        sc = this.get_scale()
        this.set_scale(sc.x + scx, sc.y+scy)
    # @private
    finish: (runnable, args) ->
        super(runnable, args)
        sc = this.get_scale()
        this.set_scale(sc.x + (runnable.scalebyx-runnable.cumulativescalex), sc.y+(runnable.scalebyy - runnable.cumulativescaley))

# Scale an {RBEntity} over time 
class RBScaleTo extends RBScaleBy
    # @param time [Number]
    # @param scaletox [Number] The x scale
    # @param scaletoy [Number] The y scale (defaults to x scale)
    constructor: (time, scaletox, scaletoy=scaletox) ->
        super(time, scaletox, scaletoy)
        @time = time
        @scaletox = scaletox
        @scaletoy = scaletoy

    init: (runnable, args) ->
        super(runnable, args)
        sc = this.get_scale()
        runnable.scalebyx = args[0] - sc.x
        runnable.scalebyy = args[1] - sc.y

# Calls the function specified (immediately - there is no time)
# Any arguments to use in the function should be passed as a list.
# @example
#   #If you normally call the function like so:
#   myfunc(arg1, arg2, arg3)
#   #Then you could use an RBCallFunction action like so:
#   action = new RBCallFunction(myfunc, [arg1, arg2, arg3])
#
# It may not always be useful to spefify a function object when
# creating this action, as you may want the function to resolve
# dynamically when this action runs. So the func argument to the 
# constructor can also be a string, which will be looked up on 
# the target object at runtime.
#
# This is useful when you need to call a method on the object that
# is has the action running on it, and you reuse that action over
# a bunch of objects. You want each running action to call the .blah
# method of its target - so when you create the action, you do it like
# so:
# @example
#   action = new RBCallFunction("blah", [arg1, arg2])
#   action.run_on(obj1) #will call obj1.blah(arg1, arg2)
#   action.run_on(obj2) #will call obj2.blah(arg1, arg2)
class RBCallFunction extends RBAction
    constructor: (func, argslist) ->
        super(0, func, argslist)
        @func = func
    # @private
    init: (runnable, args) ->
        runnable.func = args[0]

    # @private
    action: (runnable, args) ->
        super(runnable, args)
        #args[0] holds func
        #args[1] holds an array of arguments
        thisargs = args[1]#[this].concat(args[1])
        if typeof runnable.func is "function"
            runnable.func.apply(undefined, thisargs)
        else if typeof runnable.func is "string"
            func = runnable.target[runnable.func]
            if typeof func is "function"
                #console.log("successfully resolved #{runnable.func} with args #{thisargs}")
                func.apply(runnable.target, thisargs)

# Tint by the amounts r, g, b and a
class RBTintBy extends RBAction
    constructor: (time, r, g, b, a = 255) ->
        super(time, r, g, b, a)
        @time = time
        @r = r
        @g = g
        @b = b
        @a = a
    # @private
    init: (runnable, args) ->
        super(runnable, args)
        #runnable.dr = clamp_num(this._colour.r + args[0], 0, 255)
        #runnable.dg = clamp_num(this._colour.g + args[1], 0, 255)
        #runnable.db = clamp_num(this._colour.b + args[2], 0, 255)
        #runnable.da = clamp_num(this._colour.a + args[3], 0, 255)
        runnable.dr = args[0]
        runnable.dg = args[1] 
        runnable.db = args[2] 
        runnable.da = args[3] 
        
        #console.log("Tint By Dests: #{runnable.dr}, #{runnable.dg}, #{runnable.db}, #{runnable.da} over #{runnable.time} seconds")
        runnable.cumulativer = 0
        runnable.cumulativeg = 0
        runnable.cumulativeb = 0
        runnable.cumulativea = 0
    # @private
    action: (runnable, args) ->
        super(runnable, args)
        elapsed = runnable.dt_seconds()
        dr = elapsed*(runnable.dr/runnable.time)
        dg = elapsed*(runnable.dg/runnable.time)
        db = elapsed*(runnable.db/runnable.time)
        da = elapsed*(runnable.da/runnable.time)
        runnable.cumulativer += dr
        runnable.cumulativeg += dg
        runnable.cumulativeb += db
        runnable.cumulativea += da
        c = this._colour
        this.set_colour(c.r+dr, c.g+dg, c.b+db, c.a+da)

    # @private
    finish: (runnable, args) ->
        super(runnable, args)
        c = this._colour
        #console.log("Final pre-colour: ")
        #console.log(c)
        this.set_colour(c.r + (runnable.dr-runnable.cumulativer),
            c.g + (runnable.dg-runnable.cumulativeg),
            c.b + (runnable.db-runnable.cumulativeb),
            c.a + (runnable.da-runnable.cumulativea))
        #console.log(c)

# Tint to the colour r, g, b and a elements specified
class RBTintTo extends RBTintBy
    constructor: (time, r, g, b, a=255) ->
        super(time, r, g, b, a)
        @time = time
        @r = r
        @g = g
        @b = b
        @a = a

    # @private
    init: (runnable, args) ->
        super(runnable, args)
        runnable.dr = args[0] - this._colour.r
        runnable.dg = args[1] - this._colour.g
        runnable.db = args[2] - this._colour.b
        runnable.da = args[3] - this._colour.a
        #console.log("Tint To Dests: #{runnable.dr}, #{runnable.dg}, #{runnable.db}, #{runnable.da}")
        
# Fades your rbentity down 255 alpha values
class RBFadeOut extends RBTintBy
    constructor: (time) ->
        super(time, 0,0,0,-255)
        @time = time

    finish: (runnable, args) ->
        super(runnable, args)
        #console.log("End RBFadeOut, colour is:")
        #console.log(this._colour)

# Fades your rbentity up 255 alpha values.
# Does not set your rbentity alpha to zero first
# you should do that with .set_alpha(0)
class RBFadeIn extends RBTintBy
    constructor: (time) ->
        super(time, 0,0,0,255)
        @time = time


# Rhubarbs Keys object
class RBKeys
    # @property [Number] Escape Key
    esc:  27
    # @property [Number] Enter Key
    enter:  13
    # @property [Number] Space Key
    space:  32
    # @property [Number] Up Arrow Key
    up:  38
    # @property [Number] Down Arrow Key
    down:  40
    # @property [Number] Left Arrow Key
    left:  37
    # @property [Number] Right Arrow Key
    right:  39
    # @property [Number] Question Mark Key
    qmark:  191
    # @property [Number] Tilde Key
    tilde:  192
    # @property [Number] Plus Key (not numpad)
    plus:  187
    # @property [Number] Minus Key (not numpad)
    minus:  189
    # @property [Number] Tab Key
    tab:  9
    # @property [Number] Delete Key
    delete: 46
    # @property [Number] Shift Key
    shift:  16
    # @property [Number] Ctrl Key
    ctrl:  17
    # @property [Number] Alt Key
    alt:  18
    # @property [Number] Number 0 Key (not numpad)
    num_0:  48
    # @property [Number] Number 1 Key (not numpad)
    num_1:  49
    # @property [Number] Number 2 Key (not numpad)
    num_2:  50
    # @property [Number] Number 3 Key (not numpad)
    num_3:  51
    # @property [Number] Number 4 Key (not numpad)
    num_4:  52
    # @property [Number] Number 5 Key (not numpad)
    num_5:  53
    # @property [Number] Number 6 Key (not numpad)
    num_6:  54
    # @property [Number] Number 7 Key (not numpad)
    num_7:  55
    # @property [Number] Number 8 Key (not numpad)
    num_8:  56
    # @property [Number] Number 9 Key (not numpad)
    num_9:  57
    # @property [Number] a Key
    a:  65
    # @property [Number] b Key
    b:  66
    # @property [Number] c Key
    c:  67
    # @property [Number] d Key
    d:  68
    # @property [Number] e Key
    e:  69
    # @property [Number] f Key
    f:  70
    # @property [Number] g Key
    g:  71
    # @property [Number] h Key
    h:  72
    # @property [Number] i Key
    i:  73
    # @property [Number] j Key
    j:  74
    # @property [Number] k Key
    k:  75
    # @property [Number] l Key
    l:  76
    # @property [Number] m Key
    m:  77
    # @property [Number] n Key
    n:  78
    # @property [Number] o Key
    o:  79
    # @property [Number] p Key
    p:  80
    # @property [Number] q Key
    q:  81
    # @property [Number] r Key
    r:  82
    # @property [Number] s Key
    s:  83
    # @property [Number] t Key
    t:  84
    # @property [Number] u Key
    u:  85
    # @property [Number] v Key
    v:  86
    # @property [Number] w Key
    w:  87
    # @property [Number] x Key
    x:  88
    # @property [Number] y Key
    y:  89
    # @property [Number] z Key
    z:  90
    # @property [Number] \ Key
    backslash:  220
    # @property [Number] / Key
    forwardslash:  191
    # @property [Number] . Key
    period:  190
    # @property [Number] - Key
    dash:  189
    # @property [Number] , Key
    comma:  188
    # @property [Number] =  Key
    equals:  187
    # @property [Number] numlock Key
    numlock:  144

_RBKeys = new RBKeys()


_KeyStates =
    up: 1   #the key has been released, but not checked
    down: 2 #the key is pressed down
    clear: 0 #the key has not been touched since last check


_RBInputEvents =
    mousemove: RB_MOUSE_MOVE
    mousedown: RB_MOUSE_DOWN
    mouseup:   RB_MOUSE_UP
    mousewheel: RB_MOUSE_WHEEL
    touchstart: RB_TOUCH_START
    touchend: RB_TOUCH_END
    touchcancel: RB_TOUCH_CANCEL
    touchmove: RB_TOUCH_MOVE
    keydown: RB_KEY_DOWN
    keyup: RB_KEY_UP
    dblclick: RB_MOUSE_DOUBLECLICK

###
class RBInputType
    constructor: ()

class RBPressableInput
    constructor: () ->
        @_state = _KeyStates.clear
    is_clear:() ->
        return @_state == _KeyStates.clear
    is_pressed:() ->
        return @.is_down()
    is_up:() ->
        return @_state == _KeyStates.up
    is_down:() ->
        return @_state == _KeyStates.down
    update_state: (newstate) =>
        @_state = newstate
    true: () =>
        #override with some logic that evaluates to true for some specific context
        #i.e. if the key is held down, you call this 'true'

class RBTimedPressableInput extends RBPressableInput
    constructor: (@threshold)
        super()
        
        @_time_since_last_event = 0.0
        @_timer = new RBTimer()
    true: ()
        if super() and @_time_since_last_event > @threshold
            return true
        else
            return false

class RBKeyHeld extends RBPressableInput
    constructor: (@keycode) ->
        super()

class RBMouseHeld
    constructor: (@mousecode) ->
        super()

class RBTimedKeyHeld extends RBTimedPressableInput
    constructor(@keycode, @mousecode)

class RBTimedMouseHeld

class RBKeyPressed

class RBMousePressed

#example
class UpKeyHeldDown extends RBStatefulInput
    constructor: ()
        super()
        @keycode = _RBKeys.up
        @upevent = _RBInputEvents.keyup
        @downevent = _RBInputEvents.keydown
    true:() =>
        return @.is_down()


class RBInputListenerList
    true: () =>
        #is true when any of its elements are true.

class RBInputListener
    
    #RBinputlisteners can be configured with actions which map to lists of input events
    #They end up binding to RBInput events
    constructor: () ->

    registerInputAction: () ->
    
    unregisterInputAction: () ->

        #registerInputAction takes the name, and a list of lists. Within in the inner lists, they will evaluate to true when any
        #of their contents is true. i.e. they are ored together
        #the entire action will be true when all lists given evaluate to true, i.e. they are anded together.
    #'jump' will be true, when any of the list is true
    RBInputListener.registerInputAction("jump", [[KeyHeld(_RBKeys.up), MouseHeld(_RBKeys.mouse1)]]) #'held' means is true if key is down
    RBInputListener.registerInputAction("walkright", [[KeyHeld(_RBKeys.right)]])
    RBInputListener.registerInputAction("fire", [[KeyPressed(_RBKeys.ctrl), MousePressed(_RBKeys.mouse2)]]) #'pressed' means pushed down, resets when pushed again
    RBInputListener.registerInputAction("chargeweapon", [[TimedKeyHeld(_RBKeys.z, 0.5)]]) #timedkeyheld means true if key is held more than n seconds
###

# A class to represent the mouse
class RBMouse extends RBRect
    constructor: () ->
        super(0, 0, 2, 2)
        @_entlist = new RBGroup()
        @dragentity = null
        @rawx = 0
        @rawy = 0
        @leftpressed = false #up
        @rightpressed = false #up
        @middlepressed = false #up
        @wheeldelta = 0

    #cross platform mouse wheel delta (FF is reversed. This handles it)
    calc_wheel_delta: (e) ->
        @wheeldelta = Math.max(-1, Math.min(1, (e.wheelDelta || -e.detail)))
    reset_wheel_delta: () ->
        @wheeldelta = 0
    
    # Pass it a single RBEntity (Generally RBLayer), or
    # a list of entities. If one of these entities is 
    # destroyed, it will also be removed from drag registration
    # 
    register_drag: (entity) ->
        @_entlist.add_member(entity)
    # You can manually call this to remove an entity from drag 
    # registration, but only if it was directly registered.
    # If it is a child of an RBEntity that was registered
    # e.g. an RBLayer, then the only way to deregister it is
    # to remove it from that RBEntity.
    unregister_drag: (entity) ->
        @_entlist.remove_member(entity)
        #if...
        #console.log("removed a draggable entity with id: #{entity.id}")
        
    # @private
    # Updates the mouse position. Called by RB().input's mouse move
    update_position: (@rawx, @rawy) ->
        @.set_position(
            @rawx / RBV(RBV_ENG).screenscalex + RB().screen().x,
            @rawy / RBV(RBV_ENG).screenscaley + RB().screen().y)


    # @private
    # Checks for draggable entities having collisions with the mouse
    check_drag: () ->
        if @dragentity is null
            #console.log("checking for drag")
            e = @.overlaps_in_rlist(@_entlist.members)
            #console.log(e)
            if e? and e isnt null
                @dragentity = e
                if @dragentity.on_drag_start?
                    @dragentity.on_drag_start(@)
                return true
        return false

    # @private
    # Drops the currently held entity
    check_drop: () ->
        if @dragentity isnt null
            #console.log("checking for drop")
            if @dragentity.on_drag_end?
                @dragentity.on_drag_end(@)
            @dragentity = null
            return true
        return false

    # @updates the position of entities.
    do_drag_update: (dt) ->
        if @dragentity isnt null
            @dragentity.set_position(@.x, @.y)



class RBInput extends RBDelegateCollection
    constructor: (@_RB) ->

        super("RBInputDelegates",
            [_RBInputEvents.mousemove,
            _RBInputEvents.mousedown,
            _RBInputEvents.mouseup,
            _RBInputEvents.mousewheel,
            _RBInputEvents.dblclick,
            _RBInputEvents.touchstart,
            _RBInputEvents.touchend,
            _RBInputEvents.touchcancel,
            _RBInputEvents.touchmove,
            _RBInputEvents.keydown,
            _RBInputEvents.keyup,
            _RBInputEvents.keydown])

        @mouse = new RBMouse()
        @keys = _RBKeys
        @keysvalues = []
        for k, v of @keys
            @keysvalues.push(v)
        @keynav = null
        @keystate = {}
        @_inited = false
        @.init()
    
    init: () ->
        if @_inited
            return
        #Ok. Bind the dom level events
        window.addEventListener('resize', @.on_resize, false)
        binder = document
        #binder = @_RB._canvas #note, at this point, canvas is null
        
                       
        binder.addEventListener('mousemove', @.on_mouse_move, false)
        binder.addEventListener('mousedown', @.on_mouse_down, false)
        binder.addEventListener('mouseup', @.on_mouse_up, false)
        
        #Mousewheel IE8+, Chrome, Safari
        binder.addEventListener('mousewheel', @.on_mouse_wheel, false)
        #binder.addEventListener('dblclick', @.on_mouse_doubleclick, false)
        #binder.addEventListener('click', @.on_mouse_click, false)
        #Mousewheel FF
        binder.addEventListener('DOMMouseScroll', @.on_mouse_wheel, false)
        binder.addEventListener('touchstart', @.on_touch_start, false)
        binder.addEventListener('touchend', @.on_touch_end, false)
        binder.addEventListener('touchcancel', @.on_touch_cancel, false)
        binder.addEventListener('touchmove', @.on_touch_move, false)
        binder.addEventListener('keydown', @.on_key_down, false)
        binder.addEventListener('keyup', @.on_key_up, false)
    
    bind: (evname, callback, consume=false) ->
        @.add_delegate(evname, callback, consume)
    
    unbind: (evname, callback) ->
        @.remove_delegate(evname, callback)
    
    call_handlers: (evname, event) ->
        @.call_delegates(evname, [event])
        @.call_scene_handlers(evname, [event])
    
    #Automatically call the input handlers for the current scene.
    call_scene_handlers: (evname, args) ->
        cs = RBSceneManager.get_instance().current_scene()
        if cs?
            #console.log("Calling delegates in #{cs.name} for event #{evname}")
            cs.input.call_delegates(evname, args)

    on_mouse_click: (event) =>
        console.log("click")
        @.swallow(event)

    on_mouse_doubleclick: (event) =>
        console.log("double click")
        @.swallow(event)

    on_mouse_wheel: (event) =>
        @.mouse.calc_wheel_delta(event)
        @.call_handlers(_RBInputEvents.mousewheel, event)
        @.mouse.reset_wheel_delta()

    on_mouse_down: (event) =>
        #@mouse.clicking = true
        #if @mouse.mouseoverentity
        #    @mouse.dragentity = @mouse.mouseoverentity
        # TODO: This won't work for IE, which uses a different map
        switch event.button
            when 0
                @mouse.leftpressed = true
            when 1
                @mouse.middlepressed = true
            when 2
                @mouse.rightpressed = true
        if not @mouse.check_drag()
            @.call_handlers(_RBInputEvents.mousedown, event)

    on_mouse_move: (event) =>
        @mouse.update_position(
            event.clientX - @_RB.canvas().x,
            event.clientY - @_RB.canvas().y)
        ###
        @mouse.rawx = event.clientX - @_RB.canvas().x
        @mouse.rawy = event.clientY - @_RB.canvas().y

        @mouse.x = @mouse.rawx / RBV(RBV_ENG).screenscalex + @_RB.screen().x
        @mouse.y = @mouse.rawy / RBV(RBV_ENG).screenscaley + @_RB.screen().y

        #check for mouseover
        mrect = new RBRect(@mouse.x, @mouse.y, 10,10)
        @mouse.mouseoverentity = null
        if @mouse.dragentity
            @mouse.dragentity = @mouse.x
            @mouse.dragentity = @mouse.y
        ###
        @mouse.do_drag_update()
        @.call_handlers(_RBInputEvents.mousemove, event)

    on_mouse_up: (event) =>
        switch event.button
            when 0
                @mouse.leftpressed = false
            when 1
                @mouse.middlepressed = false
            when 2
                @mouse.rightpressed = false
        #@mouse.clicking = false
        #@mouse.dragentity = null
        if not @mouse.check_drop()
            @.call_handlers(_RBInputEvents.mouseup, event)

    on_touch_start: (event) =>
        @.swallow(event)
        #@mouse.clicking = true
        console.log("Touch start")
        @.call_handlers(_RBInputEvents.touchstart, event)
    
    on_touch_end: (event) =>
        @.swallow(event)
        #@mouse.clicking = false
        console.log("Touch end")
        @.call_handlers(_RBInputEvents.touchend, event)
    
    on_touch_cancel: (event) =>
        return
    
    on_touch_move: (event) =>
        return
    
    on_key_down: (event) =>
        if event.keyCode in @keysvalues
            @keystate[event.keyCode] = _KeyStates.down
        @.call_handlers(_RBInputEvents.keydown, event)
        if (event.keyCode is @keys.tab)
            @.swallow(event)

    on_key_up: (event) =>
        if event.keyCode in @keysvalues
            @keystate[event.keyCode] = _KeyStates.up
        @.call_handlers(_RBInputEvents.keyup, event)

    on_resize: (event) =>
        #Do nothing
        #@.resize_canvas()

    key_released: (keyname) =>
        if @keystate[keyname]? and @keystate[keyname] is _KeyStates.up
            @keystate[keyname] = _KeyStates.clear 
            return true
        else
            return false
    key_pressed: (keyname) =>
        if @keystate[keyname]? and @keystate[keyname] is _KeyStates.down
            return true
        else
            return false

    swallow: (event) ->
        event.preventDefault()



# A class for media objects
class RBMediaObject
    constructor: () ->
        @is_ready = false
    on_ready: () =>
        @is_ready = true

# A Sound
class RBAudio extends RBMediaObject
    # Whether or not the sound should loop
    looping: false

    # Create an RBAudio object
    constructor:(soundname, cachesize=RB_DEFAULT_SOUND_CACHE_SIZE, onrdy) ->
        super()
        if !onrdy?
            onrdy = @.snd_on_ready
        @soundname = soundname
        @cachesize = cachesize
        @soundbank = []
        @loadedsounds = 0
        @_volume = 1
        for count in [0...@cachesize]
            #console.log("adding a sound to the soundbank")
            @_audio = new Audio(@soundname)
            @_audio.soundname = @soundname #for snd_on_ready to work
            @soundbank.push(@_audio)
            @_audio.onload = onrdy
            #@_audio.on('loadeddata', onrdy)
            @_audio.addEventListener('canplaythrough', onrdy)
            @_audio.load()

            #for now, we just call..
            #console.log("TODO: forcing sound onload callback")
            #@.on_ready()
    
    # Set the volume of this sound (0 to 1)
    volume: (num) ->
        if num?
            if num >=0 and num <= 1
                @_volume = num
        
        @.set_effective_volume()
        @_volume

    # @private
    # the actual volume that this sound should play at
    # considering the global volume settings
    set_effective_volume: () ->
        v = @_volume * RB().media.sfxVolume
        if @_audio isnt null
            @_audio.volume = v
        v

    # @private
    snd_on_ready: () ->
        #console.log("snd_on_ready!")
        sound = RB().media.sounds[@soundname]
        sound.on_ready()

    # @private
    on_ready: () ->
        #console.log("sound was ready")
        @loadedsounds +=1
        if @loadedsounds == @cachesize
            #console.log("sound #{@soundname} ready")
            super()
    
    # @private
    find_playable_sound: () ->
        for au in @soundbank
            if au.ended or au.currentTime == 0
                return au
        return null

    set_looping: (looping = true) ->
        if (typeof @_audio.loop == 'boolean')
            @_audio.loop = looping
        else
            if looping

                replay = () =>
                    this.currentTime = 0
                    this.play()

                @_audio.addEventListener('ended', replay, false)
            else
                @_audio.removeEventListener('ended', replay, false)

    # Stop the sound, and by default rewind it.
    stop: (rewind = true) ->
        @_audio.pause()
        if rewind
            @_audio.currentTime = 0

    # Play the sound
    play:() ->
        au = @.find_playable_sound()
        if au isnt null
            @_audio = au
            @.set_effective_volume()
            @_audio.play()

# Music class
# Only caches one object.
# Defaults to looping = true.
class RBMusic extends RBAudio
    # Create an RBMusic object
    constructor: (soundname) ->
        #console.log("Constructing RB Music")
        super(soundname, 1)
        @soundname = soundname
        @playing = false

    # @private
    # the actual volume that this sound should play at
    # considering the global volume settings
    set_effective_volume: () ->
        v = @_volume * RB().media.musicVolume
        if @_audio isnt null
            @_audio.volume = v
        v

    # Play the music, but only if it isn't already playing.
    # The default is to loop it (pass false if you dont want to)
    play: (looping = true) ->
        if !@playing
            @.set_looping(looping)
            @_audio = @soundbank[0]
            @.set_effective_volume()
            @_audio.play()
            @playing = true

    # Stop the music, and rewind by default.
    # Pass false if you don't want to
    stop: (rewind = true) ->
        super(rewind)
        @playing = false



# An Image
class RBImage extends RBMediaObject
    # Construct an RBImage
    constructor: (imagename, onrdy, src) ->
        super()
        @imagename = imagename
        if !onrdy?
            onrdy = @.img_on_ready
        @_image = new Image()
        @_image.imagename = @imagename #for img_on_ready to work
        @_image.onload = onrdy
        #console.log(@_image.onload)
        if src?
            @_image.src = src
        else
            @_image.src = @imagename
    # @private
    img_on_ready: () =>
        #at this point, we are from the perspective of
        #the HTMLImage object, not THIS object
        rbimage = RB().media.images[@imagename]
        rbimage.on_ready() 
    # @private
    on_ready: () ->
        #console.log("image #{@src} is ready")
        super()
    # Returns the HTMLElement image for this RBImage
    image: () ->
        @_image


# A spritesheet
class RBSpriteSheet extends RBMediaObject

    # Construct a spritesheet
    constructor: (@ssname, @imagename, @xframes, @yframes, @framewidth=0, @frameheight=0) ->
        #console.log("Constructing spritesheet #{@ssname} for #{@imagename}")
        super()
        @_RBimage = RB().media.load_image(@imagename, @.on_ready)
    
    # @private
    on_ready: () =>
        super()

        #console.log("spritesheet for #{@imagename} ready")
        #@_image = @_RBimage.image()
        @_image = RB().media.images[@imagename]._image
        #console.log(RB().media.images[@imagename])
        #console.log("Image width,height: #{@_image.naturalWidth},#{@_image.naturalHeight}")
        if @framewidth == 0
            @framewidth = (@_image.naturalWidth / @xframes) | 0
        if @frameheight == 0
            @frameheight = (@_image.naturalHeight / @yframes) | 0
        #console.log("Framewidth was #{@framewidth} and frameheight was #{@frameheight}")
    
    get_image: () ->
        return @_image
    # @private
    frame_at: (index, rect, xoffset=0, yoffset=0) ->
        #console.log("Frameat: #{index}")
        rect.x = (index % @xframes)*@framewidth
        rect.y = ((index / @xframes) | 0) * @frameheight
        rect.height = @frameheight
        rect.width = @framewidth
        #if index == 0
        #    console.log("currentFrameRect: x:#{rect.x}, y:#{rect.y}, w: #{rect.width}, h: #{rect.height}")
        return rect

# A container to hold and cache media for a project.
# RBMedia contains four main sections:
#   images
#   sounds
#   music
#   spritesheets
#
# It contains an {RBPreloader} which can preload sets of
# images, spritesheets, and sounds, or you can load them
# manually with
#   load_image(path)
#   load_sound(path)
#   load_music(path)
#   load_spritesheet(name, image, xframes, yframes)
#
# Once loaded, they can be used when needed in the project
# by retrieving them:
# @example
#   myimage = RB().media.get_image(yourimagename)
#   mysound = RB().media.get_sound(yoursoundname)
#   mymusic = RB().media.get_music(yourmusicname)
#   myspritesheet = RB().media.get_spritesheet(yourssname)
#
# The other important parts of RBMedia are the controls for
# sound volume:
# #sfx_volume()
# #music_volume()
class RBMedia
    constructor: () ->
        @images = {}
        @sounds = {}
        @spritesheets = {}
        @preloader = new PreLoaderStats()
        @sfxVolume = 1
        @musicVolume = 1
        @.load_image("rblogo", undefined, __rblogo)

    # Set the global sfx volume (0-1)
    sfx_volume: (num) ->
        if num?
            if num >= 0 and num <=1
                @sfxVolume = num
                for sound in @sounds
                    sound.set_effective_volume()
        @sfxVolume

    # Set the global music volume (0-1)
    music_volume: (num) ->
        if num?
            if num >= 0 and num <=1
                @musicVolume = num
                for name, sound of @sounds
                    sound.set_effective_volume()
        @musicVolume

    # Load and store spritesheet parameters
    # If the image specified is not yet loaded, RBMedia will
    # attempt to load it.
    load_spritesheet: (spritesheetname, imagename, xframes, yframes, framewidth=0, frameheight=0) =>
        console.log("load_spritesheet arguments: #{spritesheetname}, #{imagename}, #{xframes}, #{yframes}, #{framewidth}, #{frameheight}")
        if not @.get_spritesheet(spritesheetname)?
            #console.log("loading spritesheet")
            newss = new RBSpriteSheet(spritesheetname, imagename, xframes, yframes, framewidth, frameheight)
            @spritesheets[spritesheetname] = newss
        @spritesheets[spritesheetname]
    
    # Gets the named spritesheet.
    # You can either pass a spritesheet name as a string,
    # or pass a spritesheet initialiseation array, in which the
    # first element is the spritesheet name
    get_spritesheet: (spritesheetname) ->
        if typeof spritesheetname == "object" and spritesheetname.length>1
            spritesheetname = spritesheetname[0]
        return @spritesheets[spritesheetname]

    # Load an image resource and cache it
    load_image: (imagename, ready_callback, src) =>
        if not @images[imagename]?
            #console.log("creating new RBImage(#{imagename})")
            @images[imagename] = new RBImage(imagename, ready_callback, src)
        #return
        else
            #console.log("Image retrieved from cache.")
            if ready_callback?
                ready_callback()
            #else
                #@images[imagename].on_ready()
        @images[imagename]

    # Get an image from the cache.
    get_image: (imagename) ->
        return @images[imagename]

    # Load a sound resource and cache it.
    load_sound: (soundname, cachesize=RB_DEFAULT_SOUND_CACHE_SIZE) =>
        if not @sounds[soundname]?
            #load the sound
            @sounds[soundname] = new RBAudio(soundname, RB_DEFAULT_SOUND_CACHE_SIZE)
        #return
        @sounds[soundname]

    # Get a sound from the cache
    get_sound: (soundname) ->
        return @sounds[soundname]

    # Load music from a resource and cache it.
    load_music: (music_name) =>
        if not @sounds[music_name]?
            #console.log("loading music")
            @sounds[music_name] = new RBMusic(music_name)
        @sounds[music_name]

    # Get music from the cache
    get_music: (music_name) ->
        return @sounds[music_name]

# @private
# A controller class for aggregating several preloaders
class PreLoaderStats
    constructor: () ->
        @images = new PreLoader()
        @spritesheets = new PreLoader()
        @sounds = new PreLoader()
        @music = new PreLoader()
        @stats = @.statistics()
        @first_non_loaded = null
        @done = false

    queue_images: (imageslist) ->
        @images.queue_items(imageslist)
    queue_sounds: (soundslist) ->
        @sounds.queue_items(soundslist)
    queue_music: (musiclist) ->
        @music.queue_items(musiclist)
    queue_spritesheets: (spritesheetslist) ->
        @spritesheets.queue_items(spritesheetslist)
        
    start: () ->
        @images.preload(RB().media.load_image)

    update: () ->
        if not @done
            if not @images.done
                @images.update("is_ready")
                if @images.first_non_loaded isnt null
                    @first_non_loaded = @images.first_non_loaded.imagename
            else if not @sounds.done
                if not @sounds.started
                    @sounds.preload(RB().media.load_sound)
                @sounds.update("is_ready")
                if @sounds.first_non_loaded isnt null
                    @first_non_loaded = @sounds.first_non_loaded.soundname
            else if not @music.done
                if not @music.started
                    @music.preload(RB().media.load_music)
                @music.update("is_ready")
                if @music.first_non_loaded isnt null
                    @first_non_loaded = @music.first_non_loaded.soundname
            else if not @spritesheets.done
                if not @spritesheets.started
                    @spritesheets.preload(RB().media.load_spritesheet)
                @spritesheets.update("is_ready")
                if @spritesheets.first_non_loaded isnt null
                    @first_non_loaded = @spritesheets.first_non_loaded.ssname
            @stats = @.statistics()
            if @stats.total > 99
                @done = true
                #console.log("preloader is done!")
    
    statistics: () ->
        #console.log("Images: #{@images.percentageDone}")
        #console.log("Sounds: #{@sounds.percentageDone}")
        #console.log("Spritesheets: #{@spritesheets.percentageDone}")
        ret = {}
        ret.images = @images.percentageDone
        ret.sounds = @sounds.percentageDone
        ret.music = @music.percentageDone
        ret.spritesheets = @spritesheets.percentageDone
        ret.total = ((ret.images + ret.sounds + ret.music + ret.spritesheets) / 4.0)
        ret.waiting_asset = @first_non_loaded
        return ret

# @private
# A class for preloading objects.
class PreLoader
    constructor: () ->
        @items = []
        @itemsToLoad = 0
        @itemsLoaded = 0
        @percentageDone = 0
        @loadingItems = []
        @loadedItems = []
        @done = false
        @started = false
        @first_non_loaded = null

    queue_items: (qitems) ->
        for i in qitems
            @items.push(i)
            @itemsToLoad++
    
    preload: (func) ->
        if not @started
            for i in @items
                #console.log("preloading #{i}")
                #console.log(typeof i)
                if typeof i == "object" && i.length > 1
                    @loadingItems.push(func.apply(RB().media, i))
                else
                    @loadingItems.push(func(i))
            @started = true

    update: (loadedproperty) ->
        @first_non_loaded = null
        for i in @loadingItems
            #console.log(i)
            if i not in @loadedItems
                if i[loadedproperty]
                    @loadedItems.push(i)
                    @itemsLoaded+=1
                else if @first_non_loaded is null
                    @first_non_loaded = i
        if @itemsToLoad == 0
            @percentageDone = 100.0
        else
            @percentageDone = (@itemsLoaded/@itemsToLoad) * 100.0

        if @itemsToLoad == @itemsLoaded
            @done = true
            @percentageDone = 100.0

__rblogo = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAACXBIWXMAAAsTAAALEwEAmpwYAAAKT2lDQ1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAHjanVNnVFPpFj333vRCS4iAlEtvUhUIIFJCi4AUkSYqIQkQSoghodkVUcERRUUEG8igiAOOjoCMFVEsDIoK2AfkIaKOg6OIisr74Xuja9a89+bN/rXXPues852zzwfACAyWSDNRNYAMqUIeEeCDx8TG4eQuQIEKJHAAEAizZCFz/SMBAPh+PDwrIsAHvgABeNMLCADATZvAMByH/w/qQplcAYCEAcB0kThLCIAUAEB6jkKmAEBGAYCdmCZTAKAEAGDLY2LjAFAtAGAnf+bTAICd+Jl7AQBblCEVAaCRACATZYhEAGg7AKzPVopFAFgwABRmS8Q5ANgtADBJV2ZIALC3AMDOEAuyAAgMADBRiIUpAAR7AGDIIyN4AISZABRG8lc88SuuEOcqAAB4mbI8uSQ5RYFbCC1xB1dXLh4ozkkXKxQ2YQJhmkAuwnmZGTKBNA/g88wAAKCRFRHgg/P9eM4Ors7ONo62Dl8t6r8G/yJiYuP+5c+rcEAAAOF0ftH+LC+zGoA7BoBt/qIl7gRoXgugdfeLZrIPQLUAoOnaV/Nw+H48PEWhkLnZ2eXk5NhKxEJbYcpXff5nwl/AV/1s+X48/Pf14L7iJIEyXYFHBPjgwsz0TKUcz5IJhGLc5o9H/LcL//wd0yLESWK5WCoU41EScY5EmozzMqUiiUKSKcUl0v9k4t8s+wM+3zUAsGo+AXuRLahdYwP2SycQWHTA4vcAAPK7b8HUKAgDgGiD4c93/+8//UegJQCAZkmScQAAXkQkLlTKsz/HCAAARKCBKrBBG/TBGCzABhzBBdzBC/xgNoRCJMTCQhBCCmSAHHJgKayCQiiGzbAdKmAv1EAdNMBRaIaTcA4uwlW4Dj1wD/phCJ7BKLyBCQRByAgTYSHaiAFiilgjjggXmYX4IcFIBBKLJCDJiBRRIkuRNUgxUopUIFVIHfI9cgI5h1xGupE7yAAygvyGvEcxlIGyUT3UDLVDuag3GoRGogvQZHQxmo8WoJvQcrQaPYw2oefQq2gP2o8+Q8cwwOgYBzPEbDAuxsNCsTgsCZNjy7EirAyrxhqwVqwDu4n1Y8+xdwQSgUXACTYEd0IgYR5BSFhMWE7YSKggHCQ0EdoJNwkDhFHCJyKTqEu0JroR+cQYYjIxh1hILCPWEo8TLxB7iEPENyQSiUMyJ7mQAkmxpFTSEtJG0m5SI+ksqZs0SBojk8naZGuyBzmULCAryIXkneTD5DPkG+Qh8lsKnWJAcaT4U+IoUspqShnlEOU05QZlmDJBVaOaUt2ooVQRNY9aQq2htlKvUYeoEzR1mjnNgxZJS6WtopXTGmgXaPdpr+h0uhHdlR5Ol9BX0svpR+iX6AP0dwwNhhWDx4hnKBmbGAcYZxl3GK+YTKYZ04sZx1QwNzHrmOeZD5lvVVgqtip8FZHKCpVKlSaVGyovVKmqpqreqgtV81XLVI+pXlN9rkZVM1PjqQnUlqtVqp1Q61MbU2epO6iHqmeob1Q/pH5Z/YkGWcNMw09DpFGgsV/jvMYgC2MZs3gsIWsNq4Z1gTXEJrHN2Xx2KruY/R27iz2qqaE5QzNKM1ezUvOUZj8H45hx+Jx0TgnnKKeX836K3hTvKeIpG6Y0TLkxZVxrqpaXllirSKtRq0frvTau7aedpr1Fu1n7gQ5Bx0onXCdHZ4/OBZ3nU9lT3acKpxZNPTr1ri6qa6UbobtEd79up+6Ynr5egJ5Mb6feeb3n+hx9L/1U/W36p/VHDFgGswwkBtsMzhg8xTVxbzwdL8fb8VFDXcNAQ6VhlWGX4YSRudE8o9VGjUYPjGnGXOMk423GbcajJgYmISZLTepN7ppSTbmmKaY7TDtMx83MzaLN1pk1mz0x1zLnm+eb15vft2BaeFostqi2uGVJsuRaplnutrxuhVo5WaVYVVpds0atna0l1rutu6cRp7lOk06rntZnw7Dxtsm2qbcZsOXYBtuutm22fWFnYhdnt8Wuw+6TvZN9un2N/T0HDYfZDqsdWh1+c7RyFDpWOt6azpzuP33F9JbpL2dYzxDP2DPjthPLKcRpnVOb00dnF2e5c4PziIuJS4LLLpc+Lpsbxt3IveRKdPVxXeF60vWdm7Obwu2o26/uNu5p7ofcn8w0nymeWTNz0MPIQ+BR5dE/C5+VMGvfrH5PQ0+BZ7XnIy9jL5FXrdewt6V3qvdh7xc+9j5yn+M+4zw33jLeWV/MN8C3yLfLT8Nvnl+F30N/I/9k/3r/0QCngCUBZwOJgUGBWwL7+Hp8Ib+OPzrbZfay2e1BjKC5QRVBj4KtguXBrSFoyOyQrSH355jOkc5pDoVQfujW0Adh5mGLw34MJ4WHhVeGP45wiFga0TGXNXfR3ENz30T6RJZE3ptnMU85ry1KNSo+qi5qPNo3ujS6P8YuZlnM1VidWElsSxw5LiquNm5svt/87fOH4p3iC+N7F5gvyF1weaHOwvSFpxapLhIsOpZATIhOOJTwQRAqqBaMJfITdyWOCnnCHcJnIi/RNtGI2ENcKh5O8kgqTXqS7JG8NXkkxTOlLOW5hCepkLxMDUzdmzqeFpp2IG0yPTq9MYOSkZBxQqohTZO2Z+pn5mZ2y6xlhbL+xW6Lty8elQfJa7OQrAVZLQq2QqboVFoo1yoHsmdlV2a/zYnKOZarnivN7cyzytuQN5zvn//tEsIS4ZK2pYZLVy0dWOa9rGo5sjxxedsK4xUFK4ZWBqw8uIq2Km3VT6vtV5eufr0mek1rgV7ByoLBtQFr6wtVCuWFfevc1+1dT1gvWd+1YfqGnRs+FYmKrhTbF5cVf9go3HjlG4dvyr+Z3JS0qavEuWTPZtJm6ebeLZ5bDpaql+aXDm4N2dq0Dd9WtO319kXbL5fNKNu7g7ZDuaO/PLi8ZafJzs07P1SkVPRU+lQ27tLdtWHX+G7R7ht7vPY07NXbW7z3/T7JvttVAVVN1WbVZftJ+7P3P66Jqun4lvttXa1ObXHtxwPSA/0HIw6217nU1R3SPVRSj9Yr60cOxx++/p3vdy0NNg1VjZzG4iNwRHnk6fcJ3/ceDTradox7rOEH0x92HWcdL2pCmvKaRptTmvtbYlu6T8w+0dbq3nr8R9sfD5w0PFl5SvNUyWna6YLTk2fyz4ydlZ19fi753GDborZ752PO32oPb++6EHTh0kX/i+c7vDvOXPK4dPKy2+UTV7hXmq86X23qdOo8/pPTT8e7nLuarrlca7nuer21e2b36RueN87d9L158Rb/1tWeOT3dvfN6b/fF9/XfFt1+cif9zsu72Xcn7q28T7xf9EDtQdlD3YfVP1v+3Njv3H9qwHeg89HcR/cGhYPP/pH1jw9DBY+Zj8uGDYbrnjg+OTniP3L96fynQ89kzyaeF/6i/suuFxYvfvjV69fO0ZjRoZfyl5O/bXyl/erA6xmv28bCxh6+yXgzMV70VvvtwXfcdx3vo98PT+R8IH8o/2j5sfVT0Kf7kxmTk/8EA5jz/GMzLdsAAAAgY0hSTQAAeiUAAICDAAD5/wAAgOkAAHUwAADqYAAAOpgAABdvkl/FRgAAAqlJREFUeNp8k11IFFEUx38zc0dXXfuQyi216AMrlgwjSjEirBehj4ewDwp66IvAB+3FB6OgECMKeqgohTAiiLAMeksw6iEJDIoCDUvK2j7WYdZJd3fWnZnbg9dYeujC4XDOvf9zz73//9Hulu+R/GcZ0kCXAiEFQpoImYeQeZhBHqbMQ6xKVlGbuLAPKFCYdA5eA0xAAAbgAVnlC9JG3x292tmGAk8DNvAtx74/4OLmcZ53b6emBEgCFhAD7Kd+CvEocGZvmwLiwPecDkI3iWfuM8xbnAIgABLAOOC18hlxhq8ALuAAv9UTTNV+tl9kAoQLWd/E/5t3gcnhpYWI0RVhGEUncriOSEMH+vQwofAaTAPSI51kbY1lq8E7e4K5S0oRMok9dI0nTX00VqOzbwOATvXCIk5ugcNRycvGFiqHujhaV0nD8mLO7YBd823u1V+m3h3k+vEmVlaup7UandPRmQLR/Hz2FsOL9mdAiK62V5zcco1I3KN0EHra3wEuN1oHcHuLaIjWMN6N4NftGboSgwZjt+DzGxeYBH4AFs7AJB9iEPtoAxav3k8x2gPuyDy+9CIYe6w4GIGf0yAnUsAvYAywCs2RyVDmG6kFGVxwiraGQ+X5b0hkLLdizhR6bckQACuKbHaVfWRRQdpXv+wC6Z0rx73ja2Psbwo2AXOPtAS7T62Op7x+50tzlYVoXmcxALJmcdo/FvVom+8l4zNUekBwoCLpbSwDaxORK55x1c9kUpfOp7vsmLQPFibRpIfUBIdmAf8I6jcQASqB5UBISdkBQvIhnYIHoICOMktJdkodTgCfgAlVIABSQJgOEHQAkK/mwVWDE1a5QMVC7flK4h6gZV8LtIxuSolGoGlIdHxtxgLNwEfH0ww83cDXBFlNkNVzvcmfAQCcOxan14psMgAAAABJRU5ErkJggg=="
class RBComponent
    globalInitDone = false
    constructor:(@name) ->
        if !globalInitDone
            console.log("Component inited: " + @name)
            @.global_init()
            globalInitDone = true

        @_active = true #false switches off update
    is_global_inited:() ->
        return globalInitDone
    global_deinit:() ->
        globalInitDone = false
    global_init:() ->
        #override this
    deactivate: () ->
        @_active = false
    activate: () ->
        @_active = true
    is_active: () ->
        return @_active
    update: (dt) ->
        if @_active
            a = 1
    on_add: (parentEnt) ->
        @parent = parentEnt
    on_remove: () ->
        @deactivate()
        @parent = null
#holds variables and methods for the global physics simulation,
#used by all RBPhysics components. This will be in the RB global
#named dictionary - RB().env("RBPhysicsSystem")
class RBPhysicsSystem
    constructor: (paramdict = {}) ->
        @gravity = 260#9.8
        @viscosity = 0.1
        @default_drag = 0.1
        @maxVelocity = 100 #pixels per second?
        #Override with paramdict
        for prop, val in paramdict
            @[prop] = val

class RBPhysics extends RBComponent
    constructor: (name="physics", systemname="default") ->
        super(name)
        @parent = null
        @physics = null
        @.set_system(systemname)
        
        @velocity = new RBPoint()
        @maxVelocity = new RBPoint()
        
        @acceleration = new RBPoint()
        @angularAcceleration = 0
        @angularVelocity = 0
        @maxAngularVelocity = 0
        
        @drag = new RBPoint(@physics.default_drag, @physics.default_drag)
        @angularDrag = 0
        
        @solid = true #collidable
        @staticphysics = false #true=can never move
        @floating = false #true=can move, not affeced by grav

    global_init: () ->
        super()
        systems = RB().env("RBPhysicsSystems")
        if not systems?
            systems = {}
            RB().env("RBPhysicsSystems", systems)
        systems["default"] = new RBPhysicsSystem()
        console.log("global_init done for RBPhysics")

    set_system: (systemname) ->
        systems = RB().env("RBPhysicsSystems")
        if systems[systemname]?
            @physics = systems[systemname]
        else
            @physics = systems["default"]

    update: (dt) ->
        super(dt)
        if @_active
            #console.log("update phys")
            if not @staticphysics
                norm_dt = dt/1000
                if not @floating
                    @velocity.y += @physics.gravity * norm_dt
                
                dx = @velocity.x * norm_dt
                dy = @velocity.y * norm_dt
                #TODO maxvelocity
                @parent.parentOffset.x += dx
                @parent.parentOffset.y += dy
                @parent.move_by(dx, dy) #also does a refresh
                @parent.dirty = true
                #TODO: dirty = true
                
#requires RBBase, RBGroup, RBMedia, RBInput, RBTimer

# Rhubarb
# =======
# _A Game engine for HTML5 and Javascript_

_RB = null

# Returns the current game engine.
#
# @return [RBInit] The game engine
RB = () ->
    return _RB
# A Function for getting or setting the Rhubarb Variables collection
# of the game engine.
# Calling this function with a key only returns the value of the
# collection at that key. There are 5 predefined key constants for 
# vaious # engine settings sections:
# @note
#   RBV_ENG : For engine settings (fps, screen dimensions, etc)
#   RBV_IMG : For images used by your game
#   RBV_SND : For sounds used by your game
#   RB_SS   : For spritesheets used by your game
#   RBV_GAME: For miscellaneous variables/constants youd like to
#       set for your game.
#   
# Calling this function with a key and a value sets the collection
#   at that key.
#
# @param [String] key
#   A key to store data against. Default predefined constants are
#       {RBV_ENG}, {RBV_GAME}, {RBV_IMG}, {RBV_SS}, and {RBV_SND}
# @param [*] value [Optional]
#   Any value to store against this key.
#
# @example
#   RBV(RBV_GAME.max_enemies) returns the 'max_enemies' value from RBV_GAME
#   RBV(RBV_ENG.screenwidth) returns the 'screenwidth' value from RBV_ENG
#   RBV(RBV_IMG).playerimage = "data/player.png"
#
#   This class now does a translation on RBV_ENG and related values,
#   so that you can use those as variables to pull data out.
#   So you could either do:
#   RBV_ENG.<someimagename>
#   or
#   RBV(RBV_ENG).<someimagename>
RBV_SS = {}
RBV_ENG = {}
RBV_IMG = {}
RBV_SND = {}
RBV_GAME = {}
RBV_MUSIC = {}

RBV = (key, value) ->
    s = RB().env("settings")
    if not key?
        return s
    if not value?
        if key in [RBV_SS, RBV_ENG, RBV_GAME, RBV_IMG, RBV_SND, RBV_MUSIC]
            switch (key)
                when RBV_SS then return s[RBV_SS_STR]
                when RBV_IMG then return s[RBV_IMG_STR]
                when RBV_ENG then return s[RBV_ENG_STR]
                when RBV_SND then return s[RBV_SND_STR]
                when RBV_MUSIC then return s[RBV_MUSIC_STR]
                when RBV_GAME then return s[RBV_GAME_STR]
        else
            return s[key]
    else if key in [RBV_SS, RBV_ENG, RBV_GAME, RBV_IMG, RBV_SND, RBV_MUSIC] and typeof value == "object"
        collection = RBV(key)
        for valkey, val of value
            collection[valkey] = value[valkey]
    else
        s[key] = value


class RBStats
    constructor:() ->
        @_RB = RB()
        @updatetimer = new RBTimer()
        @drawtimer = new RBTimer()
        @updatetime = 0
        @drawtime = 0

    startupdate: () ->
        @updatetimer.reset()
    startdraw: () ->
        @drawtimer.reset()
    endupdate: () ->
        @updatetime = ((@updatetime + @updatetimer.elapsed()) /2) | 0
    enddraw: () ->
        @drawtime = ((@drawtime + @drawtimer.elapsed())/2) | 0

    draw: () =>
        scr = RB().canvas()
        fsize = Math.max(12, Math.floor(scr.width/40))
        currline = fsize
        writeline = (str) =>
            @_RB.cctx.fillText(str, 5, currline)
            currline += fsize
        @_RB.cctx.fillStyle = "rgba(255, 255, 255, 0.3)"
        @_RB.cctx.fillRect(0, 0, scr.width/3, scr.height/4)
        @_RB.cctx.fillStyle = "#000000"
        @_RB.cctx.font = "#{fsize}px helvetica"
        writeline("Rhubarb v#{RB_VERSION}")
        writeline("Frame: #{@_RB.dt().toString()}ms")
        writeline("Update: #{@updatetime}ms")
        writeline("Draw: #{@drawtime}ms")
        writeline("Idle: #{(@_RB.dt() - (@updatetime + @drawtime)).toString()}")
        writeline("Created: #{@_RB._entityCount}")
        writeline("Destroyed: #{@_RB._destroyedEntityCount}")
        if @_RB._offscreenCulling
            writeline("Culled #{@_RB._culledEntityCount}")



# RBInit is the object which creates the entire game engine.
# Instancing this class returns an object which holds the 
# state of the game engine. Once instanced, this class should
# not be instanced again. If you want global access to the game
# engine, call the .RB() function.
class RBInit
    # @private 
    #   Logging object
    _log: null

    # @private 
    #   The function that will be called as the game main loop
    _mainloop: null

    #@private 
    #   The internal timer for RB
    _timer: null

    # @private 
    #   The 'dictionary' used by the RB.env() API @see env() 
    _namedCollection: null

    # @private 
    #   The number of milliseconds since the last engine tick.
    _dt: 0

    # @private 
    #   The interval object from the setInterval() for 
    #   the main loop
    _mainloopinterval: null

    # @private 
    #   The FPS requested by the client
    _desiredfps: 1

    # @private 
    #   How many ms each interval should be to get @_desiredfps
    _frametick: 0

    # @private 
    #   Should timestep be locked? i.e. constant 1000/fps? 
    #   (experimental)
    #
    _lockstep: false

    # @private 
    #   Starting value for RB Object ID's
    _globalObjectID: 1

    # @private 
    #   RB Destroy list, used for cleaning up entities 
    #   after destroy.
    _destroylist: null

    # @private 
    #   A list for collisions, to save being recreated 
    #   each frame
    _collisions: null

    # @private 
    #   A group for UI Objects to be tracked (experimental)
    _uiobjects: null

    # @private 
    #   Rect describing screen coords
    _screenRect: null

    # @private 
    #   Rect describing canvs coords
    _canvasRect: null

    # @private Flag to indicate if image interpolation should be
    #   on or off for the canvas
    _canvasSmoothing: false

    # Don't draw elements when they're offscreen
    _offscreenCulling: false
    _culledEntityCount: 0
    _entityCount: 0
    _destroyedEntityCount: 0

    # @private
    #   Whether or not we are in debug mode
    _debug: false

    # @private
    #   Array of debug categories
    _debugs: []
    
    # @private
    #   If paused is true, no updates are done
    _paused: false

    # @property [HTMLCanvas]
    # The context accessible to elements trying to draw
    # All elements should draw to RB().cctx
    cctx: null

    # @private
    # Internal context stack
    _cctxstack: []

    # @private 
    #   The current canvas object
    _canvas: null

    # @private 
    #   The current internal canvas context
    _context: null

    # @property [RBInput]
    # Global RB Input object
    input: null

    # @property [RBDelegateCollection]
    # Global events object
    events: null

    # @property [RBMedia]
    # Global RB media repository for the game (images  / sounds)
    media: null

    # @property [RBActionList]
    # Global RB Actions List
    actions: null
    
    # @property [RBPreloader]
    # Preloader
    preloader: null

    #collisionFlagAbsolute: false


    constructor: (debug = false) ->
        _RB = @ #set the global
        @_RB = @
        @_log = new RBLog(true)
        @_log.enable()
        @_debug = debug
        @_namedCollection = {}
        @_timer = new RBTimer()

        @_uiobjects = new RBGroup()
        @_screenRect = new RBRect(0,0,0,0)
        @_canvasRect = new RBRect(0,0,0,0)

        @_destroylist = []
        @_collisions = []
        
        # Global RB Input object
        @input = new RBInput(@)
        # Global events object
        @events = new RBDelegateCollection("RB Global Events", 
            [RB_EVENT_ON_START, 
            RB_EVENT_ON_STOP, 
            RB_EVENT_ON_PAUSE, 
            RB_EVENT_ON_UNPAUSE])
        # Global RB media repository for the game (images  / sounds)
        @media = new RBMedia()
        # Global RB Actions List
        @actions = new RBActionList()
        # @property [PreloaderStats] Preloader
        @preloader = new PreLoaderStats()

        settings = {}
        RBV_ENG.fps = 30
        RBV_ENG.canvaswidth = 400
        RBV_ENG.screenwidth = 400
        RBV_ENG.screenheight = 400
        RBV_ENG.canvassmoothing = false

        settings[RBV_ENG_STR] = RBV_ENG
        settings[RBV_GAME_STR] = RBV_GAME
        settings[RBV_IMG_STR] = RBV_IMG
        settings[RBV_SND_STR] = RBV_SND
        settings[RBV_MUSIC_STR] = RBV_MUSIC
        settings[RBV_SS_STR] = RBV_SS

        @.env("settings", settings)

        
    # Set or retrieve a 'global' named value that you are registering
    # with RB.
    # It is a good place to store a reference to a variable you will
    # need in many places - for example the player object.
    # @example RB().env("Player", playerobj) # you now have a registred 
    # 'Player' object.
    # @example RB().env("Player") #returns the current object registerd
    # under the 'Player' name
    env: (name, value) ->
        if value?
            @_namedCollection[name] = value
        return @_namedCollection[name]

    calculate_screen_scale: () ->
        #These lines are needed so that an RBGame knows how to scale.
        xscale = 1
        yscale = 1
        if @_screenRect.width != 0
            xscale = @_canvasRect.width / @_screenRect.width
        if @_screenRect.height != 0
            yscale = @_canvasRect.height / @_screenRect.height
        RBV(RBV_ENG).screenscalex  = xscale
        RBV(RBV_ENG).screenscaley = yscale

    resize_screen: (w, h) ->
        RBV(RBV_ENG).screenwidth = w
        RBV(RBV_ENG).screenheight = h
        @_screenRect.set_width(w).set_height(h)
        @_screenRect.set_anchor(0,0)
        @.calculate_screen_scale()
        @_canvasSmoothing = @.set_smoothing(@cctx, @_canvasSmoothing)

    resize_canvas: (w, h) ->
        RBV(RBV_ENG).canvaswidth = w
        RBV(RBV_ENG).canvasheight = h
        #resize_canvas: (w=window.innerWidth, h = window.innerHeight) =>
        @_RB.get_canvas().width = w
        @_RB.get_canvas().height = h
        canvaspos = get_cumulative_offset(@_canvas)
        @_canvas.x = canvaspos.x
        @_canvas.y = canvaspos.y
        @_canvasRect.x = canvaspos.x
        @_canvasRect.y = canvaspos.y
        @_canvasRect.set_width(w).set_height(h)
        @_canvasRect.set_anchor(0,0)
        @.calculate_screen_scale()
        @_canvasSmoothing =  @.set_smoothing(@cctx, @_canvasSmoothing)

    
    # Gets the current screen rect
    screen: () ->
        @_screenRect
    # Gets the current canvas rect
    canvas: () ->
        @_canvasRect

    # @private
    log: (msg) ->
        @_log.log(msg)
    # @private
    debug: (msg) ->
        @_log.debug(msg)
    # @private
    warn: (msg) ->
        @_log.warn(msg)
    # @private
    error: (msg) ->
        @_log.error(msg)

    # Switches the current context that RB clients should draw to.
    set_cctx: (ctx) ->
        @cctx = ctx

    push_cctx: () ->
        @_cctxstack.push(@cctx)
    restore_cctx: () ->
        c = @_cctxstack.pop(@cctx)
        if c?
            @.set_cctx(c)
        else
            console.warn("No context to restore off cctxstack")
    # Gets the canvas object
    get_canvas: () ->
        @_canvas
    
    # Sets image interpolation for the canvas either on or off
    set_smoothing: (ctx, val) ->
        if ctx isnt null
            ctx.imageSmoothingEnabled = val
            ctx.webkitImageSmoothingEnabled = val
            ctx.mozImageSmoothingEnabled = val
        return val

    # Pauses the engine
    pause: () ->
        if !@_paused
            @.stop(true)
            console.log("pausing")
    
    # Unpauses the engine
    unpause: () ->
        if @_paused
            @.start(true)
            console.log("unpausing")
    # Toggles pause state with successive calls
    toggle_pause: () ->
        if @_paused
            @.unpause()
        else
            @.pause()

    # Starts the game engine
    # @param [Boolean] fire_start_event
    #   Whether or not to allow the engine to
    #   raise the RB_EVENT_ON_START event. A
    #   false value will suppress it
    start: (fire_start_event = true) ->
        @_paused = false
        @.reset_frame_rate(@_desiredfps, @_mainloop)
        if fire_start_event
            @events.call_delegates(RB_EVENT_ON_START)

    # Stops the game engine
    # @param [Boolean] fire_stop_event
    #   Whether or not to allow the engine to
    #   raise the RB_EVENT_ON_STOP event. A
    #   false value will suppress it
    stop: (fire_stop_event = true) ->
        @_paused = true
        @.reset_frame_rate(0)
        if fire_stop_event
            @events.call_delegates(RB_EVENT_ON_STOP)

    # Returns the currently running scene (if any)
    current_scene: () ->
        sm = RBSceneManager.get_instance()
        if sm?
            return sm.current_scene()
        else
            return null


    # Set the screen parameters.
    # @param canvaselement [Object] The HTML5 canvas element
    # @param canvaswidth [Number] The width of the canvas
    # @param canvasheight [Number] The height of the canvas
    # @param screenwidth [Number] The 'virtual' width of our game screen 
    #  <=canvaswidth, default is canvaswidth
    # @param sreenheight [Number] The 'virtual' width of our game screen 
    #  <=canvasheight, default is canvasheight
    #
    # So you could set screenwidth and screenheight to be less than
    # canvaswidth and canvasheight if you wanted to run the game at a 
    # lower resolution than the canvas.
    set_screen: (canvaselement, canvaswidth, canvasheight, screenwidth=canvaswidth, screenheight=canvasheight, smoothing=false) ->
        
        @_canvas = canvaselement
        #disable double-click select of text outside canvas:
        @_canvas.onselectstart = () ->
            return false

        @_context = canvaselement.getContext('2d')
        @cctx = @_context
        @.resize_canvas(canvaswidth, canvasheight)
        #canvaspos = get_cumulative_offset(@_canvas)
        #@_canvas.x = canvaspos.x
        #@_canvas.y = canvaspos.y
        #@_canvasRect.x = canvaspos.x
        #@_canvasRect.y = canvaspos.y
        #@_canvasRect.refresh()
        #Resize the canvas with RBInput
        #@input.resize_canvas(canvaswidth, canvasheight)
        
        @.resize_screen(screenwidth, screenheight)
        #@_screenRect.set_width(screenwidth).set_height(screenheight)
        #@_screenRect.set_anchor(0,0)
        #@_canvasRect.set_width(canvaswidth).set_height(canvasheight)
        #@_canvasRect.set_anchor(0,0)
        #RB().log(@_screenRect)

        @.env("width", canvaswidth)
        @.env("height", canvasheight)
        
        #These lines are needed so that an RBGame knows how to scale.
        #RBV(RBV_ENG).screenscalex = canvaswidth / screenwidth
        #RBV(RBV_ENG).screenscaley = canvasheight / screenheight
        

        #if our window loses focus, automatically push
        #the game into lockstep mode, so that our dt remains
        #constant while we are away. This prevents gravity pulling us
        #through the floor when we return to focus because dt doesnt
        #become massive, since the time since the last interval fire
        #is irrelevant in lockstep mode.
        window.onblur = () -> 
            _RB.lockstep(true)
        window.onfocus = () -> 
            _RB.lockstep(false)
            _RB._timer.reset()
        
        #This line attaches the log to the HTML document.
        #Commented out at the moment because I'm undecided
        #about using it.
        #@_log.attach()

        @_canvasSmoothing = @.set_smoothing(@cctx, smoothing)
    
    # Sets the game framerate according to the desired fps
    # Calling with an fps of 0 or mainloop of null will stop the game
    # and clear the timer callback
    # @param [Number] fps
    #   The desired frames per second
    # @param [function] mainloop
    #   The functionto call each frame
    reset_frame_rate: (fps, mainloop) ->
                
        if not (@_mainloopinterval is null)
            clearInterval(@_mainloopinterval)
        
        if mainloop?
            @_mainloop = mainloop

        if (fps > 0) #and (@_mainloop is not null) 
            @_desiredfps = fps
            @_frametick = 1000/fps
            #Call @.step every frametick
            @_mainloopinterval = setInterval(@step, @_frametick)
        #Reset the @_timer and @_dt
        @_dt = @_timer.elapsed()
        @_dt = 0

    # @private
    lockstep: (enable) =>
        if enable
            @_lockstep = true
            #@_dt = @_frametick
        else
            @_lockstep = false
            #@.reset_frame_rate(@_desiredfps)
            #@_dt = 0

    # @private
    step: () =>
        if @_lockstep
            @_dt = @_frametick
        else
            @_dt = @_timer.elapsed()
        #Reset culled entitycount
        @_culledEntityCount = 0
        @_mainloop(@_dt)

    # Gets the time since the last frame, in milliseconds
    dt: () ->
        @_dt

    # @private
    # pass in the category as a string
    # available:
    # "bb" (bounding boxes)
    # nothing (default 'debug is on')
    isDebug: (category) ->
        if !category?
            @_debug
        else
            if @_debugs[category]?
                @_debugs[category]
            else
                false #better than returning null

    # @private
    setDebug: (val, category) ->
        if !category?
            @_debug = val
            console.log("RB._debug is now #{@_debug}")
        else
            @_debugs[category] = val
            console.log("RB._debugs[#{category}] is now #{val}")
        

    # @private
    newID: () ->
        @_globalObjectID++

    # @private
    add_ui_object: (obj) ->
        @._uiobjects.add_member(obj)

    # @private
    remove_ui_object: (obj) ->
        @._uiobjects.remove_member(obj)

    # @private
    destroy_entities: () ->
        for ent in @_destroylist
            console.log('destroy')
            ent._destroy()
            ent.cancel_actions()
            @input.mouse.unregister_drag(ent)
            @_destroyedEntityCount+=1

        while @_destroylist.length
            #console.log('pop')
            @_destroylist.pop()

    # Collides one array of RBEntity objects with another.
    # Each group needs to be an array object, so passing
    # an array you have made, or passing an RBEnitty.children
    # or RBGroup.members property is fine.
    #
    # @param [Array<RBEntity>] group1
    #   The first group to consider for collision
    # @param [Array<RBEntity>] group2
    #   The second group to consider for collision
    #
    collide: (group1, group2) ->
 
        
        build_collisions = (gr1, gr2, col) ->

            try
                for g1 in gr1
                    for g2 in gr2
                        if g1.solid and g2.solid and !g1.destroyed and !g2.destroyed
                            if g1.overlaps(g2)
                                col.push([g1, g2])
                            else
                                if g1.members? and g1.members.count > 0
                                    #console.log("build_collisions recurse 1");
                                    build_collisions(g1.members, g2, col)
                                if g2.members? and g2.members.count ? 0
                                    #console.log("build_collisions recurse 2");
                                    build_collisions(g1, g2.members, col)
            catch error
                console.log("Error in collide phase 1")
                console.log(error)

        build_collisions3 = (gr1, gr2, col) ->
            try
                for g1 in gr1
                    for g2 in gr2
                        if g1 != g2 and !g1.destroyed and !g2.destroyed
                            if g1.solid
                                if g2.solid
                                    if g1.overlaps_in_rlist([g2]) != null
                                        col.push([g1, g2])
                                    else
                                        if g2.overlaps_in_rlist([g1]) != null
                                            col.push([g1, g2])
                                else if g2.members? and g2.members.count > 0
                                    #c = g1.overlaps_in_rlist(g2.members)
                                    #if c != null
                                    #    col.push([g1, c])
                                    build_collisions3([g1], g2.members, col)
                            else if g1.members? and g1.members.count > 0
                                #c = g2.overlaps_in_rlist(g1.members)
                                #if c != null
                                #    col.push([c, g2])
                                build_collisions3(g1.members, [g2], col)
            catch error
                console.log("Error in collide phase 1")
                console.log(error)

        build_collisions2 = (gr1, gr2, col) ->
            for g1 in gr1
                c = g1.overlaps_in_rlist(gr2)
                if c != null #something in children collided
                    col.push([g1, c])
                else if g1.members?
                    build_collisions2(g1.members, gr2, col)
          
        resolve_collisions2 = (collist) ->
            try
                for colitems in collist
                    g1 = colitems[0]
                    g2 = colitems[1]
                    #topdiff = g2.top - g1.bottom #pixels from g1.bottom->g2.top
                    #bottomdiff = g2.bottom - g1.top

                    #get the overlapping rect
                    r = l = t = b = 0
                    topofg1 = true
                    rightofg1 = true
                    bottomofg1 = true
                    leftofg1 = true
                    #because coord system is increasing towards bottom and right,
                    #intersecting rect is max tops, min bottoms, min rights, max lefts
                    if g1.top > g2.top
                        t = g1.top
                    else
                        t = g2.top
                        topofg1 = false

                    if g1.bottom < g2.bottom
                        b = g1.bottom
                    else
                        b = g2.bottom
                        bottomofg1 = false

                    if g1.right < g2.right
                        r = g1.right
                    else
                        r = g2.right
                        rightofg1 = false

                    if g1.left > g2.left
                        l = g1.left
                    else
                        l = g2.left
                        leftofg1 = false

                    leftrightcoll = true #a left-right collision, not top bottom
                    if r-l < b-t
                        leftrightcoll = true
                        if leftofg1 #but only if not right of g1 also
                            g1.hit_left(g2, 0)
                            g2.hit_right(g1, 0)
                            if @_debug
                                g1.debugColour.set_rgb(RB_DEBUG_HIT_LEFT)
                                g2.debugColour.set_rgb(RB_DEBUG_HIT_RIGHT)
                        else
                            g1.hit_right(g2, 0)
                            g2.hit_left(g1, 0)
                            if @_debug
                                g1.debugColour.set_rgb(RB_DEBUG_HIT_RIGHT)
                                g2.debugColour.set_rgb(RB_DEBUG_HIT_LEFT)
                    else
                        leftrightcoll = false
                        if topofg1 #but only if not bottom of g1 also
                            g1.hit_top(g2, 0)
                            g2.hit_bottom(g1, 0)
                            if @_debug
                                g1.debugColour.set_rgb(RB_DEBUG_HIT_TOP)
                                g2.debugColour.set_rgb(RB_DEBUG_HIT_BOTTOM)
                        else
                            g1.hit_bottom(g2, 0)
                            g2.hit_top(g1, 0)
                            if @_debug
                                g1.debugColour.set_rgb(RB_DEBUG_HIT_BOTTOM)
                                g2.debugColour.set_rgb(RB_DEBUG_HIT_TOP)
            catch error
                console.log("Error in collide phase 2")
                console.log(error)


                    

        resolve_collisions = (collist) ->

            try
                for colitems in collist
                    #if @_debug
                    #    console.log(g1, g2)
                    g1 = colitems[0]
                    g2 = colitems[1]
                    #console.log(g1, g2)
                    # To work out if we are hitting
                    # left,right,up,down, we use a 
                    # pretty simple method:
                    # whichever relative velocity is
                    # the highest, that is probably
                    # the direction from which we are hitting
                    
                    #physics is now a component, and I don't want to have to check that.
                    #rel_vel = new RBPoint(g1.velocity.x, g1.velocity.y)
                    #rel_vel.sub(g2.velocity)
                    
                    #instead, entities now have @_lastposition (RBPoint)
                    rel_vel = new RBPoint(g1.x - g1._lastposition.x, g1.y - g1._lastposition.y)
                    g2vel = new RBPoint(g2.x - g2._lastposition.x, g2.y - g2._lastposition.y)
                    
                    rel_vel.sub(g2vel)
                    
                    #ok, lets try something else instead
                    if (g1.bottom > g2.top) and rel_vel.y > 0
                        g1.hit_bottom(g2, 0)
                        g2.hit_top(g1, 0)
                        if @_debug
                            g1.debugColour.set_rgb(RB_DEBUG_HIT_BOTTOM)
                            g2.debugColour.set_rgb(RB_DEBUG_HIT_TOP)
                        #break
                    else
                        if (g1.top < g2.bottom) and rel_vel.y < 0
                            g1.hit_top(g2, 0)
                            g2.hit_bottom(g1, 0)
                            if @_debug
                                g1.debugColour.set_rgb(RB_DEBUG_HIT_TOP)
                                g2.debugColour.set_rgb(RB_DEBUG_HIT_BOTTOM)
                            #break
                        else
                            if (g1.right > g2.left) and rel_vel.x > 0
                                g1.hit_right(g2, 0)
                                g2.hit_left(g1, 0)
                                if @_debug
                                    g1.debugColour.set_rgb(RB_DEBUG_HIT_RIGHT)
                                    g2.debugColour.set_rgb(RB_DEBUG_HIT_LEFT)
                                #break
                            else
                                if (g1.left < g2.right) and rel_vel.x < 0
                                    g1.hit_left(g2, 0)
                                    g2.hit_right(g1, 0)
                                    if @_debug
                                        g1.debugColour.set_rgb(RB_DEBUG_HIT_LEFT)
                                        g2.debugColour.set_rgb(RB_DEBUG_HIT_RIGHT)
                                    #break
                
            catch error
                console.log("Error in collide phase 2")
                console.log(error)



        resolve_collisions3 = (collist) ->
            _debug = RB().isDebug()
            try
                for colitems in collist
                    #if @_debug
                    #    console.log(g1, g2)
                    g1 = colitems[0]
                    g2 = colitems[1]
                    #console.log(g1, g2)
                    # To work out if we are hitting
                    # left,right,up,down, we use a 
                    # pretty simple method:
                    # whichever relative velocity is
                    # the highest, that is probably
                    # the direction from which we are hitting
                    
                    #physics is now a component, and I don't want to have to check that.
                    #rel_vel = new RBPoint(g1.velocity.x, g1.velocity.y)
                    #rel_vel.sub(g2.velocity)
                    
                    #instead, entities now have @_lastposition (RBPoint)
                    rel_vel = new RBPoint(g1.x - g1._lastposition.x, g1.y - g1._lastposition.y)
                    g2vel = new RBPoint(g2.x - g2._lastposition.x, g2.y - g2._lastposition.y)
                    
                    colitems.push(rel_vel.sub(g2vel))

                for colitems in collist
                    g1 = colitems[0]
                    g2 = colitems[1]
                    rel_vel = colitems[2]
                    
                    #ok, lets try something else instead
                    if (g1.bottom > g2.top) and rel_vel.y > 0
                        g1.hit_bottom(g2, 0)
                        g2.hit_top(g1, 0)
                        if _debug
                            g1.debugColour.set_rgb(RB_DEBUG_HIT_BOTTOM)
                            g2.debugColour.set_rgb(RB_DEBUG_HIT_TOP)
                        #break
                    else
                        if (g1.top < g2.bottom) and rel_vel.y < 0
                            g1.hit_top(g2, 0)
                            g2.hit_bottom(g1, 0)
                            if _debug
                                g1.debugColour.set_rgb(RB_DEBUG_HIT_TOP)
                                g2.debugColour.set_rgb(RB_DEBUG_HIT_BOTTOM)
                            #break
                        else
                            if (g1.right > g2.left) and rel_vel.x > 0
                                g1.hit_right(g2, 0)
                                g2.hit_left(g1, 0)
                                if _debug
                                    g1.debugColour.set_rgb(RB_DEBUG_HIT_RIGHT)
                                    g2.debugColour.set_rgb(RB_DEBUG_HIT_LEFT)
                                #break
                            else
                                if (g1.left < g2.right) and rel_vel.x < 0
                                    g1.hit_left(g2, 0)
                                    g2.hit_right(g1, 0)
                                    if _debug
                                        g1.debugColour.set_rgb(RB_DEBUG_HIT_LEFT)
                                        g2.debugColour.set_rgb(RB_DEBUG_HIT_RIGHT)
                                    #break
                
            catch error
                console.log("Error in collide phase 2")
                console.log(error)

        resolve_collisions4 = (collist) ->
            _debug = RB().isDebug()
            crect = new RBRect()
            try
                for colitems in collist
                    #if @_debug
                    #    console.log(g1, g2)
                    g1 = colitems[0]
                    g2 = colitems[1]
                    #console.log(g1, g2)
                    # To work out if we are hitting
                    # left,right,up,down, we use a 
                    # pretty simple method:
                    # whichever relative velocity is
                    # the highest, that is probably
                    # the direction from which we are hitting
                    
                    #physics is now a component, and I don't want to have to check that.
                    #rel_vel = new RBPoint(g1.velocity.x, g1.velocity.y)
                    #rel_vel.sub(g2.velocity)
                    
                    
                    #FOR NOW, WE DON'T NEED REL VEL
                    #IF WE FIND WE DO, BUILD IT IN BUILD_COLLISIONS
                    #ACTUALLY THAT MAY BE SLOWER. BUILDS A LOT OF NEEDLESS
                    #RELVELS FOR THINGS THAT DON'T OVERLAP
                    #instead, entities now have @_lastposition (RBPoint)
                    #rel_vel = new RBPoint(g1.x - g1._lastposition.x, g1.y - g1._lastposition.y)
                    #g2vel = new RBPoint(g2.x - g2._lastposition.x, g2.y - g2._lastposition.y)
                    
                    #colitems.push(rel_vel.sub(g2vel))

                for colitems in collist
                    g1 = colitems[0]
                    g2 = colitems[1]
                    rel_vel = colitems[2]
                    
                    g1.overlaps(g2, crect)
                    if crect.width > crect.height
                        #its a top / bottom collision
                        if crect.y >0 
                            g1.hit_bottom(g2, 0)
                            g2.hit_top(g1, 0)
                            if _debug
                                g1.debugColour.set_rgb(RB_DEBUG_HIT_BOTTOM)
                                g2.debugColour.set_rgb(RB_DEBUG_HIT_TOP)
                        else
                            g1.hit_top(g2, 0)
                            g2.hit_bottom(g1, 0)
                            if _debug
                                g1.debugColour.set_rgb(RB_DEBUG_HIT_TOP)
                                g2.debugColour.set_rgb(RB_DEBUG_HIT_BOTTOM)
                    else
                        #its a right left collision
                        if crect.x > 0
                            g1.hit_right(g2, 0)
                            g2.hit_left(g1, 0)
                            if _debug
                                g1.debugColour.set_rgb(RB_DEBUG_HIT_RIGHT)
                                g2.debugColour.set_rgb(RB_DEBUG_HIT_LEFT)
                        else
                            g1.hit_left(g2, 0)
                            g2.hit_right(g1, 0)
                            if _debug
                                g1.debugColour.set_rgb(RB_DEBUG_HIT_LEFT)
                                g2.debugColour.set_rgb(RB_DEBUG_HIT_RIGHT)

            catch error
                console.log("Error in collide phase 2")
                

        if @_debug
            elem.debugColour.set_rgb(RB_DEBUG_HIT_NONE) for elem in group1
            elem.debugColour.set_rgb(RB_DEBUG_HIT_NONE) for elem in group2
        #build_collisions(group1, group2, @collisions)
        #build_collisions2(group1, group2, @collisions)
        build_collisions3(group1, group2, @_collisions)

        resolve_collisions4(@_collisions)
        #resolve_collisions2(@_collisions)

        while @_collisions.length
            @_collisions.pop()

_RB = new RBInit(false)

#Animation frameset.
 # Pass in the width and height of your animation frame, and 
 # the list of indexes in your image that make up the 
 # animation. You can also pass in a frametime,
 # Playtype: string, values can be "Normal", "Loop", "Pingpong" (pingpong TODO)
 # Pingpong: bool, whether self animation should loop alternately backwards
 # 
 # Internally we have:
 # currentFrame: which frame of animation we are at
 # totalFrames: how many frames in the animation list
 # currentImageIndex: the value at the currentFrame
 # isFinished: if we have finished playing the animation 
 #              (false while loop or pingpong is true)
 # sx, sy: The current x and y offsets needed to draw self slice of image
 #

class RBAnimation
    #constructor: (@xframes=1, @yframes=1, @width, @height, framelist, @xoffset=0, @yoffset=0, @framedt=100, playtype = "Normal") ->
    constructor: (@spritesheet, framelist, playtype = RB_ANIM_NORMAL, @framedt=100, @xoffset=0, @yoffset=0) ->
        @_frames = framelist
        @isLoop = @isPingPong = @isNormal = false
        @currentFrame = 0
        @totalFrames = 0
        @currentImageIndex = 0
        @isFinished = false
        @animationTimer = 0
        @sx = 0
        @sy = 0
        @currentFrameRect = new RBRect(0, 0, 0, 0)
        @on_finished = new RBDelegateGroup()
        #looping options
        if playtype is RB_ANIM_LOOP
            @isLoop = true
        #not yet supporting ping pong
        else if playtype is RB_ANIM_NORMAL
            @isNormal = true

        @totalFrames = @_frames.length
        @currentImageIndex = @_frames[@currentFrame]
        if @spritesheet != null
            #console.log("anim (#{framelist}) initial framerect (frame #{@currentImageIndex}): ")
            #console.log(@currentFrameRect)
            #console.log(@spritesheet)
            @currentFrameRect = @spritesheet.frame_at(@currentImageIndex, @currentFrameRect, @xoffset, @yoffset)
        #else
        #    console.log("spritesheet was null in constructor of anim")

    loop: () ->
        @isLoop = true
        @isNormal = false
    normal: () ->
        @isNormal = true
        @isLoop = false

    reset: () ->
        @isFinished = false
        @currentFrame = 0

    next: (dt) ->
        if not @isFinished
            @animationTimer += dt
            if @animationTimer >= @framedt
                if @currentFrame < @totalFrames-1
                    @animationTimer = 0
                    @currentFrame++
                else if @isLoop
                    @animationTimer = 0
                    @currentFrame = 0
                else
                    @isFinished = true
                    @currentFrame = @totalFrames-1 #stay on last frame
                    @on_finished.call_delegates()
                @currentImageIndex = @_frames[@currentFrame]
                if @spritesheet != null
                    @currentFrameRect = @spritesheet.frame_at(@currentImageIndex, @currentFrameRect, @xoffset, @yoffset)
                #else
                #    console.log("Spritesheet was null!")
        @ #return this


# The Basic "Entity" in RB games. RBEntity is the base type from which
# most useful things in Rhubarb come.
#
# Having inherited from {RBPoint} and {RBRect}, they have a box-like structure (x, y, width, height).
# They also have the ability to be collided with other entities, and can
# have children attached, who will gain scale, postion, and alpha value traits from
# their parent.
# 
# RBEntities (and their subclassed variations, notably RBSprite) can have actions 
# run on them to transform them over time - see {RBAction}.
#
# When collisions happen RBEntities have four functions which get called by default:
# hit_top, hit_bottom, hit_right, and hit_left - each of these is passed the 
# colliding object as the first argument.
#
# The main two methods of RBEntity which are useful are update(dt) and draw().
# Both of these methods will operate on all their children also, automatically. So
# adding children to an RBEntity object effectly groups all these objects, and
# only the root object update/draw needs to be called in order to draw all these
# entities.
#
# Note that by default, an RBEntity draw call draws nothing for itself, and then
# draws all it's children - an RBEntity is a 'blank' object and doesn't have anything
# inherently drawable about it.
class RBEntity extends RBGroup
    # @private
    # @property [Number] 
    #   Is the entity rotation, in radians. The accessors {#get_angle}
    #   and {#set_angle} allow you to manipulate this property *in degrees*.
    angle: 0
    
    # @property [Boolean] 
    #   Sets whether or not this object is collideable.
    #   If **true**, this entity will be considered when detecting collisions, 
    #   otherwise it will be ignored.
    # Default: true
    solid: true

    # @property [Boolean] 
    #   Sets whether or not this object will be drawn.
    #
    #   Default: true
    visible: true

    # @property [Boolean] 
    #   Sets whether this entity gets updated or not. Setting this to 
    #   false effectively disables this entity's update loop.
    #
    #   Default: true
    active: true

    # @property [RBEntity] 
    #   The parent of this object (if any). Do not set this variable,
    #   only read from it.
    parent: null

    # @property [Boolean] 
    #   Whether or not this entity's position should be considered as
    #   being parent relative, or not. Setting this to false effectively 'detatches' this 
    #   entity from it's parent position. It is still a child, but it won't move with the 
    #   parent.
    #
    #   Default: false, until the entity becomes parented to an object - then it is true.
    parentRelative: false

    # Always marked 'on screen', so never culled from draws.
    alwaysOnScreen: false

    # @property [Array<RBEntity>] A list of the children of this entity. You should use
    #   add_child and remove_child to manipulate this list.
    children: []

    constructor: (X, Y, Width, Height, name="NoName") ->
        super(name, X, Y, Width, Height)
        @_RB._entityCount+=1
        #rotation
        @angle = 0 #angle is in radians. We expose in degrees
        @debugColour = new RBColour(RB_GREY)
        
        @renderMode = RB_RENDER_MODE_NORMAL
        
        @tags = []
        @_lastposition = new RBPoint() #we use this to help with collisions
        @_collisionState = 0 #number representing colstate.
        #flags
        @solid = true #collidable
        @visible = true #drawn or not
        @active = true #false switches off update
        @dirty = true #if true, draw() is a noop
        @destroyed = false #if true, this entity is ready to be destroyed
        #grouping
        @parent = null
        @parentRelative = false
        @parentOffset = new RBPoint(0,0)
        #aliasing
        @children = @members
        @components = {}

    # This method is for use with RBEditor
    to_list: () ->
        return [@x, @y]

    hide: () ->
        @.visible = false
    show: () ->
        @.visible = true

    # @private
    is_on_screen: () ->
        if @alwaysOnScreen
            return true
        if @_RB._screenRect.overlaps(@)
            return true
        return false

    get_abs_rect: () ->
        rect = new RBRect(@x, @y, @width, @height)
        child = @
        parent = @parent
        while parent isnt null
            if child.parentRelative
                rect.add(parent)
            parent = parent.parent
            child = parent
        return rect

    # Sets the angle of this entity, in degrees.
    #
    # @param angle [Number] The angle, in degrees.
    set_angle: (num) ->
        @.angle = (num * 2 * Math.PI) / 360

    # Gets the angle of this entity, in degrees.
    get_angle: () ->
        return 360*@.angle / (2*Math.PI)

    # Adds a tag to this object - a tag can be anything,
    # but strings mostly make sense, or possibly integers.
    #
    # @param [String] tag The tag to add
    #
    # @example
    #   ent.add_tag("wood")
    #   ent.add_tag("interactable")
    #   The entity now has 'wood' and 'interactable' as tags.
    #   This may be useful in later code that checks for these
    #   tags
    add_tag: (tag) ->
        if tag not in @tags
            @tags.push(tag)
    # Remove a tag from an object
    #
    # @param [String] tag The tag to remove
    remove_tag:(tag) ->
        i = @tags.indexOf(tag)
        if i > -1
            @tags.splice(i, 1)
    # Check to see if this entity has a particular tag
    # @param [String] tag The tag to check for.
    has_tag:(tag) ->
        if tag in @tags
            return true
        return false

    # Add a child to this entity. The child's position will become
    # relative to this entity, and will move with this entity.
    # Scale and opacity settings will affect children also.
    # Children will automatically have their update and draw
    # methods called.
    #
    # @param [RBEntity] rbentity The entity to add as a child.
    add_child: (rbentity) ->
        @.add_member(rbentity)
        #This looks strange because rbentity will have a position like
        #(10,10) or something, which was the posiiton it was given on
        #creation, but should be considered an offset.
        rbentity.parentRelative = true
        rbentity.parentOffset.x = @x - rbentity.x
        rbentity.parentOffset.y = @y - rbentity.y
        #console.log("Parentoffset ("+rbentity.name+"): " + rbentity.parentOffset.x + "," + rbentity.parentOffset.y)
        rbentity.parent = @

    # Remove a child from an entity.
    #
    # @param [RBEntity] The entity to remove
    remove_child: (rbentity) ->
        #console.log("Parent is removing child id:#{rbentity.id}")
        @.remove_member(rbentity)
        rbentity.parent = null
        rbentity.parentRelative = false
        rbentity.x -= (@x + rbentity.parentOffset.x)
        rbentity.y -= (@y + rbentity.parentOffset.y)

    # Add a component to an entity
    #
    # @param [RBComponent] component The component to add
    add_component: (component) ->
        @components[component.name] = component
        component.on_add(@)

    # Remove a component from an entity
    #
    # @param [RBComponent] component The component to remove
    remove_component: (component) ->
        console.log("implement remove component...")
        
    # Remove a component from an entity, by name.
    #
    # @param [String] name The name of the component to remove
    remove_component_by_name: (name) ->
        if @components[name]?
            @components[name].deactivate()
            @components[name].on_remove()
            delete @components[name]

    # Get a component by name
    #
    # @param [String] name The name of the component to get.
    get_component: (name) ->
        return @components[name]

    # Stop the given list of actions for this entity
    #
    # @param [Array<RBAction>] actionlist The list of actions 
    #   to stop. Not passing a list will stop all actions
    stop_actions: (actionlist) ->
        @_RB.actions.stop_for_entity(@, actionlist)
    
    # Start the given list of actions for this entity
    #
    # @param [Array<RBAction>] actionlist The list of actions 
    #   to start. Not passing a list will start all actions
    start_actions: (actionlist) ->
        @_RB.actions.start_for_entity(@, actionlist)

    # Force a finish of the given list of actions for this entity
    #
    # @param [Array<RBAction>] actionlist The list of actions 
    #   to finish. Not passing a list will finish all actions
    finish_actions: (actionlist) ->
        @_RB.actions.finish_for_entity(@, actionlist)

    # Force a cancel of the given list of actions for this entity
    #
    # @param [Array<RBAction>] actionlist The list of actions 
    #   to finish. Not passing a list will cancel all actions
    cancel_actions: (actionlist) ->
        @_RB.actions.cancel_for_entity(@, actionlist)


    # Called by Rhubarb's collide method when this entity 
    # collides with another entity along its bottom border.
    #
    # @param [RBEntity] robject The entity that was collided with.
    #
    # @see {RBInit.collide}
    hit_bottom: (robject, collider_velocity) ->
        @_collisionState |= RB_COLLIDE_BOTTOM
        return null #empty! override me!
    
    # Called by Rhubarb's collide method when this entity 
    # collides with another entity along its top border.
    #
    # @param [RBEntity] robject The entity that was collided with.
    #
    # @see {RBInit#collide}
    hit_top: (robject, collider_velocity) ->
        @_collisionState |= RB_COLLIDE_TOP
        return null #empty! override me!
    
    # Called by Rhubarb's collide method when this entity 
    # collides with another entity along its right border.
    #
    # @param [RBEntity] robject The entity that was collided with.
    #
    # @see {RBInit#collide}
    hit_right: (robject, collider_velocity) ->
        @_collisionState |= RB_COLLIDE_RIGHT
        return null #empty! override me!
    
    # Called by Rhubarb's collide method when this entity 
    # collides with another entity along its left border.
    #
    # @param [RBEntity] robject The entity that was collided with.
    #
    # @see {RBInit#collide}
    hit_left: (robject, collider_velocity) ->
        @_collisionState |= RB_COLLIDE_LEFT
        return null #empty! override me!
    
    # @private
    is_hit: (statecheck) ->
        if statecheck isnt RB_COLLIDE_NONE
            return ((@_collisionState & statecheck) == statecheck)
        else
            return @_collisionState == RB_COLLIDE_NONE
    # Update this entity, and all its children.
    # If {#active} is false, this entity (and children) 
    # will not be updated.
    # Updates happen according to dt - the time since the last
    # frame of the game engine was executed, in milliseconds.
    # @param [Number] dt
    #   The time (milliseonds) taken so far this frame.
    #
    update: (dt) ->
        @_collisionState &= RB_COLLIDE_NONE #reset collision state
        @_lastposition.set(@)
        if @active
            for name, component of @components
                component.update(dt)
            for child in @children
                if child?
                    child.update(dt)

    # @private
    pre_render: () -> # (xoffset=0, yoffset=0) ->
        #@_RB.cctx.translate(@x |0, @y |0)
        @_RB.cctx.translate(@x, @y)

        #rect at X Y
        #@_RB.cctx.strokeStyle = "0xff0000"
        #@_RB.cctx.strokeRect(0, 0, @width, @height)
        
        @_RB.cctx.rotate(@angle)
        @_RB.cctx.scale(@_scale.x, @_scale.y)
        #Make alpha propagate
        @_RB.cctx.globalAlpha *= clamp_num(@_colour.a/255, 0, 1)
        #Allow alpha blending based on renderMode
        @_RB.cctx.globalCompositeOperation = @.renderMode

    # @private
    post_render: () ->
        if @_RB.isDebug("bb")
            @_RB.cctx.strokeStyle = @debugColour.htmlrgb
            #@_RB.cctx.strokeRect(-@_anchor.x*@width, -@_anchor.y*@height, @width, @height)
            
            #@_RB.cctx.strokeRect(@left-@x, @top-@y, @width, @height)
            @_RB.cctx.strokeRect(0, 0, @width, @height)

    # Draw this entity and all its children
    draw : () -> # (xoffset, yoffset) ->
        #override this in descendents
        #drawing anything more basic than this, we need to translate
        #back to the top left of canvas.

    # Only called as the top layer 'draw' call of an
    # entity heirarchy. Most of the time, you will be calling
    # draw(), not render.
    render: () -> #(xoffset=0, yoffset=0) ->
        if !@visible
            return
        if @_RB._offscreenCulling
            if not @.is_on_screen()
                @_RB._culledEntityCount+=1
                return

        @.dirty = false
        @_RB.cctx.save()
        @.pre_render()
        
        @fillStyle = @_colour.htmlrgba
        #we are translated to the top left of the entity
        #for the canvas draw
        #translate back to the centre so that child draws
        #are properly position relative
        #@_RB.cctx.translate(@x-@left, @y-@top)
        @.draw()
        for child in @children
            if child?
                child.render() #xoffset, yoffset)
        
        #translate back for post render ops.
        #@_RB.cctx.translate((@left-@x)|0, (@top-@y)|0)
        @_RB.cctx.translate(@left-@x, @top-@y)
        @.post_render()
        @_RB.cctx.restore()

    # Clear the canvas where this RBEntity is.
    clear: () ->
        @_RB.cctx.clearRect(@x|0, @y|0, @width|0, @height|0)
        child.clear() for child in @children
        for child in @children
            if child?
                child.clear()

    # Destroy an RBEntity, removing it from its parents
    # removing all components from it, and removing all
    # its children from it also.
    destroy: () =>
        #console.log("Destroying Entity #{@name}")
        for child in @children
            if child?
                child.destroy()
        for componentname in @components
            @.remove_component_by_name(componentname)
        @destroyed = true
        @active = false
        @_RB._destroylist.push(@) 
        #console.log("destroylist now has #{@_RB._destroylist.length} members") 
        return @

    # This is the real method that does the destroying.
    # @private
    _destroy: () ->
        if @parent != null
            @parent.remove_child(@)
        @parent = null



class RBRectEntity extends RBEntity
    constructor:(X, Y, Width, Height, name="RBRectEntity")->
        super(X, Y, Width, Height, name)
        @shouldFill = false
        @fillColour = new RBColour(0,0,0,255)
    #call set_fill with null or false as first arg to 
    #turn off fill for this rect. Otherwise pass
    #r, g, g, a as 0-255 values
    set_fill:(r=0, g=0, b=0, a=255) -> #expect [r,g,b,a]
        if r == null or r == false
            @shouldFill = false
        else
            @shouldFill = true
            @fillColour.set_rgba([r, g, b, a])
    draw:() ->
        if @shouldFill
            @_RB.cctx.fillStyle = @fillColour.htmlrgba
            #@_RB.cctx.fillRect(@left,@top,@width,@height)
            @_RB.cctx.fillRect(@_XYtoTL.x,@_XYtoTL.y,@width,@height)

        @_RB.cctx.strokeStyle = @_colour.htmlrgb
        #@_RB.cctx.strokeRect(@left, @top, @width, @height)
        @_RB.cctx.strokeRect(@_XYtoTL.x, @_XYtoTL.y, @width, @height)
        
class RBCircleEntity extends RBEntity
    constructor: (X, Y, radius, name="RBCircleEntity") ->
        super(X, Y, radius*2, radius*2, name)
        @radius = radius
        @shouldFill = false
        @fillColour = new RBColour(0, 0, 0, 255)
    #call set_fill with null or false as first arg to 
    #turn off fill for this rect. Otherwise pass
    #r, g, g, a as 0-255 values
    set_fill:(r=0, g=0, b=0, a=255) -> #expect [r,g,b,a]
        if r == null or r == false
            @shouldFill = false
        else
            @shouldFill = true
            @fillColour.set_rgba([r, g, b, a])
    draw:() ->
        RB().cctx.beginPath()
        RB().cctx.arc(0, 0, @radius, 0, 2 * Math.PI, false)
        RB().cctx.closePath()
        if @shouldFill
            @_RB.cctx.fillStyle = @fillColour.htmlrgba
            @_RB.cctx.fill()
        @_RB.cctx.strokeStyle = @_colour.htmlrgb
        @_RB.cctx.stroke()       
# A Sprite.
# Pass in your X and Y, 
# An image. If null, no image is used.
# A width and height. If not specified, the 
# image width and height are used. If there is no
# image, these default to zero.
#
#
class RBSprite extends RBEntity
    # @property [Boolean] Whether this sprite should be flipped in X
    flipx: false
    # @property [Boolean] Whether this sprite should be flipped in Y
    flipy: false

    constructor: (X, Y, Imagename=null, Width=null, Height=null, name="NoName") ->
        image = null
        if not (Imagename is null)
            rbimage = RB().media.load_image(Imagename)
            image = rbimage.image()
            w = image.naturalWidth
            h = image.naturalHeight
            #console.log("Sprite loaded " + Imagename + ": [" + @width + "," + @height + "]")
        else
            w = 0
            h = 0
        super(X, Y, w, h, name)
        #console.log(Imagename + "[" + @width + "," + @height + "]")
        @_image = image
        
        @_currAnimName = null
        @_currAnim = null
        @flipx = false
        @flipy = false
        
        if Width isnt null
            @.set_width(Width)
        if Height isnt null
            @.set_height(Height)
        
        @_animations = {"default": new RBAnimation(null, [0])}
        @_animations.default.currentFrameRect.width = w
        @_animations.default.currentFrameRect.height = h
        @.set_animation("default")

    flip_x: (val) ->
        #console.log("flipped x")
        @flipx = val
    flip_y: (val) ->
        @flipy = val

    add_animation: (animationname, animation) ->
        @_animations[animationname] = animation
    
    set_animation: (animationname) ->
        change_anim = false
        if @_animations[animationname]?
            if @_currAnimName != animationname
                @_currAnimName = animationname
                change_anim = true
        else
            @_currAnimName = "default"
            change_anim = true
        @_currAnim = @_animations[@_currAnimName]
        if change_anim
            @_currAnim.reset()

    get_animation: (animationname) ->
        @_animations[animationname]

    update: (dt)->
        super(dt)
        @_currAnim.next(dt)

    draw: () -> # (xoffset=0, yoffset=0)->
        ca = @_currAnim
        if ca?
            car = ca.currentFrameRect
            if ca.spritesheet?
                @_image = ca.spritesheet.get_image()
        if @_image?
            if (@flipx or @flipy)
                fx = fy = 1 # 1 = the right way around, -1 = flipped 
                if @flipx
                    fx = -1
                if @flipy
                    fy = -1
                tomid = @.xy_to_middle()
                @_RB.cctx.translate(tomid.x|0, tomid.y|0)
                @_RB.cctx.transform(fx,0,0,fy,0,0)
                @_RB.cctx.translate(-tomid.x|0, -tomid.y|0)
            #we draw the image at (0, 0, width, height) because we have already been translated to the correct position
            try
                @_RB.cctx.drawImage(@_image, car.x|0, car.y|0, car.width|0, car.height|0,
                                    @_XYtoTL.x, @_XYtoTL.y, @width|0, @height|0 )
            catch error
                console.log("Draw Error: #{error}")
                console.log("#{car.x}, #{car.y}, #{car.width}, #{car.height}")
                console.log("#{totopleft.x|0}, #{totopleft.y|0}, #{@width|0}, #{@height|0}")
            @dirty = false
            super()
#requres RBGroup
class RBCamera extends RBEntity
    constructor: (X, Y, Width, Height, @screenwidth=0, @screenheight=0, name="UnknownCamera") ->
        super(X, Y, Width, Height, name)
        @offset = new RBPoint() #the x/y of where the cam is looking
        @.set_anchor(0,0)
        @.alwaysOnScreen = true
        @followentity = null
        @scale = new RBPoint(1, 1)
        @canvas = document.createElement('canvas')
        @canvas.width = Width
        @canvas.height = Height
        ###
        if @screenwidth is 0
            @screenwidth = Width
        if @screenheight is 0
            @screenheight = Height
        ###
        @ctx = @canvas.getContext('2d')
        @_RB.set_smoothing(@ctx, @_RB._canvasSmoothing)

    resize: (w, h) ->
        @.set_width(w)
        @.set_height(h)

    set_width: (w) ->
        super(w)
        @canvas.width = w
        @_RB.set_smoothing(@ctx, @_RB._canvasSmoothing)
    set_height: (h) ->
        super(h)
        @canvas.height = h
        @_RB.set_smoothing(@ctx, @_RB._canvasSmoothing)

    zoom: (xzoom, yzoom) ->
        xchange = @width*xzoom - @width
        ychange = @height*yzoom - @height
        @ctx.translate(-xchange/2, -ychange/2)
        @ctx.scale(xzoom, yzoom)
        @_RB.set_smoothing(@ctx, @_RB._canvasSmoothing)
                
        

    follow: (entitytofollow, someparams) ->
        @followentity = entitytofollow
    
    update:(dt) ->
        if dt <= 0
            #console.log("cam update dt was 0")
            return
        if @followentity?
            @offset.x = -@followentity.x + @width/(2 * @scale.x) # |0
            @offset.y = -@followentity.y + @height/(2 * @scale.y) #|0
        
        #Store offsets so we can use them
        
        @_RB.screen().set_position( -@offset.x, -@offset.y)
        

    zoomdata: () ->
        #zoom code
        imgData = @ctx.getImageData(0,0,@width, @height).data
        zoom = 2
        #Draw the zoomed-up pixels to a different canvas context
        for x in [0..@width/2]
            for y in [0..@height/2]
                #Find the starting index in the one-dimensional image data
                i = (y*@width + x)*4
                r = imgData[i  ]
                g = imgData[i+1]
                b = imgData[i+2]
                a = imgData[i+3]
                @_RB.cctx.fillStyle = "rgba("+r+","+g+","+b+","+(a/255)+")"
                @_RB.cctx.fillRect(x*zoom,y*zoom,zoom,zoom)


    #get this camera to draw an object
    draw: (object) ->
        #Clear our local canvas
        @ctx.clearRect(0,0,@width, @height)
        #Draw the scene into our canvas
        @_RB.push_cctx()
        @_RB.set_cctx(@ctx)
        @ctx.translate(@offset.x|0, @offset.y|0)
        #@ctx.translate(@offset.x, @offset.y)
        object.render()
        @ctx.translate(-@offset.x|0, -@offset.y|0)
        #@ctx.translate(-@offset.x, -@offset.y)
        #now draw it on the main canvas
        @_RB.restore_cctx()
        @_RB.cctx.save()
        #clip to this camera rect only
        @_RB.cctx.beginPath()
        @_RB.cctx.rect(@left|0, @top|0, @width|0, @height|0)
        @_RB.cctx.closePath()
        @_RB.cctx.clip()
        #draw the local camera canvas onto the main context
        @_RB.cctx.drawImage(@canvas, @x|0, @y|0)
        @_RB.cctx.restore()


# Text entities.
# RBText has an anchor in it's centre by default.
class RBText extends RBEntity
    constructor:(X, Y, @text="Test", @fontname="arial", @size=10, @maxWidth=null, @isStroke=false, @isFill=true, isstatic = true) ->
        super(X, Y)
        @static = isstatic
        @.set_font(@fontname, @size)
        @.set_colour(0,0,0)
        @.set_text(@text)
        @strokeWidth = 1
        @strokeColour = new RBColour([0,40,0,255])
        @isShadow = false
        @_shadowOffsetX = 0
        @_shadowOffsetY = 0
        @_shadowBlur = 0
        @_shadowColour = new RBColour([0, 0, 0, 255])
    
    # Whether or not the text should fill (on by default)
    # set_colour() determines fill colour
    set_fill: (fill=true) ->
        @isFill = fill
    
    # Whether or not the text should stroke. (off by default)
    # set_stroke_colour() determines stroke colour
    set_stroke: (stroke=true) ->
        @isStroke = stroke

    # Sets the stroke colour for a text object
    set_stroke_colour: (r, g, b, a=255) ->
        @strokeColour.set_rgba([r, g, b, a])

    set_stroke_width: (val) ->
        if val >= 0
            @strokeWidth = val

    # Set shadow params. Offset x, offset y, colour=[R, G, B, A], blur
    set_shadow: (offx, offy, colour=[0, 0, 0, 255], blur=0) ->
        if offx is false
            @isShadow = false
        else if (offx? and offy?)
            @isShadow = true
            @_shadowOffsetX = offx
            @_shadowOffsetY = offy
            @_shadowBlur = blur
            @_shadowColour.set_rgba(colour)
        else
            @isShadow = false

    # Sets the font for a text object
    set_font:(@fontname, @size) ->
        @cssfont = "#{@size}px #{@fontname}"
        @_lines = []
        @.refresh()

    set_size:(@size) ->
        @.set_font(@fontname, @size)
    
    refresh: () ->
        @height = @size
        super()

    # Sets the text for a text object
    set_text:(text) ->
        @text = text
        @_RB.cctx.save()
        @_RB.cctx.font=@cssfont
        @width = @_RB.cctx.measureText(@text).width
        @_RB.cctx.restore()
        @.refresh()
        
    #wrap_text: () ->
    #    words = @text.split(' ')
    #    line = ''
    #    @lines = []
    #    for n in [0..words.length]
    #        testLine = line + words[n] + ' '
    #        metrics = context.measureText(testLine)
    #        testWidth = metrics.width
    #        if (testWidth > maxWidth && n > 0)
    #            context.fillText(line, x, y)
    #            line = words[n] + ' '
    #            #y += lineHeight
    #      
    #        else 
    #            line = testLine
    #      
    #    
    #    context.fillText(line, x, y);


    append_text:(text) ->
        @.set_text(@text + text)

    draw:() ->
        #@.pre_render()
        @_RB.cctx.font=@cssfont
        totopleft = @.xy_to_topleft()
        totopleft.y += @height
        if @isFill
            if @isShadow
                @_RB.cctx.save()
                @_RB.cctx.shadowOffsetX = @_shadowOffsetX
                @_RB.cctx.shadowOffsetY = @_shadowOffsetY
                @_RB.cctx.shadowBlur = @_shadowBlur
                @_RB.cctx.shadowColor = @_shadowColour.htmlrgba
            @_RB.cctx.fillStyle = @_colour.htmlrgb
            #@_RB.cctx.fillText(@text, 0, 0) #TODO: maxwidth
            @_RB.cctx.fillText(@text, totopleft.x|0, totopleft.y|0) #TODO: maxwidth
            if @isShadow
                @_RB.cctx.restore()

        if @isStroke #or @isShadow
            @_RB.cctx.strokeStyle = @strokeColour.htmlrgb
            @_RB.cctx.lineWidth = @strokeWidth
            @_RB.cctx.strokeText(@text, totopleft.x|0, totopleft.y|0) #TODO: maxwidth
        #@.post_render()
###
 A simple test:
 Assuming TestProtocolHandler is:

class TestProtocolHandler extends RBProtocolHandler
    constructor: () ->
        super(RBProtocol) #construct the super with RBProtocol as the prot
        @.add_function_mapping('hello', @.hello)

    hello:(arglist) ->
        console.log("TestProtocolHandler says Hello! Args: " + arglist)


 Then a test would be:
 var a = new TestProtocolHandler()
 var b = new RBProtocol().set_opcode('hello')
 a.deliver(b.to_JSON())
 a.update()

 This should print out the 'hello' output
###



#This is an auto-incrementing value for RBProtocol ID's. Don't mess with it.

_RB_PACKET_ID = 1
###
    @class RBProtocol
    This is simply a class to encapsulate a protocol.
    The only things is MUST have are an id, and a to/from json method.
    Requires json2.js
###

class RBProtocol
    constructor:(from=null, to=null, resp_id=null, opcode=null, args=[]) ->
        #the following could be done as functions. Lets just make it quicker.
        @definition = {'id':_RB_PACKET_ID++, 'ts':0, 'from':from, 'to':to, 'resp_id':resp_id, 'fn':opcode, 'args':args, 'resp':0}

    set_timestamp:(ts) ->
        @definition['ts'] = ts
    get_timestamp:()->
        return @definition['ts']
    
    set_id:(id) ->
        @definition['id'] = id
        @
    get_id:() ->
        return @definition['id']
    
    set_from:(from)->
        @definition['from'] = from
        @
    get_from:()->
        return @definition['from']

    set_to:(to)->
        @definition['to'] = to
        @
    get_to:() ->
        return @definition['to']

    get_resp_id:() ->
        return @definition['resp_id']
    set_resp_id:(resp_id) ->
        @definition['resp_id'] = resp_id
        @

    get_opcode:() ->
        return @definition['fn']
    set_opcode:(opcode) ->
        @definition['fn'] = opcode
        @
   
    get_args:() ->
        return @definition['args']
    set_args:(args) ->
        @definition['args'] = args
        @

    to_JSON:() ->
        return JSON.stringify(@definition)

    from_JSON:(somejson) ->
        @definition = JSON.parse(somejson)
        @
        

###
    @class RBProtocolHandler
    You pass this the protocol class - that is, the type of class that needs to 
    be instantiated which encapsulates the protocol used here.
    This should be something inheriting from RBProtocol.
###
class RBProtocolHandler
    constructor:(protocolclass)->
        @protocolclass = protocolclass
        @name = "RBProtocol"
        @version = "1.0"
        @classmappings = {}     #this is a mapping of strings to class names
        @functionmappings = {}  #this is a mapping of opcodes to functions
        @incomingqueue = []
        @outgoingqueue = []

    add_class_mapping:(ident, classinstance) ->
        @classmappings[ident] = classinstance

    get_class_mapping:(ident) ->
        return @classmappings[ident]

    add_function_mapping:(ident, func) ->
        @functionmappings[ident] = func

    ###
       This function should always break the incoming raw message into items which
       are then added to the incoming queue to be processed.
       This is the default pre_process - you probably want to override this
       default behaviour will just be to assume the packet is valid, doesn't nee
       to be split up, and we can just from_JSON it.
       Since JS event model is all part of a single thread, mutating the array
       from an event fire is safe - we will be out of our update code
       by this stage.
    ###
    deliver:(msg) ->
        item = new @protocolclass()
        item.from_JSON(msg)
        @incomingqueue.push(item)
        
    update:(dt) ->
        #here we could write a 'while incoming length > 0 unshift' type loop
        #but instead, I will just use a for item in coffeescript style loop, and
        #process the whole queue.
        #you might be able to be leaner or meaner by only processing one item each 
        #update cycle, or only enough items from a certain time window, or
        #even restricting your update loop to take no more than n milliseconds.

        for item in @incomingqueue
            #console.log("Item was " + item)
            #check the opcode.
            opcode = item.get_opcode()
            #console.log("Opcode was " + opcode)
            for map_oc of @functionmappings
                #console.log("checking opcode against " + map_oc)
                #if it matches
                if map_oc is opcode
                    #console.log("opcode matched: " + opcode)
                    #run the function
                    @functionmappings[opcode]( item.get_args() )

        #now empty the queue, since we processed all items.
        @incomingqueue = []
        
class RBNet
    constructor:(name="netconnection", host="127.0.0.1", port="2000", protocolHandler=null) ->
        @name = name
        @host = host
        @port = port
        @protocolHandler = protocolHandler
        @protocolHandler.set_transport(@)
        @websocket = null
        @connected = false

    #todo: if they pass an url, save out the host and port?
    connect:() ->
        @websocket = new WebSocket("ws://" + @host + ":" + @port + "/" + "chat")
        @websocket.onmessage = @.onmessage
        @websocket.onopen = @.onopen
        @websocket.onerror = @.onerror
        @websocket.onclose = @.onclose
        
    onerror: (obj) =>
        console.log("Websocket error: " + obj)
    onmessage:(obj) =>
        console.log('received message: ' + obj.data)

    
    onopen:() =>
        console.log('socket opened')
        @connected = true
        @protocolHandler.set_state(DelveProtocolStates.STATE_PRE_LOGIN)

    onclose:(obj) =>
        console.log('socket closed')
        @connected = false
        @protocolHandler.set_state(DelveProtocolStates.STATE_DISCONNECTED)

    send:(obj) =>
        if (@connected)
            console.log('sending data: ' + obj)
            @websocket.send(obj)
        else
            console.log('could not send data: not connected')
        
class RBRequest
    @GET = "GET"
    @POST = "POST"
    constructor:(@url, @onsuccess, @onfailure) ->
        @reqobj = new XMLHttpRequest()
        #@reqobj.onload = @onsuccess
        if @onfailure?
            @reqobj.onerror = @onfailure
        else
            @reqobj.onerror = @.default_onfailure
        @reqobj.onreadystatechange = @.statechange
        @mode = @POST

    statechange:() =>
        switch @reqobj.readyState   
            when 0 then a = 1 #UNSENT
            when 1 then a = 1 #OPENED
            when 2 then a = 1 #HEADERS_RECIEVED 
            when 3 then a = 1#LOADING - responsetext holds partial data
            when 4
                if @reqobj.status == 200
                    @onsuccess(@reqobj.responseText)
                else
                    console.log("Response to #{@url} was code #{@reqobj.status}")
    default_onfailure:() =>
        console.log("Web request to #{@url} failed")

    get:(@params) ->
        @reqobj.open("GET", @url, true)
        @reqobj.send()
    post:(@data) ->
        @reqobj.open("POST", @url, true)
        console.log("Posting: #{JSON.stringify(@data)}")
        @reqobj.send(JSON.stringify(@data))
_SUBSCRIBERS = {}

class RBMessage
    constructor:( @key, @args ) ->
    
RBBroadcast = (key, args...) ->
    #m = new RBMessage(key, args)
    if _SUBSCRIBERS[key]?
        for item in _SUBSCRIBERS[key]
            item(args)

###*
    subscriber should just be a callback function
    that will know what to do with ARGS
###
RBSubscribeMessage = (key, subscriber) ->
    if not _SUBSCRIBERS[key]?
        _SUBSCRIBERS[key] = []
    _SUBSCRIBERS[key].push(subscriber)
RBUI_LEFT = 1
RBUI_RIGHT = 2
RBUI_CENTER = 3
RBUI_TOP = 4
RBUI_BOTTOM = 5
RBUI_AUTO = 20

class RBUIComponent extends RBEntity #base class
    constructor: (X, Y, Width=RBUI_AUTO, Height=RBUI_AUTO) ->
        super(X, Y, Width, Height)
        @margin = [0,0,0,0] #top, right, bottom, left
        @padding = [0,0,0,0] #top, right, bottom, left
        @top = 0
        @bottom = 0
        @left = 0
        @right = 0
        @valign = RBUI_LEFT
        @halign = RBUI_TOP
        @zdepth = 0
        @_RB.add_ui_object(@)
        @eventDelegates = {'on_click':[]}
        @mouse_is_over = false

    hide: () =>

    on_hover: () =>

    #this function is just for checking if a click was on this component
    _pre_on_click: (args) =>
        m = @_RB.input.mouse
        mrect = new RBRect(m.x, m.y, 2, 2)
        if @.overlaps(mrect)
            @.on_click(args)
    _pre_on_touch: (args) =>
        $("#testlog").append("x: #{args.targetTouches[0].pageX}, y: #{args.targetTouches[0].pageY}")
    _pre_on_mm: (args) =>
        m = @_RB.input.mouse
        mrect = new RBRect(m.x, m.y, 2, 2)
        if @.overlaps(mrect)
            console.log("Mouse over!")
            if not @mouse_is_over
                @.on_mouse_enter(args)
                @mouse_is_over = true
        else if @mouse_is_over
            @.on_mouse_leave(args)
            @mouse_is_over = false
        

    on_click: (args) =>

    on_drag: () =>

    on_blur: () =>

    on_mouse_enter: (args) =>
    on_mouse_leave: (args) =>

    destroy: () =>
        @_RB.remove_ui_object(@)
        super()


#class RBFrame extends RBUIComponent #frame is just a focussable rect, for layout

#class RBWindow extends RBFrame  #window has a frame but also some controls, is draggable

class RBButton extends RBUIComponent
    constructor: (btnText = "Button Text", X, Y, Width=RBUI_AUTO, Height=RBUI_AUTO) ->
        super(X, Y, Width, Height)
        @txt = new RBText(0, 0, btnText, "arial", 10)
        @txt.set_colour(200, 200, 200)
        @.set_colour(0,0,0)
        @.add_child(@txt)
        @_RB.input.bind(RB_MOUSE_UP, @._pre_on_click, consume=true)
        @_RB.input.bind(RB_TOUCH_START, @._pre_on_touch, consume=true)
        @_RB.input.bind(RB_MOUSE_MOVE, @._pre_on_mm, consume=true)
    

    destroy: () ->
        @_RB.input.unbind(RB_MOUSE_UP, @._pre_on_click, consume=true)
        @_RB.input.unbind(RB_TOUCH_START, @._pre_on_touch, consume=true)
        @_RB.input.unbind(RB_MOUSE_MOVE, @._pre_on_mm, consume=true)

    # Set the text for the button
    set_text: (newtext) ->
        @txt.set_text(newtext)

    # Set the font for the button
    # @param [String] fontname The name of the font to use
    # @param [Number] size The size of the font to use
    set_text_font: (fontname, size=10) ->
        @txt.set_font(fontname, size)
    
    # Set the colour of the text
    # r, g, b, a - (0-255). a defaults to 255
    set_text_colour: (r, g, b, a=255) ->
        @txt.set_colour(r, g, b, a)

    on_click: (args) =>
        super(args)
        #console.log("clicked!: args=" + args)
        onclickdelegates = @eventDelegates['on_click']
        for delegate in onclickdelegates
            delegate(args)

    on_mouse_enter: (args) =>
        super(args)
        if @_RB.isDebug()
            @.set_colour(255, 0, 0)
    on_mouse_leave: (args) =>
        super(args)
        if @_RB.isDebug()
            @.set_colour(0,0,0)


    add_on_click: (fn) =>
        @eventDelegates['on_click'].push(fn)

    remove_on_click: (fn) =>
        index = @eventDelegates['on_click'].indexOf(fn)
        if index >= 0
            console.log("removing onclick delegate")
            @eventDelegates['on_click'].splice(index, 1)

    pre_render: () ->
        super()
        p = @.xy_to_topleft()
        @_RB.cctx.strokeStyle = @._colour.htmlrgba
        @_RB.cctx.strokeRect(p.x, p.y, @width, @height)
#class RBTextArea extends RBUIComponent
#
class RBImageButton extends RBButton
    constructor: (buttontext, X, Y, W, H, upimagename, downimagename, hoverimagename) ->
        super(buttontext, X, Y, W, H)
        @upimage = null
        @downimage = null
        @hoverimage = null
        
        #@upimage = new RBSprite(0, 0, "data/button_up.png", 50, 50)
        #@.add_child(@upimage)
        if upimagename?
            @upimage = new RBSprite(0, 0, upimagename, @.width, @.height)
            @.add_child(@upimage)
            #@upimage.hide()
        if downimagename?
            @downimage = new RBSprite(0, 0, downimagename, @.width, @.height)
            @downimage.hide()
            @.add_child(@downimage)
        if hoverimagename?
            @hoverimage = new RBSprite(0, 0, hoverimagename, @.width, @.height)
            @hoverimage.hide()
            @.add_child(@hoverimage)
    on_mouse_enter: (args) ->
        super(args)
        if @hoverimage isnt null
            @hoverimage.show()
        if @upimage isnt null
            @upimage.hide()
    on_mouse_leave: (args) ->
        super(args)
        if @hoverimage isnt null
            @hoverimage.hide()
        if @upimage isnt null
            @upimage.show()
    on_click: (args) ->
        super(args)
        if @downimage isnt null
            @downimage.show()
        if @upimage isnt null
            @upimage.hide()
        if @hoverimage isnt null
            @hoverimage.hide()
class RBSceneManager
    _instance = null
    @get_instance: () ->
        _instance ?= new RBSceneManagerPrivate()
    
    class RBSceneManagerPrivate
        constructor: () ->
            @_scenes = new RBGroup("SceneStack") #the scene stack
            @_sceneCache = []
            @_currentscene = null
        current_scene: () ->
            return @_currentscene
        
        #returns named scene from cache
        #null if it didn't exist
        #if name is not unique, returns first found scene with that name
        #which may not be in the order pushed
        get_cached_scene: (scenename) ->
            for scene in @_sceneCache
                if scene.name == scenename
                    return scene
            return null
        push_scene: (scene) ->
            if @_currentscene == null
                @_scenes.add_member(scene)
            else
                @_scenes.add_after(scene, @_currentscene)
            @_currentscene = scene
            @_sceneCache[scene.id] = scene

        pop_scene:(scene, removeFromCache=false) ->
            if @_currentscene != scene
                console.warn("You tried to pop a scene that was not the current scene")
                return
            beforescene = @_scenes.get_before(scene)
            @_scenes.remove_member_by_id(scene.id)
            @_currentscene = beforescene
            if removeFromCache
                cacheindex = @_sceneCache.indexOf(scene)
                if cacheindex >= 0
                    @_sceneCache.splice(cacheindex, 1)

        replace_scene:(scene, existingscene) ->
            if !@_scenes.is_member(scene)
                @_scenes.add_before(scene, existingscene)
            @_scenes.remove_member_by_id(existingscene.id)
            @_currentscene = scene
             

class RBScene extends RBEntity
    constructor: (scenename="UnnamedScene") ->
        super(0,0,0,0,scenename)
        @manager = RBSceneManager.get_instance()
        @stage = new RBEntity(0,0,0,0)
        @stage.alwaysOnScreen = true
        @.add_child(@stage)
        @has_input_focus = false
        @events = new RBDelegateCollection(scenename,
            [RB_EVENT_ON_SCENE_ENTER,
            RB_EVENT_ON_SCENE_EXIT,
            RB_EVENT_ON_SCENE_REVEAL,
            RB_EVENT_ON_SCENE_ECLIPSE] )
        @input = new RBDelegateCollection("#{scenename}_input",
            [_RBInputEvents.mousemove,
            _RBInputEvents.mousedown,
            _RBInputEvents.mouseup,
            _RBInputEvents.touchstart,
            _RBInputEvents.touchend,
            _RBInputEvents.touchcancel,
            _RBInputEvents.touchmove,
            _RBInputEvents.keydown,
            _RBInputEvents.keyup,
            _RBInputEvents.keydown] )

    destroy: () ->
        #console.log("destroying Scene #{@name}")
        @stage.destroy()
        @input.destroy()
        @events.destroy()
        super()


    #push this scene onto the scene manager stack.
    #we should become the current scene
    push: () ->
        cs = @manager.current_scene()
        @manager.push_scene(@)
        if cs != null
            cs.on_eclipse()
        @.on_enter()
        return @
    
    #pop this scene off the stack (wherever it is)
    pop: (removeFromCache=false) ->
        @manager.pop_scene(@, removeFromCache)
        @.on_exit()
        cs = @manager.current_scene()
        if cs != null
            cs.on_reveal()
        return @

    #update: (dt) ->
    #
    #    @stage.update(dt)
    #    #override this

    draw: () ->
        #override this

    focus: (value) ->
        if value? and value != @focus
            @has_input_focus = value
            if @focus
                @.on_gain_focus()
            else
                @.on_lose_focus()

        else
            @has_input_focus
    has_focus: () ->
        @.focus()

    #what to do when we get input focus
    on_gain_focus: () ->
        #console.log("Scene #{@name} enabling input")
        @input.enable()
    #what to do when we lose input focus
    on_lose_focus: () ->
        #console.log("Scene #{@name} disabling input")
        @input.disable()
    #what to do on enter
    on_enter: () ->
        #console.log("Scene #{@name} entered.")
        @focus(true)
        @events.call_delegates(RB_EVENT_ON_SCENE_ENTER)
    #what to do on exit
    on_exit: () ->
        #console.log("Scene #{@name} exited.")
        @focus(false)
        @events.call_delegates(RB_EVENT_ON_SCENE_EXIT)
    #called on a scene when it is the current scene, and 
    #another scene is pushed on top
    on_eclipse: () ->
        #console.log("Scene #{@name} eclipsed by #{@manager.current_scene().name}.")
        @events.call_delegates(RB_EVENT_ON_SCENE_ECLIPSE)

    on_reveal: () ->
        #console.log("Scene #{@name} revealed.")
        @events.call_delegates(RB_EVENT_ON_SCENE_REVEAL)

# This type of scene will load all the images, sounds, and spritesheets
# in the global settings object, and call the on_preloaded function
# when done.
class RBPreLoaderScene extends RBScene
    constructor: (name) -> #, @imagelist, @soundlist, @spritesheetlist) ->
        super(name)
        @pl = RB().preloader
        @preloaded = false

    on_enter: () ->
        super()
        #@_RB.input.bind(RB_KEY_UP, @.on_key_up)
        if not @finished
            s = @_RB.env("settings")
            @imagelist = (img for name, img of s.images)
            #console.log(@imagelist)
            @soundlist = (snd for name, snd of s.sounds)
            #console.log(@soundlist)
            @musiclist = (ms for name, ms of s.music)
            #console.log(@musiclist)
            @spritesheetlist = (args for name, args of s.spritesheets)
            #console.log(@spritesheetlist)
            @pl.queue_images(@imagelist)
            @pl.queue_sounds(@soundlist)
            @pl.queue_music(@musiclist)
            @pl.queue_spritesheets(@spritesheetlist)
            @pl.start()

    on_exit: () ->
        super()
        #@_RB.input.unbind(RB_KEY_UP, @.on_key_up)


    update: (dt) ->
        super(dt)
        if not @preloaded
            @pl.update()
            if @pl.done
                @preloaded = true
                @.on_preloaded()

    draw: () ->
        super()
        @stage.render()

    on_preloaded: () ->
        #override me
# A class to hold groups of entities in scenes, which can 
# be considered part of the same layer, and will all be drawn together.
# Layers can use parallax effects, however these will not be seen
# unless you are using an RBCamera to render your scene (and the camera
# is moving).
class RBLayer extends RBEntity
    constructor:(name="Layer_NoName") ->
        super(undefined, undefined, undefined, undefined, name)
        @_fade = false
        @solid = false #very important
        @_parallax = new RBPoint(1.0, 1.0) #what your parallax ratio should be
        @.set_anchor(0,0)
        @_cumulative_parallax_offset = new RBPoint(0, 0)
        @alwaysOnScreen = true #never cull

    # Set the parallax ratios for this layer.
    # A value of 1 (default) is a 1 to 1 'opposite' movement to the
    # camera - i.e. no parallax at all. 
    # A value of 0 is a stationary layer - i.e moves with the camera,
    # so it is useful for GUI's etc.
    # Any value greater than 1 results in a parallax ratio.
    set_parallax: (xratio = 1, yratio = 1) ->
        @_parallax.x = xratio
        @_parallax.y = yratio

    update: (dt) ->
        super(dt)
        #The reason we calculate this in such a roundabout way
        #(instead of just doing a setposition based on 
        #your parallax ratios and the screen rect) is that
        #we want to use 'move_by', not set_position - i.e. we
        #don't want to clobber other set_pos type operations that 
        #could be happening on the entity.
        
        xoff = (-@_parallax.x + 1) * RB().screen().x # |0
        yoff = (-@_parallax.y + 1) * RB().screen().y # |0
        dxoff = xoff - @_cumulative_parallax_offset.x
        dyoff = yoff - @_cumulative_parallax_offset.y
        
        #@.move_by(dxoff | 0, dyoff | 0)
        @.move_by(dxoff , dyoff )
        #@.set_position(@x|0, @y|0)
        @_cumulative_parallax_offset.x += dxoff
        @_cumulative_parallax_offset.y += dyoff
        #@.set_position((-@_parallax.x + 1) * RB().screen().x, (-@_parallax.y + 1) * RB().screen().y)

    fade: () ->
        @_fade = true

    unfade: () ->
        @_fade = false

    draw: () ->
        if @_fade
            #@_RB.cctx.globalCompositeOperation = "lighter"
            @_RB.cctx.globalAlpha = 0.3
        super()

# A class for creating layers which loop their backgrounds.
# Reasonably experimental, and potentially buggy.
# The main arguments to the constructor are the 
# loopwidth and loopheight. Specifying 0, or omitting the 
# parameter, means the layer will not loop in that dimension.
# Any other value will cause the layer to be looped / repeated 
# after that many units in screenspace.
# It is important that this loop value be *equal to or less than* the 
# screen width / height (whichever is relevant).
# Setting to a value less than screen width (taking width, for example) 
# will repeat the contents several times over the course of a screen.
# Setting to a value equal to the screen width will fit the screen width
# exactly, and will repeat if the screen or layer scrolls / moves 
# for some reason.
# Setting to a value greater than the width of the screen (or height for
# the loopheight parameter) will cause the layer *not to display at all!*
#
# So be careful!
#
# You can see the result of your repeating layers either via
# parallax x and y settings and the use of a camera, or via
# x / y velocities on the layers themselves, and a stationary 
# view. Or - both!
class RBLoopLayer extends RBLayer
    constructor: (@loopwidth=0, @loopheight=0, name) ->
        super(name)
        @canvas = document.createElement("canvas")
        @canvas.width = @_RB.screen().width 
        @canvas.height = @_RB.screen().height
        @ctx = @canvas.getContext('2d')
        @_RB.set_smoothing(@ctx, @_RB._canvasSmoothing)
        if @loopwidth > 0
            @loopx = true
        if @loopheight > 0
            @loopy = true
        @_loopRect = new RBRect(@x, @y, @loopwidth, @loopheight)
        @_drawRect = new RBRect()
        @_loopRect.set_anchor(0, 0)
        @_drawRect.set_anchor(0, 0)
    ###
    update: (dt) ->
        super(dt)
        scr = RB().screen()
        x = @x
        y = @y
        if @loopx
            x = Math.abs(@x % scr.width)
        if @loopy
            y = Math.abs(@y % scr.height)
        @.set_position(x, y)
    ###

    render: () ->
        #Clear our local canvas
        @ctx.clearRect(0,0,@canvas.width, @canvas.height)
        #Draw the scene into our canvas
        @_RB.push_cctx()
        @_RB.set_cctx(@ctx)
        @ctx.translate(-@x, -@y)
        super()
        @ctx.translate(@x, @y)
        #now draw it on the main canvas
        @_RB.restore_cctx()
        @_RB.cctx.save()
        #clip to this layer only
        #@_RB.cctx.beginPath()
        #@_RB.cctx.rect(0, 0, @width, @height)
        #@_RB.cctx.closePath()
        #@_RB.cctx.clip()
        #draw the local camera canvas onto the main context
        scr = RB().screen()
        x = 0
        y = 0
        loopxbounds = []
        loopybounds = []
        if @loopx
            x =@x%@loopwidth
            loopxbounds.push(scr.left-@_loopRect.width, scr.right+@_loopRect.width)
        else
            x = scr.x
            @_loopRect.set_width(scr.width)
            loopxbounds.push(scr.left, scr.right)
        
        if @loopy
            y = @y%@loopheight
            loopybounds.push(scr.top-@_loopRect.height, scr.bottom+@_loopRect.height)
        else
            y = scr.y
            @_loopRect.set_height(scr.height)
            loopybounds.push(0, scr.bottom)

        for xdiff in [loopxbounds[0]..loopxbounds[1]] by @_loopRect.width
            for ydiff in [loopybounds[0]..loopybounds[1]] by @_loopRect.height
                #console.log(xdiff, ydiff)
                @_loopRect.set_position(x,y)#-loopRect.height)
                @_loopRect.move_by(xdiff, ydiff)
                if (scr.overlaps(@_loopRect, @_drawRect) )
                    ###
                    @_RB.cctx.strokeStyle = rgba_to_html(0, 255, 0, 255) 
                    @_RB.cctx.strokeRect(@_loopRect.left, @_loopRect.top, @_loopRect.width, @_loopRect.height)
                    @_RB.cctx.strokeStyle = rgba_to_html(0, 255, 0, 255) 
                    @_RB.cctx.strokeRect(@_drawRect.left, @_drawRect.top, @_drawRect.width, @_drawRect.height)
                    @_RB.cctx.strokeStyle = rgba_to_html(0, 0, 255, 255) 
                    @_RB.cctx.strokeRect(scr.left, scr.top, scr.width, scr.height)
                    ###
                    @_RB.cctx.drawImage(@canvas,
                        0,
                        0
                        @_loopRect.width, 
                        @_loopRect.height
                        
                        @_loopRect.x|0, 
                        @_loopRect.y|0, 
                        @_loopRect.width, 
                        @_loopRect.height, )
        @_RB.cctx.restore()

# RBGame is the class to use to create your main game object.
# It takes the settings you pass in, and stores it in RB().env("settings")
# Your settings should have the following sub-objects
# @example
#    .engine
#      fps = [Number]
#      screensizex = your game screen width
#      screensizey = your game screen height
#      canvaswidth = your canvas screen width
#      canvasheight = your canvas sccren height
#      canvasid = the HTML id of the canvas object to use
#      canvassmoothing = [Boolean] whether or not to use image interpolation.
#
#    .images
#      imagename : "path/to/image"
#      imagename2: "path/to/image"
#
#    .sounds
#     soundname : "path/to/sound"
#
#    .spritesheets
#      ssname : [name, image, xsprites, ysprites]
#      ss2name : [name2, image2, xsprites, ysprites]
#
#    .game
#      miscvar1 : value
#      miscvar2 : value
#
# Any RBPreloader scene which you create will interrogate this structure
# and preload your images, sounds, and spritesheets for you.
#
# An RBGame will automatically run and draw the current {RBScene}.
# So any scene that you create and push() onto the scene stack will be 
# the currently executing scene. You can use the RB_EVENT_ON_SCENE_ENTER
# and RB_EVENT_ON_SCENE_EXIT events to control logic flow in your game.
# Simply add an event delegate to the a scene, and specify the function 
# you would like to be called when that scene exits.
#
# RBGame objects have automatic pausing when pressing p.
# They also have the TAB key bound to show debug statistics.
#
class RBGame
    constructor: (@settings, @canvas) ->
        @_RB = RB()
        @currentScene = null
        @stats = new RBStats()
        @screen

        if @settings? #copy passed settings in, preserving defaults
            for key, val of @settings
                RBV(key, val)
        @settings = RBV()
        
        if not @canvas?
            @canvas = document.getElementById(@settings.engine.canvasid)
        RB().set_screen(@canvas, @settings.engine.canvaswidth, @settings.engine.canvasheight, @settings.engine.screenwidth, @settings.engine.screenheight, @settings.engine.canvassmoothing)
        #RB().set_smoothing(@settings.engine.canvassmoothing)
        
        #RB().env("settings", @settings)
        RB().env("__game", @)
        @_canvasRect = @_RB._canvasRect #so we don't need to call each frame
        
        @scenemanager = RBSceneManager.get_instance()
        @_showstats = false
        @_paused = false
        @_dt = 0
        @.bind_debug_keys()
        #console.log(@settings)
        #console.log("RBGame: Resetting frame rate to #{@settings.engine.fps}")
        @_RB.reset_frame_rate(@settings.engine.fps, @.runloop)
        @_RB.stop(false)
    
    bind_debug_keys: () =>
        @_RB.input.bind(RB_KEY_UP, @.debug_keys)
    unbind_debug_keys: () =>
        @_RB.input.unbind(RB_KEY_UP, @.debug_keys)

    pre_update: (dt) ->
        #reset the update timer
        @stats.startupdate() if @_showstats

        
    update: (dt) ->
        #update everything in the current scene
        @currentscene = @scenemanager.current_scene()
        if @currentscene != null
            @currentscene.update(dt)


    post_update: (dt) ->
        #update and truncate the update time.
        @stats.endupdate() if @_showstats

    pre_render: () ->
        #reset the draw timer
        @stats.startdraw() if @_showstats

        #scale - if the screen size is different to canvas size
        @_RB.cctx.scale(RBV(RBV_ENG).screenscalex, (RBV(RBV_ENG).screenscaley))

    render: () ->
        #draw everything in the current scene
        if @currentscene != null
            @currentscene.draw()


    post_render: () ->
        if @_showstats
            #update and truncate the draw time
            @stats.enddraw()
            @.stats.draw()
            #This loop is run every frame.

    debug_keys: (event) =>
        keycodes = @_RB.input.keys
        code = event.keyCode
        if code is keycodes.p #detect pause
            @_RB.toggle_pause()
            @_RB.input.swallow(event)
        if code is keycodes.numlock #detect debug
            @_RB.setDebug(not @_RB.isDebug() )
            @_showstats = @_RB.isDebug()
            @_RB.input.swallow(event)
        if code is keycodes.tab and @_RB.isDebug()
            @_RB.setDebug(not @_RB.isDebug("bb"), "bb")
            @_RB.input.swallow(event)
    
    #If the current FPS is set to be 30,
    #then this loop is being called every 33ms.
    runloop: () =>
        @_RB.cctx.clearRect(0, 0, @_canvasRect.width, @_canvasRect.height)
        @_dt = @_RB.dt()
        @.pre_update(@_dt)
        @.update(@_dt)
        @.post_update(@_dt)
        @_RB.cctx.save()
        @.pre_render()
        @.render()
        @.post_render()
        @_RB.cctx.restore()
        @_RB.destroy_entities()
        @_RB.actions.update(@_dt)
class RBFactory
    constructor: () ->
        @named_classes = {}

    add_class: (name, classtype) ->
        @named_classes[name] = classtype

    create: (classname, args) ->
        switch args.length
            when 0 then return new @named_classes[classname]()
            when 1 then return new @named_classes[classname](args[0])
            when 2 then return new @named_classes[classname](args[0],args[1])
            when 3 then return new @named_classes[classname](args[0],args[1],args[2])
            when 4 then return new @named_classes[classname](args[0],args[1],args[2],args[3])
            when 5 then return new @named_classes[classname](args[0],args[1],args[2],args[3],args[4])
            when 6 then return new @named_classes[classname](args[0],args[1],args[2],args[3],args[4],args[5])
            when 7 then return new @named_classes[classname](args[0],args[1],args[2],args[3],args[4],args[6])



#RB Editor is a component that allows you to create layers/groups
#and populate them with entities
#and then spit those out as a json structure.
#it will also generate UI for each element so that you can set individual properties
#you can attach it to a currently running scene and it can see all of the classes available.

class PropertiesPane
    constructor: () ->
        @pane = document.createElement("div")
        panetext = document.createTextNode("this is the pane content")
        @pane.appendChild(panetext)
        #$(@pane).insertBefore(RB().canvas)

class EditorClearTile extends RBRectEntity
    constructor: (X, Y, blank, Width=32, Height=32, name="Clear") ->
        super(X, Y, Width, Height, name)
        @.set_fill([255,0,0,128])
        @.set_anchor(0,0)
        @.alwaysOnScreen = true

class RBEditor extends RBEntity
    constructor:(@gridx, @gridy, @layers, @namedClasses, @scene, @scenecam) ->
        super(0, 0, 0, 0, name="RBEditor")
        @_RB.input.bind(RB_KEY_UP, @.detect_editor_key)
        @_previous_scene_update = null
        @_previous_stage_position = new RBPoint() 
        if @scene?
            @_previous_scene_update = @scene.update
            

        @alwaysOnScreen = true
        #The rect of the map that we are over. Often identical to currentRect,
        
        @parallax_offset = new RBPoint()

        @map = new RBGroup()
        @classes = []
        @factory = new RBFactory()
        @gridcolour = new RBColour([0, 0, 255, 32])

        for layer in @layers
            @map.add_member( layer )

        @namedClasses["Clear"] = EditorClearTile

        #we assume tiles are x, y, and some numeric param
        for classname, classsymbol of @namedClasses
            @factory.add_class(classname, classsymbol)
            @classes.push({'name':classname, 'class':classsymbol, 'param': 0})

        @currentClass = @classes[0]
        @currentLayer = @map.members[0]

        @uiLayer = new RBLayer()
        @.add_child(@uiLayer)
        @uiLayer.set_parallax(0, 0)
        w = @_RB.screen().width
        size = w/20
        @layerLabel = new RBText(w/2, 1*size, "Layer ??", "Arial", size)
        @classLabel = new RBText(w/2, 2*size, "Class ??", "Arial", size)
        @paramLabel = new RBText(w/2, 3*size, "Param ??", "Arial", size)
        @currentInfo = new RBText(w/2, 4*size, "x: y: ", "Arial", size)
        @layerLabel.alwaysOnScreen = true
        @classLabel.alwaysOnScreen = true
        @paramLabel.alwaysOnScreen = true

        @uiLayer.add_child(@layerLabel)
        @uiLayer.add_child(@classLabel)
        @uiLayer.add_child(@paramLabel)
        @uiLayer.add_child(@currentInfo)
        
        @currentRect = new RBRectEntity(0, 0, @gridx, @gridy)
        @currentRect.set_colour(0, 255, 0, 255)
        @currentRect.set_fill([255,0,0,128])
        @currentRect.set_anchor(0,0)
        @currentRect.alwaysOnScreen = true
        @.add_child(@currentRect)

        @currentTile = new RBEntity()
        @currentRect.add_child(@currentTile)
        @currentTile.alwaysOnScreen = true

        @mapoffset = new RBPoint(0, 0)
        #except in parallax situations.
        @mapRect = new RBRectEntity(0, 0, @gridx, @gridy)
        @mapRect.set_anchor(0, 0)

        
        @.hide()

        @dragTileUnder = null #keep track of tile to destroy when dragging

    load_map_from_file: (path) ->
        f = document.createElement('script')
        f.type = "text/javascript"
        f.src = path
        RBV("__editor", @)
        document.body.appendChild(f)
        document.body.removeChild(f)

    # UNTESTED
    load_map_from_url: (url) ->
        successfunc = (rtext) =>
            console.log("got it!")
            console.log(rtext)
        failfunc = (rtext) =>
            console.log("failed")
            console.log(rtext)

        req = new RBRequest(url, successfunc, failfunc)
        req.get()


    load_map_from_text: (jsontext) ->
        console.log("loading map")
        
        mapdata = JSON.parse(jsontext)
        @.load_map(mapdata)
    
    load_map: (mapdata) ->
        console.log(mapdata)
        find_layer_by_name = (name, obj) ->
            if obj.name == name
                return true
            else
                return false

        for layername, layerdata of mapdata
            #console.log("Layername: #{layername}, LayerData: #{layerdata}")
            layer = @map.find(layername, find_layer_by_name)
            console.log(layer)
            for classname, instancelist of layerdata
                for instanceargs in instancelist
                    x = instanceargs[0]
                    y = instanceargs[1]
                    console.log("Creating #{classname} at #{x},#{y}")
                    newtile = @factory.create(classname, [x, y])
                    layer.add_child(newtile)


    create_current_tile:(x=0, y=0) ->
        newtile = @factory.create(@currentClass.name, [x, y, @currentClass.param])
        anchoroffset = newtile.xy_to_topleft()
        newtile.active = false
        newtile.move_by(-anchoroffset.x, -anchoroffset.y)
        return newtile

    disable_scene: () ->
        if @scene?
            @scene.focus(false)
            RB().actions.paused = true #hack to pause all actions
            p = @_previous_scene_update
            s = @scene
            @scene.update = (dt) ->
                p.call(s, 0)

            @_previous_stage_position.x = @scene.stage.x
            @_previous_stage_position.y = @scene.stage.y

    enable_scene: () ->
        if @scene?
            @scene.focus(true)
            RB().actions.paused = false #hack to enable all actions
            @scene.update = @_previous_scene_update
            @scene.stage.x = @_previous_stage_position.x
            @scene.stage.y = @_previous_stage_position.y

    update_map_tile_position: () ->
        #update the map tile position relative to the
        #currentTile position

    recreate_display_tile:() ->
        @currentTile.destroy()
        @currentTile = null
        @currentTile = @.create_current_tile()
        @currentRect.add_child(@currentTile)
        @currentRect.alwaysOnScreen = true
        @currentTile.alwaysOnScreen = true

        @classLabel.set_text("Class: #{@currentClass.name}")
        @layerLabel.set_text("Layer: #{@currentLayer.name}")
        @paramLabel.set_text("Param: #{@currentClass.param}")
        
        @.fadestate_all_layers(true)
        @currentLayer.unfade()

    fadestate_all_layers: (val) ->
        
        for layer in @map.members
            if val
                layer.fade()
            else
                layer.unfade()

    next_layer: () ->
        @currentLayer = @map.get_after(@currentLayer)
        if @currentLayer is null
            @currentLayer = @map.members[0]
        @.recreate_display_tile()
    prev_layer: () ->
        @currentLayer = @map.get_before(@currentLayer)
        if @currentLayer is null
            @currentLayer = @map.members[@map.members.length-1]
        @.recreate_display_tile()


    next_class: () ->
        nextindex = @classes.indexOf(@currentClass) + 1
        if nextindex >= @classes.length
            nextindex = 0
        @currentClass = @classes[nextindex]
        @.recreate_display_tile()
    
    prev_class: () ->
        previndex = @classes.indexOf(@currentClass) - 1
        if previndex < 0
            previndex+=@classes.length
        @currentClass = @classes[previndex]
        @.recreate_display_tile()

    set_param: (val) ->
        @currentClass.param = val
        @.recreate_display_tile()

    to_json: (for_string = false) ->
        output = {}
        outputstr = ""
        outputhdr = ""
        outputftr = ""
        serialise_layer_classtypes = (l, ct, cn, out) ->
            for o in l.members
                if o instanceof ct
                    out[l.name][cn].push(o.to_list() )

        if not for_string
            outputhdr = "RBV(\"__editor\").load_map("
        else
            outputhdr = "\'"
        for layer in @map.members
            output[layer.name] = {}
            for classname of @namedClasses
                output[layer.name][classname] = []
            for classname, classtype of @namedClasses
                for object in layer.members
                    if object instanceof classtype
                        output[layer.name][classname].push(object.to_list() )
        if not for_string
            formatfn = (k,v) ->
                if v instanceof Array
                    return "[#{v}]"#JSON.stringify(v)
                return v
            outputstr = JSON.stringify(output, undefined, 2)
            outputftr = ");"
        else
            outputstr = JSON.stringify(output)
            outputftr = "\'"

        console.log("//TOJSON - COPY FROM BELOW THIS LINE")
        console.log("#{outputhdr}#{outputstr}#{outputftr}")
        console.log("//END TOJSON - COPY FROM ABOVE THIS LINE")

    show: () ->
        @visible = true
        @_RB.input.bind(RB_KEY_UP, @.on_key_up, true)
        @_RB.input.bind(RB_KEY_DOWN, @.on_key_down, true)
        @_RB.input.bind(RB_MOUSE_MOVE, @.on_mouse_move, true)
        @_RB.input.bind(RB_MOUSE_DOWN, @.on_mouse_down, true)
        @_RB.input.bind(RB_MOUSE_WHEEL, @.on_mouse_wheel, true)
        @.recreate_display_tile()
        @.disable_scene()

        @mapoffset.x = 0 #-RB().screen().x/2
        @mapoffset.y = 0 #-RB().screen().y/2
        #@_RB.screen().set_position(0, 0)
        @.warp_to_right_position()
        #@scene.stage.set_position(@mapoffset.x, @mapoffset.y)
        #@.set_position(@mapoffset.x, @mapoffset.y)
        #@_RB.screen().set_position(-@mapoffset.x, -@mapoffset.y) 


    hide: () ->
        @visible = false
        @_RB.input.unbind(RB_KEY_UP, @.on_key_up, true)
        @_RB.input.unbind(RB_KEY_DOWN, @.on_key_down, true)
        @_RB.input.unbind(RB_MOUSE_MOVE, @.on_mouse_move)
        @_RB.input.unbind(RB_MOUSE_DOWN, @.on_mouse_down)
        @_RB.input.unbind(RB_MOUSE_WHEEL, @.on_mouse_wheel)
        @.fadestate_all_layers(false)
        @.enable_scene()

    detect_editor_key: (event) =>
        keycode = event.keyCode
        keys = @_RB.input.keys
        if keycode is keys.e and 
                      @_RB.input.key_pressed(keys.ctrl)
            if @visible
                console.log("Hiding RBEditor")
                @.hide()
            else
                console.log("Showing RBEditor")
                @.show()

    on_key_up: (event) =>
        code = event.keyCode
        keys = @_RB.input.keys
        
    on_mouse_wheel: (event) =>
        i = @_RB.input
        delta = i.mouse.wheeldelta
        if delta > 0
                @.next_class()
            else if delta < 0
                @.prev_class()

        ###
        if i.key_pressed(i.keys.c)
            if delta > 0
                @.next_class()
            else if delta < 0
                @.prev_class()
        if i.key_pressed(i.keys.l)
            if delta > 0
                @.next_layer()
            else if delta < 0
                @.prev_layer()
        ###
        i.swallow(event)

    on_key_down: (event) =>
        keys = @_RB.input.keys
        code = event.keyCode
        mvmtx = 0
        mvmty = 0
        switch code
            when keys.right
                mvmtx += 1
                #@_RB.screen().x += 1
            when keys.left
                #@_RB.screen().x -= 1
                mvmtx -= 1
            when keys.up
                #@_RB.screen().y -= 1
                mvmty -= 1
            when keys.down
                #@_RB.screen().y += 1
                mvmty += 1

            when keys.l
                if @_RB.input.key_pressed(keys.shift)
                    @.prev_layer()
                else
                    @.next_layer()
            when keys.c
                if @_RB.input.key_pressed(keys.shift)
                    @.prev_class()
                else
                    @.next_class()
            when keys.v
                @.set_param(@currentClass.param-1)
            when keys.b
                @.set_param(@currentClass.param+1)
            when keys.j
                @.to_json()
        @_RB.input.swallow(event)
        
        if @scene?
            xdiff = -mvmtx*@gridx
            ydiff = -mvmty*@gridy
            @mapoffset.x += xdiff
            @mapoffset.y += ydiff

            @.warp_to_right_position()
            #console.log("scene pos is now: #{@scene.stage.x}, #{@scene.stage.y}")
            #@scene.cam.offsetx += mvmtx
            #@scene.cam.offsety += mvmty
        #else
        #    @_RB.screen().set_position(-mvmtx, -mvmty)


    warp_to_right_position: () ->
        if @scene?
            @scene.stage.set_position(@mapoffset.x, @mapoffset.y)
            @.set_position(@mapoffset.x, @mapoffset.y)
            @_RB.screen().set_position(-@mapoffset.x, -@mapoffset.y)

    on_mouse_move: (event) =>
        if @visible
            #mx = ~~(@_RB.input.mouse.x / @gridx)
            #my = ~~(@_RB.input.mouse.y / @gridy)
            scr = RB().screen()
            @parallax_offset.x = (@currentLayer.x % @gridx)# * @gridx
            @parallax_offset.y = (@currentLayer.y % @gridy)# * @gridy
            #mx = Math.floor((@_RB.input.mouse.x + @parallax_offset.x) / @gridx)
            #my = Math.floor((@_RB.input.mouse.y + @parallax_offset.y) / @gridy)
            mx = Math.floor((@_RB.input.mouse.x) / @gridx)
            my = Math.floor((@_RB.input.mouse.y) / @gridy)
            
            #console.log("Mouse: #{@_RB.input.mouse.x}, #{@_RB.input.mouse.y}, Editor mouse: #{mx}, #{my}")
            
            
            
            #@currentRect.set_position( (mx - @parallax_offset.x) * @gridx, (my-@parallax_offset.y) * @gridy)
            #@currentRect.set_position( (mx * @gridx) + @mapoffset.x, (my * @gridy) + @mapoffset.y)
            @currentRect.set_position( (mx * @gridx), (my * @gridy))
            @currentInfo.set_text("x: #{@currentRect.x}, y: #{@currentRect.y}")

            #@currentRect.sub(@parallax_offset)
            if @_RB.input.mouse.leftpressed
                #console.log("mouse held down")
                tileunder = @currentRect.overlaps_in_rlist(@currentLayer.members)
                if tileunder == null or tileunder != @dragTileUnder
                    @.on_mouse_down(event)
                    @dragTileUnder =  @currentRect.overlaps_in_rlist(@currentLayer.members)
            #For the purposes of draw, we need to adjust for parallax offset
            #@currentRect.move_by(@parallax_offset.x, -@parallax_offset.y)

    on_mouse_down: (event) =>
        tileunder = @currentRect.overlaps_in_rlist(@currentLayer.members)
        if tileunder != null
            console.log("Removal - tileunder = ")
            console.log(tileunder)
            @_RB.env('tileunder', tileunder)
            tileunder.destroy() #destroy removes child from parent
        if ! (@currentClass.class is EditorClearTile)
            #when we create the child, we remove the layer parallax offset (@currentLayer.x%@gridx etc, since
            #the layer position should be relative to itself
            @currentLayer.add_child(@.create_current_tile(@currentRect.x-(@currentLayer.x%@gridx), @currentRect.y-(@currentLayer.y%@gridy)))

        return @currentLayer

    draw: () ->
        @_RB.cctx.strokeStyle = @gridcolour.htmlrgba
        scr = @_RB.screen()
        rows = scr.height / @gridy
        cols = scr.width / @gridx
        #offx = scr.left % @gridx
        #offy = scr.top % @gridy
        offx = (scr.left-@currentLayer.left) % @gridx
        offy = (scr.top-@currentLayer.top) % @gridy
        
        for colx in [0..cols]
            @_RB.cctx.strokeRect(-offx+scr.left+colx*@gridx, -offy+scr.top, @gridx, scr.height)
        for rowy in [0..rows]
            @_RB.cctx.strokeRect(-offx+scr.left, -offy+scr.top+rowy*@gridy, scr.width, @gridy)

class PaneUI
    constructor:(title) ->
        @element = $("<div />", {
            'class': "paneui"}
        )
        @titleelement = $("<div />", {
            text: title,
            'class': "panetitle"
        })
        @bodyelement = $("<div />", {
            text: "pane body text",
            'class' : "panebody"
        })
        $(@element).append(@titleelement)
        $(@element).append(@bodyelement)

class UISpecDisplay
    constructor: (@obj) ->
        @elements = {}
        @spec = @obj.uispec()
        console.log("uispec was: #{@spec}")
        for key, item of @spec
            console.log("key is #{key}, val is #{item}")
            #title
            @elements[key] = $("<div class=\"uispec_property\">")
            $(@elements[key]).append("<div class=\"uispec_proptitle\"><b>#{key}: </b></div>")
            itemdata = @.update_value(key)
            #based on the type of the itemdata, you would 
            #change the display type of what we are about
            #to append
            $(@elements[key]).append($("<div class=\"uispec_propvalue\">#{itemdata}</div>"))

    update_value: (key) ->
        return @obj[@spec[key][0]]
            
class ObjectUI extends PaneUI
    constructor: (obj, name="Unknown") ->
        if obj.name?
            name = obj.name
        super(name)
        
        if obj instanceof RBObject
            console.log("was an rbobject")
            displayobj = new UISpecDisplay(obj)
            for key, uielement of displayobj.elements
                $(@bodyelement).append(uielement)
        else
            console.log("populating a non RBObject")
            for own key, val of obj
                console.log("key: #{key}, val: #{val}")
                $(@bodyelement).append($("<p>", {
                    text: "#{key} : #{val}",
                }))

        #$(@bodyelement).text("ObjectUI")
        #$(@bodyelement).append("<textarea />")

###
So what I need is:
    each class should have a static method for printing? or at least 'displaying'
        so an RPPoint should be able to look like "{x:30, y:40}"
        That actually begins to look like a uispec, which would
        expose x and y.
        expose(propname, valuetype (widget for display eg text, number, slider), readonly=false)
    ALSO
        the UI for the editor needs to be updated as the game is running.
        so all the values need to be refreshed.
        How do I do that? How do I have the structs REFERENCING the values instead of literals?
###

populate_objects = (objectlist) ->
    objspane = $("#objectpane")
    console.log(objspane)
    for obj in objectlist
        #objspane.append($("<div></div>").append($("<p>Hello #{count}</p>")))
        #objspane.append(new ObjectUI().element)
        objspane.append(new ObjectUI(obj, "Object").element)


