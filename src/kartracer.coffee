class KREnvTile extends RBSprite
    constructor: (X, Y, framenum) ->
        s = RB().env("settings")
        super(X, Y, RBV_IMG.trackimage, RBV_GAME.tracktilewidth, RBV_GAME.tracktileheight)
        ss = @_RB.media.get_spritesheet( RBV_SS.track )
        @.add_animation("default", new RBAnimation(ss, [framenum], RB_ANIM_NORMAL, 100) )
        @.set_animation("default")

class Grass extends KREnvTile
    constructor: (X, Y) ->
        super(X, Y, 3)
class VStraight extends KREnvTile
    constructor:(X, Y) ->
        super(X, Y, 5)
class HStraight extends KREnvTile
    constructor:(X, Y) ->
        super(X, Y, 1)
class Quarter1 extends KREnvTile
    constructor:(X, Y) ->
        super(X, Y, 0)
class Quarter2 extends KREnvTile
    constructor:(X, Y) ->
        super(X, Y, 2)
class Quarter3 extends KREnvTile
    constructor:(X, Y) ->
        super(X, Y, 7)
class Quarter4 extends KREnvTile
    constructor:(X, Y) ->
        super(X, Y, 6)

class Kart extends RBSprite
    constructor: (X, Y, imagename) ->
        super(X, Y, imagename, RBV_GAME.carwidth, RBV_GAME.carheight)
        #@physics = new RBPhysics()
        #@.add_component(@physics)
        #@physics.floating = true

class PlayerKart extends Kart
    constructor: (X, Y) ->
        super(X, Y, RBV_IMG.playerimage)
        @velocity = 0
        @acceleration = 5
        @maxVelocity = 200
        @heading = 0
        @turnRate = 360 / 100
        @delta = new RBPoint()
    accelerate: () ->
        @velocity += @acceleration
        if @velocity > @maxVelocity
            @velocity = @maxVelocity
    decelerate:() ->
        @velocity -= @acceleration
        if @velocity < 0
            @velocity = 0
    turn_right: () ->
        @heading += @turnRate
    turn_left: () ->
        @heading -= @turnRate

    update: (dt) ->
        super(dt)
        @.set_angle(@heading)
        @delta.cartesian(@heading, @velocity * dt/1000)
        @.move_by(@delta.x, @delta.y)

class KartRacerTitleScene extends RBPreLoaderScene
    constructor: () ->
        super("KartRacerTitleScreen")
        @input.add(RB_KEY_UP, @.on_key_up)
        @complete = new RBText(100, 100, "")
        @continue = new RBText(100, 130, "")
        @stage.add_child(@complete)
        @stage.add_child(@continue)
        
    on_preloaded: () ->
        #play a sound here?
        console.log("Preloaded!")
    on_key_up: (event) =>
        code = event.keyCode
        keys = @_RB.input.keys
        if code is keys.space
            @.pop()

    update: (dt) ->
        super(dt)
        @complete.set_text("#{@pl.stats.total}")
        if @finished
            @continue.set_text("Press SPACE to continue")
            

class KartRacerGameScene extends RBScene
    constructor:() ->
        super("KartRacerScene")
        @player = new PlayerKart(100, 100)
        @envgroup = new RBLayer()
        @envgroup.name = "Enviroment"
        @bggroup = new RBLayer()
        @bggroup.name = "Background"
        @playergroup = new RBLayer()
        @playergroup.name = "Player Group"

        @stage.add_child(@bggroup)
        @stage.add_child(@envgroup)
        @stage.add_child(@playergroup)

        #@cam = new RBCamera(0, 0, RBV(RBV_ENG).canvaswidth, RBV(RBV_ENG).canvasheight, RBV(RBV_ENG).screenwidth, RBV(RBV_ENG).screenheight)
        @cam = new RBCamera(0, 0, RBV_ENG.screenwidth, RBV_ENG.screenheight)
        @cam.follow(@player)
        #@cam.scalex = @cam.scaley = 1
        #@cam.zoom(1.0, 1.0)

        @playergroup.add_child(@player)
        
        @editor = new RBEditor(32, 32, 
            [@envgroup, @bggroup, @playergroup], 
            {"Grass":Grass, 
            "VStraight": VStraight,
            "HStraight": HStraight,
            "Quarter1": Quarter1,
            "Quarter2": Quarter2,
            "Quarter3": Quarter3,
            "Quarter4": Quarter4,
            } )
        
        @editor.load_map_from_text('{"Enviroment":{"Grass":[[[256,288],0]],"VStraight":[[64,192]],"HStraight":[[192,320]],"Quarter1":[[64,64]],"Quarter2":[[192,64],[320,192]],"Quarter3":[[320,320]],"Quarter4":[[192,192],[64,320]],"Clear":[]},"Background":{"Grass":[[192,64],[320,64],[320,192],[192,192],[64,320],[192,320],[320,320],[64,64],[64,192]],"VStraight":[],"HStraight":[],"Quarter1":[],"Quarter2":[],"Quarter3":[],"Quarter4":[],"Clear":[]},"Player Group":{"Grass":[],"VStraight":[],"HStraight":[],"Quarter1":[],"Quarter2":[],"Quarter3":[],"Quarter4":[],"Clear":[]}}')

    update: (dt) ->
        super(dt)
        @cam.update(dt)
        input = @_RB.input
        if input.key_pressed(input.keys.left)
                @player.turn_left()
            else if input.key_pressed(input.keys.right)
                @player.turn_right()
            else if input.key_pressed(input.keys.up)
                @player.accelerate()
            else if input.key_pressed(input.keys.down)
                @player.decelerate()

    draw: () ->
        super()
        #@stage.render()
        #@editor.render()
        @cam.draw(@stage)
        @cam.draw(@editor)

class KartRacer extends RBGame
    constructor: () ->
        super()
        @titlescene = @gamescene = @gameoverscene = null
        @_RB.events.add(RB_EVENT_ON_START, @.start_state)
        
    start_state: () =>
        #Remove this event from RB.start() - it isn't needed again
        @_RB.events.remove(RB_EVENT_ON_START, @.start_state)
        #Preload
        if @titlescene is null
            @titlescene = new KartRacerTitleScene()
            @titlescene.events.add(RB_EVENT_ON_SCENE_EXIT, @.start_game)
        @titlescene.push()

    start_game: () =>
        @gamescene = new KartRacerGameScene()
        @gamescene.events.add(RB_EVENT_ON_SCENE_EXIT, @.game_over)
        @gamescene.push()

    game_over: () =>
        console.log("Game Over - Restarting!")
        if @gamescene isnt null
            @gamescene.destroy()
        @.start_state()


    

#This variable, _game, will hold our entire game.
_game = null
#This function is called to create a new game
window.run_kartracer = () ->
    
    RBV(RBV_ENG, {
        fps: 60
        canvasid: "canvas"
        screenwidth: 400
        screenheight: 400
        canvaswidth: 400
        canvasheight: 400

    })

    RBV(RBV_IMG, {
        playerimage: "data/kartracer/car.png"
        trackimage: "data/kartracer/track.png"
    })
    RBV(RBV_SND, {
        laser: "data/sfx.wav"
    })
    RBV(RBV_SS, {
        track: ["track", RBV_IMG.trackimage, 5, 5] 
    })
    RBV(RBV_GAME, {
        carwidth: 38
        carheight: 53
        tracktilewidth: 128
        tracktileheight: 128
    })


    if not (_game is null)
        return #prevent multiple inits
    _game = new KartRacer()
    RB().start()


