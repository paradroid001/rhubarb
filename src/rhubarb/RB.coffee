#requires RBBase, RBGroup, RBMedia, RBInput, RBTimer

# Rhubarb
# =======
# _A Game engine for HTML5 and Javascript_

_RB = null

# Returns the current game engine.
#
# @return [RBInit] The game engine
RB = () ->
    return _RB
# A Function for getting or setting the Rhubarb Variables collection
# of the game engine.
# Calling this function with a key only returns the value of the
# collection at that key. There are 5 predefined key constants for 
# vaious # engine settings sections:
# @note
#   RBV_ENG : For engine settings (fps, screen dimensions, etc)
#   RBV_IMG : For images used by your game
#   RBV_SND : For sounds used by your game
#   RB_SS   : For spritesheets used by your game
#   RBV_GAME: For miscellaneous variables/constants youd like to
#       set for your game.
#   
# Calling this function with a key and a value sets the collection
#   at that key.
#
# @param [String] key
#   A key to store data against. Default predefined constants are
#       {RBV_ENG}, {RBV_GAME}, {RBV_IMG}, {RBV_SS}, and {RBV_SND}
# @param [*] value [Optional]
#   Any value to store against this key.
#
# @example
#   RBV(RBV_GAME.max_enemies) returns the 'max_enemies' value from RBV_GAME
#   RBV(RBV_ENG.screenwidth) returns the 'screenwidth' value from RBV_ENG
#   RBV(RBV_IMG).playerimage = "data/player.png"
#
#   This class now does a translation on RBV_ENG and related values,
#   so that you can use those as variables to pull data out.
#   So you could either do:
#   RBV_ENG.<someimagename>
#   or
#   RBV(RBV_ENG).<someimagename>
RBV_SS = {}
RBV_ENG = {}
RBV_IMG = {}
RBV_SND = {}
RBV_GAME = {}
RBV_MUSIC = {}

RBV = (key, value) ->
    s = RB().env("settings")
    if not key?
        return s
    if not value?
        if key in [RBV_SS, RBV_ENG, RBV_GAME, RBV_IMG, RBV_SND, RBV_MUSIC]
            switch (key)
                when RBV_SS then return s[RBV_SS_STR]
                when RBV_IMG then return s[RBV_IMG_STR]
                when RBV_ENG then return s[RBV_ENG_STR]
                when RBV_SND then return s[RBV_SND_STR]
                when RBV_MUSIC then return s[RBV_MUSIC_STR]
                when RBV_GAME then return s[RBV_GAME_STR]
        else
            return s[key]
    else if key in [RBV_SS, RBV_ENG, RBV_GAME, RBV_IMG, RBV_SND, RBV_MUSIC] and typeof value == "object"
        collection = RBV(key)
        for valkey, val of value
            collection[valkey] = value[valkey]
    else
        s[key] = value


class RBStats
    constructor:() ->
        @_RB = RB()
        @updatetimer = new RBTimer()
        @drawtimer = new RBTimer()
        @updatetime = 0
        @drawtime = 0

    startupdate: () ->
        @updatetimer.reset()
    startdraw: () ->
        @drawtimer.reset()
    endupdate: () ->
        @updatetime = ((@updatetime + @updatetimer.elapsed()) /2) | 0
    enddraw: () ->
        @drawtime = ((@drawtime + @drawtimer.elapsed())/2) | 0

    draw: () =>
        scr = RB().canvas()
        fsize = Math.max(12, Math.floor(scr.width/40))
        currline = fsize
        writeline = (str) =>
            @_RB.cctx.fillText(str, 5, currline)
            currline += fsize
        @_RB.cctx.fillStyle = "rgba(255, 255, 255, 0.3)"
        @_RB.cctx.fillRect(0, 0, scr.width/3, scr.height/4)
        @_RB.cctx.fillStyle = "#000000"
        @_RB.cctx.font = "#{fsize}px helvetica"
        writeline("Rhubarb v#{RB_VERSION}")
        writeline("Frame: #{@_RB.dt().toString()}ms")
        writeline("Update: #{@updatetime}ms")
        writeline("Draw: #{@drawtime}ms")
        writeline("Idle: #{(@_RB.dt() - (@updatetime + @drawtime)).toString()}")
        writeline("Created: #{@_RB._entityCount}")
        writeline("Destroyed: #{@_RB._destroyedEntityCount}")
        if @_RB._offscreenCulling
            writeline("Culled #{@_RB._culledEntityCount}")



# RBInit is the object which creates the entire game engine.
# Instancing this class returns an object which holds the 
# state of the game engine. Once instanced, this class should
# not be instanced again. If you want global access to the game
# engine, call the .RB() function.
class RBInit
    # @private 
    #   Logging object
    _log: null

    # @private 
    #   The function that will be called as the game main loop
    _mainloop: null

    #@private 
    #   The internal timer for RB
    _timer: null

    # @private 
    #   The 'dictionary' used by the RB.env() API @see env() 
    _namedCollection: null

    # @private 
    #   The number of milliseconds since the last engine tick.
    _dt: 0

    # @private 
    #   The interval object from the setInterval() for 
    #   the main loop
    _mainloopinterval: null

    # @private 
    #   The FPS requested by the client
    _desiredfps: 1

    # @private 
    #   How many ms each interval should be to get @_desiredfps
    _frametick: 0

    # @private 
    #   Should timestep be locked? i.e. constant 1000/fps? 
    #   (experimental)
    #
    _lockstep: false

    # @private 
    #   Starting value for RB Object ID's
    _globalObjectID: 1

    # @private 
    #   RB Destroy list, used for cleaning up entities 
    #   after destroy.
    _destroylist: null

    # @private 
    #   A list for collisions, to save being recreated 
    #   each frame
    _collisions: null

    # @private 
    #   A group for UI Objects to be tracked (experimental)
    _uiobjects: null

    # @private 
    #   Rect describing screen coords
    _screenRect: null

    # @private 
    #   Rect describing canvs coords
    _canvasRect: null

    # @private Flag to indicate if image interpolation should be
    #   on or off for the canvas
    _canvasSmoothing: false

    # Don't draw elements when they're offscreen
    _offscreenCulling: false
    _culledEntityCount: 0
    _entityCount: 0
    _destroyedEntityCount: 0

    # @private
    #   Whether or not we are in debug mode
    _debug: false

    # @private
    #   Array of debug categories
    _debugs: []
    
    # @private
    #   If paused is true, no updates are done
    _paused: false

    # @property [HTMLCanvas]
    # The context accessible to elements trying to draw
    # All elements should draw to RB().cctx
    cctx: null

    # @private
    # Internal context stack
    _cctxstack: []

    # @private 
    #   The current canvas object
    _canvas: null

    # @private 
    #   The current internal canvas context
    _context: null

    # @property [RBInput]
    # Global RB Input object
    input: null

    # @property [RBDelegateCollection]
    # Global events object
    events: null

    # @property [RBMedia]
    # Global RB media repository for the game (images  / sounds)
    media: null

    # @property [RBActionList]
    # Global RB Actions List
    actions: null
    
    # @property [RBPreloader]
    # Preloader
    preloader: null

    #collisionFlagAbsolute: false


    constructor: (debug = false) ->
        _RB = @ #set the global
        @_RB = @
        @_log = new RBLog(true)
        @_log.enable()
        @_debug = debug
        @_namedCollection = {}
        @_timer = new RBTimer()

        @_uiobjects = new RBGroup()
        @_screenRect = new RBRect(0,0,0,0)
        @_canvasRect = new RBRect(0,0,0,0)

        @_destroylist = []
        @_collisions = []
        
        # Global RB Input object
        @input = new RBInput(@)
        # Global events object
        @events = new RBDelegateCollection("RB Global Events", 
            [RB_EVENT_ON_START, 
            RB_EVENT_ON_STOP, 
            RB_EVENT_ON_PAUSE, 
            RB_EVENT_ON_UNPAUSE])
        # Global RB media repository for the game (images  / sounds)
        @media = new RBMedia()
        # Global RB Actions List
        @actions = new RBActionList()
        # @property [PreloaderStats] Preloader
        @preloader = new PreLoaderStats()

        settings = {}
        RBV_ENG.fps = 30
        RBV_ENG.canvaswidth = 400
        RBV_ENG.screenwidth = 400
        RBV_ENG.screenheight = 400
        RBV_ENG.canvassmoothing = false

        settings[RBV_ENG_STR] = RBV_ENG
        settings[RBV_GAME_STR] = RBV_GAME
        settings[RBV_IMG_STR] = RBV_IMG
        settings[RBV_SND_STR] = RBV_SND
        settings[RBV_MUSIC_STR] = RBV_MUSIC
        settings[RBV_SS_STR] = RBV_SS

        @.env("settings", settings)

        
    # Set or retrieve a 'global' named value that you are registering
    # with RB.
    # It is a good place to store a reference to a variable you will
    # need in many places - for example the player object.
    # @example RB().env("Player", playerobj) # you now have a registred 
    # 'Player' object.
    # @example RB().env("Player") #returns the current object registerd
    # under the 'Player' name
    env: (name, value) ->
        if value?
            @_namedCollection[name] = value
        return @_namedCollection[name]

    calculate_screen_scale: () ->
        #These lines are needed so that an RBGame knows how to scale.
        xscale = 1
        yscale = 1
        if @_screenRect.width != 0
            xscale = @_canvasRect.width / @_screenRect.width
        if @_screenRect.height != 0
            yscale = @_canvasRect.height / @_screenRect.height
        RBV(RBV_ENG).screenscalex  = xscale
        RBV(RBV_ENG).screenscaley = yscale

    resize_screen: (w, h) ->
        RBV(RBV_ENG).screenwidth = w
        RBV(RBV_ENG).screenheight = h
        @_screenRect.set_width(w).set_height(h)
        @_screenRect.set_anchor(0,0)
        @.calculate_screen_scale()
        @_canvasSmoothing = @.set_smoothing(@cctx, @_canvasSmoothing)

    resize_canvas: (w, h) ->
        RBV(RBV_ENG).canvaswidth = w
        RBV(RBV_ENG).canvasheight = h
        #resize_canvas: (w=window.innerWidth, h = window.innerHeight) =>
        @_RB.get_canvas().width = w
        @_RB.get_canvas().height = h
        canvaspos = get_cumulative_offset(@_canvas)
        @_canvas.x = canvaspos.x
        @_canvas.y = canvaspos.y
        @_canvasRect.x = canvaspos.x
        @_canvasRect.y = canvaspos.y
        @_canvasRect.set_width(w).set_height(h)
        @_canvasRect.set_anchor(0,0)
        @.calculate_screen_scale()
        @_canvasSmoothing =  @.set_smoothing(@cctx, @_canvasSmoothing)

    
    # Gets the current screen rect
    screen: () ->
        @_screenRect
    # Gets the current canvas rect
    canvas: () ->
        @_canvasRect

    # @private
    log: (msg) ->
        @_log.log(msg)
    # @private
    debug: (msg) ->
        @_log.debug(msg)
    # @private
    warn: (msg) ->
        @_log.warn(msg)
    # @private
    error: (msg) ->
        @_log.error(msg)

    # Switches the current context that RB clients should draw to.
    set_cctx: (ctx) ->
        @cctx = ctx

    push_cctx: () ->
        @_cctxstack.push(@cctx)
    restore_cctx: () ->
        c = @_cctxstack.pop(@cctx)
        if c?
            @.set_cctx(c)
        else
            console.warn("No context to restore off cctxstack")
    # Gets the canvas object
    get_canvas: () ->
        @_canvas
    
    # Sets image interpolation for the canvas either on or off
    set_smoothing: (ctx, val) ->
        if ctx isnt null
            ctx.imageSmoothingEnabled = val
            ctx.webkitImageSmoothingEnabled = val
            ctx.mozImageSmoothingEnabled = val
        return val

    # Pauses the engine
    pause: () ->
        if !@_paused
            @.stop(true)
            console.log("pausing")
    
    # Unpauses the engine
    unpause: () ->
        if @_paused
            @.start(true)
            console.log("unpausing")
    # Toggles pause state with successive calls
    toggle_pause: () ->
        if @_paused
            @.unpause()
        else
            @.pause()

    # Starts the game engine
    # @param [Boolean] fire_start_event
    #   Whether or not to allow the engine to
    #   raise the RB_EVENT_ON_START event. A
    #   false value will suppress it
    start: (fire_start_event = true) ->
        @_paused = false
        @.reset_frame_rate(@_desiredfps, @_mainloop)
        if fire_start_event
            @events.call_delegates(RB_EVENT_ON_START)

    # Stops the game engine
    # @param [Boolean] fire_stop_event
    #   Whether or not to allow the engine to
    #   raise the RB_EVENT_ON_STOP event. A
    #   false value will suppress it
    stop: (fire_stop_event = true) ->
        @_paused = true
        @.reset_frame_rate(0)
        if fire_stop_event
            @events.call_delegates(RB_EVENT_ON_STOP)

    # Returns the currently running scene (if any)
    current_scene: () ->
        sm = RBSceneManager.get_instance()
        if sm?
            return sm.current_scene()
        else
            return null


    # Set the screen parameters.
    # @param canvaselement [Object] The HTML5 canvas element
    # @param canvaswidth [Number] The width of the canvas
    # @param canvasheight [Number] The height of the canvas
    # @param screenwidth [Number] The 'virtual' width of our game screen 
    #  <=canvaswidth, default is canvaswidth
    # @param sreenheight [Number] The 'virtual' width of our game screen 
    #  <=canvasheight, default is canvasheight
    #
    # So you could set screenwidth and screenheight to be less than
    # canvaswidth and canvasheight if you wanted to run the game at a 
    # lower resolution than the canvas.
    set_screen: (canvaselement, canvaswidth, canvasheight, screenwidth=canvaswidth, screenheight=canvasheight, smoothing=false) ->
        
        @_canvas = canvaselement
        #disable double-click select of text outside canvas:
        @_canvas.onselectstart = () ->
            return false

        @_context = canvaselement.getContext('2d')
        @cctx = @_context
        @.resize_canvas(canvaswidth, canvasheight)
        #canvaspos = get_cumulative_offset(@_canvas)
        #@_canvas.x = canvaspos.x
        #@_canvas.y = canvaspos.y
        #@_canvasRect.x = canvaspos.x
        #@_canvasRect.y = canvaspos.y
        #@_canvasRect.refresh()
        #Resize the canvas with RBInput
        #@input.resize_canvas(canvaswidth, canvasheight)
        
        @.resize_screen(screenwidth, screenheight)
        #@_screenRect.set_width(screenwidth).set_height(screenheight)
        #@_screenRect.set_anchor(0,0)
        #@_canvasRect.set_width(canvaswidth).set_height(canvasheight)
        #@_canvasRect.set_anchor(0,0)
        #RB().log(@_screenRect)

        @.env("width", canvaswidth)
        @.env("height", canvasheight)
        
        #These lines are needed so that an RBGame knows how to scale.
        #RBV(RBV_ENG).screenscalex = canvaswidth / screenwidth
        #RBV(RBV_ENG).screenscaley = canvasheight / screenheight
        

        #if our window loses focus, automatically push
        #the game into lockstep mode, so that our dt remains
        #constant while we are away. This prevents gravity pulling us
        #through the floor when we return to focus because dt doesnt
        #become massive, since the time since the last interval fire
        #is irrelevant in lockstep mode.
        window.onblur = () -> 
            _RB.lockstep(true)
        window.onfocus = () -> 
            _RB.lockstep(false)
            _RB._timer.reset()
        
        #This line attaches the log to the HTML document.
        #Commented out at the moment because I'm undecided
        #about using it.
        #@_log.attach()

        @_canvasSmoothing = @.set_smoothing(@cctx, smoothing)
    
    # Sets the game framerate according to the desired fps
    # Calling with an fps of 0 or mainloop of null will stop the game
    # and clear the timer callback
    # @param [Number] fps
    #   The desired frames per second
    # @param [function] mainloop
    #   The functionto call each frame
    reset_frame_rate: (fps, mainloop) ->
                
        if not (@_mainloopinterval is null)
            clearInterval(@_mainloopinterval)
        
        if mainloop?
            @_mainloop = mainloop

        if (fps > 0) #and (@_mainloop is not null) 
            @_desiredfps = fps
            @_frametick = 1000/fps
            #Call @.step every frametick
            @_mainloopinterval = setInterval(@step, @_frametick)
        #Reset the @_timer and @_dt
        @_dt = @_timer.elapsed()
        @_dt = 0

    # @private
    lockstep: (enable) =>
        if enable
            @_lockstep = true
            #@_dt = @_frametick
        else
            @_lockstep = false
            #@.reset_frame_rate(@_desiredfps)
            #@_dt = 0

    # @private
    step: () =>
        if @_lockstep
            @_dt = @_frametick
        else
            @_dt = @_timer.elapsed()
        #Reset culled entitycount
        @_culledEntityCount = 0
        @_mainloop(@_dt)

    # Gets the time since the last frame, in milliseconds
    dt: () ->
        @_dt

    # @private
    # pass in the category as a string
    # available:
    # "bb" (bounding boxes, used in RBEntity)
    # nothing (default 'debug is on')
    isDebug: (category) ->
        if !category?
            @_debug
        else
            if @_debugs[category]?
                @_debugs[category]
            else
                false #better than returning null

    # @private
    setDebug: (val, category) ->
        if !category?
            @_debug = val
            console.log("RB._debug is now #{@_debug}")
        else
            @_debugs[category] = val
            console.log("RB._debugs[#{category}] is now #{val}")
        

    # @private
    newID: () ->
        @_globalObjectID++

    # @private
    add_ui_object: (obj) ->
        @._uiobjects.add_member(obj)

    # @private
    remove_ui_object: (obj) ->
        @._uiobjects.remove_member(obj)

    # @private
    destroy_entities: () ->
        for ent in @_destroylist
            console.log('destroy')
            ent._destroy()
            ent.cancel_actions()
            @input.mouse.unregister_drag(ent)
            @_destroyedEntityCount+=1

        while @_destroylist.length
            #console.log('pop')
            @_destroylist.pop()

    # Collides one array of RBEntity objects with another.
    # Each group needs to be an array object, so passing
    # an array you have made, or passing an RBEnitty.children
    # or RBGroup.members property is fine.
    #
    # @param [Array<RBEntity>] group1
    #   The first group to consider for collision
    # @param [Array<RBEntity>] group2
    #   The second group to consider for collision
    #
    collide: (group1, group2) ->
 
        
        build_collisions = (gr1, gr2, col) ->

            try
                for g1 in gr1
                    for g2 in gr2
                        if g1.solid and g2.solid and !g1.destroyed and !g2.destroyed
                            if g1.overlaps(g2)
                                col.push([g1, g2])
                            else
                                if g1.members? and g1.members.count > 0
                                    #console.log("build_collisions recurse 1");
                                    build_collisions(g1.members, g2, col)
                                if g2.members? and g2.members.count ? 0
                                    #console.log("build_collisions recurse 2");
                                    build_collisions(g1, g2.members, col)
            catch error
                console.log("Error in collide phase 1")
                console.log(error)

        build_collisions3 = (gr1, gr2, col) ->
            try
                for g1 in gr1
                    for g2 in gr2
                        if g1 != g2 and !g1.destroyed and !g2.destroyed
                            if g1.solid
                                if g2.solid
                                    if g1.overlaps_in_rlist([g2]) != null
                                        col.push([g1, g2])
                                    else
                                        if g2.overlaps_in_rlist([g1]) != null
                                            col.push([g1, g2])
                                else if g2.members? and g2.members.count > 0
                                    #c = g1.overlaps_in_rlist(g2.members)
                                    #if c != null
                                    #    col.push([g1, c])
                                    build_collisions3([g1], g2.members, col)
                            else if g1.members? and g1.members.count > 0
                                #c = g2.overlaps_in_rlist(g1.members)
                                #if c != null
                                #    col.push([c, g2])
                                build_collisions3(g1.members, [g2], col)
            catch error
                console.log("Error in collide phase 1")
                console.log(error)

        build_collisions2 = (gr1, gr2, col) ->
            for g1 in gr1
                c = g1.overlaps_in_rlist(gr2)
                if c != null #something in children collided
                    col.push([g1, c])
                else if g1.members?
                    build_collisions2(g1.members, gr2, col)
          
        resolve_collisions2 = (collist) ->
            try
                for colitems in collist
                    g1 = colitems[0]
                    g2 = colitems[1]
                    #topdiff = g2.top - g1.bottom #pixels from g1.bottom->g2.top
                    #bottomdiff = g2.bottom - g1.top

                    #get the overlapping rect
                    r = l = t = b = 0
                    topofg1 = true
                    rightofg1 = true
                    bottomofg1 = true
                    leftofg1 = true
                    #because coord system is increasing towards bottom and right,
                    #intersecting rect is max tops, min bottoms, min rights, max lefts
                    if g1.top > g2.top
                        t = g1.top
                    else
                        t = g2.top
                        topofg1 = false

                    if g1.bottom < g2.bottom
                        b = g1.bottom
                    else
                        b = g2.bottom
                        bottomofg1 = false

                    if g1.right < g2.right
                        r = g1.right
                    else
                        r = g2.right
                        rightofg1 = false

                    if g1.left > g2.left
                        l = g1.left
                    else
                        l = g2.left
                        leftofg1 = false

                    leftrightcoll = true #a left-right collision, not top bottom
                    if r-l < b-t
                        leftrightcoll = true
                        if leftofg1 #but only if not right of g1 also
                            g1.hit_left(g2, 0)
                            g2.hit_right(g1, 0)
                            if @_debug
                                g1.debugColour.set_rgb(RB_DEBUG_HIT_LEFT)
                                g2.debugColour.set_rgb(RB_DEBUG_HIT_RIGHT)
                        else
                            g1.hit_right(g2, 0)
                            g2.hit_left(g1, 0)
                            if @_debug
                                g1.debugColour.set_rgb(RB_DEBUG_HIT_RIGHT)
                                g2.debugColour.set_rgb(RB_DEBUG_HIT_LEFT)
                    else
                        leftrightcoll = false
                        if topofg1 #but only if not bottom of g1 also
                            g1.hit_top(g2, 0)
                            g2.hit_bottom(g1, 0)
                            if @_debug
                                g1.debugColour.set_rgb(RB_DEBUG_HIT_TOP)
                                g2.debugColour.set_rgb(RB_DEBUG_HIT_BOTTOM)
                        else
                            g1.hit_bottom(g2, 0)
                            g2.hit_top(g1, 0)
                            if @_debug
                                g1.debugColour.set_rgb(RB_DEBUG_HIT_BOTTOM)
                                g2.debugColour.set_rgb(RB_DEBUG_HIT_TOP)
            catch error
                console.log("Error in collide phase 2")
                console.log(error)


                    

        resolve_collisions = (collist) ->

            try
                for colitems in collist
                    #if @_debug
                    #    console.log(g1, g2)
                    g1 = colitems[0]
                    g2 = colitems[1]
                    #console.log(g1, g2)
                    # To work out if we are hitting
                    # left,right,up,down, we use a 
                    # pretty simple method:
                    # whichever relative velocity is
                    # the highest, that is probably
                    # the direction from which we are hitting
                    
                    #physics is now a component, and I don't want to have to check that.
                    #rel_vel = new RBPoint(g1.velocity.x, g1.velocity.y)
                    #rel_vel.sub(g2.velocity)
                    
                    #instead, entities now have @_lastposition (RBPoint)
                    rel_vel = new RBPoint(g1.x - g1._lastposition.x, g1.y - g1._lastposition.y)
                    g2vel = new RBPoint(g2.x - g2._lastposition.x, g2.y - g2._lastposition.y)
                    
                    rel_vel.sub(g2vel)
                    
                    #ok, lets try something else instead
                    if (g1.bottom > g2.top) and rel_vel.y > 0
                        g1.hit_bottom(g2, 0)
                        g2.hit_top(g1, 0)
                        if @_debug
                            g1.debugColour.set_rgb(RB_DEBUG_HIT_BOTTOM)
                            g2.debugColour.set_rgb(RB_DEBUG_HIT_TOP)
                        #break
                    else
                        if (g1.top < g2.bottom) and rel_vel.y < 0
                            g1.hit_top(g2, 0)
                            g2.hit_bottom(g1, 0)
                            if @_debug
                                g1.debugColour.set_rgb(RB_DEBUG_HIT_TOP)
                                g2.debugColour.set_rgb(RB_DEBUG_HIT_BOTTOM)
                            #break
                        else
                            if (g1.right > g2.left) and rel_vel.x > 0
                                g1.hit_right(g2, 0)
                                g2.hit_left(g1, 0)
                                if @_debug
                                    g1.debugColour.set_rgb(RB_DEBUG_HIT_RIGHT)
                                    g2.debugColour.set_rgb(RB_DEBUG_HIT_LEFT)
                                #break
                            else
                                if (g1.left < g2.right) and rel_vel.x < 0
                                    g1.hit_left(g2, 0)
                                    g2.hit_right(g1, 0)
                                    if @_debug
                                        g1.debugColour.set_rgb(RB_DEBUG_HIT_LEFT)
                                        g2.debugColour.set_rgb(RB_DEBUG_HIT_RIGHT)
                                    #break
                
            catch error
                console.log("Error in collide phase 2")
                console.log(error)



        resolve_collisions3 = (collist) ->
            _debug = RB().isDebug()
            try
                for colitems in collist
                    #if @_debug
                    #    console.log(g1, g2)
                    g1 = colitems[0]
                    g2 = colitems[1]
                    #console.log(g1, g2)
                    # To work out if we are hitting
                    # left,right,up,down, we use a 
                    # pretty simple method:
                    # whichever relative velocity is
                    # the highest, that is probably
                    # the direction from which we are hitting
                    
                    #physics is now a component, and I don't want to have to check that.
                    #rel_vel = new RBPoint(g1.velocity.x, g1.velocity.y)
                    #rel_vel.sub(g2.velocity)
                    
                    #instead, entities now have @_lastposition (RBPoint)
                    rel_vel = new RBPoint(g1.x - g1._lastposition.x, g1.y - g1._lastposition.y)
                    g2vel = new RBPoint(g2.x - g2._lastposition.x, g2.y - g2._lastposition.y)
                    
                    colitems.push(rel_vel.sub(g2vel))

                for colitems in collist
                    g1 = colitems[0]
                    g2 = colitems[1]
                    rel_vel = colitems[2]
                    
                    #ok, lets try something else instead
                    if (g1.bottom > g2.top) and rel_vel.y > 0
                        g1.hit_bottom(g2, 0)
                        g2.hit_top(g1, 0)
                        if _debug
                            g1.debugColour.set_rgb(RB_DEBUG_HIT_BOTTOM)
                            g2.debugColour.set_rgb(RB_DEBUG_HIT_TOP)
                        #break
                    else
                        if (g1.top < g2.bottom) and rel_vel.y < 0
                            g1.hit_top(g2, 0)
                            g2.hit_bottom(g1, 0)
                            if _debug
                                g1.debugColour.set_rgb(RB_DEBUG_HIT_TOP)
                                g2.debugColour.set_rgb(RB_DEBUG_HIT_BOTTOM)
                            #break
                        else
                            if (g1.right > g2.left) and rel_vel.x > 0
                                g1.hit_right(g2, 0)
                                g2.hit_left(g1, 0)
                                if _debug
                                    g1.debugColour.set_rgb(RB_DEBUG_HIT_RIGHT)
                                    g2.debugColour.set_rgb(RB_DEBUG_HIT_LEFT)
                                #break
                            else
                                if (g1.left < g2.right) and rel_vel.x < 0
                                    g1.hit_left(g2, 0)
                                    g2.hit_right(g1, 0)
                                    if _debug
                                        g1.debugColour.set_rgb(RB_DEBUG_HIT_LEFT)
                                        g2.debugColour.set_rgb(RB_DEBUG_HIT_RIGHT)
                                    #break
                
            catch error
                console.log("Error in collide phase 2")
                console.log(error)

        resolve_collisions4 = (collist) ->
            _debug = RB().isDebug()
            crect = new RBRect()
            try
                for colitems in collist
                    #if @_debug
                    #    console.log(g1, g2)
                    g1 = colitems[0]
                    g2 = colitems[1]
                    #console.log(g1, g2)
                    # To work out if we are hitting
                    # left,right,up,down, we use a 
                    # pretty simple method:
                    # whichever relative velocity is
                    # the highest, that is probably
                    # the direction from which we are hitting
                    
                    #physics is now a component, and I don't want to have to check that.
                    #rel_vel = new RBPoint(g1.velocity.x, g1.velocity.y)
                    #rel_vel.sub(g2.velocity)
                    
                    
                    #FOR NOW, WE DON'T NEED REL VEL
                    #IF WE FIND WE DO, BUILD IT IN BUILD_COLLISIONS
                    #ACTUALLY THAT MAY BE SLOWER. BUILDS A LOT OF NEEDLESS
                    #RELVELS FOR THINGS THAT DON'T OVERLAP
                    #instead, entities now have @_lastposition (RBPoint)
                    #rel_vel = new RBPoint(g1.x - g1._lastposition.x, g1.y - g1._lastposition.y)
                    #g2vel = new RBPoint(g2.x - g2._lastposition.x, g2.y - g2._lastposition.y)
                    
                    #colitems.push(rel_vel.sub(g2vel))

                for colitems in collist
                    g1 = colitems[0]
                    g2 = colitems[1]
                    rel_vel = colitems[2]
                    
                    g1.overlaps(g2, crect)
                    if crect.width > crect.height
                        #its a top / bottom collision
                        if crect.y >0 
                            g1.hit_bottom(g2, 0)
                            g2.hit_top(g1, 0)
                            if _debug
                                g1.debugColour.set_rgb(RB_DEBUG_HIT_BOTTOM)
                                g2.debugColour.set_rgb(RB_DEBUG_HIT_TOP)
                        else
                            g1.hit_top(g2, 0)
                            g2.hit_bottom(g1, 0)
                            if _debug
                                g1.debugColour.set_rgb(RB_DEBUG_HIT_TOP)
                                g2.debugColour.set_rgb(RB_DEBUG_HIT_BOTTOM)
                    else
                        #its a right left collision
                        if crect.x > 0
                            g1.hit_right(g2, 0)
                            g2.hit_left(g1, 0)
                            if _debug
                                g1.debugColour.set_rgb(RB_DEBUG_HIT_RIGHT)
                                g2.debugColour.set_rgb(RB_DEBUG_HIT_LEFT)
                        else
                            g1.hit_left(g2, 0)
                            g2.hit_right(g1, 0)
                            if _debug
                                g1.debugColour.set_rgb(RB_DEBUG_HIT_LEFT)
                                g2.debugColour.set_rgb(RB_DEBUG_HIT_RIGHT)

            catch error
                console.log("Error in collide phase 2")
                

        if @_debug
            elem.debugColour.set_rgb(RB_DEBUG_HIT_NONE) for elem in group1
            elem.debugColour.set_rgb(RB_DEBUG_HIT_NONE) for elem in group2
        #build_collisions(group1, group2, @collisions)
        #build_collisions2(group1, group2, @collisions)
        build_collisions3(group1, group2, @_collisions)

        resolve_collisions4(@_collisions)
        #resolve_collisions2(@_collisions)

        while @_collisions.length
            @_collisions.pop()

_RB = new RBInit(false)

