# RBActionList is the global list of running actions on objects.
# It is for internal use only
# @private
class RBActionList extends RBGroup
    constructor:(name="RBActionList") ->
        super(name)
        @entity_map = {}
        @paused = false #used by RBEditor to pause all actions

    add_for_entity: (ent, runnable) ->
        if not @entity_map[ent.id]?
            @entity_map[ent.id] = []
        @entity_map[ent.id].push(runnable)
        @.add_member(runnable)
        return runnable

    op_for_entity: (op, ent, actionlist) ->
        ids = null
        l = @entity_map[ent.id]
        if l? and l.length>0
            if actionlist? and actionlist.length>0
                ids = (action.id for action in actionlist)
            for runnable in l
                if (ids is null) or (runnable.action.id in ids)
                    #console.log("Running #{op} for ent #{ent.id}")
                    if op is "stop"
                        runnable.stop()
                    else if op is "start"
                        runnable.start()
                    else if op is "finish"
                        runnable.isFinished = true
                    else if op is "cancel"
                        runnable.isCancelled = true

    start_for_entity:(ent, actionlist) ->
        @.op_for_entity("start", ent, actionlist)
    stop_for_entity:(ent, actionlist) ->
        @.op_for_entity("stop", ent, actionlist)
    finish_for_entity:(ent, actionlist) ->
        @.op_for_entity("finish", ent, actionlist)
    cancel_for_entity:(ent, actionlist) ->
        @.op_for_entity("cancel", ent, actionlist)



    update: (dt) =>
        if @paused #used by RBEditor to pause all actions
            return
        finishedactions = []
        #console.log("total actions: " + @count)
        for action in @members
            if action? #why is the action even staying here?
                action.update(dt)

        for action in @members
            if action.isFinished or action.isCancelled
                #console.log("finishing action" + action)
                finishedactions.push(action)

        #now remove finished actions
        for action in finishedactions
            #console.log("Removed finished action " + action)
            entactionlist = @entity_map[action.target.id]
            if entactionlist? and action in entactionlist
                index = entactionlist.indexOf(action)
                if index >= 0
                    entactionlist.splice(index, 1)
                if entactionlist.length is 0
                    delete @entity_map[action.target.id]
            @.remove_member(action)

# RBRunnable represents the running state of an action
# It is for internal use only
class RBRunnable extends RBObject
    constructor:(@target, @action, @time, @args) ->
        super()
        @.reset()
    # @private
    dt_seconds: () ->
        return @dt / 1000.0

    on_finished:(func) ->
        @onFinished = func
    
    stop: () ->
        @stopped = true
    start: () ->
        @stopped = false

    reset: () ->
        @currenttime = 0
        @endtime = @time * 1000
        @dt = 0
        @isFinished = false
        @isCancelled = false
        @onFinished = null #callback when this finishes.
        @isInited = false
        @onInit = null
        @stopped = false



    # @private
    update: (dt) ->
        if @stopped or @isFinished
            return
        
        @dt = dt
        if @currenttime == 0 and not @isInited
            @isInited = true
            #console.log("should init")
            @action.init.apply(@target, [this, @args])
        if not (@isFinished or @isCancelled)
            @currenttime += dt
            if @currenttime <= @endtime
                @action.action.apply(@target, [this, @args])
            else
                #do the last little step.
                @dt -= (@endtime - @currenttime)
                @currenttime = @endtime
                @action.action.apply(@target, [this, @args])
                @isFinished = true
                @action.finish.apply(@target, [this, @args])
                if @onFinished isnt null
                    #console.log("calling action on finished")
                    @onFinished(@)

# An RBAction defines a transformation to apply to an object 
# over a duration. It can be targetted on an object, and 
# instanced into an RBRunnable, and inserted into the 
# RBActionList object, where it will be cleaned up when finished.
#
# Most RBActions require a time, for which they will execute, and 
# then some other data to define some transformation - for
# example moving position or changing colour. Other RBActions are
# simple one offs, like calling a function, or they provide a way
# of chaining several actions together to run in series or parallel.
#
# RBActions are independent of their targets - that is, you create
# an RBAction without specifying what it will act on. Then you can tell
# several objects to be acted on by the RB@currenttime = 0
# act in the same manner, but independently.
class RBAction extends RBObject
    constructor:(time=1.0, args...) ->
        super()
        @time = time
        @args = args
    # Initialises the action. Usually properties are set on
    # the runnable, which represents the dynamic or 'running'
    # state of the action.
    # @private
    init:(runnable, args) ->
        return

    # @private
    action:(runnable, args) ->
        return

    # @private
    finish: (runnable, args) ->
        return

    run_on: (rbobject) ->
        return @.run_action_on(rbobject)

    # @private
    run_action_on:(rbobject) ->
        #this is where you instance off the runnable
        #implicit return of runnable
        return @_RB.actions.add_for_entity(rbobject, new RBRunnable(rbobject, @, @time, @args))

# RB Repeat
class RBRepeatAction extends RBAction
    constructor: (repeatedaction, times=0) ->
        super(1, repeatedaction, times)
        @repeatedaction = repeatedaction
        @times = times

    init: (runnable, args) ->
        runnable.repeatedaction = args[0]
        #runnable.repeatedrunnable = null
        if runnable.repeatedrunnable?
            runnable.repeatedrunnable.times = args[1]
            if runnable.repeatedrunnable.times <= 0
                runnable.repeatedrunnable.repeatforever = true
            else
                runnable.repeatedrunnable.repeatforever = false

    fire_action: (repeatedrunnable) ->
        #console.log("firing action")
        repeatedrunnable.times -= 1
        if repeatedrunnable.repeatforever or repeatedrunnable.times > 0
            #console.log('repeating runnable')
            tmp = repeatedrunnable.onFinished
            repeatedrunnable.reset()
            repeatedrunnable.on_finished(tmp)
    #        return true
    #    else
    #        return false

    ###
    action: (runnable, args) ->
        #console.log("RBRepeatAction.action()")
        #console.log(runnable)
        runnable.currenttime = -1000
        super(runnable, args)
        runnable.isFinished = false
        #if runnable.repeatedrunnable?
        #    if runnable.repeatedrunnable.isFinished
        #        if runnable.repeatforever or runnable.times > 0
        #            runnable.times -= 1
        #            console.log('repeating runnable')
        #            runnable.repeatedrunnable.reset()
        #            runnable.isFinished = false
        #        else
        #            runnable.isFinished = true
        #    #else
        #    #    console.log("runnable repeatedaction wasn't finished")
        #else
        #    console.log("there was no repeated runnable.")
    ###
    
    #finish: (runnable, args) ->

    run_action_on: (rbobject) ->
        runnable = super(rbobject)
        runnable.repeatedrunnable = @repeatedaction.run_action_on(rbobject)
        #console.log("RBRepearAction runnable and repeatedrunnable")
        #console.log(runnable)
        #console.log(runnable.repeatedrunnable)
        #@@.fire_action(rbobject)
        runnable.repeatedrunnable.on_finished(@.fire_action)
        return runnable

class RBActionSequence extends RBAction
    constructor: (actionlist) ->
        totaltime = 0
        for action in actionlist
            totaltime += action.time
        super(totaltime, actionlist)

    init: (runnable, args) ->
        #console.log("action sequence 2 args...:")
        #console.log(args)
        runnable.actionlist = args[0]
        runnable.currentactionindex = -1
        runnable.next_action = (rbobject) ->
            runnable.currentactionindex += 1
            if runnable.currentactionindex < runnable.actionlist.length
                action = runnable.actionlist[runnable.currentactionindex]
                return new RBRunnable(rbobject, action, action.time, action.args)
            else
                return null
        runnable.start_next_action = () ->
            #console.log("starting next action, current index is: #{runnable.currentactionindex}, actionlist is #{runnable.actionlist}")
            nextrunnable = runnable.next_action(runnable.target)
            if nextrunnable isnt null
                #console.log("got next action")
                nextrunnable.on_finished(runnable.start_next_action)
                RB().actions.add_for_entity(runnable.target, nextrunnable)
        runnable.start_next_action()


    #run_action_on: (rbobject) ->
    #    runnable = super(rbobject)
        

class RBDelay extends RBAction
    constructor: (time) ->
        super(time)
        @time = time

# Move an object by xmove, ymove, (pixels) over time (seconds).
class RBMoveBy extends RBAction
    constructor:(time, xmove, ymove) ->
        super(time, xmove, ymove)
        @time = time
        @xmove = xmove
        @ymove = ymove

    # @private
    init: (runnable, args) ->
        super(runnable, args)
        runnable.dx = args[0]
        runnable.dy = args[1]
        runnable.cumulativex = 0
        runnable.cumulativey = 0
    
    # @private
    action: (runnable, args) ->
        super(runnable, args)
        dtsec = runnable.dt_seconds()
        dx = dtsec*(runnable.dx/runnable.time)
        dy = dtsec*(runnable.dy/runnable.time)
        runnable.cumulativex += dx
        runnable.cumulativey += dy
        this.move_by(dx, dy)

    # @private
    finish: (runnable, args) ->
        super(runnable, args)
        this.move_by(runnable.dx-runnable.cumulativex, runnable.dy-runnable.cumulativey)

# Move an object to X, Y, over time (seconds)
class RBMoveTo extends RBMoveBy
    constructor: (time, xdest, ydest) ->
        super(time, xdest, ydest)
        @time = time
        @xdest = xdest
        @ydest = ydest

    # @private
    init:(runnable, args) ->
        super(runnable, args)
        runnable.dx = args[0] - this.x
        runnable.dy = args[1] - this.y

# Rotate an object around its anchor by some angle over time
class RBRotateBy extends RBAction
    constructor: (time, rotby) ->
        super(time, rotby)
        @time = time
        @rotby = rotby
    # @private
    init: (runnable, args) ->
        super(runnable, args)
        runnable.rotby = args[0]
        runnable.cumulativerot = 0
    # @private
    action: (runnable, args) ->
        super(runnable, args)
        drot = runnable.dt_seconds()*(runnable.rotby/runnable.time)
        runnable.cumulativerot +=drot
        angle = this.get_angle()
        this.set_angle(angle + drot)
    # @private
    finish: (runnable, args) ->
        super(runnable, args)
        angle = this.get_angle()
        this.set_angle( angle +  runnable.rotby - runnable.cumulativerot)

# Rotate an object around its anchor to some angle over time
class RBRotateTo extends RBRotateBy
    constructor: (time, rotdest) ->
        super(time, rotdest)
        @time = time
        @rotdest = rotdest
    # @private
    init: (runnable, args) ->
        super(runnable, args)
        runnable.rotby = args[0] - this.get_angle()
        #console.log("Rotby is #{runnable.rotby} over #{runnable.time}")

# Scale an {RBEntity} by a particular amount (from its existing amount)
class RBScaleBy extends RBAction
    # @param time [Number]
    # @param scalebyx [Number] The x scale
    # @param scalebyy [Number] The y scale (defaults to x scale)
    constructor: (time, scalebyx, scalebyy=scalebyx) ->
        super(time, scalebyx, scalebyy)
        @time = time
        @scalebyx = scalebyx
        @scalebyy = scalebyy

    # @private
    init: (runnable, args) ->
        super(runnable, args)
        runnable.scalebyx = args[0]
        runnable.scalebyy = args[1]
        runnable.cumulativescalex = 0
        runnable.cumulativescaley = 0
    # @private
    action: (runnable, args) ->
        super(runnable, args)
        scx = runnable.dt_seconds()*(runnable.scalebyx/runnable.time)
        scy = runnable.dt_seconds()*(runnable.scalebyy/runnable.time)
        runnable.cumulativescalex += scx
        runnable.cumulativescaley += scy
        sc = this.get_scale()
        this.set_scale(sc.x + scx, sc.y+scy)
    # @private
    finish: (runnable, args) ->
        super(runnable, args)
        sc = this.get_scale()
        this.set_scale(sc.x + (runnable.scalebyx-runnable.cumulativescalex), sc.y+(runnable.scalebyy - runnable.cumulativescaley))

# Scale an {RBEntity} over time 
class RBScaleTo extends RBScaleBy
    # @param time [Number]
    # @param scaletox [Number] The x scale
    # @param scaletoy [Number] The y scale (defaults to x scale)
    constructor: (time, scaletox, scaletoy=scaletox) ->
        super(time, scaletox, scaletoy)
        @time = time
        @scaletox = scaletox
        @scaletoy = scaletoy

    init: (runnable, args) ->
        super(runnable, args)
        sc = this.get_scale()
        runnable.scalebyx = args[0] - sc.x
        runnable.scalebyy = args[1] - sc.y

# Calls the function specified (immediately - there is no time)
# Any arguments to use in the function should be passed as a list.
# @example
#   #If you normally call the function like so:
#   myfunc(arg1, arg2, arg3)
#   #Then you could use an RBCallFunction action like so:
#   action = new RBCallFunction(myfunc, [arg1, arg2, arg3])
#
# It may not always be useful to spefify a function object when
# creating this action, as you may want the function to resolve
# dynamically when this action runs. So the func argument to the 
# constructor can also be a string, which will be looked up on 
# the target object at runtime.
#
# This is useful when you need to call a method on the object that
# is has the action running on it, and you reuse that action over
# a bunch of objects. You want each running action to call the .blah
# method of its target - so when you create the action, you do it like
# so:
# @example
#   action = new RBCallFunction("blah", [arg1, arg2])
#   action.run_on(obj1) #will call obj1.blah(arg1, arg2)
#   action.run_on(obj2) #will call obj2.blah(arg1, arg2)
class RBCallFunction extends RBAction
    constructor: (func, argslist) ->
        super(0, func, argslist)
        @func = func
    # @private
    init: (runnable, args) ->
        runnable.func = args[0]

    # @private
    action: (runnable, args) ->
        super(runnable, args)
        #args[0] holds func
        #args[1] holds an array of arguments
        thisargs = args[1]#[this].concat(args[1])
        if typeof runnable.func is "function"
            runnable.func.apply(undefined, thisargs)
        else if typeof runnable.func is "string"
            func = runnable.target[runnable.func]
            if typeof func is "function"
                #console.log("successfully resolved #{runnable.func} with args #{thisargs}")
                func.apply(runnable.target, thisargs)

# Tint by the amounts r, g, b and a
class RBTintBy extends RBAction
    constructor: (time, r, g, b, a = 255) ->
        super(time, r, g, b, a)
        @time = time
        @r = r
        @g = g
        @b = b
        @a = a
    # @private
    init: (runnable, args) ->
        super(runnable, args)
        #runnable.dr = clamp_num(this._colour.r + args[0], 0, 255)
        #runnable.dg = clamp_num(this._colour.g + args[1], 0, 255)
        #runnable.db = clamp_num(this._colour.b + args[2], 0, 255)
        #runnable.da = clamp_num(this._colour.a + args[3], 0, 255)
        runnable.dr = args[0]
        runnable.dg = args[1] 
        runnable.db = args[2] 
        runnable.da = args[3] 
        
        #console.log("Tint By Dests: #{runnable.dr}, #{runnable.dg}, #{runnable.db}, #{runnable.da} over #{runnable.time} seconds")
        runnable.cumulativer = 0
        runnable.cumulativeg = 0
        runnable.cumulativeb = 0
        runnable.cumulativea = 0
    # @private
    action: (runnable, args) ->
        super(runnable, args)
        elapsed = runnable.dt_seconds()
        dr = elapsed*(runnable.dr/runnable.time)
        dg = elapsed*(runnable.dg/runnable.time)
        db = elapsed*(runnable.db/runnable.time)
        da = elapsed*(runnable.da/runnable.time)
        runnable.cumulativer += dr
        runnable.cumulativeg += dg
        runnable.cumulativeb += db
        runnable.cumulativea += da
        c = this._colour
        this.set_colour(c.r+dr, c.g+dg, c.b+db, c.a+da)

    # @private
    finish: (runnable, args) ->
        super(runnable, args)
        c = this._colour
        #console.log("Final pre-colour: ")
        #console.log(c)
        this.set_colour(c.r + (runnable.dr-runnable.cumulativer),
            c.g + (runnable.dg-runnable.cumulativeg),
            c.b + (runnable.db-runnable.cumulativeb),
            c.a + (runnable.da-runnable.cumulativea))
        #console.log(c)

# Tint to the colour r, g, b and a elements specified
class RBTintTo extends RBTintBy
    constructor: (time, r, g, b, a=255) ->
        super(time, r, g, b, a)
        @time = time
        @r = r
        @g = g
        @b = b
        @a = a

    # @private
    init: (runnable, args) ->
        super(runnable, args)
        runnable.dr = args[0] - this._colour.r
        runnable.dg = args[1] - this._colour.g
        runnable.db = args[2] - this._colour.b
        runnable.da = args[3] - this._colour.a
        #console.log("Tint To Dests: #{runnable.dr}, #{runnable.dg}, #{runnable.db}, #{runnable.da}")
        
# Fades your rbentity down 255 alpha values
class RBFadeOut extends RBTintBy
    constructor: (time) ->
        super(time, 0,0,0,-255)
        @time = time

    finish: (runnable, args) ->
        super(runnable, args)
        #console.log("End RBFadeOut, colour is:")
        #console.log(this._colour)

# Fades your rbentity up 255 alpha values.
# Does not set your rbentity alpha to zero first
# you should do that with .set_alpha(0)
class RBFadeIn extends RBTintBy
    constructor: (time) ->
        super(time, 0,0,0,255)
        @time = time


