####### UTILS####################

# Random Numbers
# --------------

random_float = (min, max) ->
    max - (Math.random() * (max-min))

random_int = (min, max) ->
    max - Math.floor(Math.random()*(max-min+1))

# Chance is a number between 0 and 1
random_chance = (chance) ->
    num = Math.random()
    if num >= (1.0-chance)
        return true
    return false

random_item = (list) ->
    r = random_int(0, list.length-1)
    return list[r]

clamp_num = (num, min, max) ->
    if num < min
        num = min
    if num > max
        num = max
    return num

#util function to work out where our canvas is
#Based on: http://www.quirksmode.org/js/findpos.html
get_cumulative_offset = (obj) ->
    left = top = 0
    if obj.offsetParent
        left += obj.offsetLeft
        top += obj.offsetTop

    retval =
        x: left
        y: top

array_contains = (arr, obj) ->
    for item in arr
        if item is obj
            return true
    false

print_array = (arr) ->
    for item in arr
        console.log(item)

int_to_hex = (i) ->  
    hex = parseInt(i).toString(16)  
    if hex.length < 2 
        return "0" + hex
    else hex


hex_to_hex = (hex) ->
    return parseInt("0x" + hex)


rgb_to_hex =(r, g, b) ->
    return ("0x" + int_to_hex(r)+int_to_hex(g)+int_to_hex(b))
rgb_to_html =(r, g, b) ->
    return ("#" + int_to_hex(r)+int_to_hex(g)+int_to_hex(b))
rgba_to_html = (r, g, b, a) ->
    return "rgba(#{r}, #{g}, #{b}, #{a/255})"

class RBColour
    constructor: (colour = [0,0,0,255]) ->
        @.set_rgba(colour)

    set_alpha: (alpha=255) ->
        @a = alpha
        @.set_rgba() #refresh

    set_rgb:(col) ->
        @.set_rgba([col[0],col[1],col[2],@a])
    #calling without value is like doing a refresh, setting nothing
    set_rgba:(colour) ->
        if colour?
            @r = colour[0]
            @g = colour[1]
            @b = colour[2]
            if colour[3]?
                @a = colour[3]
            else
                @a =255
        @rgba = [@r,@g,@b,@a]
        _r = clamp_num(@r, 0, 255)|0
        _g = clamp_num(@g, 0, 255)|0
        _b = clamp_num(@b, 0, 255)|0
        _a = clamp_num(@a, 0, 255)|0
        @htmlrgb = rgb_to_html(_r,_g,_b)
        @htmlrgba = rgba_to_html(_r, _g, _b, _a)
