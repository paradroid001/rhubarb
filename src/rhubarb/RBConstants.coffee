# Constants for RB
#RB_VERSION_MAJOR = 1
#RB_VERSION_MINOR = 0
#RB_VERSION_REV = 7
RB_VERSION = "2.0.1"

#Constants for RBVars
RBV_GAME_STR = "game"
RBV_IMG_STR = "images"
RBV_SND_STR = "sounds"
RBV_MUSIC_STR = "music"
RBV_SS_STR = "spritesheets"
RBV_ENG_STR = "engine"

# The Colour Red
RB_RED =    [255,0,0]
# The Colour Green
RB_GREEN =  [0,255,0]
# The Colour Blue
RB_BLUE =   [0,0,255]
# The Colour Yellow
RB_YELLOW = [0,255,255]
# The Colour Purple
RB_PURPLE = [255,0,255]
# The Colour Grey
RB_GREY =   [128,128,128]
# The Colour White
RB_WHITE =   [255,255,255]

# The colour used for HIT TOP in RBDebug
RB_DEBUG_HIT_TOP = [0, 128, 255]
# The colour used for HIT TOP in RBDebug
RB_DEBUG_HIT_BOTTOM = [255, 128, 0]
# The colour used for HIT  in RBDebug
RB_DEBUG_HIT_RIGHT = [255, 128, 255]
# The colour used for HIT LEFT in RBDebug
RB_DEBUG_HIT_LEFT = [128, 255, 128]
# The colour used for HIT NONE in RBDebug
RB_DEBUG_HIT_NONE = RB_GREY

RB_COLLIDE_NONE = 0
RB_COLLIDE_TOP = 1
RB_COLLIDE_BOTTOM = 2
RB_COLLIDE_LEFT = 4
RB_COLLIDE_RIGHT = 8

# Event name for key down
RB_KEY_DOWN = "keydown"
# Event name for key down
RB_KEY_UP = "keyup"
# Event name for key down
RB_MOUSE_MOVE = "mousemove"
# Event name for key down
RB_MOUSE_DOWN = "mousedown"
# Event name for key down
RB_MOUSE_UP = "mouseup"
# Event name for double click
RB_MOUSE_DOUBLECLICK = "dblclick"
# Event name for mouse wheel
RB_MOUSE_WHEEL = "mousewheel"
# Event name for key down
RB_TOUCH_START = "touchstart"
# Event name for key down
RB_TOUCH_END = "touchend"
# Event name for key down
RB_TOUCH_MOVE = "touchmove"
# Event name for key down
RB_TOUCH_CANCEL = "touchcancel"

#RBScene.enter, .exit, .eclipse, .reveal
RB_EVENT_ON_SCENE_ENTER = "on_enter"
RB_EVENT_ON_SCENE_EXIT = "on_exit"
RB_EVENT_ON_SCENE_ECLIPSE = "on_eclipse"
RB_EVENT_ON_SCENE_REVEAL = "on_reveal"
#RB.start() and RB.stop()
RB_EVENT_ON_START = "on_start"
RB_EVENT_ON_STOP = "on_stop"
RB_EVENT_ON_PAUSE = "on_pause"
RB_EVENT_ON_UNPAUSE= "on_unpause"

RB_RENDER_MODE_NORMAL = "none"
RB_RENDER_MODE_MULTIPLY = "lighter"

RB_ANIM_LOOP = "Loop"
RB_ANIM_NORMAL = "Normal"

RB_DEFAULT_SOUND_CACHE_SIZE = 3 #Every sfx loaded 3 times
