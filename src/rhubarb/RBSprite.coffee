# A Sprite.
# Pass in your X and Y, 
# An image. If null, no image is used.
# A width and height. If not specified, the 
# image width and height are used. If there is no
# image, these default to zero.
#
#
class RBSprite extends RBEntity
    # @property [Boolean] Whether this sprite should be flipped in X
    flipx: false
    # @property [Boolean] Whether this sprite should be flipped in Y
    flipy: false

    constructor: (X, Y, Imagename=null, Width=null, Height=null, name="NoName") ->
        image = null
        if not (Imagename is null)
            rbimage = RB().media.load_image(Imagename)
            image = rbimage.image()
            w = image.naturalWidth
            h = image.naturalHeight
            #console.log("Sprite loaded " + Imagename + ": [" + @width + "," + @height + "]")
        else
            w = 0
            h = 0
        super(X, Y, w, h, name)
        #console.log(Imagename + "[" + @width + "," + @height + "]")
        @_image = image
        
        @_currAnimName = null
        @_currAnim = null
        @flipx = false
        @flipy = false
        
        if Width isnt null
            @.set_width(Width)
        if Height isnt null
            @.set_height(Height)
        
        @_animations = {"default": new RBAnimation(null, [0])}
        @_animations.default.currentFrameRect.width = w
        @_animations.default.currentFrameRect.height = h
        @.set_animation("default")

    flip_x: (val) ->
        #console.log("flipped x")
        @flipx = val
    flip_y: (val) ->
        @flipy = val

    add_animation: (animationname, animation) ->
        @_animations[animationname] = animation
    
    set_animation: (animationname) ->
        change_anim = false
        if @_animations[animationname]?
            if @_currAnimName != animationname
                @_currAnimName = animationname
                change_anim = true
        else
            @_currAnimName = "default"
            change_anim = true
        @_currAnim = @_animations[@_currAnimName]
        if change_anim
            @_currAnim.reset()

    get_animation: (animationname) ->
        @_animations[animationname]

    update: (dt)->
        super(dt)
        @_currAnim.next(dt)

    draw: () -> # (xoffset=0, yoffset=0)->
        ca = @_currAnim
        if ca?
            car = ca.currentFrameRect
            if ca.spritesheet?
                @_image = ca.spritesheet.get_image()
        if @_image?
            if (@flipx or @flipy)
                fx = fy = 1 # 1 = the right way around, -1 = flipped 
                if @flipx
                    fx = -1
                if @flipy
                    fy = -1
                tomid = @.xy_to_middle()
                @_RB.cctx.translate(tomid.x|0, tomid.y|0)
                @_RB.cctx.transform(fx,0,0,fy,0,0)
                @_RB.cctx.translate(-tomid.x|0, -tomid.y|0)
            #we draw the image at (0, 0, width, height) because we have already been translated to the correct position
            try
                @_RB.cctx.drawImage(@_image, car.x|0, car.y|0, car.width|0, car.height|0,
                                    @_XYtoTL.x, @_XYtoTL.y, @width|0, @height|0 )
            catch error
                console.log("Draw Error: #{error}")
                console.log("#{car.x}, #{car.y}, #{car.width}, #{car.height}")
                console.log("#{totopleft.x|0}, #{totopleft.y|0}, #{@width|0}, #{@height|0}")
            @dirty = false
            super()
