class RBNet
    constructor:(name="netconnection", host="127.0.0.1", port="2000", protocolHandler=null) ->
        @name = name
        @host = host
        @port = port
        @protocolHandler = protocolHandler
        @protocolHandler.set_transport(@)
        @websocket = null
        @connected = false

    #todo: if they pass an url, save out the host and port?
    connect:() ->
        @websocket = new WebSocket("ws://" + @host + ":" + @port + "/" + "chat")
        @websocket.onmessage = @.onmessage
        @websocket.onopen = @.onopen
        @websocket.onerror = @.onerror
        @websocket.onclose = @.onclose
        
    onerror: (obj) =>
        console.log("Websocket error: " + obj)
    onmessage:(obj) =>
        console.log('received message: ' + obj.data)

    
    onopen:() =>
        console.log('socket opened')
        @connected = true
        @protocolHandler.set_state(DelveProtocolStates.STATE_PRE_LOGIN)

    onclose:(obj) =>
        console.log('socket closed')
        @connected = false
        @protocolHandler.set_state(DelveProtocolStates.STATE_DISCONNECTED)

    send:(obj) =>
        if (@connected)
            console.log('sending data: ' + obj)
            @websocket.send(obj)
        else
            console.log('could not send data: not connected')
        
class RBRequest
    @GET = "GET"
    @POST = "POST"
    constructor:(@url, @onsuccess, @onfailure) ->
        @reqobj = new XMLHttpRequest()
        #@reqobj.onload = @onsuccess
        if @onfailure?
            @reqobj.onerror = @onfailure
        else
            @reqobj.onerror = @.default_onfailure
        @reqobj.onreadystatechange = @.statechange
        @mode = @POST

    statechange:() =>
        switch @reqobj.readyState   
            when 0 then a = 1 #UNSENT
            when 1 then a = 1 #OPENED
            when 2 then a = 1 #HEADERS_RECIEVED 
            when 3 then a = 1#LOADING - responsetext holds partial data
            when 4
                if @reqobj.status == 200
                    @onsuccess(@reqobj.responseText)
                else
                    console.log("Response to #{@url} was code #{@reqobj.status}")
    default_onfailure:() =>
        console.log("Web request to #{@url} failed")

    get:(@params) ->
        @reqobj.open("GET", @url, true)
        @reqobj.send()
    post:(@data) ->
        @reqobj.open("POST", @url, true)
        console.log("Posting: #{JSON.stringify(@data)}")
        @reqobj.send(JSON.stringify(@data))
