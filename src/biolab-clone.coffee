biolab = null


class EnvTile extends RBSprite
    constructor: (X, Y) ->
        super(X, Y, RBV_IMG.biolabimg, RBV_GAME.tilewidth, RBV_GAME.tileheight)
        @static = true
        #@velocity.x = @velocity.y = 0

class ET_01 extends EnvTile
    constructor: (X, Y) ->
        super(X, Y)
        ss = @_RB.media.get_spritesheet(RBV_SS.env)
        @.add_animation("default", new RBAnimation(ss,[0],RB_ANIM_NORMAL, 100))
        @.set_animation("default")

class ET_02 extends EnvTile
    constructor: (X, Y) ->
        super(X, Y)
        ss = @_RB.media.get_spritesheet(RBV_SS.env)
        @.add_animation("default", new RBAnimation(ss,[1],RB_ANIM_NORMAL, 100))
        @.set_animation("default")

class ET_03 extends EnvTile
    constructor: (X, Y) ->
        super(X, Y)
        ss = @_RB.media.get_spritesheet(RBV_SS.env)
        @.add_animation("default", new RBAnimation(ss,[2],RB_ANIM_NORMAL, 100))
        @.set_animation("default")

class ET_04 extends EnvTile
    constructor: (X, Y) ->
        super(X, Y)
        ss = @_RB.media.get_spritesheet(RBV_SS.env)
        @.add_animation("default", new RBAnimation(ss,[3],RB_ANIM_NORMAL, 100))
        @.set_animation("default")

class ET_05 extends EnvTile
    constructor: (X, Y) ->
        super(X, Y)
        ss = @_RB.media.get_spritesheet(RBV_SS.env)
        @.add_animation("default", new RBAnimation(ss,[4],RB_ANIM_NORMAL, 100))
        @.set_animation("default")

class ET_06 extends EnvTile
    constructor: (X, Y) ->
        super(X, Y)
        ss = @_RB.media.get_spritesheet(RBV_SS.env)
        @.add_animation("default", new RBAnimation(ss,[5],RB_ANIM_NORMAL, 100))
        @.set_animation("default")

class ET_56 extends EnvTile
    constructor: (X, Y) ->
        super(X, Y)
        ss = @_RB.media.get_spritesheet(RBV_SS.env)
        @.add_animation("default", new RBAnimation(ss,[56],RB_ANIM_NORMAL, 100))
        @.set_animation("default")
class ET_57 extends EnvTile
    constructor: (X, Y) ->
        super(X, Y)
        ss = @_RB.media.get_spritesheet(RBV_SS.env)
        @.add_animation("default", new RBAnimation(ss,[57],RB_ANIM_NORMAL, 100))
        @.set_animation("default")
class ET_58 extends EnvTile
    constructor: (X, Y) ->
        super(X, Y)
        ss = @_RB.media.get_spritesheet(RBV_SS.env)
        @.add_animation("default", new RBAnimation(ss,[58],RB_ANIM_NORMAL, 100))
        @.set_animation("default")
class ET_59 extends EnvTile
    constructor: (X, Y) ->
        super(X, Y)
        ss = @_RB.media.get_spritesheet(RBV_SS.env)
        @.add_animation("default", new RBAnimation(ss,[59],RB_ANIM_NORMAL, 100))
        @.set_animation("default")
class ET_60 extends EnvTile
    constructor: (X, Y) ->
        super(X, Y)
        ss = @_RB.media.get_spritesheet(RBV_SS.env)
        @.add_animation("default", new RBAnimation(ss,[60],RB_ANIM_NORMAL, 100))
        @.set_animation("default")
class ET_61 extends EnvTile
    constructor: (X, Y) ->
        super(X, Y)
        ss = @_RB.media.get_spritesheet(RBV_SS.env)
        @.add_animation("default", new RBAnimation(ss,[61],RB_ANIM_NORMAL, 100))
        @.set_animation("default")
class ET_62 extends EnvTile
    constructor: (X, Y) ->
        super(X, Y)
        ss = @_RB.media.get_spritesheet(RBV_SS.env)
        @.add_animation("default", new RBAnimation(ss,[62],RB_ANIM_NORMAL, 100))
        @.set_animation("default")

        


class BL_Avatar extends RBSprite
    constructor: (X, Y) ->
        super(X, Y, RBV_IMG.playerimg, RBV_GAME.tilewidth*2, RBV_GAME.tileheight*2)
        ss = RB().media.get_spritesheet(RBV_SS.player)
        @.add_animation("default", new RBAnimation(ss,[0],RB_ANIM_NORMAL, 100))
        @.add_animation("walk_left", new RBAnimation(ss,[30, 31, 32, 33, 34, 35],RB_ANIM_LOOP, 100))
        @.add_animation("walk_right", new RBAnimation(ss,[30, 31, 32, 33, 34, 35],RB_ANIM_LOOP, 100))
        #@.add_animation("walk_right", new RBAnimation(6,8,16,16,[30,31,32,33,34,35],0,0,60, "Loop"))
        #@.add_animation("walk_left", new RBAnimation(6,8,16,16,[30,31,32,33,34,35],0,0,60, "Loop"))
        @.set_animation("default")
        @physics = new RBPhysics()
        @.add_component(@physics)
        @.set_anchor(0, 0)
        #@_RB.input.bind('keydown', @.walk, false)
    
    hit_bottom: (robject, vel) ->
        #console.log("hit bottom: yvel = " + @physics.velocity.y)
        @.adjust_bottom(robject.top)
        if @physics.velocity.y > 0
            @physics.velocity.y = 0
        @isonground = true

    hit_top: (robject, vel) ->
        #console.log("hit top")
        return
   
    update:(dt) ->
        super(dt)
        #This is a hack. We should be receiving messages
        #about key events we are interested in,
        #not polling for them. Ahh well.
        ks = @_RB.input.keystate
        if @_RB.input.key_pressed(@_RB.input.keys.up)
            @.jump()
            console.log("jump pressed")
            return
        if @_RB.input.key_pressed(@_RB.input.keys.left)
            @.walkleft()
            return
        else
            @.walkleft(true)
            
        if @_RB.input.key_pressed(@_RB.input.keys.right)
            @.walkright()
            return
        else
            @.walkright(true)

        #if neigther left or right
        @physics.velocity.x = 0
        @.set_animation("default")


    walkleft:(stop=false) =>
        if stop
            @iswalkleft = false
            return
        if @iswalkleft? and @iswalkleft
            return
        else
            #console.log("walk left")
            @.flip_x(true)
            @iswalkleft = true
            @physics.velocity.x = -50
            @set_animation("walk_left")
    walkright:(stop=false) =>
        if stop
            @iswalkright = false
            return
        if @iswalkright? and @iswalkright
            return
        else
            @.flip_x(false)
            #console.log("walk right")
            @iswalkright = true
            @physics.velocity.x = +50
            @set_animation("walk_right")

    jump:() =>
        if @isonground? and @isonground
            console.log("setting velocity")
            @physics.velocity.y = -150
            @isonground = false
    

class BL_Harness
    constructor: (@settings, @canvas) ->
        #window.runloop = (dt)=>
        #    @runloop(dt)
        @canvaswidth = @settings.canvaswidth
        @canvasheight = @settings.canvasheight
        @_RB = RB()

        @stage = new RBEntity(0,0,0,0)
        @stage.static = true
        #console.log("Created harness stage")

        @updatetimer = new RBTimer()
        @drawtimer = new RBTimer()
        @updatetime = 0
        @drawtime = 0
        @socket = null

        @groups = {}

    update: () ->
        @updatetimer.reset()
        @stage.update(@_RB.dt()) #This updates everthing that was on the stage
        @updatetime = ((@updatetime + @updatetimer.elapsed()) /2) | 0

    draw: () ->
        @drawtimer.reset()
        @stage.draw() #This draws everything on the stage
        @drawtime = ((@drawtime + @drawtimer.elapsed())/2) | 0
    
    draw_stats: () ->
        @_RB.cctx.fillStyle = "#000000"
        @_RB.cctx.font = "12px courier"
        @_RB.cctx.fillText("Frame: " + (@_RB.dt().toString() + "ms"), 10, 10)
        @_RB.cctx.fillText("Update: " + @updatetime + "ms", 10, 25)
        @_RB.cctx.fillText("Draw: " + @drawtime + "ms", 10, 40)
        @_RB.cctx.fillText("Idle: " + (@_RB.dt() - (@updatetime + @drawtime)).toString() + "ms", 10, 55)
    runloop: (dt) =>
        @_RB.cctx.clearRect(0,0,400,400)
        
        @.update()
        @.draw()
        @.draw_stats()

initBioLabClone = () ->

    RBV(RBV_GAME, {
        tilewidth: 8
        tileheight: 8
    })
    RBV(RBV_ENG, {
        canvaswidth: 400
        canvasheight: 400
        screenheight: 400
        screenwidth: 400
        fps: 30
    })
    RBV(RBV_IMG, {
        biolabimg: "data/biolab/biolab.png"
        playerimg: "data/biolab/player.png"
    })
    RBV(RBV_SND, {
    })
    RBV(RBV_MUSIC, {
    })
    RBV(RBV_SS, {
        env: ["env", RBV_IMG.biolabimg, 16,7]
        player: ["player", RBV_IMG.playerimg, 6, 8]
    })


    if not (biolab is null)
        return #prevent multiple inits

    #console.log("Creating App")
    biolab = new BL_Harness(RB().env("settings"), document.getElementById('canvas'))

    group_env = new RBEntity()
    group_env.name = "ENV"
    group_player = new RBEntity()
    group_player.name = "PLAYER"
    group_nocolenv = new RBEntity()

    plat_classes = [ET_01, ET_02, ET_03, ET_04, ET_05]
    dirt_classes = [ET_56, ET_57, ET_58, ET_59, ET_60, ET_61, ET_62]

    group_env.add_member(new ET_01(200,200))
    group_env.add_member(new ET_02(208,200))
    group_env.add_member(new ET_03(216,200))
    group_env.add_member(new ET_04(224,200))
    group_env.add_member(new ET_05(232,200))
   
    for count in [0..400] by settings.tilewidth
        cl = random_item(plat_classes)
        group_env.add_member(new cl(count, 392) )


    for count in [0..40] by settings.tilewidth
        cl = random_item(plat_classes)
        group_env.add_member(new cl(count, 200) )
    for count in [80..120] by settings.tilewidth
        cl = random_item(plat_classes)
        group_env.add_member(new cl(count, 200) )
    for count in [160..200] by settings.tilewidth
        cl = random_item(plat_classes)
        group_env.add_member(new cl(count, 280) )
    
    for countx in [0..400] by settings.tilewidth
        for county in [400..480] by settings.tileheight
            cl = random_item(dirt_classes)
            group_nocolenv.add_member(new cl(countx, county))


    pl1 = new BL_Avatar(216, 80)
    group_player.add_member(pl1)
   
    biolab.stage.add_member(group_env)
    biolab.stage.add_member(group_player)
    biolab.stage.add_member(group_nocolenv)

    cam = new RBCamera(0,0, RBV_ENG.canvaswidth, RBV_ENG.canvasheight)
    cam.follow(pl1)
    cam.scale.x = cam.scale.y = 1
    cam.zoom(2.0, 2.0)
    runLoop = (dt) ->
        @_RB.cctx.clearRect(0,0, RBV_ENG.canvaswidth, RBV_ENG.canvasheight)

        #custom update portion
        biolab.updatetimer.reset()
        biolab.stage.update(@_RB.dt()) #This updates everthing that was on the stage
        cam.update(@_RB.dt())
        @_RB.collide(group_env.members, group_player.members)
        biolab.updatetime = ((biolab.updatetime + biolab.updatetimer.elapsed()) /2) | 0
        
        #end custom update
        biolab.drawtimer.reset()
        cam.draw(biolab.stage)
        biolab.draw_stats() 
        biolab.drawtime = ((biolab.drawtime + biolab.drawtimer.elapsed())/2) | 0


    RB().set_screen(biolab.canvas,biolab.canvaswidth, biolab.canvasheight)
    RB().reset_frame_rate(RBV_ENG.fps, runLoop)

