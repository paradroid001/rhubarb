# Rhubarb
  
Rhubarb is a HTML5/Canvas game engine for javascript,
written in coffeescript. Coffeescript is a language
which compiles into (nice, fast) javascript, but
is itself quite sane and nice to use - like a mix
of python and ruby.

You can develop for Rhubarb in pure javascript, 
but it is much nicer to use coffeescript.

To use coffeescript, see the project at:
http://coffeescript.org/

## Unit tests

Unit tests use the Jasmine framework (jasmine.org), a small
portion of which is included in this release
so that you can run the tests. The unit tests are linked off the test.html page.

## Building the source

With node.js and coffeescript installed, you can build all the
rhubarb sources into javascript by running 

build.sh


## Development

If you have dnotify installed (Ubuntu: sudo apt-get install dnotify) you can develop like this:
from the root of the project (i.e. from where this readme is)

dnotify --all -r ./src -e `./build.sh` &

This runs the build.sh script every time one of the files in src changes.

## Documentation

You can build the documentation with

build_docs.sh

Documentation is done with codo

The source repository:
hg://bitbucket.org/paradroid001/rhubarb

## Contributions

Rhubarb is by Brad Power
brad.power@gmail.com

## License

Copyright (c) 2013 Brad Power

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
