class RBRectEntity extends RBEntity
    constructor:(X, Y, Width, Height, name="RBRectEntity")->
        super(X, Y, Width, Height, name)
        @shouldFill = false
        @fillColour = new RBColour(0,0,0,255)
    #call set_fill with null or false as first arg to 
    #turn off fill for this rect. Otherwise pass
    #r, g, g, a as 0-255 values
    set_fill:(r=0, g=0, b=0, a=255) -> #expect [r,g,b,a]
        if r == null or r == false
            @shouldFill = false
        else
            @shouldFill = true
            @fillColour.set_rgba([r, g, b, a])
    draw:() ->
        if @shouldFill
            @_RB.cctx.fillStyle = @fillColour.htmlrgba
            #@_RB.cctx.fillRect(@left,@top,@width,@height)
            @_RB.cctx.fillRect(@_XYtoTL.x,@_XYtoTL.y,@width,@height)

        @_RB.cctx.strokeStyle = @_colour.htmlrgb
        #@_RB.cctx.strokeRect(@left, @top, @width, @height)
        @_RB.cctx.strokeRect(@_XYtoTL.x, @_XYtoTL.y, @width, @height)
        
