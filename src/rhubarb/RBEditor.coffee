class RBFactory
    constructor: () ->
        @named_classes = {}

    add_class: (name, classtype) ->
        @named_classes[name] = classtype

    create: (classname, args) ->
        switch args.length
            when 0 then return new @named_classes[classname]()
            when 1 then return new @named_classes[classname](args[0])
            when 2 then return new @named_classes[classname](args[0],args[1])
            when 3 then return new @named_classes[classname](args[0],args[1],args[2])
            when 4 then return new @named_classes[classname](args[0],args[1],args[2],args[3])
            when 5 then return new @named_classes[classname](args[0],args[1],args[2],args[3],args[4])
            when 6 then return new @named_classes[classname](args[0],args[1],args[2],args[3],args[4],args[5])
            when 7 then return new @named_classes[classname](args[0],args[1],args[2],args[3],args[4],args[6])



#RB Editor is a component that allows you to create layers/groups
#and populate them with entities
#and then spit those out as a json structure.
#it will also generate UI for each element so that you can set individual properties
#you can attach it to a currently running scene and it can see all of the classes available.

class PropertiesPane
    constructor: () ->
        @pane = document.createElement("div")
        panetext = document.createTextNode("this is the pane content")
        @pane.appendChild(panetext)
        #$(@pane).insertBefore(RB().canvas)

class EditorClearTile extends RBRectEntity
    constructor: (X, Y, blank, Width=32, Height=32, name="Clear") ->
        super(X, Y, Width, Height, name)
        @.set_fill([255,0,0,128])
        @.set_anchor(0,0)
        @.alwaysOnScreen = true

class RBEditor extends RBEntity
    constructor:(@gridx, @gridy, @layers, @namedClasses, @scene, @scenecam) ->
        super(0, 0, 0, 0, name="RBEditor")
        @_RB.input.bind(RB_KEY_UP, @.detect_editor_key)
        @_previous_scene_update = null
        @_previous_stage_position = new RBPoint() 
        if @scene?
            @_previous_scene_update = @scene.update
            

        @alwaysOnScreen = true
        #The rect of the map that we are over. Often identical to currentRect,
        
        @parallax_offset = new RBPoint()

        @map = new RBGroup()
        @classes = []
        @factory = new RBFactory()
        @gridcolour = new RBColour([0, 0, 255, 32])

        for layer in @layers
            @map.add_member( layer )

        @namedClasses["Clear"] = EditorClearTile

        #we assume tiles are x, y, and some numeric param
        for classname, classsymbol of @namedClasses
            @factory.add_class(classname, classsymbol)
            @classes.push({'name':classname, 'class':classsymbol, 'param': 0})

        @currentClass = @classes[0]
        @currentLayer = @map.members[0]

        @uiLayer = new RBLayer()
        @.add_child(@uiLayer)
        @uiLayer.set_parallax(0, 0)
        w = @_RB.screen().width
        size = w/20
        @layerLabel = new RBText(w/2, 1*size, "Layer ??", "Arial", size)
        @classLabel = new RBText(w/2, 2*size, "Class ??", "Arial", size)
        @paramLabel = new RBText(w/2, 3*size, "Param ??", "Arial", size)
        @currentInfo = new RBText(w/2, 4*size, "x: y: ", "Arial", size)
        @layerLabel.alwaysOnScreen = true
        @classLabel.alwaysOnScreen = true
        @paramLabel.alwaysOnScreen = true

        @uiLayer.add_child(@layerLabel)
        @uiLayer.add_child(@classLabel)
        @uiLayer.add_child(@paramLabel)
        @uiLayer.add_child(@currentInfo)
        
        @currentRect = new RBRectEntity(0, 0, @gridx, @gridy)
        @currentRect.set_colour(0, 255, 0, 255)
        @currentRect.set_fill([255,0,0,128])
        @currentRect.set_anchor(0,0)
        @currentRect.alwaysOnScreen = true
        @.add_child(@currentRect)

        @currentTile = new RBEntity()
        @currentRect.add_child(@currentTile)
        @currentTile.alwaysOnScreen = true

        @mapoffset = new RBPoint(0, 0)
        #except in parallax situations.
        @mapRect = new RBRectEntity(0, 0, @gridx, @gridy)
        @mapRect.set_anchor(0, 0)

        
        @.hide()

        @dragTileUnder = null #keep track of tile to destroy when dragging

    load_map_from_file: (path) ->
        f = document.createElement('script')
        f.type = "text/javascript"
        f.src = path
        RBV("__editor", @)
        document.body.appendChild(f)
        document.body.removeChild(f)

    # UNTESTED
    load_map_from_url: (url) ->
        successfunc = (rtext) =>
            console.log("got it!")
            console.log(rtext)
        failfunc = (rtext) =>
            console.log("failed")
            console.log(rtext)

        req = new RBRequest(url, successfunc, failfunc)
        req.get()


    load_map_from_text: (jsontext) ->
        console.log("loading map")
        
        mapdata = JSON.parse(jsontext)
        @.load_map(mapdata)
    
    load_map: (mapdata) ->
        console.log(mapdata)
        find_layer_by_name = (name, obj) ->
            if obj.name == name
                return true
            else
                return false

        for layername, layerdata of mapdata
            #console.log("Layername: #{layername}, LayerData: #{layerdata}")
            layer = @map.find(layername, find_layer_by_name)
            console.log(layer)
            for classname, instancelist of layerdata
                for instanceargs in instancelist
                    x = instanceargs[0]
                    y = instanceargs[1]
                    console.log("Creating #{classname} at #{x},#{y}")
                    newtile = @factory.create(classname, [x, y])
                    layer.add_child(newtile)


    create_current_tile:(x=0, y=0) ->
        newtile = @factory.create(@currentClass.name, [x, y, @currentClass.param])
        anchoroffset = newtile.xy_to_topleft()
        newtile.active = false
        newtile.move_by(-anchoroffset.x, -anchoroffset.y)
        return newtile

    disable_scene: () ->
        if @scene?
            @scene.focus(false)
            RB().actions.paused = true #hack to pause all actions
            p = @_previous_scene_update
            s = @scene
            @scene.update = (dt) ->
                p.call(s, 0)

            @_previous_stage_position.x = @scene.stage.x
            @_previous_stage_position.y = @scene.stage.y

    enable_scene: () ->
        if @scene?
            @scene.focus(true)
            RB().actions.paused = false #hack to enable all actions
            @scene.update = @_previous_scene_update
            @scene.stage.x = @_previous_stage_position.x
            @scene.stage.y = @_previous_stage_position.y

    update_map_tile_position: () ->
        #update the map tile position relative to the
        #currentTile position

    recreate_display_tile:() ->
        @currentTile.destroy()
        @currentTile = null
        @currentTile = @.create_current_tile()
        @currentRect.add_child(@currentTile)
        @currentRect.alwaysOnScreen = true
        @currentTile.alwaysOnScreen = true

        @classLabel.set_text("Class: #{@currentClass.name}")
        @layerLabel.set_text("Layer: #{@currentLayer.name}")
        @paramLabel.set_text("Param: #{@currentClass.param}")
        
        @.fadestate_all_layers(true)
        @currentLayer.unfade()

    fadestate_all_layers: (val) ->
        
        for layer in @map.members
            if val
                layer.fade()
            else
                layer.unfade()

    next_layer: () ->
        @currentLayer = @map.get_after(@currentLayer)
        if @currentLayer is null
            @currentLayer = @map.members[0]
        @.recreate_display_tile()
    prev_layer: () ->
        @currentLayer = @map.get_before(@currentLayer)
        if @currentLayer is null
            @currentLayer = @map.members[@map.members.length-1]
        @.recreate_display_tile()


    next_class: () ->
        nextindex = @classes.indexOf(@currentClass) + 1
        if nextindex >= @classes.length
            nextindex = 0
        @currentClass = @classes[nextindex]
        @.recreate_display_tile()
    
    prev_class: () ->
        previndex = @classes.indexOf(@currentClass) - 1
        if previndex < 0
            previndex+=@classes.length
        @currentClass = @classes[previndex]
        @.recreate_display_tile()

    set_param: (val) ->
        @currentClass.param = val
        @.recreate_display_tile()

    to_json: (for_string = false) ->
        output = {}
        outputstr = ""
        outputhdr = ""
        outputftr = ""
        serialise_layer_classtypes = (l, ct, cn, out) ->
            for o in l.members
                if o instanceof ct
                    out[l.name][cn].push(o.to_list() )

        if not for_string
            outputhdr = "RBV(\"__editor\").load_map("
        else
            outputhdr = "\'"
        for layer in @map.members
            output[layer.name] = {}
            for classname of @namedClasses
                output[layer.name][classname] = []
            for classname, classtype of @namedClasses
                for object in layer.members
                    if object instanceof classtype
                        output[layer.name][classname].push(object.to_list() )
        if not for_string
            formatfn = (k,v) ->
                if v instanceof Array
                    return "[#{v}]"#JSON.stringify(v)
                return v
            outputstr = JSON.stringify(output, undefined, 2)
            outputftr = ");"
        else
            outputstr = JSON.stringify(output)
            outputftr = "\'"

        console.log("//TOJSON - COPY FROM BELOW THIS LINE")
        console.log("#{outputhdr}#{outputstr}#{outputftr}")
        console.log("//END TOJSON - COPY FROM ABOVE THIS LINE")

    show: () ->
        @visible = true
        @_RB.input.bind(RB_KEY_UP, @.on_key_up, true)
        @_RB.input.bind(RB_KEY_DOWN, @.on_key_down, true)
        @_RB.input.bind(RB_MOUSE_MOVE, @.on_mouse_move, true)
        @_RB.input.bind(RB_MOUSE_DOWN, @.on_mouse_down, true)
        @_RB.input.bind(RB_MOUSE_WHEEL, @.on_mouse_wheel, true)
        @.recreate_display_tile()
        @.disable_scene()

        @mapoffset.x = 0 #-RB().screen().x/2
        @mapoffset.y = 0 #-RB().screen().y/2
        #@_RB.screen().set_position(0, 0)
        @.warp_to_right_position()
        #@scene.stage.set_position(@mapoffset.x, @mapoffset.y)
        #@.set_position(@mapoffset.x, @mapoffset.y)
        #@_RB.screen().set_position(-@mapoffset.x, -@mapoffset.y) 


    hide: () ->
        @visible = false
        @_RB.input.unbind(RB_KEY_UP, @.on_key_up, true)
        @_RB.input.unbind(RB_KEY_DOWN, @.on_key_down, true)
        @_RB.input.unbind(RB_MOUSE_MOVE, @.on_mouse_move)
        @_RB.input.unbind(RB_MOUSE_DOWN, @.on_mouse_down)
        @_RB.input.unbind(RB_MOUSE_WHEEL, @.on_mouse_wheel)
        @.fadestate_all_layers(false)
        @.enable_scene()

    detect_editor_key: (event) =>
        keycode = event.keyCode
        keys = @_RB.input.keys
        if keycode is keys.e and 
                      @_RB.input.key_pressed(keys.ctrl)
            if @visible
                console.log("Hiding RBEditor")
                @.hide()
            else
                console.log("Showing RBEditor")
                @.show()

    on_key_up: (event) =>
        code = event.keyCode
        keys = @_RB.input.keys
        
    on_mouse_wheel: (event) =>
        i = @_RB.input
        delta = i.mouse.wheeldelta
        if delta > 0
                @.next_class()
            else if delta < 0
                @.prev_class()

        ###
        if i.key_pressed(i.keys.c)
            if delta > 0
                @.next_class()
            else if delta < 0
                @.prev_class()
        if i.key_pressed(i.keys.l)
            if delta > 0
                @.next_layer()
            else if delta < 0
                @.prev_layer()
        ###
        i.swallow(event)

    on_key_down: (event) =>
        keys = @_RB.input.keys
        code = event.keyCode
        mvmtx = 0
        mvmty = 0
        switch code
            when keys.right
                mvmtx += 1
                #@_RB.screen().x += 1
            when keys.left
                #@_RB.screen().x -= 1
                mvmtx -= 1
            when keys.up
                #@_RB.screen().y -= 1
                mvmty -= 1
            when keys.down
                #@_RB.screen().y += 1
                mvmty += 1

            when keys.l
                if @_RB.input.key_pressed(keys.shift)
                    @.prev_layer()
                else
                    @.next_layer()
            when keys.c
                if @_RB.input.key_pressed(keys.shift)
                    @.prev_class()
                else
                    @.next_class()
            when keys.v
                @.set_param(@currentClass.param-1)
            when keys.b
                @.set_param(@currentClass.param+1)
            when keys.j
                @.to_json()
        @_RB.input.swallow(event)
        
        if @scene?
            xdiff = -mvmtx*@gridx
            ydiff = -mvmty*@gridy
            @mapoffset.x += xdiff
            @mapoffset.y += ydiff

            @.warp_to_right_position()
            #console.log("scene pos is now: #{@scene.stage.x}, #{@scene.stage.y}")
            #@scene.cam.offsetx += mvmtx
            #@scene.cam.offsety += mvmty
        #else
        #    @_RB.screen().set_position(-mvmtx, -mvmty)


    warp_to_right_position: () ->
        if @scene?
            @scene.stage.set_position(@mapoffset.x, @mapoffset.y)
            @.set_position(@mapoffset.x, @mapoffset.y)
            @_RB.screen().set_position(-@mapoffset.x, -@mapoffset.y)

    on_mouse_move: (event) =>
        if @visible
            #mx = ~~(@_RB.input.mouse.x / @gridx)
            #my = ~~(@_RB.input.mouse.y / @gridy)
            scr = RB().screen()
            @parallax_offset.x = (@currentLayer.x % @gridx)# * @gridx
            @parallax_offset.y = (@currentLayer.y % @gridy)# * @gridy
            #mx = Math.floor((@_RB.input.mouse.x + @parallax_offset.x) / @gridx)
            #my = Math.floor((@_RB.input.mouse.y + @parallax_offset.y) / @gridy)
            mx = Math.floor((@_RB.input.mouse.x) / @gridx)
            my = Math.floor((@_RB.input.mouse.y) / @gridy)
            
            #console.log("Mouse: #{@_RB.input.mouse.x}, #{@_RB.input.mouse.y}, Editor mouse: #{mx}, #{my}")
            
            
            
            #@currentRect.set_position( (mx - @parallax_offset.x) * @gridx, (my-@parallax_offset.y) * @gridy)
            #@currentRect.set_position( (mx * @gridx) + @mapoffset.x, (my * @gridy) + @mapoffset.y)
            @currentRect.set_position( (mx * @gridx), (my * @gridy))
            @currentInfo.set_text("x: #{@currentRect.x}, y: #{@currentRect.y}")

            #@currentRect.sub(@parallax_offset)
            if @_RB.input.mouse.leftpressed
                #console.log("mouse held down")
                tileunder = @currentRect.overlaps_in_rlist(@currentLayer.members)
                if tileunder == null or tileunder != @dragTileUnder
                    @.on_mouse_down(event)
                    @dragTileUnder =  @currentRect.overlaps_in_rlist(@currentLayer.members)
            #For the purposes of draw, we need to adjust for parallax offset
            #@currentRect.move_by(@parallax_offset.x, -@parallax_offset.y)

    on_mouse_down: (event) =>
        tileunder = @currentRect.overlaps_in_rlist(@currentLayer.members)
        if tileunder != null
            console.log("Removal - tileunder = ")
            console.log(tileunder)
            @_RB.env('tileunder', tileunder)
            tileunder.destroy() #destroy removes child from parent
        if ! (@currentClass.class is EditorClearTile)
            #when we create the child, we remove the layer parallax offset (@currentLayer.x%@gridx etc, since
            #the layer position should be relative to itself
            @currentLayer.add_child(@.create_current_tile(@currentRect.x-(@currentLayer.x%@gridx), @currentRect.y-(@currentLayer.y%@gridy)))

        return @currentLayer

    draw: () ->
        @_RB.cctx.strokeStyle = @gridcolour.htmlrgba
        scr = @_RB.screen()
        rows = scr.height / @gridy
        cols = scr.width / @gridx
        #offx = scr.left % @gridx
        #offy = scr.top % @gridy
        offx = (scr.left-@currentLayer.left) % @gridx
        offy = (scr.top-@currentLayer.top) % @gridy
        
        for colx in [0..cols]
            @_RB.cctx.strokeRect(-offx+scr.left+colx*@gridx, -offy+scr.top, @gridx, scr.height)
        for rowy in [0..rows]
            @_RB.cctx.strokeRect(-offx+scr.left, -offy+scr.top+rowy*@gridy, scr.width, @gridy)

class PaneUI
    constructor:(title) ->
        @element = $("<div />", {
            'class': "paneui"}
        )
        @titleelement = $("<div />", {
            text: title,
            'class': "panetitle"
        })
        @bodyelement = $("<div />", {
            text: "pane body text",
            'class' : "panebody"
        })
        $(@element).append(@titleelement)
        $(@element).append(@bodyelement)

class UISpecDisplay
    constructor: (@obj) ->
        @elements = {}
        @spec = @obj.uispec()
        console.log("uispec was: #{@spec}")
        for key, item of @spec
            console.log("key is #{key}, val is #{item}")
            #title
            @elements[key] = $("<div class=\"uispec_property\">")
            $(@elements[key]).append("<div class=\"uispec_proptitle\"><b>#{key}: </b></div>")
            itemdata = @.update_value(key)
            #based on the type of the itemdata, you would 
            #change the display type of what we are about
            #to append
            $(@elements[key]).append($("<div class=\"uispec_propvalue\">#{itemdata}</div>"))

    update_value: (key) ->
        return @obj[@spec[key][0]]
            
class ObjectUI extends PaneUI
    constructor: (obj, name="Unknown") ->
        if obj.name?
            name = obj.name
        super(name)
        
        if obj instanceof RBObject
            console.log("was an rbobject")
            displayobj = new UISpecDisplay(obj)
            for key, uielement of displayobj.elements
                $(@bodyelement).append(uielement)
        else
            console.log("populating a non RBObject")
            for own key, val of obj
                console.log("key: #{key}, val: #{val}")
                $(@bodyelement).append($("<p>", {
                    text: "#{key} : #{val}",
                }))

        #$(@bodyelement).text("ObjectUI")
        #$(@bodyelement).append("<textarea />")

###
So what I need is:
    each class should have a static method for printing? or at least 'displaying'
        so an RPPoint should be able to look like "{x:30, y:40}"
        That actually begins to look like a uispec, which would
        expose x and y.
        expose(propname, valuetype (widget for display eg text, number, slider), readonly=false)
    ALSO
        the UI for the editor needs to be updated as the game is running.
        so all the values need to be refreshed.
        How do I do that? How do I have the structs REFERENCING the values instead of literals?
###

populate_objects = (objectlist) ->
    objspane = $("#objectpane")
    console.log(objspane)
    for obj in objectlist
        #objspane.append($("<div></div>").append($("<p>Hello #{count}</p>")))
        #objspane.append(new ObjectUI().element)
        objspane.append(new ObjectUI(obj, "Object").element)


