app = null
testsettings = {
    "setting1": 2, 
    "setting2": 3, 
    "canvaswidth":400, 
    "canvasheight":400,
    "canvasid": "canvas"
    "canvassmoothing": false,
    "fps": 30
}
testsettings.engine = 
        canvaswidth:400, 
        canvasheight:400,
        screenwidth:400, 
        screenheight:400,
        canvasid: "canvas"
        canvassmoothing: false,
        fps: 30
testsettings.images=
testsettings.sounds=
testsettings.spritesheets=
testsettings.game=

FRAME_RATE = 30

class Harness
    constructor: (@settings, @canvas) ->
        #window.runloop = (dt)=>
        #    @runloop(dt)
        @canvaswidth = @settings.canvaswidth
        @canvasheight = @settings.canvasheight
        @_RB = RB()
        @_RB.input.bind(RB_KEY_UP, @.dbg_keys)
        @stage = new RBEntity(0,0,0,0)
        #@stage.set_anchor(0, 0)
        #@stage.staticphysics = true
        #console.log("Created harness stage")
        @socket = null

        @groups = {}
        @stats = new RBStats()
        @pl = RB().preloader

    dbg_keys: (event) =>
        if event.keyCode == @_RB.input.keys.tab
            @_RB.setDebug(!@_RB.isDebug())

    preload: () =>
        console.log("preloader loop")
        if @pl.done != true
            console.log(@pl.stats.total)
            @pl.update()
        else
            @.start()

    start: () ->
        RB().set_screen(@canvas,@canvaswidth, @canvasheight, testsettings.canvaswidth, testsettings.canvasheight, testsettings.smoothing)
        @.init()
        RB().reset_frame_rate(FRAME_RATE, @.runloop)


    load: (images, sounds, spritesheets, music) ->
        @pl.queue_images(images)
        @pl.queue_spritesheets(spritesheets)
        @pl.queue_sounds(sounds)
        @pl.queue_music(music)
        @pl.start()
        RB().reset_frame_rate(2, @.preload)

    init: () ->
        console.log("Nothing done in init!")
        #override this.


    update: (dt) ->
        #override this
    
    render: () ->
        @stage.render() #This draws everything on the stage
    
    runloop: (dt) =>
        @_RB.cctx.clearRect(0,0, @canvaswidth, @canvasheight)
        RB().actions.update(RB().dt())
        @stats.startupdate()
        @stage.update(@_RB.dt())
        @.update(dt)
        @stats.endupdate()
        
        @stats.startdraw()
        @.render()
        @stats.enddraw()
        @stats.draw()

initApp = (numdots, numdudes) ->
    if not (app is null)
        return #prevent multiple inits

    #console.log("Creating App")
    app = new Harness(testsettings, document.getElementById('canvas'))
        
    app.init = () ->
        RB().media.get_spritesheet("warrior")
        for i in [0...numdots]
            x = random_int(0, 400)
            y = random_int(0, 400) 
            dot = new Dot(x, y, 'dotty')
            dx = random_int(0, 100)
            dy = random_int(0, 100)
            dot.get_component("physics").velocity = new RBPoint(dx, dy)
            app.stage.add_child(dot)

        for j in [0...numdudes]
            x = random_int(0, 400)
            y = random_int(0, 400)
            dude = new Dude(x, y, 'dude')
            dx = random_int(0, 100)
            dy = random_int(0, 100)
            dude.get_component("physics").velocity = new RBPoint(dx, dy)
            s = random_float(0.2, 2.0)
            dude.set_scale(s, s)
            app.stage.add_child(dude)
    
    app.load(["data/platformer/Warrior.png"], [], [["warrior", "data/platformer/Warrior.png", 3, 4]], [])

testRects = () ->
    app = new Harness(testsettings, document.getElementById('canvas'))
    for i in [1..10]
        r = new RBRectEntity(random_int(10, 300), random_int(10, 300), random_int(10, 80), random_int(10, 80))
        r.set_fill(random_int(128, 255), random_int(128, 255), random_int(128, 255) )
        app.stage.add_child(r)
    app.start()

testCollisions = (num) ->
    #console.log("Running testCollisions")
    app = new Harness(testsettings, document.getElementById('canvas'))

    #Test data...
    group1 = new RBEntity()
    group2 = new RBEntity()
    app.init = () ->
        for a in [0..num]
            #greens
            greenguy = new Brick(random_int(0,100), random_int(200,300), 3, 3, "Green guy",  0, 200, 50)
            greenguy.get_component("physics").velocity.x = 32
            #greenguy.get_component("physics").floating = true
            group1.add_member(greenguy)
            #blues
            blueguy = new Brick(random_int(300,400), random_int(200,300), 3, 3, "Blue guy", 0, 50, 200) 
            blueguy.get_component("physics").velocity.x = -32
            #blueguy.get_component("physics").floating = true
            group2.add_member(blueguy)
        app.stage.add_member(group1)
        app.stage.add_member(group2)

    app.update = (dt) ->
        @_RB.collide(group1.members, group2.members)

    app.start()

testCollisions2 = (num) ->
    testsettings.images= {
        env: "data/platformer/env_tiles.png"
    }
    testsettings.spritesheets= {
        env: ["env", "data/platformer/env_tiles.png", 10, 10]
    }
    app = new RBGame(testsettings)
    RB().start()
    
    scene = new TestCollisions2Scene()
    scene.push()



testParenting = (num) ->
    #console.log("Running testParenting")
    app = new Harness(testsettings, document.getElementById('canvas'))

    app.init = () ->
        #test data
        a = new Brick(200, 200, 20, 20, "a", 40, 200, 80)
        b = new Brick(20,20,20,20, "b", 30, 100, 40)
        c = new Brick(-20, -20, 20, 20, "c", 30, 100, 40)
        d = new Brick(-20, 20, 20, 20 , "d", 30, 100, 40)
        e = new Brick(20, -20, 20, 20, "e", 30, 100, 40)
        a.components = {} #get_component("physics").floating = true
        #f = new RBText(30, 30, "Hello!")
        a.add_child(b)
        a.add_child(c)
        a.add_child(d)
        a.add_child(e)
        #a.add_child(f)
        p = new RBPhysics()
        a.add_component(p)
        p.velocity.x = 40
        p.velocity.y = 40
        
        #RB().world.gravity = 0
        RB().env("RBPhysicsSystems").default.gravity = 0

        app.stage.add_member(a)

    app.start()

testCamera = (num) ->
    #console.log("Running testCamera")
    app = new Harness(testsettings, document.getElementById('canvas'))
    camera1 = new RBCamera(200,200,200,200)
    camera2 = new RBCamera(0,200,200,200)
    camera3 = new RBCamera(200,0,200,200)


    app.init = () ->
        #RB().set_screen(app.canvas, app.canvaswidth, app.canvasheight)

        #test data
        a = new Brick(200, 200, 20, 20, "", 40, 200, 80)
        b = new Brick(20,20,20,20, "", 30, 100, 40)
        c = new Brick(-20, -20, 20, 20, "", 30, 100, 40)
        d = new Brick(-20, 20, 20, 20 , "", 30, 100, 40)
        e = new Brick(20, -20, 20, 20, "", 30, 100, 40)
        a.add_child(b)
        a.add_child(c)
        a.add_child(d)
        a.add_child(e)
    
        a.get_component("physics").velocity.x = 40
        a.get_component("physics").velocity.y = 40
        RB().env("RBPhysicsSystems").default.gravity = 0
        
        @_RB._screenRect.set_width(200).set_height(200)
        
        app.stage.add_member(a)

        app.stage.add_member(new Block(32*n, 100, 32, 32)) for n in [0..5]
        app.stage.add_member(new Block(100, 100, 32, 32))

        dude = null
        for j in [0...5]
            x = random_int(0, 200)
            y = random_int(0, 200)
            dude = new Dude(x, y, 'dude')
            dx = random_int(0, 100)
            dy = random_int(0, 100)
            dude.get_component("physics").velocity = new RBPoint(dx, dy)
            app.stage.add_child(dude)
        
        camera2.follow(dude)
        camera1.zoom(1.4, 1.4)
        camera3.offset.x = camera3.offset.y = 50

    app.update = (dt) ->
        camera1.update(dt)
        camera2.update(dt)
        camera3.update(dt)

    app.render = () ->
        app.stage.render()
        camera1.draw(app.stage)
        camera2.draw(app.stage)
        camera3.draw(app.stage)
        
        #outline that cam
        @_RB.cctx.strokeRect(camera1.left, camera1.top, camera1.width, camera1.height)
        @_RB.cctx.strokeRect(camera2.left, camera2.top, camera2.width, camera2.height)
        @_RB.cctx.strokeRect(camera3.left, camera3.top, camera3.width, camera3.height)

    app.load(["data/platformer/env_tiles.png", "data/platformer/Warrior.png"], [], [["env", "data/platformer/env_tiles.png", 10, 10], ["warrior", "data/platformer/Warrior.png", 3, 4] ], [])
   

testActions = (num) ->
    #console.log("Running testActions")
    app = new Harness(testsettings, document.getElementById('canvas'))

    #test data
    a = new Brick(200, 200, 20, 20, "Main Parent", 40, 200, 80)
    b = new Brick(20,20,20,20, "", 30, 100, 40)
    c = new Brick(-20, -20, 20, 20, "", 30, 100, 40)
    d = new Brick(-20, 20, 20, 20 , "", 30, 100, 40)
    e = new Brick(20, -20, 20, 20, "", 30, 100, 40)
    a.add_child(b)
    a.add_child(c)
    a.add_child(d)
    a.add_child(e)
   
    #a.velocity.x = 0
    #a.velocity.y = 0
    #RB().world.gravity = 0
    RB().env("RBPhysicsSystems").default.gravity = 0
    app.stage.add_member(a)

    f = new Brick(100, 100, 50, 50, "Test", 255, 0, 0)
    f.set_colour(255, 0, 0, 128)
    app.stage.add_member(f)
    g = new Brick(100, 100, 50, 50, "Test", 255, 0, 0)
    app.stage.add_member(g)
    g.set_colour(255, 0, 0, 128)


    #make an action
    action = new RBMoveBy(1.2, 130, -130)
    action.run_on(a)
    action2 = new RBRotateTo(2.4, 3*360)
    action2.run_on(a)
    actionscale = new RBScaleBy(2.4, 1.5)
    actionscale.run_on(a)

    action3 = new RBRotateTo(2.4, 3*360)
    action3.run_on(g)

    basicfunc = () ->
        console.log("Basic func was called!")
        console.log(arguments)
    #Calling a function
    action4 = new RBCallFunction(basicfunc, [3, 4, 5])
    #Running the same action on multiple targets
    action4.run_on(g)
    action4.run_on(a)
    action4.run_on(f)

    action5 = new RBActionSequence(
        [ new RBMoveTo(0.5, 300, 300), 
        new RBDelay(1), 
        new RBMoveBy(1, -150, -150), 
        new RBScaleBy(2.0, 2, 2), 
        new RBScaleTo(1.0, 1, 1) ])
    action5.run_on(f)

    action6 = new RBTintBy(1.0, 0, 255, 0)
    for count in [0..10]
        nb = new Brick(10, 200+10*count, 10, 10, "Item #{count}", 0, 0, 255)
        app.stage.add_member(nb)
        action.run_on(nb)
        action2.run_on(nb)
        action6.run_on(nb)

    action7 = new RBTintTo(4.0, 0, 255, 0, 255)
    action7.run_on(f)
    action8 = new RBTintTo(4.0, 0, 0, 255, 128)
    action8.run_on(g)
    runloop = (dt) ->
        RB().cctx.clearRect(0,0,500,500)
       
        RB().actions.update(RB().dt())
        app.update()
        app.render()
        app.stats.draw()


    RB().set_screen(app.canvas, app.canvaswidth, app.canvasheight)
    RB().reset_frame_rate(FRAME_RATE, runloop)

testText = (num) ->
    console.log("Running testText")
    app = new Harness(testsettings, document.getElementById('canvas'))

    
    RB().set_screen(app.canvas, app.canvaswidth, app.canvasheight, app.runloop)
    
    #test data
    a = new RBText(30, 200, "Courier 40, no stroke", "Courier", 40, null)
    a.set_colour(200,50,70)
    b = new RBText(30, 250, "Arial 40, stroke", "arial", 40, null,isStroke = true)
    b.set_colour(30,80,220)
    b.strokeColour.set_rgb(0,0,0,255)
    c = new RBText(30, 300, "Helvetica 40, angle", "Helvetica", 40, null, isStroke = true)
    c.set_colour(130,90,20)
    c.angle = 0.1

    #d = new Brick(30, 30, 100, 100, "testbrick", 255, 128, 0)
    #d = new RBEntity(30, 30, 100, 100)
    d = new Dot(30,30)
    #d.set_colour([random_int(0,255), random_int(0,255), random_int(0,255)])
    e = new RBText(0, 0, "Child text", "Helvetica", 40, null, isStroke = true)
    d.add_child(e)
    app.stage.add_member(a)
    app.stage.add_member(b)
    app.stage.add_member(c)
    app.stage.add_member(d)
    action = new RBMoveBy(5.0, 130, 130)
    action.run_action_on(d)

    runloop= () ->
        RB().cctx.clearRect(0,0,500,500)
        RB().actions.update(RB().dt())
        app.update()
        app.render()
        app.stats.draw()


    RB().set_screen(app.canvas, app.canvaswidth, app.canvasheight)
    RB().reset_frame_rate(FRAME_RATE, runloop)

testUI = (num) ->
    console.log("Running testUI")
    app = new Harness(testsettings, document.getElementById('canvas'))
    #RB().set_screen(app.canvas, app.canvaswidth, app.canvasheight)

    app.init = () ->
        resp_fn = (args) =>
            console.log("I have responded to the message, I have these args: " + args)

        RBSubscribeMessage("BTN_CLICKED", resp_fn)
    
        class testbutton extends RBButton
            constructor: (btnText, X, Y, Width, Height) ->
                super(btnText, X, Y, Width, Height)

            on_click: (args) =>
                super(args)
                RBBroadcast("BTN_CLICKED", 1, 2, 3, 4)
    
        a = new testbutton("Click Me", 200, 100, 150, 50)
        app.stage.add_child(a)

        b = new RBImageButton("Hello", 200, 300, 150, 50, "data/rb/button_up.png", "data/rb/button_down.png", "data/rb/button_hover.png")
        app.stage.add_member(b)
        upimage = new RBSprite(100, 100, "data/rb/button_up.png")#, 250, 50)
        app.stage.add_child(upimage)

    app.load(["data/rb/button_up.png", "data/rb/button_down.png", "data/rb/button_hover.png"], [], [], [])

testSound = (num) ->
    console.log("Testing sound")
    ###
    app = new RBGame(testsettings, document.getElementById('canvas'))
    scene = new RBScene()
    a = new RBText(30, 200, "Courier 40, no stroke", "Courier", 40, null)
    a.set_colour(200,50,70)
    scene.stage.add_member(a)
    scene.push()
    RB().start()
    ###
    app = new Harness(testsettings, document.getElementById('canvas'))
    #RB().set_screen(app.canvas, app.canvaswidth, app.canvasheight, app.runloop)
    
    app.init = () ->
        a = new RBText(RB().screen().width/2, 200, "3 buffered sounds. 1 for sfx, 2 for music", "Courier", 12, null)
        b = new RBText(RB().screen().width/2, 220, "+/- for sfx vol", "Courier", 12, null)
        a.set_colour(200,50,70)
        b.set_colour(50,200,70)
        app.stage.add_member(a)
        app.stage.add_member(b)

        snd = RB().media.load_sound("data/sfx.wav")
        music = RB().media.load_music("data/5D.mp3")

        on_key_down= (event) =>
            keys = RB().input.keys
            if event.keyCode == keys.num_1
                snd.play()
                RB().input.swallow(event)
            if event.keyCode == keys.num_2
                music.play(true)
                RB().input.swallow(event)
            if event.keyCode == keys.num_3
                music.stop()
                RB().input.swallow(event)
            if event.keyCode == keys.minus
                v = RB().media.sfx_volume()
                v = RB().media.sfx_volume(v-0.1)
                console.log("Global SFX Volume is now #{v}")
            if event.keyCode == keys.plus
                v = RB().media.sfx_volume()
                v = RB().media.sfx_volume(v+0.1)
                console.log("Global SFX Volume is now #{v}")


    
        RB().input.bind(RB_KEY_DOWN, on_key_down)

    app.start()    

testProtocol = (num) ->
    
    app = new Harness(testsettings, document.getElementById('canvas'))

    #create groups.
    enemygroup = new RBGroup("enemy")
    testprotocol = new TestProtocolHandler(app.stage)

    servertimer = 14000
    serverentities = []
    
    
    serverLoop = (dt) ->
        #So the server is going to create a new guy every 5 seconds.
        servertimer += dt
        if servertimer > 15000
            servertimer = 0
            #add the new guy to the server entities
            serverentities.push(newdot())
       
            #now possibly make an update to each serverentitiy
            for ent in serverentities
                if random_chance(0.3)
                    #change direction
                    ent.physics.velocity.x += random_int(-5, 5)
                    ent.physics.velocity.y += random_int(-5, 5)
                #actually update the entities
                
                 
        #now update all the clients (every ent - not just dirty)
        for ent in serverentities
            ent.update(dt)
            testprotocol.deliver(new RBProtocol().set_opcode('uobj').set_args(ent.id, [ent.x, ent.y]).to_JSON())

    newdot = () ->
        #create a new guy
        x = random_int(0, 200)
        y = random_int(0, 200)
        newguy = new Dot(x, y, 'dotty')
        dx = random_int(-20, 20)
        dy = random_int(-20, 20)
        newguy.physics.velocity.x = dx
        newguy.physics.velocity.y = dy
        #now notify the 'client' of it via the protocol
        testprotocol.deliver(new RBProtocol().set_opcode('nobj').set_args(newguy.id, 'dot', [newguy.x, newguy.y, newguy.physics.velocity.x, newguy.physics.velocity.y]).to_JSON() )

        return newguy

    newavatar = () ->
        #create a new avatar
        #create a new guy
        x = random_int(0, 200)
        y = random_int(0, 200)
        newguy = new Avatar(x, y, 'Testguy')
        dx = random_int(-20, 20)
        dy = random_int(-20, 20)
        newguy.physics.velocity.x = dx
        newguy.physics.velocity.y = dy
        #now notify the 'client' of it via the protocol
        testprotocol.deliver(new RBProtocol().set_opcode('nobj').set_args(newguy.id, 'avatar', [newguy.x, newguy.y, newguy.physics.velocity.x, newguy.physics.velocity.y]).to_JSON() )
        return newguy


    runLoop = (dt) ->
        @_RB.cctx.clearRect(0,0,500,500)

        #custom update portion
        app.stats.startupdate()
        #simulate the server
        serverLoop(dt)
        #update the protocol
        testprotocol.update(dt)
        #update the entities
        app.stage.update(dt) #This updates everthing that was on the stage
        app.stats.endupdate()
        #end custom update

        app.render()
        app.stats.draw()


    RB().set_screen(app.canvas, app.canvaswidth, app.canvasheight)
    serverentities.push(newavatar())
    RB().reset_frame_rate(FRAME_RATE, runLoop)

testNetClient = (num) ->
    app = new Harness(testsettings, document.getElementById('canvas'))
    RB().world.gravity = 0
    testprotocol = new TestProtocolHandler(app.stage)
    testnet = new RBNet(name="test1", host="127.0.0.1", port="8888", protocolHandler = testprotocol)

    newavatar = () ->
        #create a new avatar
        #create a new guy
        x = random_int(0, 200)
        y = random_int(0, 200)
        newguy = new Avatar(x, y, 'Testguy')
        dx = random_int(-20, 20)
        dy = random_int(-20, 20)
        newguy.physics.velocity.x = dx
        newguy.physics.velocity.y = dy
        #now notify the 'client' of it via the protocol
        testprotocol.deliver(new RBProtocol().set_opcode('nobj').set_args(newguy.id, 'avatar', [newguy.x, newguy.y, newguy.physics.velocity.x, newguy.physics.velocity.y]).to_JSON() )
        return newguy



    
    clientLoop = (dt) ->
        a = 1

    runLoop = (dt) ->
        @_RB.cctx.clearRect(0,0,500,500)

        #custom update portion
        app.stats.startupdate()
        #simulate the server
        clientLoop(dt)
        #update the protocol
        testprotocol.update(dt)
        #update the entities
        app.stage.update(dt) #This updates everthing that was on the stage
        app.stats.endupdate()
        #end custom update

        app.render()
        app.stats.draw()


    
    RB().set_screen(app.canvas, app.canvaswidth, app.canvasheight)
    avatar = newavatar()
    RB().reset_frame_rate(FRAME_RATE, runLoop)

testHTTPRequests = () ->
    app = new Harness(testsettings, document.getElementById('canvas'))
    RB().set_screen(app.canvas, app.canvaswidth, app.canvasheight)

    postsuccess = () =>
        console.log("the post was good")
    requestfail = () =>
        console.log("the request failed")
    getsuccess = (text) =>
        console.log("the get was good: #{text}")

    do_get = () =>
        console.log("running do_get")
        testGet.get()

    testPost = new RBRequest("posttest.html", postsuccess, requestfail)
    testGet = new RBRequest("gettest.html", getsuccess, requestfail) 

    postbtn = new RBButton("Get", 200, 200, 50, 20)
    postbtn.add_on_click(do_get) 
    app.stage.add_member(postbtn)

    clientLoop = (dt) ->
        a = 1

    runLoop = (dt) ->
        @_RB.cctx.clearRect(0,0,500,500)

        #custom update portion
        app.stats.startupdate()
        #simulate the server
        clientLoop(dt)
        #update the entities
        app.stage.update(dt) #This updates everthing that was on the stage
        app.stats.endupdate()
        #end custom update

        app.render()
        app.stats.draw()
    
    RB().reset_frame_rate(FRAME_RATE, runLoop)



