class RBLog
    LOG=1
    DEBUG=2
    WARN=3
    ERROR=4
    constructor: (@_useconsole=true) ->
        @_enabled = true
        @linenum = 0
        @outer = $('<div style="display:block; position: absolute; width:100%; bottom:0px;"></div>')
        @header = $('<div style="border-top:1px solid #6f6f6f; border-bottom: 1px solid #6f6f6f;  background-color: #afafaf; -moz-box-shadow:0 1px 3px rgba(0,0,0,0.5); -webkit-box-shadow: 0 1px 3px rgba(0,0,0,0.5); text-shadow: 0 -1px 1px rgba(0,0,0,0.25);"><div style="color: white; padding-left:7px; font-weight:bold;">LOGGING</div></div>')
        @le = $('<div style="display:block; background-color: #efefef; height:100px; overflow:scroll; padding-left:7px; padding-top:3px;"></div>')
        @header.click( (e) =>
            if @le.is(":hidden")
                @le.show()
            else
                @le.hide() )

        @outer.append(@header)
        @outer.append(@le)

        if !console?
            @_useconsole = false

    attach: () ->
        $("body").append(@outer)

    enable: () ->
        @_enabled = true
    disable: () ->
        @_enabled = false


    log: (message) ->
        @.out(message, LOG)
    debug: (message) ->
        @.out(message, DEBUG)
    warn: (message) ->
        @.out(message, WARN)
    error: (message) ->
        @.out(message, ERROR)

    out: (message, msgtype=LOG) ->
        if @_enabled
            if @_useconsole and console?
                switch msgtype
                    when LOG then console.log(message)
                    when DEBUG then console.debug(message)
                    when WARN then console.warn(message)
                    when ERROR then console.error(message)
            else
                @le.append(@linenum++ + ": " + message+'<br />')

###
See below for reasonably awesome logger that keeps line numbers.
See heere: http://stackoverflow.com/questions/11308239/console-log-wrapper-that-keeps-line-numbers-and-supports-most-methods


                _log = (function (methods, undefined) {

    var Log = Error; // does this do anything?  proper inheritance...?
    Log.prototype.write = function (args, method) {
        /// <summary>
        /// Paulirish-like console.log wrapper.  Includes stack trace via @fredrik SO suggestion (see remarks for sources).
        /// </summary>
        /// <param name="args" type="Array">list of details to log, as provided by `arguments`</param>
        /// <param name="method" type="string">the console method to use:  debug, log, warn, info, error</param>
        /// <remarks>Includes line numbers by calling Error object -- see
        /// * http://paulirish.com/2009/log-a-lightweight-wrapper-for-consolelog/
        /// * http://stackoverflow.com/questions/13815640/a-proper-wrapper-for-console-log-with-correct-line-number
        /// * http://stackoverflow.com/a/3806596/1037948
        /// </remarks>

        // via @fredrik SO trace suggestion; wrapping in special construct so it stands out
        var suffix = {
            "@": (this.lineNumber
                    ? this.fileName + ':' + this.lineNumber + ":1" // add arbitrary column value for chrome linking
                    : extractLineNumberFromStack(this.stack)
            )
        };

        args = args.concat([suffix]);
        // via @paulirish console wrapper
        if (console && console[method]) {
            if (console[method].apply) { console[method].apply(console, args); } else { console[method](args); } // nicer display in some browsers
        }
    };
    var extractLineNumberFromStack = function (stack) {
        /// <summary>
        /// Get the line/filename detail from a Webkit stack trace.  See http://stackoverflow.com/a/3806596/1037948
        /// </summary>
        /// <param name="stack" type="String">the stack string</param>

        // correct line number according to how Log().write implemented
        var line = stack.split('\n')[3];
        // fix for various display text
        line = (line.indexOf(' (') >= 0
            ? line.split(' (')[1].substring(0, line.length - 1)
            : line.split('at ')[1]
            );
        return line;
    };

    // method builder
    var logMethod = function(method) {
        return function (params) {
            /// <summary>
            /// Paulirish-like console.log wrapper
            /// </summary>
            /// <param name="params" type="[...]">list your logging parameters</param>

            // only if explicitly true somewhere
            if (typeof DEBUGMODE === typeof undefined || !DEBUGMODE) return;

            // call handler extension which provides stack trace
            Log().write(Array.prototype.slice.call(arguments, 0), method); // turn into proper array & declare method to use
        };//--  fn  logMethod
    };
    var result = logMethod('log'); // base for backwards compatibility, simplicity
    // add some extra juice
    for(var i in methods) result[methods[i]] = logMethod(methods[i]);

    return result; // expose
})(['error', 'debug', 'info', 'warn']);//--- _log

###
