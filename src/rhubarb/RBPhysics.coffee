#holds variables and methods for the global physics simulation,
#used by all RBPhysics components. This will be in the RB global
#named dictionary - RB().env("RBPhysicsSystem")
class RBPhysicsSystem
    constructor: (paramdict = {}) ->
        @gravity = 260#9.8
        @viscosity = 0.1
        @default_drag = 0.1
        @maxVelocity = 100 #pixels per second?
        #Override with paramdict
        for prop, val in paramdict
            @[prop] = val

class RBPhysics extends RBComponent
    constructor: (name="physics", systemname="default") ->
        super(name)
        @parent = null
        @physics = null
        @.set_system(systemname)
        
        @velocity = new RBPoint()
        @maxVelocity = new RBPoint()
        
        @acceleration = new RBPoint()
        @angularAcceleration = 0
        @angularVelocity = 0
        @maxAngularVelocity = 0
        
        @drag = new RBPoint(@physics.default_drag, @physics.default_drag)
        @angularDrag = 0
        
        @solid = true #collidable
        @staticphysics = false #true=can never move
        @floating = false #true=can move, not affeced by grav

    global_init: () ->
        super()
        systems = RB().env("RBPhysicsSystems")
        if not systems?
            systems = {}
            RB().env("RBPhysicsSystems", systems)
        systems["default"] = new RBPhysicsSystem()
        console.log("global_init done for RBPhysics")

    set_system: (systemname) ->
        systems = RB().env("RBPhysicsSystems")
        if systems[systemname]?
            @physics = systems[systemname]
        else
            @physics = systems["default"]

    update: (dt) ->
        super(dt)
        if @_active
            #console.log("update phys")
            if not @staticphysics
                norm_dt = dt/1000
                if not @floating
                    @velocity.y += @physics.gravity * norm_dt
                
                dx = @velocity.x * norm_dt
                dy = @velocity.y * norm_dt
                #TODO maxvelocity
                @parent.parentOffset.x += dx
                @parent.parentOffset.y += dy
                @parent.move_by(dx, dy) #also does a refresh
                @parent.dirty = true
                #TODO: dirty = true
                
