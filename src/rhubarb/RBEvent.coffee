#A class for recording callback functions and 
#properties relating to how they should be 
#called
class RBEventCallback extends RBObject
    constructor: (callback, consume) ->
        super(0, 0, 0, 0)
        @callback = callback
        @consume = consume
    fire: (ev) ->
        @callback(ev)
    will_consume: () ->
        return @consume

class RBDelegateGroup extends RBGroup
    call_delegates: (arglist) ->
        for cb in @members
            cb.fire.apply(cb, arglist)

    find_callback: (value, obj) ->
        if (obj.callback? and obj.callback == value)
            return true
        else
            return false

    add: (callback, consume=false) ->
        if not @.find(callback, @.find_callback)
            @.add_member(new RBEventCallback(callback, consume))
    remove: (callback) ->
        cb = @.find(callback, @.find_callback)
        if cb
            @.remove_member(cb)

class RBDelegateCollection extends RBGroup
    constructor: (name="DelegateCollection", groupnames) ->
        super(name, 0,0,0,0)
        @name = name
        @enabled = true
        @delegates = {}
        for eventname in groupnames
            @.add_event_type(eventname)

    destroy: () ->
        #there is no super destroy
        console.log("destroying RBDelegateCollection #{@name}")
        for evname, group in @delegates
            group.empty()
            delete @delegates[evname]

    enable: () ->
        @enabled = true
    disable: () ->
        @enabled = false

    add_event_type: (eventname) ->
        if not @delegates[eventname]
            @delegates[eventname] = new RBDelegateGroup(eventname)

    
    add_delegate: (eventname, callback, consume = false) ->
        dgroup = @delegates[eventname]
        if dgroup?
            dgroup.add(callback, consume)
    
    add: (eventname, callback, consume) ->
        @.add_delegate(eventname, callback, consume)

    remove_delegate: (eventname, callback) ->
        dgroup = @delegates[eventname]
        if dgroup?
            dgroup.remove(callback)

    remove:(eventname, callback) ->
        @.remove_delegate(eventname, callback)

    call_delegates: (eventname, arglist) ->
        if @enabled
            dgroup = @delegates[eventname]
            if dgroup?
                dgroup.call_delegates(arglist)
