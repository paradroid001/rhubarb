class RBComponent
    globalInitDone = false
    constructor:(@name) ->
        if !globalInitDone
            console.log("Component inited: " + @name)
            @.global_init()
            globalInitDone = true

        @_active = true #false switches off update
    is_global_inited:() ->
        return globalInitDone
    global_deinit:() ->
        globalInitDone = false
    global_init:() ->
        #override this
    deactivate: () ->
        @_active = false
    activate: () ->
        @_active = true
    is_active: () ->
        return @_active
    update: (dt) ->
        if @_active
            a = 1
    on_add: (parentEnt) ->
        @parent = parentEnt
    on_remove: () ->
        @deactivate()
        @parent = null
