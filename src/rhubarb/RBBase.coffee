# A class representing a point in 2D space.
# It has an x and a y property.
class RBPoint
    # @property [Number] The x position
    x: 0
    # @property [Number] The y position
    y: 0
    
    # Construct a new point
    # @param [Number] x The x position of the point
    # @param [Number] y The y position of the point
    constructor: (@x=0, @y=0) ->

    # @private
    uispec: () ->
        ret = {}
        ret['X'] = ['x', 'x', 'number']
        ret['Y'] = ['y', 'y', 'number']
        return ret

    # Add another point's x/y to this point
    add: (point) ->
        @x += point.x
        @y += point.y
        return @

    # Subtract another point's x/y from this point
    sub: (point) ->
        @x -= point.x
        @y -= point.y
        return @
    
    # Multiply x and y by a constant
    mult: (n) ->
        @x *= n
        @y *= n
        return @

    # Set this point's x and y to another point's x and y
    set: (point) ->
        @x = point.x
        @y = point.y
        return @

    # Returns the distance to another point
    dist: (point) ->
        Math.sqrt(this.distsq(point))
    
    # Returns the square of the distance to another point
    distsq: (point) ->
        a = @x - point.x
        b = @y - point.y
        (a*a) + (b*b)
    
    # Finds the angle between this point and another, from
    # the 'up' Y axis (treating this as 0 degrees).
    angle_to: (point) ->
        dx = point.x - @.x
        dy = point.y - @.y
        return Math.atan2(dy,dx) * 180 / Math.PI

    # Sets this point to have x and y values
    # at a 'magnitude' distance and angle in degrees.
    cartesian: (angle, magnitude) ->
        angle = angle % 360
        if angle < 0
            angle = 360 + angle
        rads = angle * Math.PI / 180.0
        @x = Math.sin(rads) * magnitude
        @y = -Math.cos(rads) * magnitude


# Represents a rectangle, with x, y, width, and height.
# Also has top, bottom, left, and right.
# Rects have an anchor point which is where their x and y 
# position is considered to 'pin' them.
# 
class RBRect extends RBPoint
    # @property [Number] The object width
    width: 1
    # @property [Number] The object height
    height: 1
    # @property [Number] The top 'y' value of the object
    top: 0
    # @property [Number] The bottom 'y' value of the object
    bottom: 0
    # @property [Number] The left 'x' value of the object
    left: 0
    # @property [Number] The right 'x' value of the object
    right: 0


    constructor: (X=0, Y=0, @width=1, @height=1) ->
        super(X, Y)
        @_XYtoTL = new RBPoint() #used for storage of current offset from anchor to top left.
        @_anchor = new RBPoint()
        @_scale = new RBPoint(1.0, 1.0)
        @.set_scale(1.0, 1.0)
        @.set_anchor()
        @_colour = new RBColour([255,255,255,255])
        @.refresh()

    clone_rect: () ->
        r = new Rect(@x, @y, @width, @height)
        r.set_anchor(@_anchor.x, @_anchor.y)
        r.set_scale(@_scale.x, @_scale.y)
        return r

    # @private
    get_screen_XY: () ->
        p = new RBPoint(@x, @y)
        p.sub(@_RB._screenRect)
        return p
        #return new RBPoint(@x, @y) #TODO obviously wrong

    get_abs_rect: () ->
        return @

    # @private
    uispec: () ->
        ret = super()
        ret['Anchor'] = ['_anchor', 'set_anchor', RBPoint]
        ret['Scale'] = ['_scale', 'set_scale', RBPoint]
        ret['Colour'] = ['_colour', 'set_colour', RBColour]
        ret['Width'] = ['width', null, 'number']
        ret['Height'] = ['height', null, 'number']
        ret['Top'] = ['top', null, 'number']
        ret['Bottom'] = ['bottom', null, 'number']
        ret['Left'] = ['left', null, 'number']
        ret['Right'] = ['right', null, 'number']
        return ret



    # set the anchor point (origin) for this object
    set_anchor: (a_x = 0.5, a_y = 0.5) ->
        @_anchor.x = a_x
        @_anchor.y = a_y
        @.refresh()

    # Set the scale of this object
    set_scale:(scalex, scaley) ->
        @_scale.x = scalex
        @_scale.y = scaley
        #@height = @unscaled_height# * @_scale.y
        #@width = @unscaled_width# * @_scale.x
        #@.refresh()
    get_scale: () ->
        @_scale
        
    # Set the colour of this object - r, g, b, optionally a. (0-255)
    set_colour:(r=0, g=0, b=0, a=255) -> #colour = [0,0,0,255]) ->
        @_colour.set_rgba([r, g, b, a])

    # Set the alpha / opacity of this object (0-255)
    set_alpha: (a=255) ->
        @_colour.set_alpha(a)

    # Set the width of this object
    set_width: (@width) ->
        @.refresh()
    # Set the height of this object
    set_height: (@height) ->
        @.refresh()

    # @private
    refresh: () ->
        @top = @y - (@height * @_anchor.y)
        @left = @x - (@width * @_anchor.x)
        @bottom = @top + @height #@y + @height * (1.0-@_anchor.y)
        @right = @left + @width #@x + @width * (1.0-@_anchor.x)
        @_XYtoTL = @.xy_to_topleft(@_XYtoTL)
        @


    # Move this object so its bottom border aligns with passed value
    adjust_bottom: (newbottom) ->
        @y = newbottom - (@bottom-@y)
        @.refresh()
    # Move this object so its top border aligns with passed value
    adjust_top: (newtop) ->
        @y = newtop + (@y-@top)
        @.refresh()
    # Move this object so its left border aligns with passed value
    adjust_left :(newleft) ->
        @x = newleft + (@x-@left)
        @.refresh()
    # Move this object so its right border aligns with passed value
    adjust_right: (newright) ->
        @x = newright - (@right-@x)
        @.refresh()

    # Set position to newx, newy
    set_position: (newx, newy) ->
        @x = newx
        @y = newy
        @.refresh()
    # Move by dx, dy
    move_by: (dx, dy) ->
        @x +=dx
        @y +=dy
        @.refresh()

    # an important function to work out how far our xy pos is from the top left
    # useful for canvas rendering, since it uses top left
    # Pass in a point to modify it, otherwise a new point is generated.
    xy_to_topleft: (p) ->
        ret = p
        if not ret?
            ret = new RBPoint(0,0)
        ret.x = @left - @x
        ret.y = @top - @y
        return ret
    
    # find the distance to the middle of the object. Useful if you want to
    # rotate an object by its middle, or flip the canvas
    xy_to_middle: () ->
        return new RBPoint((@right-@left)/2 - (@x-@left), (@bottom-@top)/2 - (@y-@top) )

    # Recursively check a list of entities and their children
    # for the first overlapping entity.
    # @param [Array<RBEntity>] rlist
    #   An array of RBEntities.
    # @param [RBRect] crect
    #   Optional rect structure to populate with collision boundaries.
    overlaps_in_rlist: (rlist, crect) ->
        retval = null
        myrect = @ #@.get_abs_rect()
        for obj in rlist
            #if myrect.overlaps(obj.get_abs_rect(), crect)
            if myrect.overlaps(obj, crect)
                retval = obj
            else if obj.members?
                retval = @.overlaps_in_rlist(obj.members, crect)
            if retval != null
                return retval
        return null

    # Returns true when overlaps.
    # Optionally pass in a collision rect,
    # Which is populated if the rects collide
    # @param [RBRect] rect
    #   The rect to check for collisions with
    # @param [RBRect] crect
    #   An optional rect to populate with the boundaries
    #   of the collision.
    overlaps: (rect, crect) ->
        #track the outside boundaries
        leftmost = rightmost = topmost = bottommost = null
        #track the inside boundaries
        nleftmost = nrightmost = ntopmost = nbottommost = null
        
        #rect = null
        #If this is true, 
        #if RB().collisionFlagAbsolute
        #    rect = orect.clone_rect()
        #    p = rect.get_screen_XY()
        #    rect.set_position(p.x, p.y)
        #else
        #    rect = orect

        if @left <= rect.left
            leftmost = @
            nleftmost = rect
        else
            leftmost = rect
            nleftmost = @

        if @right >= rect.right
            rightmost = @
            nrightmost = rect
        else
            rightmost = rect
            nrightmost = @

        if @top <= rect.top
            topmost = @
            ntopmost = rect
        else
            topmost = rect
            ntopmost = @
        
        if @bottom >= rect.bottom
            bottommost = @
            nbottommost = rect
        else
            bottommost = rect
            nbottommost= @

        #calculate a collision rect which is a function of ourself.
        #So if we were entirely within the colliding rect, our
        #collision rect would have l, r, t, b equal to our own.
        if (Math.abs(rightmost.right - leftmost.left) < (leftmost.width + rightmost.width)) and (Math.abs(bottommost.bottom - topmost.top) < (bottommost.height + topmost.height) )
            if crect?
                #console.log(nleftmost)
                #console.log(nrightmost)
                crect.x = nleftmost.left - @.left
                crect.y = ntopmost.top - @.top
                crect.width = nrightmost.right - nleftmost.left
                crect.height = nbottommost.bottom - ntopmost.top
                #console.log("#{crect.x}, #{crect.y}, #{crect.width}, #{crect.height}")
                crect.set_anchor(0, 0)
            return true
        else
            return false

    # Draw the border of this rectangle, in the 'current colour'
    draw:() ->
        @_RB.cctx.strokeRect(@left, @top, @width, @height)
        #@_RB.cctx.strokeRect(0, 0, @width, @height)

#
# RBObject

class RBObject extends RBRect
    # @property The id of the object
    id: -1
    constructor:(X, Y, Width, Height) ->
        super(X, Y, Width, Height)
        @_RB = RB()
        @id = @_RB.newID()

