class YourGameOverScene extends RBScene
    constructor: (score) ->
        super("YourGame Game Over Scene")
        @input.add(RB_KEY_UP, @.on_key_up)
        @gameovertext = new RBText(100, 100, "Game Over! (Press Space to continue)")
        @scoretext = new RBText(100, 120, "Your score was: #{score}")
        @stage.add_child(@gameovertext)
        @stage.add_child(@scoretext)

    on_key_up: (event) =>
        code = event.keyCode
        keys = @_RB.input.keys
        if code is keys.space
            @.pop()

    draw: () ->
        super()
        @stage.render()

class YourGameScene extends RBScene
    constructor: () ->
        super("Your Game Main Game Scene")
        @input.add(RB_KEY_UP, @.on_key_up)
        
        #Two example (empty) layers
        @enemiesgroup = new RBLayer()
        @enemiesgroup.name = "Enemies"
        @playergroup = new RBLayer()
        @playergroup.name = "Player"
        #Some text to display
        @gametext = new RBText(100, 200, "Empty Game. Press ESC to 'die'")
        @stage.add_child(@gametext)
        @stage.add_child(@enemiesgroup)
        @stage.add_child(@playergroup)

        @score = 0

    on_key_up: (event) =>
        code = event.keyCode
        keys = @_RB.input.keys
        switch code
            when keys.esc
                @.pop()
    
    update: (dt) ->
        super(dt)
        #An example of colliding the player and enemies layers
        @_RB.collide(@playergroup.children, @enemiesgroup.children)
    
    draw: () ->
        super()
        @stage.render()

class YourGameTitleScene extends RBPreLoaderScene
    constructor: () ->
        super("YourGame Title Scene")
        @input.add(RB_KEY_UP, @.on_key_up)
        @loading_text = new RBText(100, 100, "")
        @continue_text = new RBText(100, 130, "")
        @stage.add_child(@loading_text)
        @stage.add_child(@continue_text)
    
    on_preloaded: () ->
        console.log("Preloaded")
        @continue_text.set_text("Press Space to continue")

    on_key_up: (even) =>
        code = event.keyCode
        keys = @_RB.input.keys
        if code is keys.space
            @.pop()
    update: (dt) ->
        super()
        @loading_text.set_text("Loading...[#{@.pl.stats.total}]")

class YourGame extends RBGame
    constructor: () ->
        super()
        @titlescene = @gamescene = @gameoverscene = null
        @_RB.events.add(RB_EVENT_ON_START, @.show_title)

    show_title:() =>
        if @titlescene is null
            @_RB.events.remove(RB_EVENT_ON_START, @.show_title)
            @titlescene = new YourGameTitleScene()
            @titlescene.events.add(RB_EVENT_ON_SCENE_EXIT, @.start_game)
        @titlescene.push()

    start_game: () =>
        if @gamescene isnt null
            @gamescene.destroy()
        @gamescene = new YourGameScene()
        @gamescene.events.add(RB_EVENT_ON_SCENE_EXIT, @.game_over)
        @gamescene.push()

    game_over: () =>
        if @gameoverscene is null
            @gameoverscene = new YourGameOverScene(@gamescene.score)
            @gameoverscene.events.add(RB_EVENT_ON_SCENE_EXIT, @.show_title)
        @gameoverscene.push()

_game = null

run_yourgame = () ->
    #Engine Settings (RBV_ENG)
    RBV(RBV_ENG, {
        fps: 30
        screenwidth: 400
        screenheight: 400
        canvaswidth: 400
        canvasheight: 400
        canvasid: "canvas"
        canvassmoothing: false
    })
    #Image Settings (RBV_IMG)
    RBV(RBV_IMG, {
        shipsimage: "data/vshooter/ships.png"
        bulletsimage: "data/vshooter/bullets.png"
    })
    #Sound Settings (RBV_SND)
    RBV(RBV_SND, {
        laser: "data/sfx.wav"
    })
    #Sprite Settings (RBV_SS)
    RBV(RBV_SS, {
        bullets: ["bullets", RBV(RBV_IMG).bulletsimage, 4, 4]
        ship: ["ships", RBV(RBV_IMG).shipsimage, 5, 5]
    })
    #Game Settings (RBV_GAME)
    RBV(RBV_GAME, {
        playerwidth: 35
        playerheight: 40
        enemywidth: 35
        enemyheight: 40
        bulletwidth: 8
        bulletheight: 8
    })
    _game = new YourGame()
    RB().start()
