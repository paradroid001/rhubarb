#!/bin/bash
#The object of this file is to output
#the filelist as a string when run.
#Keep the file list in the order
#that is needed for compilation
readarray -t fileslist_arr

IFS=$'\n' read -r -d '' fileslist_arr && (( ${#fileslist_arr[@]} ))
function join { local IFS="$1"; shift; echo "$*"; }

#echo $fileslist
echo join " " ${fileslist_arr[@]}
