# RBGame is the class to use to create your main game object.
# It takes the settings you pass in, and stores it in RB().env("settings")
# Your settings should have the following sub-objects
# @example
#    .engine
#      fps = [Number]
#      screensizex = your game screen width
#      screensizey = your game screen height
#      canvaswidth = your canvas screen width
#      canvasheight = your canvas sccren height
#      canvasid = the HTML id of the canvas object to use
#      canvassmoothing = [Boolean] whether or not to use image interpolation.
#
#    .images
#      imagename : "path/to/image"
#      imagename2: "path/to/image"
#
#    .sounds
#     soundname : "path/to/sound"
#
#    .spritesheets
#      ssname : [name, image, xsprites, ysprites]
#      ss2name : [name2, image2, xsprites, ysprites]
#
#    .game
#      miscvar1 : value
#      miscvar2 : value
#
# Any RBPreloader scene which you create will interrogate this structure
# and preload your images, sounds, and spritesheets for you.
#
# An RBGame will automatically run and draw the current {RBScene}.
# So any scene that you create and push() onto the scene stack will be 
# the currently executing scene. You can use the RB_EVENT_ON_SCENE_ENTER
# and RB_EVENT_ON_SCENE_EXIT events to control logic flow in your game.
# Simply add an event delegate to the a scene, and specify the function 
# you would like to be called when that scene exits.
#
# RBGame objects have automatic pausing when pressing p.
# They also have the TAB key bound to show debug statistics.
#
class RBGame
    constructor: (@settings, @canvas) ->
        @_RB = RB()
        @currentScene = null
        @stats = new RBStats()
        @screen

        if @settings? #copy passed settings in, preserving defaults
            for key, val of @settings
                RBV(key, val)
        @settings = RBV()
        
        if not @canvas?
            @canvas = document.getElementById(@settings.engine.canvasid)
        RB().set_screen(@canvas, @settings.engine.canvaswidth, @settings.engine.canvasheight, @settings.engine.screenwidth, @settings.engine.screenheight, @settings.engine.canvassmoothing)
        #RB().set_smoothing(@settings.engine.canvassmoothing)
        
        #RB().env("settings", @settings)
        RB().env("__game", @)
        @_canvasRect = @_RB._canvasRect #so we don't need to call each frame
        
        @scenemanager = RBSceneManager.get_instance()
        @_showstats = false
        @_paused = false
        @_dt = 0
        @.bind_debug_keys()
        #console.log(@settings)
        #console.log("RBGame: Resetting frame rate to #{@settings.engine.fps}")
        @_RB.reset_frame_rate(@settings.engine.fps, @.runloop)
        @_RB.stop(false)
    
    bind_debug_keys: () =>
        @_RB.input.bind(RB_KEY_UP, @.debug_keys)
    unbind_debug_keys: () =>
        @_RB.input.unbind(RB_KEY_UP, @.debug_keys)

    pre_update: (dt) ->
        #reset the update timer
        @stats.startupdate() if @_showstats

        
    update: (dt) ->
        #update everything in the current scene
        @currentscene = @scenemanager.current_scene()
        if @currentscene != null
            @currentscene.update(dt)


    post_update: (dt) ->
        #update and truncate the update time.
        @stats.endupdate() if @_showstats

    pre_render: () ->
        #reset the draw timer
        @stats.startdraw() if @_showstats

        #scale - if the screen size is different to canvas size
        @_RB.cctx.scale(RBV(RBV_ENG).screenscalex, (RBV(RBV_ENG).screenscaley))

    render: () ->
        #draw everything in the current scene
        if @currentscene != null
            @currentscene.draw()


    post_render: () ->
        if @_showstats
            #update and truncate the draw time
            @stats.enddraw()
            @.stats.draw()
            #This loop is run every frame.

    debug_keys: (event) =>
        keycodes = @_RB.input.keys
        code = event.keyCode
        if code is keycodes.p #detect pause
            @_RB.toggle_pause()
            @_RB.input.swallow(event)
        if code is keycodes.numlock #detect debug
            @_RB.setDebug(not @_RB.isDebug() )
            @_showstats = @_RB.isDebug()
            @_RB.input.swallow(event)
        if code is keycodes.tab and @_RB.isDebug()
            @_RB.setDebug(not @_RB.isDebug("bb"), "bb")
            @_RB.input.swallow(event)
    
    #If the current FPS is set to be 30,
    #then this loop is being called every 33ms.
    runloop: () =>
        @_RB.cctx.clearRect(0, 0, @_canvasRect.width, @_canvasRect.height)
        @_dt = @_RB.dt()
        @.pre_update(@_dt)
        @.update(@_dt)
        @.post_update(@_dt)
        @_RB.cctx.save()
        @.pre_render()
        @.render()
        @.post_render()
        @_RB.cctx.restore()
        @_RB.destroy_entities()
        @_RB.actions.update(@_dt)
