//Function to pull out named get params
var gup =function( name )
{
    name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
    var regexS = "[\\?&]"+name+"=([^&#]*)";
    var regex = new RegExp( regexS );
    var results = regex.exec( window.location.href );
    if( results === null )
    {    
        return 0;
    }
    else
    {
        return results[1];
    }
};

var showcontent = function(idname){
    $("#bugs").hide();
    $("#todo").hide();
    $("#features").hide();
    $("#links").hide(); 
    if (idname !== null){
        $("#" + idname).show();
    }
};

var debug = true;

//THis function requires jquery
var initlogging = function(useconsole){
    logging = {};
    cons = true;
    LOG=1;
    DEBUG=2;
    WARN=3;
    ERROR=4;

    logging.linenum = 0;
    logging.outer = $('<div style="display:block; position: absolute; width:100%; bottom:0px;"></div>')
    logging.header = $('<div style="border-top:1px solid #6f6f6f; border-bottom: 1px solid #6f6f6f;  background-color: #afafaf; -moz-box-shadow:0 1px 3px rgba(0,0,0,0.5); -webkit-box-shadow: 0 1px 3px rgba(0,0,0,0.5); text-shadow: 0 -1px 1px rgba(0,0,0,0.25);"><div style="color: white; padding-left:7px; font-weight:bold;">LOGGING</div></div>');
    logging.le = $('<div style="display:block; background-color: #efefef; height:100px; overflow:scroll; padding-left:7px; padding-top:3px;"></div>');
    logging.header.click(function(e){
        if (logging.le.is(":hidden")){
            logging.le.show();
        }
        else{
            logging.le.hide();
        }
    });
    logging.outer.append(logging.header);
    logging.outer.append(logging.le);
    $("body").append(logging.outer); 

    if (typeof(console) === 'undefined'){
        cons = false;
    }

    logging.log = function(message){
        logging.out(message, LOG);
    };

    logging.debug = function(message){
        logging.out(message, DEBUG);
    };

    logging.warn = function(message){
        logging.out(message, WARN);
    };

    logging.error = function(message){
        logging.out(message, ERROR);
    };

    logging.out = function(message, msgtype){
        if (debug){
            if (cons && useconsole){
                switch(msgtype){
                    case LOG:
                        console.log(message);
                        break;
                    case WARN:
                        console.warn(message);
                        break;
                    case DEBUG:
                        console.debug(message);
                        break;
                    case ERROR:
                        console.error(message);
                        break;
                }
            }
            else{
                logging.le.append(logging.linenum++ + ": " + message+'<br />');        
            }
        }
    };

    return logging;
};

var initpage = function(){

    //console = initlogging(false);
    
    /*
    console.log("inited logging");
    console.warn("trying a warning");
    console.debug("trying a debug");
    console.error("trying an error");
    console.log("inited logging");
    console.warn("trying a warning");
    console.debug("trying a debug");
    console.error("trying an error");
    console.log("inited logging");
    console.warn("trying a warning");
    console.debug("trying a debug");
    console.error("trying an error");
    console.log("inited logging");
    console.warn("trying a warning");
    console.debug("trying a debug");
    console.error("trying an error");
    */
    showcontent(null);
    //The topbar dropdown code
    //Makes the topbar nav work
    $("body").bind("click", function (e) {
        $('a.menu').parent("li").removeClass("open");
      });

      $("a.menu").click(function (e) {
        var $li = $(this).parent("li").toggleClass('open');
        return false;
      });

      // Disable certain links in docs
  // =============================

  $('ul.tabs a, ul.pills a, .pagination a, .well .btn, .actions .btn, .alert-message .btn, a.close').click(function(e) {
    e.preventDefault();
  });

  // Copy code blocks in docs
  $(".copy-code").focus(function() {
    var el = this;
    // push select to event loop for chrome :{o
    setTimeout(function () { $(el).select(); }, 1);
  });



    //Detect page params, and run tests
    var numdudes, numdots;
     //Dots and Dudes
     numdudes = gup("numdudes");
     numdots = gup("numdots") ;
     if ((numdudes>0) || (numdots>0)){
        initApp(numdots,numdudes);
     }
     //Test rects
     var testrects= gup("testrects");
     if (testrects > 0){
        testRects();
     }

     //Test collisions
     var testcollisions = gup("testcollisions");
     if (testcollisions > 0){
        testCollisions(testcollisions);
     }
     //Test collisions
     var testcollisions2 = gup("test2collisions");
     if (testcollisions2 > 0){
        testCollisions2(testcollisions);
     }


     //Test parenting
     var testparenting = gup("testparenting");
     if (testparenting > 0){
        testParenting(testparenting);
     }
     //Test cam
     var testcamera = gup("testcamera");
     if (testcamera > 0){
        testCamera(testcamera);
     }
     //Test actions
     var testactions = gup("testactions");
     if (testactions > 0){
        testActions(testactions);
     }
     //Test actions
     var testtext = gup("testtext");
     if (testtext > 0){
        testText(testtext);
     }
     //Test Sound
     var testsound = gup("testsound");
     if (testsound > 0)
     {
        testSound(testsound);
     }
    var testui = gup("testui");
     if (testui > 0){
        testUI(testui);
     }

     //Test protocol
     var testprot = gup("testprot");
     if (testprot > 0){
        testProtocol(testprot);
     }
     var testnetclient = gup("testnetclient");
     if (testnetclient > 0){
        console.log("testing net client");
        testNetClient(testnetclient);
     }
     var testrequests = gup("testreq");
     if (testrequests > 0){
        console.log("testing requests");
        testHTTPRequests(testrequests);
     }

     //Test biolab-clone
     var biolab = gup("biolab");
     if (biolab > 0){
        initBioLabClone(biolab);
     }
     //Test platformer template
     var platformer = gup("platformer");
     if (platformer > 0){
        run_platformer();
     }
     //Test kartracer template
     var kartracer = gup("kartracer");
     if (kartracer > 0){
        run_kartracer();
     }
     //Test vshooter template
     var vshooter = gup("vshooter");
     if (vshooter > 0){
        run_vshooter();
     }
     //Test template
     var yourgame = gup("yourgame");
     if (yourgame > 0){
        run_yourgame();
     }
     //Test runner
     var runner = gup("runner");
     if (runner > 0){
        run_runner();
     }



};
