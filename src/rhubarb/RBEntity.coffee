# The Basic "Entity" in RB games. RBEntity is the base type from which
# most useful things in Rhubarb come.
#
# Having inherited from {RBPoint} and {RBRect}, they have a box-like structure (x, y, width, height).
# They also have the ability to be collided with other entities, and can
# have children attached, who will gain scale, postion, and alpha value traits from
# their parent.
# 
# RBEntities (and their subclassed variations, notably RBSprite) can have actions 
# run on them to transform them over time - see {RBAction}.
#
# When collisions happen RBEntities have four functions which get called by default:
# hit_top, hit_bottom, hit_right, and hit_left - each of these is passed the 
# colliding object as the first argument.
#
# The main two methods of RBEntity which are useful are update(dt) and draw().
# Both of these methods will operate on all their children also, automatically. So
# adding children to an RBEntity object effectly groups all these objects, and
# only the root object update/draw needs to be called in order to draw all these
# entities.
#
# Note that by default, an RBEntity draw call draws nothing for itself, and then
# draws all it's children - an RBEntity is a 'blank' object and doesn't have anything
# inherently drawable about it.
class RBEntity extends RBGroup
    # @private
    # @property [Number] 
    #   Is the entity rotation, in radians. The accessors {#get_angle}
    #   and {#set_angle} allow you to manipulate this property *in degrees*.
    angle: 0
    
    # @property [Boolean] 
    #   Sets whether or not this object is collideable.
    #   If **true**, this entity will be considered when detecting collisions, 
    #   otherwise it will be ignored.
    # Default: true
    solid: true

    # @property [Boolean] 
    #   Sets whether or not this object will be drawn.
    #
    #   Default: true
    visible: true

    # @property [Boolean] 
    #   Sets whether this entity gets updated or not. Setting this to 
    #   false effectively disables this entity's update loop.
    #
    #   Default: true
    active: true

    # @property [RBEntity] 
    #   The parent of this object (if any). Do not set this variable,
    #   only read from it.
    parent: null

    # @property [Boolean] 
    #   Whether or not this entity's position should be considered as
    #   being parent relative, or not. Setting this to false effectively 'detatches' this 
    #   entity from it's parent position. It is still a child, but it won't move with the 
    #   parent.
    #
    #   Default: false, until the entity becomes parented to an object - then it is true.
    parentRelative: false

    # Always marked 'on screen', so never culled from draws.
    alwaysOnScreen: false

    # @property [Array<RBEntity>] A list of the children of this entity. You should use
    #   add_child and remove_child to manipulate this list.
    children: []

    constructor: (X, Y, Width, Height, name="NoName") ->
        super(name, X, Y, Width, Height)
        @_RB._entityCount+=1
        #rotation
        @angle = 0 #angle is in radians. We expose in degrees
        @debugColour = new RBColour(RB_GREY)
        
        @renderMode = RB_RENDER_MODE_NORMAL
        
        @tags = []
        @_lastposition = new RBPoint() #we use this to help with collisions
        @_collisionState = 0 #number representing colstate.
        #flags
        @solid = true #collidable
        @visible = true #drawn or not
        @active = true #false switches off update
        @dirty = true #if true, draw() is a noop
        @destroyed = false #if true, this entity is ready to be destroyed
        #grouping
        @parent = null
        @parentRelative = false
        @parentOffset = new RBPoint(0,0)
        #aliasing
        @children = @members
        @components = {}

    # This method is for use with RBEditor
    to_list: () ->
        return [@x, @y]

    hide: () ->
        @.visible = false
    show: () ->
        @.visible = true

    # @private
    is_on_screen: () ->
        if @alwaysOnScreen
            return true
        if @_RB._screenRect.overlaps(@)
            return true
        return false

    get_abs_rect: () ->
        rect = new RBRect(@x, @y, @width, @height)
        child = @
        parent = @parent
        while parent isnt null
            if child.parentRelative
                rect.add(parent)
            parent = parent.parent
            child = parent
        return rect

    # Sets the angle of this entity, in degrees.
    #
    # @param angle [Number] The angle, in degrees.
    set_angle: (num) ->
        @.angle = (num * 2 * Math.PI) / 360

    # Gets the angle of this entity, in degrees.
    get_angle: () ->
        return 360*@.angle / (2*Math.PI)

    # Adds a tag to this object - a tag can be anything,
    # but strings mostly make sense, or possibly integers.
    #
    # @param [String] tag The tag to add
    #
    # @example
    #   ent.add_tag("wood")
    #   ent.add_tag("interactable")
    #   The entity now has 'wood' and 'interactable' as tags.
    #   This may be useful in later code that checks for these
    #   tags
    add_tag: (tag) ->
        if tag not in @tags
            @tags.push(tag)
    # Remove a tag from an object
    #
    # @param [String] tag The tag to remove
    remove_tag:(tag) ->
        i = @tags.indexOf(tag)
        if i > -1
            @tags.splice(i, 1)
    # Check to see if this entity has a particular tag
    # @param [String] tag The tag to check for.
    has_tag:(tag) ->
        if tag in @tags
            return true
        return false

    # Add a child to this entity. The child's position will become
    # relative to this entity, and will move with this entity.
    # Scale and opacity settings will affect children also.
    # Children will automatically have their update and draw
    # methods called.
    #
    # @param [RBEntity] rbentity The entity to add as a child.
    add_child: (rbentity) ->
        @.add_member(rbentity)
        #This looks strange because rbentity will have a position like
        #(10,10) or something, which was the posiiton it was given on
        #creation, but should be considered an offset.
        rbentity.parentRelative = true
        rbentity.parentOffset.x = @x - rbentity.x
        rbentity.parentOffset.y = @y - rbentity.y
        #console.log("Parentoffset ("+rbentity.name+"): " + rbentity.parentOffset.x + "," + rbentity.parentOffset.y)
        rbentity.parent = @

    # Remove a child from an entity.
    #
    # @param [RBEntity] The entity to remove
    remove_child: (rbentity) ->
        #console.log("Parent is removing child id:#{rbentity.id}")
        @.remove_member(rbentity)
        rbentity.parent = null
        rbentity.parentRelative = false
        rbentity.x -= (@x + rbentity.parentOffset.x)
        rbentity.y -= (@y + rbentity.parentOffset.y)

    # Add a component to an entity
    #
    # @param [RBComponent] component The component to add
    add_component: (component) ->
        @components[component.name] = component
        component.on_add(@)

    # Remove a component from an entity
    #
    # @param [RBComponent] component The component to remove
    remove_component: (component) ->
        console.log("implement remove component...")
        
    # Remove a component from an entity, by name.
    #
    # @param [String] name The name of the component to remove
    remove_component_by_name: (name) ->
        if @components[name]?
            @components[name].deactivate()
            @components[name].on_remove()
            delete @components[name]

    # Get a component by name
    #
    # @param [String] name The name of the component to get.
    get_component: (name) ->
        return @components[name]

    # Stop the given list of actions for this entity
    #
    # @param [Array<RBAction>] actionlist The list of actions 
    #   to stop. Not passing a list will stop all actions
    stop_actions: (actionlist) ->
        @_RB.actions.stop_for_entity(@, actionlist)
    
    # Start the given list of actions for this entity
    #
    # @param [Array<RBAction>] actionlist The list of actions 
    #   to start. Not passing a list will start all actions
    start_actions: (actionlist) ->
        @_RB.actions.start_for_entity(@, actionlist)

    # Force a finish of the given list of actions for this entity
    #
    # @param [Array<RBAction>] actionlist The list of actions 
    #   to finish. Not passing a list will finish all actions
    finish_actions: (actionlist) ->
        @_RB.actions.finish_for_entity(@, actionlist)

    # Force a cancel of the given list of actions for this entity
    #
    # @param [Array<RBAction>] actionlist The list of actions 
    #   to finish. Not passing a list will cancel all actions
    cancel_actions: (actionlist) ->
        @_RB.actions.cancel_for_entity(@, actionlist)


    # Called by Rhubarb's collide method when this entity 
    # collides with another entity along its bottom border.
    #
    # @param [RBEntity] robject The entity that was collided with.
    #
    # @see {RBInit.collide}
    hit_bottom: (robject, collider_velocity) ->
        @_collisionState |= RB_COLLIDE_BOTTOM
        return null #empty! override me!
    
    # Called by Rhubarb's collide method when this entity 
    # collides with another entity along its top border.
    #
    # @param [RBEntity] robject The entity that was collided with.
    #
    # @see {RBInit#collide}
    hit_top: (robject, collider_velocity) ->
        @_collisionState |= RB_COLLIDE_TOP
        return null #empty! override me!
    
    # Called by Rhubarb's collide method when this entity 
    # collides with another entity along its right border.
    #
    # @param [RBEntity] robject The entity that was collided with.
    #
    # @see {RBInit#collide}
    hit_right: (robject, collider_velocity) ->
        @_collisionState |= RB_COLLIDE_RIGHT
        return null #empty! override me!
    
    # Called by Rhubarb's collide method when this entity 
    # collides with another entity along its left border.
    #
    # @param [RBEntity] robject The entity that was collided with.
    #
    # @see {RBInit#collide}
    hit_left: (robject, collider_velocity) ->
        @_collisionState |= RB_COLLIDE_LEFT
        return null #empty! override me!
    
    # @private
    is_hit: (statecheck) ->
        if statecheck isnt RB_COLLIDE_NONE
            return ((@_collisionState & statecheck) == statecheck)
        else
            return @_collisionState == RB_COLLIDE_NONE
    # Update this entity, and all its children.
    # If {#active} is false, this entity (and children) 
    # will not be updated.
    # Updates happen according to dt - the time since the last
    # frame of the game engine was executed, in milliseconds.
    # @param [Number] dt
    #   The time (milliseonds) taken so far this frame.
    #
    update: (dt) ->
        @_collisionState &= RB_COLLIDE_NONE #reset collision state
        @_lastposition.set(@)
        if @active
            for name, component of @components
                component.update(dt)
            for child in @children
                if child?
                    child.update(dt)

    # @private
    pre_render: () -> # (xoffset=0, yoffset=0) ->
        #@_RB.cctx.translate(@x |0, @y |0)
        @_RB.cctx.translate(@x, @y)

        #rect at X Y
        #@_RB.cctx.strokeStyle = "0xff0000"
        #@_RB.cctx.strokeRect(0, 0, @width, @height)
        
        @_RB.cctx.rotate(@angle)
        @_RB.cctx.scale(@_scale.x, @_scale.y)
        #Make alpha propagate
        @_RB.cctx.globalAlpha *= clamp_num(@_colour.a/255, 0, 1)
        #Allow alpha blending based on renderMode
        @_RB.cctx.globalCompositeOperation = @.renderMode

    # @private
    post_render: () ->
        if @_RB.isDebug("bb")
            @_RB.cctx.strokeStyle = @debugColour.htmlrgb
            #@_RB.cctx.strokeRect(-@_anchor.x*@width, -@_anchor.y*@height, @width, @height)
            
            #@_RB.cctx.strokeRect(@left-@x, @top-@y, @width, @height)
            @_RB.cctx.strokeRect(0, 0, @width, @height)

    # Draw this entity and all its children
    draw : () -> # (xoffset, yoffset) ->
        #override this in descendents
        #drawing anything more basic than this, we need to translate
        #back to the top left of canvas.

    # Only called as the top layer 'draw' call of an
    # entity heirarchy. Most of the time, you will be calling
    # draw(), not render.
    render: () -> #(xoffset=0, yoffset=0) ->
        if !@visible
            return
        if @_RB._offscreenCulling
            if not @.is_on_screen()
                @_RB._culledEntityCount+=1
                return

        @.dirty = false
        @_RB.cctx.save()
        @.pre_render()
        
        @fillStyle = @_colour.htmlrgba
        #we are translated to the top left of the entity
        #for the canvas draw
        #translate back to the centre so that child draws
        #are properly position relative
        #@_RB.cctx.translate(@x-@left, @y-@top)
        @.draw()
        for child in @children
            if child?
                child.render() #xoffset, yoffset)
        
        #translate back for post render ops.
        #@_RB.cctx.translate((@left-@x)|0, (@top-@y)|0)
        @_RB.cctx.translate(@left-@x, @top-@y)
        @.post_render()
        @_RB.cctx.restore()

    # Clear the canvas where this RBEntity is.
    clear: () ->
        @_RB.cctx.clearRect(@x|0, @y|0, @width|0, @height|0)
        child.clear() for child in @children
        for child in @children
            if child?
                child.clear()

    # Destroy an RBEntity, removing it from its parents
    # removing all components from it, and removing all
    # its children from it also.
    destroy: () =>
        #console.log("Destroying Entity #{@name}")
        for child in @children
            if child?
                child.destroy()
        for componentname in @components
            @.remove_component_by_name(componentname)
        @destroyed = true
        @active = false
        @_RB._destroylist.push(@) 
        #console.log("destroylist now has #{@_RB._destroylist.length} members") 
        return @

    # This is the real method that does the destroying.
    # @private
    _destroy: () ->
        if @parent != null
            @parent.remove_child(@)
        @parent = null



