###
 A simple test:
 Assuming TestProtocolHandler is:

class TestProtocolHandler extends RBProtocolHandler
    constructor: () ->
        super(RBProtocol) #construct the super with RBProtocol as the prot
        @.add_function_mapping('hello', @.hello)

    hello:(arglist) ->
        console.log("TestProtocolHandler says Hello! Args: " + arglist)


 Then a test would be:
 var a = new TestProtocolHandler()
 var b = new RBProtocol().set_opcode('hello')
 a.deliver(b.to_JSON())
 a.update()

 This should print out the 'hello' output
###



#This is an auto-incrementing value for RBProtocol ID's. Don't mess with it.

_RB_PACKET_ID = 1
###
    @class RBProtocol
    This is simply a class to encapsulate a protocol.
    The only things is MUST have are an id, and a to/from json method.
    Requires json2.js
###

class RBProtocol
    constructor:(from=null, to=null, resp_id=null, opcode=null, args=[]) ->
        #the following could be done as functions. Lets just make it quicker.
        @definition = {'id':_RB_PACKET_ID++, 'ts':0, 'from':from, 'to':to, 'resp_id':resp_id, 'fn':opcode, 'args':args, 'resp':0}

    set_timestamp:(ts) ->
        @definition['ts'] = ts
    get_timestamp:()->
        return @definition['ts']
    
    set_id:(id) ->
        @definition['id'] = id
        @
    get_id:() ->
        return @definition['id']
    
    set_from:(from)->
        @definition['from'] = from
        @
    get_from:()->
        return @definition['from']

    set_to:(to)->
        @definition['to'] = to
        @
    get_to:() ->
        return @definition['to']

    get_resp_id:() ->
        return @definition['resp_id']
    set_resp_id:(resp_id) ->
        @definition['resp_id'] = resp_id
        @

    get_opcode:() ->
        return @definition['fn']
    set_opcode:(opcode) ->
        @definition['fn'] = opcode
        @
   
    get_args:() ->
        return @definition['args']
    set_args:(args) ->
        @definition['args'] = args
        @

    to_JSON:() ->
        return JSON.stringify(@definition)

    from_JSON:(somejson) ->
        @definition = JSON.parse(somejson)
        @
        

###
    @class RBProtocolHandler
    You pass this the protocol class - that is, the type of class that needs to 
    be instantiated which encapsulates the protocol used here.
    This should be something inheriting from RBProtocol.
###
class RBProtocolHandler
    constructor:(protocolclass)->
        @protocolclass = protocolclass
        @name = "RBProtocol"
        @version = "1.0"
        @classmappings = {}     #this is a mapping of strings to class names
        @functionmappings = {}  #this is a mapping of opcodes to functions
        @incomingqueue = []
        @outgoingqueue = []

    add_class_mapping:(ident, classinstance) ->
        @classmappings[ident] = classinstance

    get_class_mapping:(ident) ->
        return @classmappings[ident]

    add_function_mapping:(ident, func) ->
        @functionmappings[ident] = func

    ###
       This function should always break the incoming raw message into items which
       are then added to the incoming queue to be processed.
       This is the default pre_process - you probably want to override this
       default behaviour will just be to assume the packet is valid, doesn't nee
       to be split up, and we can just from_JSON it.
       Since JS event model is all part of a single thread, mutating the array
       from an event fire is safe - we will be out of our update code
       by this stage.
    ###
    deliver:(msg) ->
        item = new @protocolclass()
        item.from_JSON(msg)
        @incomingqueue.push(item)
        
    update:(dt) ->
        #here we could write a 'while incoming length > 0 unshift' type loop
        #but instead, I will just use a for item in coffeescript style loop, and
        #process the whole queue.
        #you might be able to be leaner or meaner by only processing one item each 
        #update cycle, or only enough items from a certain time window, or
        #even restricting your update loop to take no more than n milliseconds.

        for item in @incomingqueue
            #console.log("Item was " + item)
            #check the opcode.
            opcode = item.get_opcode()
            #console.log("Opcode was " + opcode)
            for map_oc of @functionmappings
                #console.log("checking opcode against " + map_oc)
                #if it matches
                if map_oc is opcode
                    #console.log("opcode matched: " + opcode)
                    #run the function
                    @functionmappings[opcode]( item.get_args() )

        #now empty the queue, since we processed all items.
        @incomingqueue = []
        
