class PlatformerChar extends RBSprite
    constructor: (X, Y, imagename, W, H) ->
        super(X, Y, imagename, W, H)
        @physics = new RBPhysics()
        @.add_component(@physics)
        @on_ground = false

    hit_bottom: (robj) =>
        if robj instanceof PlatformerPlatform
            @physics.velocity.y = 0
            @on_ground = true
            @.adjust_bottom(robj.top)
            #console.log("hit bottom")

class PlatformerTile extends RBSprite
    constructor: (X, Y, frames) ->
        super(X, Y, RBV_IMG.envimage, 32, 32)
        ss = RB().media.get_spritesheet(RBV_SS.env)
        #console.log(ss)
        @.add_animation("default", new RBAnimation(ss, frames, RB_ANIM_NORMAL) )
        @.set_animation("default")
        #console.log("platformer anim rect: ")
        #console.log(@_animations["default"].currentFrameRect)

class PlatformerPlatform extends PlatformerTile
    constructor: (X, Y, frames) ->
        super(X, Y, frames)
        @solid = true

class Enemy extends PlatformerChar
    constructor: (X, Y, imagename, ssname) ->
        super(X, Y, imagename, 16, 18)
        ss = RB().media.get_spritesheet(ssname)
        @.add_animation("default", new RBAnimation(ss, [6, 7, 8, 7], RB_ANIM_LOOP, 100) )
        @.add_animation("walk_right", new RBAnimation(ss, [3, 4, 5, 4], RB_ANIM_LOOP, 100) )
        @.add_animation("walk_left", new RBAnimation(ss, [9, 10, 11, 10], RB_ANIM_LOOP, 100) )
        
        @.set_animation("default")
        @speed = 80
        @dead = false

class Mage extends Enemy
    constructor: (X, Y) ->
        super(X, Y, RBV_IMG.enemyimage, RBV_SS.mage)

    die: () ->
        @dead = true
        @physics.velocity.y = -80
        @solid = false
        anim = @.set_animation("default")
        #anim.isLoop = false
        #anim.isNormal = true
        #anim.on_finished.add(@.destroy)
        action = new RBActionSequence([new RBDelay(2.0), new RBCallFunction("destroy")])
        action2 = new RBRotateBy(0.3, 180)
        action3 = new RBScaleBy(1.0, 2.0)
        action.run_on(@)
        action2.run_on(@)
        action3.run_on(@)

class Healer extends Enemy
    constructor: (X, Y) ->
        super(X, Y, RBV_IMG.enemy2image, RBV_SS.healer)
        ss = RB().media.get_spritesheet(RBV_SS.healer)
        @.add_animation("die", new RBAnimation(ss, [6, 7, 8, 7, 6, 7, 8, 7], RB_ANIM_NORMAL, 200) )
        @.get_animation("die").on_finished.add(@.destroy)
        @.set_alpha(0)
        action = new RBFadeIn(0.8)
        action.run_on(@)

    die: () ->
        @dead = true
        @physics.velocity.y = -80
        @solid = false
        anim = @.set_animation("die")
        action = new RBFadeOut(1.0)
        action.run_on(@)

class PlatformerPlayer extends PlatformerChar
    constructor: (X, Y) ->
        console.log("player constructor")
        super(X, Y, RBV_SS.playerimage, 16, 18)
        ss = RB().media.get_spritesheet(RBV_SS.warrior)
        console.log("creatng anims, spritesheet is #{ss}")
        @.add_animation("default", new RBAnimation(ss, [6, 7, 8, 7], RB_ANIM_LOOP, 100) )
        @.add_animation("walk_right", new RBAnimation(ss, [3, 4, 5, 4], RB_ANIM_LOOP, 100) )
        @.add_animation("walk_left", new RBAnimation(ss, [9, 10, 11, 10], RB_ANIM_LOOP, 100) )
        
        @.set_animation("default")
        @speed = 80
        console.log("end player constructor")

    hit_bottom: (robject) =>
        super(robject)
        if robject instanceof Enemy
            if not robject.dead
                console.log("collided with enemy")
                @physics.velocity.y = -180
                @_RB.media.load_sound(@_RB.env("settings").sounds.hurt).play()
                robject.die()
    ###
    hit_top: (robj) ->
        console.log("hit top")
    hit_left: (robj) ->
        console.log("hit left")
    hit_right: (robj) ->
        console.log("hit right")
    ###
    go_right: (should_move=true) ->
        if (should_move)
            @physics.velocity.x = @speed
            @.set_animation("walk_right")
        else
            @physics.velocity.x = 0

    go_left: (should_move=true) ->
        if (should_move)
            @physics.velocity.x = -@speed
            @.set_animation("walk_left")
        else
            @physics.velocity.x = 0
    jump: () ->
        if @on_ground
            @on_ground = false
            @physics.velocity.y = -3*@speed


class PlatformGrass extends PlatformerPlatform
    constructor: (X, Y) ->
        super(X, Y, [2])
class PlatformSteel extends PlatformerPlatform
    constructor: (X, Y) ->
        super(X, Y, [14])

class PlatformSky extends PlatformerTile
    constructor: (X, Y) ->
        super(X, Y, [3])
class PlatformCloud1 extends PlatformerTile
    constructor: (X, Y) ->
        super(X, Y, [4])
class PlatformCloud2 extends PlatformerTile
    constructor: (X, Y) ->
        super(X, Y, [5])
class PlatformCloud3 extends PlatformerTile
    constructor: (X, Y) ->
        super(X, Y, [6])
class PlatformRockTopLeft extends PlatformerTile
    constructor: (X, Y) ->
        super(X, Y, [11])
class PlatformRockTop extends PlatformerTile
    constructor: (X, Y) ->
        super(X, Y, [12])
class PlatformRockTopRight extends PlatformerTile
    constructor: (X, Y) ->
        super(X, Y, [13])
class PlatformRockLeft extends PlatformerTile
    constructor: (X, Y) ->
        super(X, Y, [21])
class PlatformRockCentre extends PlatformerTile
    constructor: (X, Y) ->
        super(X, Y, [22])
class PlatformRockRight extends PlatformerTile
    constructor: (X, Y) ->
        super(X, Y, [23])
class PlatformRockBottomLeft extends PlatformerTile
    constructor: (X, Y) ->
        super(X, Y, [31])
class PlatformRockBottom extends PlatformerTile
    constructor: (X, Y) ->
        super(X, Y, [32])
class PlatformRockBottomRight extends PlatformerTile
    constructor: (X, Y) ->
        super(X, Y, [33])

class PlatformerTitleScene extends RBPreLoaderScene
    constructor: () ->
        super("PlatformerTitleScreen")
        @input.add(RB_KEY_UP, @.on_key_up)
        @complete = new RBText(100, 100, "")
        @waiting = new RBText(100, 130, "")
        @continue = new RBText(100, 160, "")
        @stage.add_child(@complete)
        @stage.add_child(@waiting)
        @stage.add_child(@continue)
        
    on_preloaded: () ->
        #play a sound here?
        console.log("Preloaded!")
    on_key_up: (event) =>
        code = event.keyCode
        keys = @_RB.input.keys
        if code is keys.space
            @.pop()

    update: (dt) ->
        super()
        @complete.set_text("#{@pl.stats.total}")
        @waiting.set_text("#{@pl.stats.waiting_asset}")
        if @finished
            @continue.set_text("Press SPACE to continue")



class PlatformerGameScene extends RBScene
    constructor:() ->
        super("Platformer Scene")
        #console.log("constructing scene")
        @envgroup = new RBLayer()
        @envgroup.name = "Enviroment"
        @playergroup = new RBLayer()
        @playergroup.name = "Player Group"
        @bg1group = new RBLayer()
        @bg1group.name = "BG1"
        @bg2group = new RBLoopLayer(400, 0)
        @bg2group.name = "BG2"
        @bg2group.set_parallax(1.5, 0.5)

        @enemygroup = new RBLayer()
        @enemygroup.name = "Player Group"

        @input.add(RB_KEY_UP, @.on_key_up)
        @input.add(RB_KEY_DOWN, @.on_key_down)
        #Scenes automatically have a stage.
        
        @stage.add_child(@bg1group)
        @stage.add_child(@bg2group)
        @stage.add_child(@envgroup)
        @stage.add_child(@enemygroup)
        @stage.add_child(@playergroup)

        #@_RB.input.mouse.register_drag(@enemygroup)
        
        #Make a player
        @player = new PlatformerPlayer(10, 210)
        @playergroup.add_child(@player)
        
        for count in [0..10]
            p = new PlatformGrass(32*count, 300)
            p.set_anchor(0,0)
            @envgroup.add_child(p)

        for count in [0..5]
            e = new Mage(100+30*count, 210)
            @enemygroup.add_child(e)

        @enemygroup.add_child(new Healer(70, 210))
        
        @cam = new RBCamera(0, 0, RB().screen().width, RB().screen().height)
        @cam.follow(@player)
        
        @editor = new RBEditor(32, 32,
            [@envgroup, @bg1group, @bg2group],
            {
                "Grass" : PlatformGrass
                "Steel" : PlatformSteel
                "Sky" : PlatformSky
                "Cloud1" : PlatformCloud1
                "Cloud2" : PlatformCloud2
                "Cloud3" : PlatformCloud3
                "RockLeft" : PlatformRockLeft
                "RockTopLeft" :PlatformRockTopLeft
                "RockBottomLeft": PlatformRockBottomLeft
                "RockRight": PlatformRockRight
                "RockTopRight": PlatformRockTopRight
                "RockBottomRight": PlatformRockBottomRight
                "RockTop": PlatformRockTop
                "RockBottom": PlatformRockBottom
            })
        @.add_child(@editor) #make it get updated
        #@.add_child(@cam) #make cam get updated
        @editor.load_map_from_file("data/platformer/platformer_level2.json")

        fadeinaction = new RBFadeIn(2.0)
        fadeinaction.run_on(@stage)
        @stage.set_alpha(0)
        console.log("finished constructing scene")

    finish: () ->
        console.log("game finished")
        @.pop(true) #remove from cache

    on_key_down: (event) =>
        keys = @_RB.input.keys
        switch event.keyCode
            when keys.right
                @player.go_right(true)
                @_RB.input.swallow(event)
            when keys.left
                @player.go_left(true)
                @_RB.input.swallow(event)
            when keys.up
                @player.jump()
                @_RB.input.swallow(event)

    on_key_up: (event) =>
        keys = @_RB.input.keys
        switch (event.keyCode)
            when keys.right
                @player.go_right(false)
                @_RB.input.swallow(event)
            when keys.left
                @player.go_left(false)
                @_RB.input.swallow(event)
            when keys.esc
                @.finish()

    update: (dt) ->
        super(dt) #updates stage, editor, etc
        @cam.update(dt)
        @_RB.collide(@envgroup.members, @playergroup.members)
        @_RB.collide(@envgroup.members, @enemygroup.members)
        @_RB.collide(@enemygroup.members, @playergroup.members)
    
    draw: () ->
        #For normal drawing (no camera)
        #@stage.render()
        #@editor.render()
        
        #When using the camera
        @cam.draw(@stage)
        @cam.draw(@editor)
        #@editor.render()

class Platformer extends RBGame
    constructor: (@settings, @canvas) ->
        super(@settings, @canvas)
        @titlescene = null
        @gamescene = null
        @gameoverscene = null
        #Run start state when RB starts
        @_RB.events.add(RB_EVENT_ON_START, @.start_state)
        
    start_state: () =>
        #Remove this event from RB.start() - it isn't needed again
        @_RB.events.remove(RB_EVENT_ON_START, @.start_state)
        #Reset Gameover
        #Preload
        if @titlescene is null
            @titlescene = new PlatformerTitleScene()
            @titlescene.events.add(RB_EVENT_ON_SCENE_EXIT, @.start_game)
        @titlescene.push()

    start_game: () =>
        console.log("start game called")

        @gamescene = new PlatformerGameScene()
        @gamescene.events.add(RB_EVENT_ON_SCENE_EXIT, @.game_over)
        @gamescene.push()

    game_over: () =>
        console.log("Game Over! Restarting!")
        if @gamescene isnt null
            @gamescene.destroy()
        @.start_state()


#This variable, _game, will hold our entire game.
_game = null
#This function is called to create a new game
run_platformer = () ->
    ###
    _settings = {}
    _settings.engine =
            fps: 30,
            screenwidth: 400,
            screenheight: 400,
            canvaswidth: 400,
            canvasheight: 400,
            canvasid: "canvas",
            canvassmoothing: false,
        
    _settings.images =
            playerimage: "data/platformer/Warrior.png",
            enemyimage: "data/platformer/mage_m.png",
            enemy2image: "data/platformer/healer_m.png",
            envimage: "data/platformer/env_tiles.png",
        
    _settings.sounds =
            laser: "data/sfx.wav",
            hurt: "data/hurt.wav",
        
    _settings.spritesheets =
            warrior : ["warrior", _settings.images.playerimage, 3, 4],
            mage : ["mage", _settings.images.enemyimage, 3, 4],
            healer : ["healer", _settings.images.enemy2image, 3, 4],
            env : ["env", _settings.images.envimage, 10, 10],

    ###
    
    RBV(RBV_ENG, {
    fps : 30
    screenwidth: 400
    screenheight: 400
    canvaswidth: 400
    canvasheight: 400
    canvasid: "canvas"
    canvassmoothing: false
    })

    RBV(RBV_IMG, {
        playerimage: "data/platformer/Warrior.png"
        enemyimage: "data/platformer/mage_m.png"
        enemy2image: "data/platformer/healer_m.png"
        envimage: "data/platformer/env_tiles.png"
    })

    RBV(RBV_SND, {
        laser: "data/sfx.wav"
        hurt: "data/hurt.wav"
    })

    RBV(RBV_SS, {
        warrior :   ["warrior", RBV(RBV_IMG).playerimage, 3, 4]
        mage :      ["mage", RBV(RBV_IMG).enemyimage, 3, 4]
        healer :    ["healer", RBV(RBV_IMG).enemy2image, 3, 4]
        env :       ["env", RBV(RBV_IMG).envimage, 10, 10]

    })

    if not (_game is null)
        return #prevent multiple inits
    _game = new Platformer()
    RB().start()


