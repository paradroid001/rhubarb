# A class for media objects
class RBMediaObject
    constructor: () ->
        @is_ready = false
    on_ready: () =>
        @is_ready = true

# A Sound
class RBAudio extends RBMediaObject
    # Whether or not the sound should loop
    looping: false

    # Create an RBAudio object
    constructor:(soundname, cachesize=RB_DEFAULT_SOUND_CACHE_SIZE, onrdy) ->
        super()
        if !onrdy?
            onrdy = @.snd_on_ready
        @soundname = soundname
        @cachesize = cachesize
        @soundbank = []
        @loadedsounds = 0
        @_volume = 1
        for count in [0...@cachesize]
            #console.log("adding a sound to the soundbank")
            @_audio = new Audio(@soundname)
            @_audio.soundname = @soundname #for snd_on_ready to work
            @soundbank.push(@_audio)
            @_audio.onload = onrdy
            #@_audio.on('loadeddata', onrdy)
            @_audio.addEventListener('canplaythrough', onrdy)
            @_audio.load()

            #for now, we just call..
            #console.log("TODO: forcing sound onload callback")
            #@.on_ready()
    
    # Set the volume of this sound (0 to 1)
    volume: (num) ->
        if num?
            if num >=0 and num <= 1
                @_volume = num
        
        @.set_effective_volume()
        @_volume

    # @private
    # the actual volume that this sound should play at
    # considering the global volume settings
    set_effective_volume: () ->
        v = @_volume * RB().media.sfxVolume
        if @_audio isnt null
            @_audio.volume = v
        v

    # @private
    snd_on_ready: () ->
        #console.log("snd_on_ready!")
        sound = RB().media.sounds[@soundname]
        sound.on_ready()

    # @private
    on_ready: () ->
        #console.log("sound was ready")
        @loadedsounds +=1
        if @loadedsounds == @cachesize
            #console.log("sound #{@soundname} ready")
            super()
    
    # @private
    find_playable_sound: () ->
        for au in @soundbank
            if au.ended or au.currentTime == 0
                return au
        return null

    set_looping: (looping = true) ->
        if (typeof @_audio.loop == 'boolean')
            @_audio.loop = looping
        else
            if looping

                replay = () =>
                    this.currentTime = 0
                    this.play()

                @_audio.addEventListener('ended', replay, false)
            else
                @_audio.removeEventListener('ended', replay, false)

    # Stop the sound, and by default rewind it.
    stop: (rewind = true) ->
        @_audio.pause()
        if rewind
            @_audio.currentTime = 0

    # Play the sound
    play:() ->
        au = @.find_playable_sound()
        if au isnt null
            @_audio = au
            @.set_effective_volume()
            @_audio.play()

# Music class
# Only caches one object.
# Defaults to looping = true.
class RBMusic extends RBAudio
    # Create an RBMusic object
    constructor: (soundname) ->
        #console.log("Constructing RB Music")
        super(soundname, 1)
        @soundname = soundname
        @playing = false

    # @private
    # the actual volume that this sound should play at
    # considering the global volume settings
    set_effective_volume: () ->
        v = @_volume * RB().media.musicVolume
        if @_audio isnt null
            @_audio.volume = v
        v

    # Play the music, but only if it isn't already playing.
    # The default is to loop it (pass false if you dont want to)
    play: (looping = true) ->
        if !@playing
            @.set_looping(looping)
            @_audio = @soundbank[0]
            @.set_effective_volume()
            @_audio.play()
            @playing = true

    # Stop the music, and rewind by default.
    # Pass false if you don't want to
    stop: (rewind = true) ->
        super(rewind)
        @playing = false



# An Image
class RBImage extends RBMediaObject
    # Construct an RBImage
    constructor: (imagename, onrdy, src) ->
        super()
        @imagename = imagename
        if !onrdy?
            onrdy = @.img_on_ready
        @_image = new Image()
        @_image.imagename = @imagename #for img_on_ready to work
        @_image.onload = onrdy
        #console.log(@_image.onload)
        if src?
            @_image.src = src
        else
            @_image.src = @imagename
    # @private
    img_on_ready: () =>
        #at this point, we are from the perspective of
        #the HTMLImage object, not THIS object
        rbimage = RB().media.images[@imagename]
        rbimage.on_ready() 
    # @private
    on_ready: () ->
        #console.log("image #{@src} is ready")
        super()
    # Returns the HTMLElement image for this RBImage
    image: () ->
        @_image


# A spritesheet
class RBSpriteSheet extends RBMediaObject

    # Construct a spritesheet
    constructor: (@ssname, @imagename, @xframes, @yframes, @framewidth=0, @frameheight=0) ->
        #console.log("Constructing spritesheet #{@ssname} for #{@imagename}")
        super()
        @_RBimage = RB().media.load_image(@imagename, @.on_ready)
    
    # @private
    on_ready: () =>
        super()

        #console.log("spritesheet for #{@imagename} ready")
        #@_image = @_RBimage.image()
        @_image = RB().media.images[@imagename]._image
        #console.log(RB().media.images[@imagename])
        #console.log("Image width,height: #{@_image.naturalWidth},#{@_image.naturalHeight}")
        if @framewidth == 0
            @framewidth = (@_image.naturalWidth / @xframes) | 0
        if @frameheight == 0
            @frameheight = (@_image.naturalHeight / @yframes) | 0
        #console.log("Framewidth was #{@framewidth} and frameheight was #{@frameheight}")
    
    get_image: () ->
        return @_image
    # @private
    frame_at: (index, rect, xoffset=0, yoffset=0) ->
        #console.log("Frameat: #{index}")
        rect.x = (index % @xframes)*@framewidth
        rect.y = ((index / @xframes) | 0) * @frameheight
        rect.height = @frameheight
        rect.width = @framewidth
        #if index == 0
        #    console.log("currentFrameRect: x:#{rect.x}, y:#{rect.y}, w: #{rect.width}, h: #{rect.height}")
        return rect

# A container to hold and cache media for a project.
# RBMedia contains four main sections:
#   images
#   sounds
#   music
#   spritesheets
#
# It contains an {RBPreloader} which can preload sets of
# images, spritesheets, and sounds, or you can load them
# manually with
#   load_image(path)
#   load_sound(path)
#   load_music(path)
#   load_spritesheet(name, image, xframes, yframes)
#
# Once loaded, they can be used when needed in the project
# by retrieving them:
# @example
#   myimage = RB().media.get_image(yourimagename)
#   mysound = RB().media.get_sound(yoursoundname)
#   mymusic = RB().media.get_music(yourmusicname)
#   myspritesheet = RB().media.get_spritesheet(yourssname)
#
# The other important parts of RBMedia are the controls for
# sound volume:
# #sfx_volume()
# #music_volume()
class RBMedia
    constructor: () ->
        @images = {}
        @sounds = {}
        @spritesheets = {}
        @preloader = new PreLoaderStats()
        @sfxVolume = 1
        @musicVolume = 1
        @.load_image("rblogo", undefined, __rblogo)

    # Set the global sfx volume (0-1)
    sfx_volume: (num) ->
        if num?
            if num >= 0 and num <=1
                @sfxVolume = num
                for sound in @sounds
                    sound.set_effective_volume()
        @sfxVolume

    # Set the global music volume (0-1)
    music_volume: (num) ->
        if num?
            if num >= 0 and num <=1
                @musicVolume = num
                for name, sound of @sounds
                    sound.set_effective_volume()
        @musicVolume

    # Load and store spritesheet parameters
    # If the image specified is not yet loaded, RBMedia will
    # attempt to load it.
    load_spritesheet: (spritesheetname, imagename, xframes, yframes, framewidth=0, frameheight=0) =>
        console.log("load_spritesheet arguments: #{spritesheetname}, #{imagename}, #{xframes}, #{yframes}, #{framewidth}, #{frameheight}")
        if not @.get_spritesheet(spritesheetname)?
            #console.log("loading spritesheet")
            newss = new RBSpriteSheet(spritesheetname, imagename, xframes, yframes, framewidth, frameheight)
            @spritesheets[spritesheetname] = newss
        @spritesheets[spritesheetname]
    
    # Gets the named spritesheet.
    # You can either pass a spritesheet name as a string,
    # or pass a spritesheet initialiseation array, in which the
    # first element is the spritesheet name
    get_spritesheet: (spritesheetname) ->
        if typeof spritesheetname == "object" and spritesheetname.length>1
            spritesheetname = spritesheetname[0]
        return @spritesheets[spritesheetname]

    # Load an image resource and cache it
    load_image: (imagename, ready_callback, src) =>
        if not @images[imagename]?
            #console.log("creating new RBImage(#{imagename})")
            @images[imagename] = new RBImage(imagename, ready_callback, src)
        #return
        else
            #console.log("Image retrieved from cache.")
            if ready_callback?
                ready_callback()
            #else
                #@images[imagename].on_ready()
        @images[imagename]

    # Get an image from the cache.
    get_image: (imagename) ->
        return @images[imagename]

    # Load a sound resource and cache it.
    load_sound: (soundname, cachesize=RB_DEFAULT_SOUND_CACHE_SIZE) =>
        if not @sounds[soundname]?
            #load the sound
            @sounds[soundname] = new RBAudio(soundname, RB_DEFAULT_SOUND_CACHE_SIZE)
        #return
        @sounds[soundname]

    # Get a sound from the cache
    get_sound: (soundname) ->
        return @sounds[soundname]

    # Load music from a resource and cache it.
    load_music: (music_name) =>
        if not @sounds[music_name]?
            #console.log("loading music")
            @sounds[music_name] = new RBMusic(music_name)
        @sounds[music_name]

    # Get music from the cache
    get_music: (music_name) ->
        return @sounds[music_name]

# @private
# A controller class for aggregating several preloaders
class PreLoaderStats
    constructor: () ->
        @images = new PreLoader()
        @spritesheets = new PreLoader()
        @sounds = new PreLoader()
        @music = new PreLoader()
        @stats = @.statistics()
        @first_non_loaded = null
        @done = false

    queue_images: (imageslist) ->
        @images.queue_items(imageslist)
    queue_sounds: (soundslist) ->
        @sounds.queue_items(soundslist)
    queue_music: (musiclist) ->
        @music.queue_items(musiclist)
    queue_spritesheets: (spritesheetslist) ->
        @spritesheets.queue_items(spritesheetslist)
        
    start: () ->
        @images.preload(RB().media.load_image)

    update: () ->
        if not @done
            if not @images.done
                @images.update("is_ready")
                if @images.first_non_loaded isnt null
                    @first_non_loaded = @images.first_non_loaded.imagename
            else if not @sounds.done
                if not @sounds.started
                    @sounds.preload(RB().media.load_sound)
                @sounds.update("is_ready")
                if @sounds.first_non_loaded isnt null
                    @first_non_loaded = @sounds.first_non_loaded.soundname
            else if not @music.done
                if not @music.started
                    @music.preload(RB().media.load_music)
                @music.update("is_ready")
                if @music.first_non_loaded isnt null
                    @first_non_loaded = @music.first_non_loaded.soundname
            else if not @spritesheets.done
                if not @spritesheets.started
                    @spritesheets.preload(RB().media.load_spritesheet)
                @spritesheets.update("is_ready")
                if @spritesheets.first_non_loaded isnt null
                    @first_non_loaded = @spritesheets.first_non_loaded.ssname
            @stats = @.statistics()
            if @stats.total > 99
                @done = true
                #console.log("preloader is done!")
    
    statistics: () ->
        #console.log("Images: #{@images.percentageDone}")
        #console.log("Sounds: #{@sounds.percentageDone}")
        #console.log("Spritesheets: #{@spritesheets.percentageDone}")
        ret = {}
        ret.images = @images.percentageDone
        ret.sounds = @sounds.percentageDone
        ret.music = @music.percentageDone
        ret.spritesheets = @spritesheets.percentageDone
        ret.total = ((ret.images + ret.sounds + ret.music + ret.spritesheets) / 4.0)
        ret.waiting_asset = @first_non_loaded
        return ret

# @private
# A class for preloading objects.
class PreLoader
    constructor: () ->
        @items = []
        @itemsToLoad = 0
        @itemsLoaded = 0
        @percentageDone = 0
        @loadingItems = []
        @loadedItems = []
        @done = false
        @started = false
        @first_non_loaded = null

    queue_items: (qitems) ->
        for i in qitems
            @items.push(i)
            @itemsToLoad++
    
    preload: (func) ->
        if not @started
            for i in @items
                #console.log("preloading #{i}")
                #console.log(typeof i)
                if typeof i == "object" && i.length > 1
                    @loadingItems.push(func.apply(RB().media, i))
                else
                    @loadingItems.push(func(i))
            @started = true

    update: (loadedproperty) ->
        @first_non_loaded = null
        for i in @loadingItems
            #console.log(i)
            if i not in @loadedItems
                if i[loadedproperty]
                    @loadedItems.push(i)
                    @itemsLoaded+=1
                else if @first_non_loaded is null
                    @first_non_loaded = i
        if @itemsToLoad == 0
            @percentageDone = 100.0
        else
            @percentageDone = (@itemsLoaded/@itemsToLoad) * 100.0

        if @itemsToLoad == @itemsLoaded
            @done = true
            @percentageDone = 100.0

__rblogo = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAACXBIWXMAAAsTAAALEwEAmpwYAAAKT2lDQ1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAHjanVNnVFPpFj333vRCS4iAlEtvUhUIIFJCi4AUkSYqIQkQSoghodkVUcERRUUEG8igiAOOjoCMFVEsDIoK2AfkIaKOg6OIisr74Xuja9a89+bN/rXXPues852zzwfACAyWSDNRNYAMqUIeEeCDx8TG4eQuQIEKJHAAEAizZCFz/SMBAPh+PDwrIsAHvgABeNMLCADATZvAMByH/w/qQplcAYCEAcB0kThLCIAUAEB6jkKmAEBGAYCdmCZTAKAEAGDLY2LjAFAtAGAnf+bTAICd+Jl7AQBblCEVAaCRACATZYhEAGg7AKzPVopFAFgwABRmS8Q5ANgtADBJV2ZIALC3AMDOEAuyAAgMADBRiIUpAAR7AGDIIyN4AISZABRG8lc88SuuEOcqAAB4mbI8uSQ5RYFbCC1xB1dXLh4ozkkXKxQ2YQJhmkAuwnmZGTKBNA/g88wAAKCRFRHgg/P9eM4Ors7ONo62Dl8t6r8G/yJiYuP+5c+rcEAAAOF0ftH+LC+zGoA7BoBt/qIl7gRoXgugdfeLZrIPQLUAoOnaV/Nw+H48PEWhkLnZ2eXk5NhKxEJbYcpXff5nwl/AV/1s+X48/Pf14L7iJIEyXYFHBPjgwsz0TKUcz5IJhGLc5o9H/LcL//wd0yLESWK5WCoU41EScY5EmozzMqUiiUKSKcUl0v9k4t8s+wM+3zUAsGo+AXuRLahdYwP2SycQWHTA4vcAAPK7b8HUKAgDgGiD4c93/+8//UegJQCAZkmScQAAXkQkLlTKsz/HCAAARKCBKrBBG/TBGCzABhzBBdzBC/xgNoRCJMTCQhBCCmSAHHJgKayCQiiGzbAdKmAv1EAdNMBRaIaTcA4uwlW4Dj1wD/phCJ7BKLyBCQRByAgTYSHaiAFiilgjjggXmYX4IcFIBBKLJCDJiBRRIkuRNUgxUopUIFVIHfI9cgI5h1xGupE7yAAygvyGvEcxlIGyUT3UDLVDuag3GoRGogvQZHQxmo8WoJvQcrQaPYw2oefQq2gP2o8+Q8cwwOgYBzPEbDAuxsNCsTgsCZNjy7EirAyrxhqwVqwDu4n1Y8+xdwQSgUXACTYEd0IgYR5BSFhMWE7YSKggHCQ0EdoJNwkDhFHCJyKTqEu0JroR+cQYYjIxh1hILCPWEo8TLxB7iEPENyQSiUMyJ7mQAkmxpFTSEtJG0m5SI+ksqZs0SBojk8naZGuyBzmULCAryIXkneTD5DPkG+Qh8lsKnWJAcaT4U+IoUspqShnlEOU05QZlmDJBVaOaUt2ooVQRNY9aQq2htlKvUYeoEzR1mjnNgxZJS6WtopXTGmgXaPdpr+h0uhHdlR5Ol9BX0svpR+iX6AP0dwwNhhWDx4hnKBmbGAcYZxl3GK+YTKYZ04sZx1QwNzHrmOeZD5lvVVgqtip8FZHKCpVKlSaVGyovVKmqpqreqgtV81XLVI+pXlN9rkZVM1PjqQnUlqtVqp1Q61MbU2epO6iHqmeob1Q/pH5Z/YkGWcNMw09DpFGgsV/jvMYgC2MZs3gsIWsNq4Z1gTXEJrHN2Xx2KruY/R27iz2qqaE5QzNKM1ezUvOUZj8H45hx+Jx0TgnnKKeX836K3hTvKeIpG6Y0TLkxZVxrqpaXllirSKtRq0frvTau7aedpr1Fu1n7gQ5Bx0onXCdHZ4/OBZ3nU9lT3acKpxZNPTr1ri6qa6UbobtEd79up+6Ynr5egJ5Mb6feeb3n+hx9L/1U/W36p/VHDFgGswwkBtsMzhg8xTVxbzwdL8fb8VFDXcNAQ6VhlWGX4YSRudE8o9VGjUYPjGnGXOMk423GbcajJgYmISZLTepN7ppSTbmmKaY7TDtMx83MzaLN1pk1mz0x1zLnm+eb15vft2BaeFostqi2uGVJsuRaplnutrxuhVo5WaVYVVpds0atna0l1rutu6cRp7lOk06rntZnw7Dxtsm2qbcZsOXYBtuutm22fWFnYhdnt8Wuw+6TvZN9un2N/T0HDYfZDqsdWh1+c7RyFDpWOt6azpzuP33F9JbpL2dYzxDP2DPjthPLKcRpnVOb00dnF2e5c4PziIuJS4LLLpc+Lpsbxt3IveRKdPVxXeF60vWdm7Obwu2o26/uNu5p7ofcn8w0nymeWTNz0MPIQ+BR5dE/C5+VMGvfrH5PQ0+BZ7XnIy9jL5FXrdewt6V3qvdh7xc+9j5yn+M+4zw33jLeWV/MN8C3yLfLT8Nvnl+F30N/I/9k/3r/0QCngCUBZwOJgUGBWwL7+Hp8Ib+OPzrbZfay2e1BjKC5QRVBj4KtguXBrSFoyOyQrSH355jOkc5pDoVQfujW0Adh5mGLw34MJ4WHhVeGP45wiFga0TGXNXfR3ENz30T6RJZE3ptnMU85ry1KNSo+qi5qPNo3ujS6P8YuZlnM1VidWElsSxw5LiquNm5svt/87fOH4p3iC+N7F5gvyF1weaHOwvSFpxapLhIsOpZATIhOOJTwQRAqqBaMJfITdyWOCnnCHcJnIi/RNtGI2ENcKh5O8kgqTXqS7JG8NXkkxTOlLOW5hCepkLxMDUzdmzqeFpp2IG0yPTq9MYOSkZBxQqohTZO2Z+pn5mZ2y6xlhbL+xW6Lty8elQfJa7OQrAVZLQq2QqboVFoo1yoHsmdlV2a/zYnKOZarnivN7cyzytuQN5zvn//tEsIS4ZK2pYZLVy0dWOa9rGo5sjxxedsK4xUFK4ZWBqw8uIq2Km3VT6vtV5eufr0mek1rgV7ByoLBtQFr6wtVCuWFfevc1+1dT1gvWd+1YfqGnRs+FYmKrhTbF5cVf9go3HjlG4dvyr+Z3JS0qavEuWTPZtJm6ebeLZ5bDpaql+aXDm4N2dq0Dd9WtO319kXbL5fNKNu7g7ZDuaO/PLi8ZafJzs07P1SkVPRU+lQ27tLdtWHX+G7R7ht7vPY07NXbW7z3/T7JvttVAVVN1WbVZftJ+7P3P66Jqun4lvttXa1ObXHtxwPSA/0HIw6217nU1R3SPVRSj9Yr60cOxx++/p3vdy0NNg1VjZzG4iNwRHnk6fcJ3/ceDTradox7rOEH0x92HWcdL2pCmvKaRptTmvtbYlu6T8w+0dbq3nr8R9sfD5w0PFl5SvNUyWna6YLTk2fyz4ydlZ19fi753GDborZ752PO32oPb++6EHTh0kX/i+c7vDvOXPK4dPKy2+UTV7hXmq86X23qdOo8/pPTT8e7nLuarrlca7nuer21e2b36RueN87d9L158Rb/1tWeOT3dvfN6b/fF9/XfFt1+cif9zsu72Xcn7q28T7xf9EDtQdlD3YfVP1v+3Njv3H9qwHeg89HcR/cGhYPP/pH1jw9DBY+Zj8uGDYbrnjg+OTniP3L96fynQ89kzyaeF/6i/suuFxYvfvjV69fO0ZjRoZfyl5O/bXyl/erA6xmv28bCxh6+yXgzMV70VvvtwXfcdx3vo98PT+R8IH8o/2j5sfVT0Kf7kxmTk/8EA5jz/GMzLdsAAAAgY0hSTQAAeiUAAICDAAD5/wAAgOkAAHUwAADqYAAAOpgAABdvkl/FRgAAAqlJREFUeNp8k11IFFEUx38zc0dXXfuQyi216AMrlgwjSjEirBehj4ewDwp66IvAB+3FB6OgECMKeqgohTAiiLAMeksw6iEJDIoCDUvK2j7WYdZJd3fWnZnbg9dYeujC4XDOvf9zz73//9Hulu+R/GcZ0kCXAiEFQpoImYeQeZhBHqbMQ6xKVlGbuLAPKFCYdA5eA0xAAAbgAVnlC9JG3x292tmGAk8DNvAtx74/4OLmcZ53b6emBEgCFhAD7Kd+CvEocGZvmwLiwPecDkI3iWfuM8xbnAIgABLAOOC18hlxhq8ALuAAv9UTTNV+tl9kAoQLWd/E/5t3gcnhpYWI0RVhGEUncriOSEMH+vQwofAaTAPSI51kbY1lq8E7e4K5S0oRMok9dI0nTX00VqOzbwOATvXCIk5ugcNRycvGFiqHujhaV0nD8mLO7YBd823u1V+m3h3k+vEmVlaup7UandPRmQLR/Hz2FsOL9mdAiK62V5zcco1I3KN0EHra3wEuN1oHcHuLaIjWMN6N4NftGboSgwZjt+DzGxeYBH4AFs7AJB9iEPtoAxav3k8x2gPuyDy+9CIYe6w4GIGf0yAnUsAvYAywCs2RyVDmG6kFGVxwiraGQ+X5b0hkLLdizhR6bckQACuKbHaVfWRRQdpXv+wC6Z0rx73ja2Psbwo2AXOPtAS7T62Op7x+50tzlYVoXmcxALJmcdo/FvVom+8l4zNUekBwoCLpbSwDaxORK55x1c9kUpfOp7vsmLQPFibRpIfUBIdmAf8I6jcQASqB5UBISdkBQvIhnYIHoICOMktJdkodTgCfgAlVIABSQJgOEHQAkK/mwVWDE1a5QMVC7flK4h6gZV8LtIxuSolGoGlIdHxtxgLNwEfH0ww83cDXBFlNkNVzvcmfAQCcOxan14psMgAAAABJRU5ErkJggg=="
