# A class to hold groups of entities in scenes, which can 
# be considered part of the same layer, and will all be drawn together.
# Layers can use parallax effects, however these will not be seen
# unless you are using an RBCamera to render your scene (and the camera
# is moving).
class RBLayer extends RBEntity
    constructor:(name="Layer_NoName") ->
        super(undefined, undefined, undefined, undefined, name)
        @_fade = false
        @solid = false #very important
        @_parallax = new RBPoint(1.0, 1.0) #what your parallax ratio should be
        @.set_anchor(0,0)
        @_cumulative_parallax_offset = new RBPoint(0, 0)
        @alwaysOnScreen = true #never cull

    # Set the parallax ratios for this layer.
    # A value of 1 (default) is a 1 to 1 'opposite' movement to the
    # camera - i.e. no parallax at all. 
    # A value of 0 is a stationary layer - i.e moves with the camera,
    # so it is useful for GUI's etc.
    # Any value greater than 1 results in a parallax ratio.
    set_parallax: (xratio = 1, yratio = 1) ->
        @_parallax.x = xratio
        @_parallax.y = yratio

    update: (dt) ->
        super(dt)
        #The reason we calculate this in such a roundabout way
        #(instead of just doing a setposition based on 
        #your parallax ratios and the screen rect) is that
        #we want to use 'move_by', not set_position - i.e. we
        #don't want to clobber other set_pos type operations that 
        #could be happening on the entity.
        
        xoff = (-@_parallax.x + 1) * RB().screen().x # |0
        yoff = (-@_parallax.y + 1) * RB().screen().y # |0
        dxoff = xoff - @_cumulative_parallax_offset.x
        dyoff = yoff - @_cumulative_parallax_offset.y
        
        #@.move_by(dxoff | 0, dyoff | 0)
        @.move_by(dxoff , dyoff )
        #@.set_position(@x|0, @y|0)
        @_cumulative_parallax_offset.x += dxoff
        @_cumulative_parallax_offset.y += dyoff
        #@.set_position((-@_parallax.x + 1) * RB().screen().x, (-@_parallax.y + 1) * RB().screen().y)

    fade: () ->
        @_fade = true

    unfade: () ->
        @_fade = false

    draw: () ->
        if @_fade
            #@_RB.cctx.globalCompositeOperation = "lighter"
            @_RB.cctx.globalAlpha = 0.3
        super()

# A class for creating layers which loop their backgrounds.
# Reasonably experimental, and potentially buggy.
# The main arguments to the constructor are the 
# loopwidth and loopheight. Specifying 0, or omitting the 
# parameter, means the layer will not loop in that dimension.
# Any other value will cause the layer to be looped / repeated 
# after that many units in screenspace.
# It is important that this loop value be *equal to or less than* the 
# screen width / height (whichever is relevant).
# Setting to a value less than screen width (taking width, for example) 
# will repeat the contents several times over the course of a screen.
# Setting to a value equal to the screen width will fit the screen width
# exactly, and will repeat if the screen or layer scrolls / moves 
# for some reason.
# Setting to a value greater than the width of the screen (or height for
# the loopheight parameter) will cause the layer *not to display at all!*
#
# So be careful!
#
# You can see the result of your repeating layers either via
# parallax x and y settings and the use of a camera, or via
# x / y velocities on the layers themselves, and a stationary 
# view. Or - both!
class RBLoopLayer extends RBLayer
    constructor: (@loopwidth=0, @loopheight=0, name) ->
        super(name)
        @canvas = document.createElement("canvas")
        @canvas.width = @_RB.screen().width 
        @canvas.height = @_RB.screen().height
        @ctx = @canvas.getContext('2d')
        @_RB.set_smoothing(@ctx, @_RB._canvasSmoothing)
        if @loopwidth > 0
            @loopx = true
        if @loopheight > 0
            @loopy = true
        @_loopRect = new RBRect(@x, @y, @loopwidth, @loopheight)
        @_drawRect = new RBRect()
        @_loopRect.set_anchor(0, 0)
        @_drawRect.set_anchor(0, 0)
    ###
    update: (dt) ->
        super(dt)
        scr = RB().screen()
        x = @x
        y = @y
        if @loopx
            x = Math.abs(@x % scr.width)
        if @loopy
            y = Math.abs(@y % scr.height)
        @.set_position(x, y)
    ###

    render: () ->
        #Clear our local canvas
        @ctx.clearRect(0,0,@canvas.width, @canvas.height)
        #Draw the scene into our canvas
        @_RB.push_cctx()
        @_RB.set_cctx(@ctx)
        @ctx.translate(-@x, -@y)
        super()
        @ctx.translate(@x, @y)
        #now draw it on the main canvas
        @_RB.restore_cctx()
        @_RB.cctx.save()
        #clip to this layer only
        #@_RB.cctx.beginPath()
        #@_RB.cctx.rect(0, 0, @width, @height)
        #@_RB.cctx.closePath()
        #@_RB.cctx.clip()
        #draw the local camera canvas onto the main context
        scr = RB().screen()
        x = 0
        y = 0
        loopxbounds = []
        loopybounds = []
        if @loopx
            x =@x%@loopwidth
            loopxbounds.push(scr.left-@_loopRect.width, scr.right+@_loopRect.width)
        else
            x = scr.x
            @_loopRect.set_width(scr.width)
            loopxbounds.push(scr.left, scr.right)
        
        if @loopy
            y = @y%@loopheight
            loopybounds.push(scr.top-@_loopRect.height, scr.bottom+@_loopRect.height)
        else
            y = scr.y
            @_loopRect.set_height(scr.height)
            loopybounds.push(0, scr.bottom)

        for xdiff in [loopxbounds[0]..loopxbounds[1]] by @_loopRect.width
            for ydiff in [loopybounds[0]..loopybounds[1]] by @_loopRect.height
                #console.log(xdiff, ydiff)
                @_loopRect.set_position(x,y)#-loopRect.height)
                @_loopRect.move_by(xdiff, ydiff)
                if (scr.overlaps(@_loopRect, @_drawRect) )
                    ###
                    @_RB.cctx.strokeStyle = rgba_to_html(0, 255, 0, 255) 
                    @_RB.cctx.strokeRect(@_loopRect.left, @_loopRect.top, @_loopRect.width, @_loopRect.height)
                    @_RB.cctx.strokeStyle = rgba_to_html(0, 255, 0, 255) 
                    @_RB.cctx.strokeRect(@_drawRect.left, @_drawRect.top, @_drawRect.width, @_drawRect.height)
                    @_RB.cctx.strokeStyle = rgba_to_html(0, 0, 255, 255) 
                    @_RB.cctx.strokeRect(scr.left, scr.top, scr.width, scr.height)
                    ###
                    @_RB.cctx.drawImage(@canvas,
                        0,
                        0
                        @_loopRect.width, 
                        @_loopRect.height
                        
                        @_loopRect.x|0, 
                        @_loopRect.y|0, 
                        @_loopRect.width, 
                        @_loopRect.height, )
        @_RB.cctx.restore()

