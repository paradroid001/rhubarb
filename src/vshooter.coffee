class Bullet extends RBSprite
    constructor: (X, Y, @heading = 0) ->
        super(X, Y, RBV_IMG.bulletsimage, 8, 8)
        @speed = 250
        @.add_tag("bullet")
        @solid = true
        ss = RB().media.get_spritesheet(RBV_SS.bullets)
        @.add_animation("default", new RBAnimation(ss, [2,6,10,14,10,6, 2, 2, 2, 2], RB_ANIM_LOOP, 50))
        @.set_animation("default")
        @headingvector = new RBPoint()
        @headingvector.cartesian(@heading, 1)
        deathaction = new RBActionSequence([new RBDelay(2.0), new RBCallFunction("destroy")])
        deathaction.run_on(@)

    update: (dt) ->
        super(dt)
        @.move_by(@headingvector.x*(dt*@speed/1000), @headingvector.y*(dt*@speed/1000) )
        if (@y < -100)
            @.destroy()
    hit_top: (robject) ->
        super(robject)
        console.log("bhit")

class EnemyShip extends RBSprite
    constructor: (X, Y, @bulletgroup) ->
        super(X, Y, RBV_IMG.shipsimage, RBV_GAME.enemywidth, RBV_GAME.enemyheight)
        
        @physics = new RBPhysics()
        @.add_component(@physics)
        @physics.floating = true
        @speed = 180
        @threshold = 50
        @.add_tag("enemy")
        @solid = true
        @shoottimer = new RBIntervalTimer(1000)
        
        ss = RB().media.get_spritesheet(RBV_SS.ship)
        @.add_animation("default", new RBAnimation(ss, [2], RB_ANIM_NORMAL))
        @.add_animation("left", new RBAnimation(ss, [2, 1, 0], RB_ANIM_NORMAL))
        @.add_animation("right", new RBAnimation(ss, [2, 3, 4], RB_ANIM_NORMAL))
        @.set_animation("default")
        @.set_angle(180)

    move_right: () ->
        @physics.velocity.x = @speed
        @.set_animation("right")
    move_left: () ->
        @physics.velocity.x = -@speed
        @.set_animation("left")
    stop: () ->
        @physics.velocity.x = 0
        @.set_animation("default")
    
    hit_bottom: (robject) ->
        super(robject)
        console.log("hit")
        if robject.has_tag("bullet")
            @.destroy()

    shoot: () ->
        if @shoottimer.has_interval_expired()
            b = new Bullet(@x, @y+20, 180)
            @bulletgroup.add_child(b)
            @shoottimer.reset()

    update: (dt) ->
        super(dt)
        p = RBV("player")
        if p.x > @x + @threshold
            @.move_right()
        else if p.x < @x - @threshold
            @.move_left()
        else
            @.stop()
            @.shoot()

class PlayerShip extends RBSprite
    constructor: (@bulletgroup) ->
        super(RBV_ENG.screenwidth/2, 4*RBV_ENG.screenheight/5, RBV_IMG.shipsimage, 40, 60)
        @physics = new RBPhysics()
        @.add_component(@physics)
        @physics.floating = true
        @speed = 180

        ss = RB().media.get_spritesheet(RBV_SS.ship)
        @.add_animation("default", new RBAnimation(ss, [22], RB_ANIM_NORMAL))
        @.add_animation("right", new RBAnimation(ss, [22, 23, 24], RB_ANIM_NORMAL, 200))
        @.add_animation("left", new RBAnimation(ss, [22, 21, 20], RB_ANIM_NORMAL, 200))
        @.set_animation("default")

    move_left: (should_move = true) ->
        if should_move
            @physics.velocity.x = -@speed
            @.set_animation("left")
        else
            @physics.velocity.x = 0
            @.set_animation("default")
    move_right: (should_move = true) ->
        if should_move
            @physics.velocity.x = @speed
            @.set_animation("right")
        else
            @physics.velocity.x = 0
            @.set_animation("default")

    shoot: () ->
        @bulletgroup.add_child(new Bullet(@x, @y-20, 0))

    update: (dt) ->
        super(dt)
        s = RB().env("settings")
        if @x > RBV_ENG.screenwidth
            @x = RBV_ENG.screenwidth
            @.move_right(false)
        if @x < 0
            @x = 0
            @.move_left(false)

class Star extends RBRectEntity
    constructor: (X, Y) ->
        size = 1
        if random_chance(0.1)
            size = 2
        super(X, Y, size, size)
        max = 255
        min = 170
        r = random_int(min, max)
        g = random_int(min, max)
        b = random_int(min, max)
        @.set_colour(r, g, b)
        @.set_fill(r, g, b)
        @speed = random_float(0.05, 0.1)

    update: (dt) ->
        super(dt)
        @y += @speed*dt
        #s = @_RB.env("settings")
        if (@y > RBV_ENG.screenheight)
            y = 0
            x = random_float(0, RBV_ENG.screenwidth)
            @.set_position(x, y)

class VShooterGameScene extends RBScene
    constructor: () ->
        super("VShooter Main Game Scene")
        @input.add(RB_KEY_UP, @.on_key_up)
        @input.add(RB_KEY_DOWN, @.on_key_down)
        @enemiesgroup = new RBLayer()
        @enemiesgroup.name = "Enemies"
        @backgroundgroup = new RBLayer()
        @backgroundgroup.name = "Background"
        @background2group = new RBLoopLayer("Background2", true, true)
        @playergroup = new RBLayer()
        @playergroup.name = "Player"
        @playerbullets = new RBLayer()
        @playerbullets.name = "Player Bullets"
        @enemybullets = new RBLayer()
        @enemybullets.name = "Enemy Bullets"
        
        black = new RBRectEntity(0, 0, RBV_ENG.screenwidth, RBV_ENG.screenheight)
        black.set_anchor(0,0)
        black.set_position(0,0)
        black.shouldFill = true
        black.set_fill(0,0,0,255)

        @stage.add_child(@backgroundgroup)
        @stage.add_child(@background2group)
        @stage.add_child(@enemiesgroup)
        @stage.add_child(@playergroup)
        @stage.add_child(@playerbullets)
        @stage.add_child(@enemybullets)

        @background2group.add_child(new RBSprite(200, 200, RBV_IMG.envimage) )
        @background2group.physics = new RBPhysics()
        @background2group.physics.floating = true
        @background2group.add_component(@background2group.physics)
        @background2group.physics.velocity.y = 200
        
        @backgroundgroup.add_child(black)
        for count in [0..200]
            @backgroundgroup.add_child(new Star(random_float(0, RBV_ENG.screenwidth), random_float(0, RBV_ENG.screenheight)))

        @player = new PlayerShip(@playerbullets)
        @playergroup.add_child(@player)
        RBV("player", @player)

        @enemiesgroup.add_child(new EnemyShip(100, 100, @enemybullets))
        @bgm = RB().media.get_sound(RBV_MUSIC.bgm)
        RB().media.music_volume(0.2)
        @bgm.play()

    on_key_down: (event) =>
        code = event.keyCode
        keys = @_RB.input.keys
        console.log(code)
        switch code
            when keys.left
                @player.move_left()
            when keys.right
                @player.move_right()

    on_key_up: (event) =>
        code = event.keyCode
        keys = @_RB.input.keys
        switch code
            when keys.left
                @player.move_left(false)
            when keys.right
                @player.move_right(false)
            when keys.space
                @player.shoot()
    update: (dt) ->
        super(dt)
        @_RB.collide(@playergroup.children, @enemiesgroup.children)
        @_RB.collide(@playergroup.children, @enemybullets.children)
        @_RB.collide(@enemiesgroup.children, @playerbullets.children)
    
    draw: () ->
        super()
        @stage.render()

class VShooterTitleScene extends RBPreLoaderScene
    constructor: () ->
        super("VShooter Title Scene")
        @input.add(RB_KEY_UP, @.on_key_up)
        @loading_text = new RBText(100, 100, "")
        @continue_text = new RBText(100, 130, "")
        @stage.add_child(@loading_text)
        @stage.add_child(@continue_text)
    
    on_preloaded: () ->
        console.log("Preloaded")
        @continue_text.set_text("Press Space to continue")

    on_key_up: (event) =>
        code = event.keyCode
        keys = @_RB.input.keys
        if code is keys.space
            @.pop()
    update: (dt) ->
        super(dt)
        @loading_text.set_text("Loading...[#{@.pl.stats.total}]")

class VShooter extends RBGame
    constructor: () ->
        super()
        @titlescene = @gamescene = @gameoverscene = null
        @_RB.events.add(RB_EVENT_ON_START, @.show_title)

    show_title:() =>
        if @titlescene is null
            @_RB.events.remove(RB_EVENT_ON_START, @.show_title)
            @titlescene = new VShooterTitleScene()
            @titlescene.events.add(RB_EVENT_ON_SCENE_EXIT, @.start_game)
        @titlescene.push()

    start_game: () =>
        if @gamescene isnt null
            @gamescene.destroy()
        @gamescene = new VShooterGameScene()
        @gamescene.events.add(RB_EVENT_ON_SCENE_EXIT, @.game_over)
        @gamescene.push()

    game_over: () =>
        if @gameoverscene is null
            @gameoverscene = new VShooterGameOverScene()
            @gameoverscene.events.add(RB_EVENT_ON_SCENE_EXIT, @.show_title)
        @gameoverscene.push()

_game = null
window.run_vshooter = () ->
    RBV(RBV_ENG, {
        fps: 60
        screenwidth: 400
        screenheight: 400
        canvaswidth: 400
        canvasheight: 400
        canvasid: "canvas"
        canvassmoothing: false
    })

    RBV(RBV_IMG, {
        shipsimage: "data/vshooter/ships.png"
        bulletsimage: "data/vshooter/bullets.png"
        envimage: "data/vshooter/spacetiles.png"
    })

    RBV(RBV_SND, {
        laser: "data/sfx.wav"
    })

    RBV(RBV_MUSIC, {
        bgm: "data/vshooter/bgm.mp3"
    })

    RBV(RBV_SS, {
        bullets: ["bullets", RBV(RBV_IMG).bulletsimage, 4, 4]
        ship: ["ships", RBV(RBV_IMG).shipsimage, 5, 5]
        env: ["env", RBV(RBV_IMG).envimage, 10, 10]
    })

    RBV(RBV_GAME, { 
        playerwidth: 35
        playerheight: 40
        enemywidth: 35
        enemyheight: 40
        bulletwidth: 8
        bulletheight: 8
    })
    
    _game = new VShooter()
    RB().start()
