describe("Rhubarb", function(){
    
    describe("RB", function(){
        it("Depends on JQuery", function(){
            expect($).toBeDefined();
        });
    });

    describe("RBPoint", function(){
        it("Should be able to define a point", function() {
            var a = new RBPoint(20,25);
            expect(a.x).toEqual(20);
            expect(a.y).toEqual(25);
        });
    });
    
    describe("RBRect", function(){
        it("Should be able to instantiate a rect", function(){
            var a = new RBRect(20,20,5,5);
            expect(a.x).toEqual(20);
            expect(a.y).toEqual(20);
            expect(a.width).toEqual(5);
            expect(a.height).toEqual(5);
            expect(a.top).toEqual(17.5);
            expect(a.bottom).toEqual(22.5);
            expect(a.left).toEqual(17.5);
            expect(a.right).toEqual(22.5);
        });

        it("Should correctly detect overlapping rects", function(){
            var a = new RBRect(20,20,100,100);
            var b = new RBRect(100,100,80,80);
            expect(a.overlaps(b)).toEqual(true);
            expect(b.overlaps(a)).toEqual(true);
        });
        it("Should correctly detect non-overlapping rects", function(){
            var a = new RBRect(20,20,100,100);
            var b = new RBRect(100,100,50,50);
            expect(a.overlaps(b)).toEqual(false);
            expect(b.overlaps(a)).toEqual(false);
        });
        it("Should correctly detect non-overlapping rects (single pixel)", function(){
            //these rects *just* butt up against each other, but dont actually overlap
            var a = new RBRect(20,20,100,100);
            var b = new RBRect(100,100,60,80);
            expect(a.overlaps(b)).toEqual(false);
            expect(b.overlaps(a)).toEqual(false);
        });
        it("Should correctly detect overlapping rects (single pixel)", function(){
            //these rects *just* overlap each other, by a single pixel
            var a = new RBRect(20,20,100,100);
            var b = new RBRect(100,100,61,80);
            expect(a.overlaps(b)).toEqual(true);
            expect(b.overlaps(a)).toEqual(true);
        });
        
        it("Should return an overlapping subrectangle", function(){
            var a = new RBRect(20, 20, 20, 20);
            var b = new RBRect(35, 35, 10, 10);
            a.set_anchor(0,0);
            b.set_anchor(0,0);
            var c = new RBRect();
            expect(a.overlaps(b, c)).toEqual(true);
            expect(c.left).toEqual(15);
            expect(c.right).toEqual(20);
            expect(c.top).toEqual(15);
            expect(c.bottom).toEqual(20);
            expect(c.width).toEqual(5);
            expect(c.height).toEqual(5);
        });

        it("Should return complete rectangle when totally overlapped", function() {
            var a = new RBRect(20, 20, 20, 20);
            var b = new RBRect(10, 10, 100, 100);
            a.set_anchor(0,0);
            b.set_anchor(0,0);
            var c = new RBRect();
            expect(a.overlaps(b, c)).toEqual(true);
            expect(c.left).toEqual(0);
            expect(c.right).toEqual(20);
            expect(c.top).toEqual(0);
            expect(c.bottom).toEqual(20);
            expect(c.width).toEqual(20);
            expect(c.height).toEqual(20);


        });
    });
        

    describe("RBObject", function() {
        it("Should get a unique id assigned on creation", function(){
            var ob1, ob2, ob3;
            ob1 = new RBObject("First");
            ob2 = new RBObject("Second");
            ob3 = new RBObject("Third");
            expect(ob1.id).not.toBeNaN(); 
            expect(ob2.id).not.toBeNaN(); 
            expect(ob3.id).not.toBeNaN(); 
            expect(ob1.id).not.toEqual(ob2.id);
            expect(ob2.id).not.toEqual(ob3.id);
            expect(ob3.id).not.toEqual(ob1.id);
        });
    });

    describe("RBGroup", function() {
        var maingroup;
        var obj1;
        
        it("Should be able to be instantiated and name", function(){
            maingroup = new RBGroup("testname");
            expect(maingroup.name).toEqual("testname");
            expect(maingroup.count).toEqual(0);
        });

        it("Should be able to add an RBObject to the group", function(){
            obj1 = new RBObject();
            maingroup.add_member(obj1);
            expect(maingroup.count).toEqual(1);
        });

        it("Should be able to retrieve an RBObject from the group", function(){
            var obj2 = maingroup.get_member_by_id(obj1.id);
            expect(obj2).toEqual(obj1);
        });

        it("Should not be able to add the same object twice", function() {
            var c = maingroup.count;
            maingroup.add_member(obj1);
            expect(maingroup.count).toEqual(c);
        });

        it("Should not be able to remove a non-existant id", function() {
            var c = maingroup.count;
            maingroup.remove_member_by_id(32);
            expect(maingroup.count).toEqual(c);
            
        });

        it("Should be able to remove an existant object", function() {
            var c = maingroup.count;
            maingroup.remove_member(obj1);
            expect(maingroup.count).toEqual(c-1);
        });

        it("Should be able to support correct for iteration, with sparse inserts", function() {
            var testarray = [];
            var testitem = null;
            
            //empty the group
            while (maingroup.count > 0){
                maingroup.remove_member_by_id(maingroup.get_member_ids()[0]);
            }
            expect(maingroup.count).toEqual(0);

            for (var count = 0; count < 10; count++){
                testitem = new RBObject();
                testarray.push(testitem);
                maingroup.add_member(testitem);
            }

            //So now the 10 items are in the test array and in the maingroup.
            expect(maingroup.get_member_ids().length).toEqual(10);

            //Test iteration
            var counter = maingroup.count;
            for (var count2 in maingroup.members){
                counter += 1;
            }
            expect(counter).toEqual(maingroup.count+10);
           

            counter = maingroup.count;
            //Remove three members
            maingroup.remove_member_by_id(testarray[5].id);
            maingroup.remove_member_by_id(testarray[6].id);
            maingroup.remove_member_by_id(testarray[7].id);

            //make sure the size is smaller
            expect(maingroup.count).toEqual(counter-3);

            //now iterate through, and make sure we only get 7 iterations
            counter = 0;
            for (var count2 in maingroup.members){
                counter += 1;
            }
            expect(counter).toEqual(7);
            expect(maingroup.get_member_ids().length).toEqual(7);

        });
        it("Should be able to empty group with .empty()", function(){
            maingroup.empty();
            expect(maingroup.count).toEqual(0);
            expect(maingroup.members.length).toEqual(0);

        });
        it("Should be able to insert members before or after existing members", function(){
            var item1 = new RBObject();
            var item2 = new RBObject();
            var item3 = new RBObject();
            var item4 = new RBObject();

            //Add the first object
            maingroup.add_member(item1, 0);
            expect(maingroup.count).toEqual(1);
            expect(maingroup.members.length).toEqual(1);
            expect(maingroup.members[0]).toEqual(item1);
            
            //Add another item after
            maingroup.add_after(item2, item1);
            expect(maingroup.count).toEqual(2);
            expect(maingroup.members.length).toEqual(2);
            expect(maingroup.members[0]).toEqual(item1);
            expect(maingroup.members[1]).toEqual(item2);
            
            //add an item before everything
            maingroup.add_before(item3, item1);
            expect(maingroup.count).toEqual(3);
            expect(maingroup.members.length).toEqual(3);
            expect(maingroup.members[0]).toEqual(item3);
            expect(maingroup.members[1]).toEqual(item1);
            expect(maingroup.members[2]).toEqual(item2);

            //now try to add an item before an element that doesnt exist
            var res = maingroup.add_before(item4, item4);
            expect(res).toEqual(null);
            expect(maingroup.count).toEqual(3);
            expect(maingroup.members.length).toEqual(3);
            
            res = maingroup.add_after(item4, item4);
            expect(res).toEqual(null);
            expect(maingroup.count).toEqual(3);
            expect(maingroup.members.length).toEqual(3);

        });

    });

});
