settings = {
    "canvaswidth" : 400,
    "canvasheight" : 400,
    "tilewidth": 8,
    "tileheight": 8,
}

_mapmaker = null
_known_classes = {}

class MapMaker
    constructor: (@settings, @canvas) ->
        @canvaswidth = @settings.canvaswidth
        @canvasheight = @settings.canvasheight
        @_RB = RB()

        @stage = new RBEntity(0,0,0,0)
        @groups = {}

        @props = new PropertiesPane()
    
    update: () ->
        @stage.update(@_RB.dt())

    draw: () ->
        @stage.draw()

    runloop: (dt) =>
        @_RB.cctx.clearRect(0, 0, @settings.canvaswidth, @settings.canvasheight)
        @.update()
        @.draw()

add_classes = (jsobj) =>
    _known_classes['zelda.js'] = []
    for classobj in [new Link(), new ZeldaDefaultTile(), new ZeldaEnvTile(), ZeldaGame, zeldasettings]
        _known_classes['zelda.js'].push(classobj)


initMapMaker = () ->
    if not (_mapmaker is null)
        return
    console.log('Initing Map Maker')
    _mapmaker = new MapMaker(settings, document.getElementById('canvas'))

    RB().set_environment(_mapmaker.canvas, _mapmaker.canvaswidth, _mapmaker.canvasheight, _mapmaker.runloop)
    RB().reset_frame_rate(30)
    add_classes(null) 
    _known_classes['zelda.js'].push(settings)
    populate_objects(_known_classes['zelda.js'])
