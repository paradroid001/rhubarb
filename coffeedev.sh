echo "Deving $1"
if [ ! -f $1 ]
then
    echo "Could not find file $1"
    exit
fi

#watchman $1 "docco $1" &
coffee -c -o js -b --watch $1
